/**
 * @file rucrypto/gost_34_12/gost_34_12.h
 */

#ifndef RUCRYPTO_GOST_34_12_GOST_34_12_H
#define RUCRYPTO_GOST_34_12_GOST_34_12_H

#include <rucrypto/common/common.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/** GOST 34.12-2018 block size in bits */
#define RUCRYPTO_GOST_34_12_BLOCK_SIZE_BITS 128
/** GOST 34.12-2018 block size in bytes */
#define RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES (RUCRYPTO_GOST_34_12_BLOCK_SIZE_BITS / 8)

/** GOST 34.12-2018 key size in bits */
#define RUCRYPTO_GOST_34_12_KEY_SIZE_BITS 256
/** GOST 34.12-2018 key size in bytes */
#define RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES (RUCRYPTO_GOST_34_12_KEY_SIZE_BITS / 8)

/** GOST 34.12-2018 block type */
typedef rucrypto_vec128_t rucrypto_gost_34_12_block_t;
/** GOST 34.12-2018 key type */
typedef rucrypto_vec256_t rucrypto_gost_34_12_key_t;

/**
 * @brief Encrypt block with 128-bit "Kuznechik" algorithm from GOST 34.12-2018
 * @param [in, out] block block of open text
 * @param [in] key secret key
 */
extern void rucrypto_gost_34_12_encrypt_128(rucrypto_gost_34_12_block_t block, const rucrypto_gost_34_12_key_t key);

/**
 * @brief Decrypt block with 128-bit "Kuznechik" algorithm from GOST 34.12-2018
 * @param [in, out] block block of cipher-text
 * @param [in] key secret key
 */
extern void rucrypto_gost_34_12_decrypt_128(rucrypto_gost_34_12_block_t block, const rucrypto_gost_34_12_key_t key);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_GOST_34_12_GOST_34_12_H
