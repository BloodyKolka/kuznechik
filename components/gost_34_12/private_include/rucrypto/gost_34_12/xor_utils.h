#ifndef RUCRYPTO_GOST_34_12_XOR_UTILS_H
#define RUCRYPTO_GOST_34_12_XOR_UTILS_H

#include <rucrypto/gost_34_12/gost_34_12.h>

/**
 * @brief Perform X (XOR) operation from GOST 34.12-2018
 * @param [in, out] a first 128-bit operand, used to store result
 * @param [in] b second 128-bit operand
 */
void rucrypto_gost_34_12_xor(rucrypto_vec128_t a, const rucrypto_vec128_t b);

#endif // RUCRYPTO_GOST_34_12_XOR_UTILS_H
