#ifndef RUCRYPTO_GOST_34_12_SUBSTITUTION_UTILS_H
#define RUCRYPTO_GOST_34_12_SUBSTITUTION_UTILS_H

#include <rucrypto/gost_34_12/gost_34_12.h>

/**
 * @brief Perform substitution S from GOST 34.12-2018
 * @param [in, out] vec128 128-bit block
 */
void rucrypto_gost_34_12_substitution(rucrypto_vec128_t vec128);

/**
 * @brief Perform substitution S^-1 from GOST 34.12-2018
 * @param [in, out] vec128 128-bit block
 */
void rucrypto_gost_34_12_substitution_reverse(rucrypto_vec128_t vec128);

#endif // RUCRYPTO_GOST_34_12_SUBSTITUTION_UTILS_H
