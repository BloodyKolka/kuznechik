#ifndef RUCRYPTO_GOST_34_12_LINEAR_UTILS_H
#define RUCRYPTO_GOST_34_12_LINEAR_UTILS_H

#include <rucrypto/gost_34_12/gost_34_12.h>

/**
 * @brief Multiply two 8-bit unsigned integers in GF[2]/p(x)
 * @param [in] a first factor
 * @param [in] b second factor
 * @return result of multiplication
 * @detail p(x) = x^8 + x^7 + x^6 + x + 1 and numbers are represented as bit sequence z0z1z2...z8 where zi is
 *         coefficient in z0 + z1x^1 + ... + z7x^7 in accordance with GOST 34.12-2018
 */
rucrypto_uint8_t rucrypto_gost_34_12_field_multiply(rucrypto_uint8_t a, rucrypto_uint8_t b);

/**
 * @brief Perform linear transformation l from GOST 34.12-2018
 * @param [in, out] vec128 vector of 16 8-bit vectors on which the transformation will be performed
 * @return result of transformation
 */
rucrypto_uint8_t rucrypto_gost_34_12_linear_part_l(rucrypto_vec128_t vec128);

/**
 * @brief Perform linear transformation R from GOST 34.12-2018
 * @param [in, out] vec128 vector of 16 8-bit vectors on which the transformation will be performed
 */
void rucrypto_gost_34_12_linear_part_r(rucrypto_vec128_t vec128);

/**
 * @brief Perform linear transformation R^-1 from GOST 34.12-2018
 * @param [in, out] vec128 vector of 16 8-bit vectors on which the transformation will be performed
 */
void rucrypto_gost_34_12_linear_reverse_part_r(rucrypto_vec128_t vec128);

/**
 * @brief Perform linear transformation L from GOST 34.12-2018
 * @param [in, out] vec128 128-bit vector
 */
void rucrypto_gost_34_12_linear(rucrypto_vec128_t vec128);

/**
 * @brief Perform linear transformation L^-1 from GOST 34.12-2018
 * @param [in, out] vec128 128-bit vector
 */
void rucrypto_gost_34_12_linear_reverse(rucrypto_vec128_t vec128);

#endif // RUCRYPTO_GOST_34_12_LINEAR_UTILS_H
