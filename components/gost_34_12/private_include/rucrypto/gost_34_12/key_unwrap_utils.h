#ifndef RUCRYPTO_GOST_34_12_KEY_UNWRAP_UTILS_H
#define RUCRYPTO_GOST_34_12_KEY_UNWRAP_UTILS_H

#include <rucrypto/gost_34_12/gost_34_12.h>

// GOST 34.12-2018 iteration key size is equal to 128 bits
#define RUCRYPTO_GOST_34_12_ITERATION_KEY_SIZE_BITS 128
#define RUCRYPTO_GOST_34_12_ITERATION_KEY_SIZE_BYTES (RUCRYPTO_GOST_34_12_ITERATION_KEY_SIZE_BITS / 8)

typedef rucrypto_vec128_t rucrypto_gost_34_12_iteration_key_t;
typedef struct {
    rucrypto_gost_34_12_iteration_key_t key1;
    rucrypto_gost_34_12_iteration_key_t key2;
} rucrypto_gost_34_12_iteration_key_pair_t;

/**
 * @brief Perform F transformation from GOST 34.12-2018 on (a1, a0)
 * @param [in, out] a1 parameter a1 of F (see GOST 34.12-2018)
 * @param [in, out] a0 parameter a0 of F (see GOST 34.12-2018)
 * @param [in] k parameter k of F (see GOST 34.12-2018)
 * @details Performs F(a1, a0) and returns result in (a1, a0)
 */
void rucrypto_gost_34_12_f_transform(rucrypto_vec128_t a1, rucrypto_vec128_t a0, const rucrypto_vec128_t k);

/**
 * @brief Get first pair of iteration key from main key for Kuznechik algorithm from GOST 34.12-2018
 * @param [in] key main key
 * @param [in, out] res struct rucrypto_gost_34_12_iteration_key_pair_t to store result
 * @return Pair of iteration keys K1 and K2 in rucrypto_gost_34_12_iteration_key_pair_t struct
 */
void rucrypto_gost_34_12_get_first_keypair(const rucrypto_gost_34_12_key_t key, rucrypto_gost_34_12_iteration_key_pair_t* res);

/**
 * @brief Get next pair of iteration keys for Kuznechik algorithm from GOST 34.12-2018
 * @param [in, out] keys Previous pair of keys, result will be stored here
 * @param [in] it iteration number of the first key in the next pair
 * @return Pair of iteration keys K_it and K_(it+1) in rucrypto_gost_34_12_iteration_key_pair_t struct
 */
void rucrypto_gost_34_12_get_next_keypair(rucrypto_gost_34_12_iteration_key_pair_t* keys, rucrypto_size_t it);

#endif // RUCRYPTO_GOST_34_12_KEY_UNWRAP_UTILS_H
