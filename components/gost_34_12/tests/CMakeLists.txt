project(${RUCRYPTO_PROJECT_PREFIX}gost_34_12_tests LANGUAGES CXX)

add_executable(${PROJECT_NAME}
        linear_transformation_tests.cpp
        substitution_transformation_tests.cpp
        key_unwrap_tests.cpp
        gost_34_12_test.cpp
        ../src/gost_34_12.c
        ../src/xor_utils.c
        ../src/substitution_utils.c
        ../src/linear_utils.c
        ../src/key_unwrap_utils.c
)

get_target_property(${PROJECT_NAME}_copt ${PROJECT_NAME} LINK_OPTIONS)
if (NOT ${PROJECT_NAME}_copt STREQUAL "${PROJECT_NAME}_copt-NOTFOUND")
    list(REMOVE_ITEM ${PROJECT_NAME}_copt -nostdlib)
    set_property(TARGET ${PROJECT_NAME} PROPERTY LINK_OPTIONS ${${PROJECT_NAME}_copt})
endif ()

find_package(GTest REQUIRED)

target_link_libraries(${PROJECT_NAME} PRIVATE
        GTest::gtest_main
        ${RUCRYPTO_PROJECT_PREFIX}common
)

target_include_directories(${PROJECT_NAME} PRIVATE
        ../include
        ../private_include
)
