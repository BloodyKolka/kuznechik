#include <rucrypto/gost_34_12/linear_utils.h>

rucrypto_uint8_t rucrypto_gost_34_12_field_multiply(rucrypto_uint8_t a, rucrypto_uint8_t b)
{
    rucrypto_uint8_t res = 0;
    for (rucrypto_size_t i = 0; i < 8; ++i)
    {
        if (b & 0x1) // If b's current factor is not 0
            res ^= a; // Add a, multiplied by x^i, to result
        bool overflow = (a & 0x80); // Remember if a contains x^7
        a <<= 1; // Multiply a by x^1
        if (overflow) // If a contained x^7
            a ^= 0xC3; // Calculate remainder of division by x^8 + x^7 + x^6 + x + 1
        b >>= 1; // Switch to next factor
    }
    return res;
}

rucrypto_uint8_t rucrypto_gost_34_12_linear_part_l(rucrypto_vec128_t vec128)
{
    static const rucrypto_uint8_t factors[16] = { 148, 32, 133, 16, 194, 192, 1, 251, 1, 192, 194, 16, 133, 32, 148, 1 };

    rucrypto_uint8_t res = 0;
    for (rucrypto_size_t i = 0; i < 16; ++i) {
        res ^= rucrypto_gost_34_12_field_multiply(factors[i], vec128[i]);
    }
    return res;
}

void rucrypto_gost_34_12_linear_part_r(rucrypto_vec128_t vec128) {
    rucrypto_uint8_t first = rucrypto_gost_34_12_linear_part_l(vec128);
    for (rucrypto_size_t i = 0; i < 15; ++i) {
        vec128[16 - i - 1] = vec128[16 - i - 2];
    }
    vec128[0] = first;
}

void rucrypto_gost_34_12_linear_reverse_part_r(rucrypto_vec128_t vec128) {
    rucrypto_uint8_t tmp = vec128[0];
    for (rucrypto_size_t i = 0; i < 15; ++i) {
        vec128[i] = vec128[i + 1];
    }
    vec128[16 - 1] = tmp;
    vec128[16 - 1] = (rucrypto_gost_34_12_linear_part_l(vec128));
}

void rucrypto_gost_34_12_linear(rucrypto_vec128_t vec128) {
    for (rucrypto_size_t i = 0; i < 16; ++i) {
        rucrypto_gost_34_12_linear_part_r(vec128);
    }
}

void rucrypto_gost_34_12_linear_reverse(rucrypto_vec128_t vec128) {
    for (rucrypto_size_t i = 0; i < 16; ++i) {
        rucrypto_gost_34_12_linear_reverse_part_r(vec128);
    }
}
