#include <rucrypto/gost_34_12/xor_utils.h>

void rucrypto_gost_34_12_xor(rucrypto_vec128_t a, const rucrypto_vec128_t b) {
    rucrypto_uint64_t* const a64 = (rucrypto_uint64_t*)a;
    rucrypto_uint64_t* const b64 = (rucrypto_uint64_t*)b;

    for (rucrypto_size_t i = 0; i < sizeof(rucrypto_vec128_t) / sizeof(rucrypto_uint64_t); ++i) {
        a64[i] ^= b64[i];
    }
}
