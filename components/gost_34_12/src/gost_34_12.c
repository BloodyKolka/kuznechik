#include <rucrypto/gost_34_12/gost_34_12.h>

#include <rucrypto/gost_34_12/key_unwrap_utils.h>
#include <rucrypto/gost_34_12/xor_utils.h>
#include <rucrypto/gost_34_12/substitution_utils.h>
#include <rucrypto/gost_34_12/linear_utils.h>

#include <rucrypto/common/secure_zero.h>

static void perform_xsl_round(rucrypto_gost_34_12_block_t block, const rucrypto_gost_34_12_iteration_key_t itKey) {
    rucrypto_gost_34_12_xor(block, itKey);
    rucrypto_gost_34_12_substitution(block);
    rucrypto_gost_34_12_linear(block);
}

void rucrypto_gost_34_12_encrypt_128(rucrypto_gost_34_12_block_t block, const rucrypto_gost_34_12_key_t key) {
    rucrypto_gost_34_12_iteration_key_pair_t keyPair;
    rucrypto_gost_34_12_get_first_keypair(key, &keyPair);
    for (rucrypto_size_t i = 0; i < 4; ++i) {
        perform_xsl_round(block, keyPair.key1);
        perform_xsl_round(block, keyPair.key2);
        rucrypto_gost_34_12_get_next_keypair(&keyPair, 3 + 2 * i);
    }
    perform_xsl_round(block, keyPair.key1);
    rucrypto_gost_34_12_xor(block, keyPair.key2);
    rucrypto_secure_zero(keyPair.key1, RUCRYPTO_GOST_34_12_ITERATION_KEY_SIZE_BYTES);
    rucrypto_secure_zero(keyPair.key2, RUCRYPTO_GOST_34_12_ITERATION_KEY_SIZE_BYTES);
}

static void get_iteration_keys(const rucrypto_gost_34_12_key_t key, rucrypto_gost_34_12_iteration_key_t iterationKeys[]) {
    rucrypto_gost_34_12_iteration_key_pair_t keyPair;
    rucrypto_gost_34_12_get_first_keypair(key, &keyPair);
    for (rucrypto_size_t i = 0; i < 4; ++i) {
        RUCRYPTO_MEMCPY(iterationKeys[2 * i], keyPair.key1, RUCRYPTO_GOST_34_12_ITERATION_KEY_SIZE_BYTES);
        RUCRYPTO_MEMCPY(iterationKeys[2 * i + 1], keyPair.key2, RUCRYPTO_GOST_34_12_ITERATION_KEY_SIZE_BYTES);
        rucrypto_gost_34_12_get_next_keypair(&keyPair, 3 + 2 * i);
    }
    RUCRYPTO_MEMCPY(iterationKeys[8], keyPair.key1, RUCRYPTO_GOST_34_12_ITERATION_KEY_SIZE_BYTES);
    RUCRYPTO_MEMCPY(iterationKeys[9], keyPair.key2, RUCRYPTO_GOST_34_12_ITERATION_KEY_SIZE_BYTES);
    rucrypto_secure_zero(keyPair.key1, RUCRYPTO_GOST_34_12_ITERATION_KEY_SIZE_BYTES);
    rucrypto_secure_zero(keyPair.key2, RUCRYPTO_GOST_34_12_ITERATION_KEY_SIZE_BYTES);
}

static void perform_reverse_xsl_round(rucrypto_gost_34_12_block_t block, const rucrypto_gost_34_12_iteration_key_t itKey) {
    rucrypto_gost_34_12_linear_reverse(block);
    rucrypto_gost_34_12_substitution_reverse(block);
    rucrypto_gost_34_12_xor(block, itKey);
}

void rucrypto_gost_34_12_decrypt_128(rucrypto_gost_34_12_block_t block, const rucrypto_gost_34_12_key_t key) {
    rucrypto_gost_34_12_iteration_key_t iterationKeys[10];
    get_iteration_keys(key, iterationKeys);

    rucrypto_gost_34_12_xor(block, iterationKeys[9]);
    rucrypto_secure_zero(iterationKeys[9], RUCRYPTO_GOST_34_12_ITERATION_KEY_SIZE_BYTES);
    for (int i = 8; i >= 0; --i) {
        perform_reverse_xsl_round(block, iterationKeys[i]);
        rucrypto_secure_zero(iterationKeys[i], RUCRYPTO_GOST_34_12_ITERATION_KEY_SIZE_BYTES);
    }
}
