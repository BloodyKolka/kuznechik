#include <rucrypto/gost_34_10/gost_34_10.h>
#include <rucrypto/gost_34_10/elliptic_curve_utils.h>

#include <rucrypto/common/secure_zero.h>

rucrypto_result_t rucrypto_gost_34_10_elliptic_curve_init_256(
        rucrypto_gost_34_10_elliptic_curve_t** curve,
        const rucrypto_vec256_t p,
        const rucrypto_vec256_t a,
        const rucrypto_vec256_t b,
        const rucrypto_vec256_t m,
        const rucrypto_vec256_t q,
        const rucrypto_vec256_t Px,
        const rucrypto_vec256_t Py) {
    static const rucrypto_size_t curveSize = 256;
    static const rucrypto_size_t numberSize = 256 / (8 * sizeof(rucrypto_gost_34_10_big_num_underlying_t));

    *curve = rucrypto_allocate(sizeof(rucrypto_gost_34_10_elliptic_curve_t));
    if (*curve == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    (*curve)->buffer = rucrypto_allocate(7 * numberSize * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if ((*curve)->buffer == RUCRYPTO_NULL) {
        rucrypto_deallocate(*curve);
        return rucrypto_result_out_of_memory;
    }

    (*curve)->size = curveSize;
    (*curve)->P.size = numberSize;

    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 0 * numberSize, &(*curve)->p, numberSize);
    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 1 * numberSize, &(*curve)->a, numberSize);
    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 2 * numberSize, &(*curve)->b, numberSize);
    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 3 * numberSize, &(*curve)->m, numberSize);
    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 4 * numberSize, &(*curve)->q, numberSize);
    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 5 * numberSize, &(*curve)->P.x, numberSize);
    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 6 * numberSize, &(*curve)->P.y, numberSize);

    rucrypto_gost_34_10_big_num_fill_from_buffer(p, sizeof(rucrypto_vec256_t), &(*curve)->p);
    rucrypto_gost_34_10_big_num_fill_from_buffer(a, sizeof(rucrypto_vec256_t), &(*curve)->a);
    rucrypto_gost_34_10_big_num_fill_from_buffer(b, sizeof(rucrypto_vec256_t), &(*curve)->b);
    rucrypto_gost_34_10_big_num_fill_from_buffer(m, sizeof(rucrypto_vec256_t), &(*curve)->m);
    rucrypto_gost_34_10_big_num_fill_from_buffer(q, sizeof(rucrypto_vec256_t), &(*curve)->q);
    rucrypto_gost_34_10_big_num_fill_from_buffer(Px, sizeof(rucrypto_vec256_t), &(*curve)->P.x);
    rucrypto_gost_34_10_big_num_fill_from_buffer(Py, sizeof(rucrypto_vec256_t), &(*curve)->P.y);

    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_gost_34_10_elliptic_curve_init_512(
        rucrypto_gost_34_10_elliptic_curve_t** curve,
        const rucrypto_vec512_t p,
        const rucrypto_vec512_t a,
        const rucrypto_vec512_t b,
        const rucrypto_vec512_t m,
        const rucrypto_vec512_t q,
        const rucrypto_vec512_t Px,
        const rucrypto_vec512_t Py) {
    static const rucrypto_size_t curveSize = 512;
    static const rucrypto_size_t numberSize = 512 / (8 * sizeof(rucrypto_gost_34_10_big_num_underlying_t));

    *curve = rucrypto_allocate(sizeof(rucrypto_gost_34_10_elliptic_curve_t));
    if (*curve == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    (*curve)->buffer = rucrypto_allocate(7 * numberSize * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if ((*curve)->buffer == RUCRYPTO_NULL) {
        rucrypto_deallocate(*curve);
        return rucrypto_result_out_of_memory;
    }

    (*curve)->size = curveSize;
    (*curve)->P.size = numberSize;

    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 0 * numberSize, &(*curve)->p, numberSize);
    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 1 * numberSize, &(*curve)->a, numberSize);
    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 2 * numberSize, &(*curve)->b, numberSize);
    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 3 * numberSize, &(*curve)->m, numberSize);
    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 4 * numberSize, &(*curve)->q, numberSize);
    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 5 * numberSize, &(*curve)->P.x, numberSize);
    rucrypto_gost_34_10_big_num_init_no_alloc((*curve)->buffer, 6 * numberSize, &(*curve)->P.y, numberSize);

    rucrypto_gost_34_10_big_num_fill_from_buffer(p, sizeof(rucrypto_vec512_t), &(*curve)->p);
    rucrypto_gost_34_10_big_num_fill_from_buffer(a, sizeof(rucrypto_vec512_t), &(*curve)->a);
    rucrypto_gost_34_10_big_num_fill_from_buffer(b, sizeof(rucrypto_vec512_t), &(*curve)->b);
    rucrypto_gost_34_10_big_num_fill_from_buffer(m, sizeof(rucrypto_vec512_t), &(*curve)->m);
    rucrypto_gost_34_10_big_num_fill_from_buffer(q, sizeof(rucrypto_vec512_t), &(*curve)->q);
    rucrypto_gost_34_10_big_num_fill_from_buffer(Px, sizeof(rucrypto_vec512_t), &(*curve)->P.x);
    rucrypto_gost_34_10_big_num_fill_from_buffer(Py, sizeof(rucrypto_vec512_t), &(*curve)->P.y);

    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_gost_34_10_generate_signature_512(
        const rucrypto_gost_34_10_elliptic_curve_t* curve,
        const rucrypto_vec256_t hash,
        const rucrypto_vec256_t privateKey,
        rucrypto_gost_34_10_random_generator_func_t random,
        rucrypto_gost_34_10_signature_512_t signature) {
    RUCRYPTO_CHECK_ARGS(curve->size == 256);
    rucrypto_size_t size = curve->size / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8);

    rucrypto_gost_34_10_big_num_underlying_t* buffer;
    buffer = rucrypto_allocate(5 * size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if (buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }

    rucrypto_result_t status;
    rucrypto_gost_34_10_big_num_t e, k, r, s, d;
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 0 * size, &e, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 1 * size, &k, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 2 * size, &r, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 3 * size, &s, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 4 * size, &d, size);

    rucrypto_gost_34_10_big_num_fill_from_buffer(hash, sizeof(rucrypto_vec256_t), &e);
    status = rucrypto_gost_34_10_rebase(&e, &e, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }
    if (rucrypto_gost_34_10_is_zero(&e)) {
        e.buffer[e.size - 1] = 1;
    }

    rucrypto_gost_34_10_big_num_fill_0(&r);
    rucrypto_gost_34_10_big_num_fill_0(&s);
    rucrypto_gost_34_10_big_num_fill_random(&k, random);
    status = rucrypto_gost_34_10_rebase(&k, &k, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }
    do {
        rucrypto_gost_34_10_point_t C = {
                .size = size,
                .x = r,
                .y = s
        };
        status = rucrypto_gost_34_10_calculate_k_point(curve, &curve->P, &k, &C, false);
        if (status != rucrypto_result_ok) {
            if (status == rucrypto_result_out_of_memory) {
                rucrypto_deallocate(buffer);
                return status;
            }
            rucrypto_gost_34_10_big_num_fill_random(&k, random);
            continue;
        }
        status = rucrypto_gost_34_10_rebase(&r, &r, &curve->q, false);
        if (status != rucrypto_result_ok) {
            rucrypto_deallocate(buffer);
            return status;
        }
        if (rucrypto_gost_34_10_is_zero(&r)) {
            rucrypto_gost_34_10_big_num_fill_random(&k, random);
            continue;
        }
        status = rucrypto_gost_34_10_mul_based(&k, &e, &k, &curve->q, false);
        if (status != rucrypto_result_ok) {
            rucrypto_deallocate(buffer);
            return status;
        }
        rucrypto_gost_34_10_big_num_copy(&r, &s);
        rucrypto_gost_34_10_big_num_fill_from_buffer(privateKey, sizeof(rucrypto_vec256_t), &d);
        status = rucrypto_gost_34_10_mul_based(&s, &d, &s, &curve->q, true);
        if (status != rucrypto_result_ok) {
            rucrypto_secure_zero(s.buffer, s.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
            rucrypto_secure_zero(d.buffer, d.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
            rucrypto_deallocate(buffer);
            return status;
        }
        status = rucrypto_gost_34_10_add_based(&s, &k, &s, &curve->q, true);
        if (status != rucrypto_result_ok) {
            rucrypto_secure_zero(s.buffer, s.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
            rucrypto_secure_zero(d.buffer, d.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
            rucrypto_deallocate(buffer);
            return status;
        }
        rucrypto_secure_zero(d.buffer, d.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
        if (rucrypto_gost_34_10_is_zero(&s)) {
            rucrypto_gost_34_10_big_num_fill_random(&k, random);
            continue;
        }
        break;
    } while (true);

    rucrypto_gost_34_10_big_num_copy_to_buffer(&r, signature, 256 / 8);
    rucrypto_gost_34_10_big_num_copy_to_buffer(&s, &signature[256 / 8], 256 / 8);

    rucrypto_deallocate(buffer);
    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_gost_34_10_verify_signature_512(
        const rucrypto_gost_34_10_elliptic_curve_t* curve,
        const rucrypto_vec256_t hash,
        const rucrypto_vec512_t publicKey,
        const rucrypto_gost_34_10_signature_512_t signature) {
    RUCRYPTO_CHECK_ARGS(curve->size == 256);
    rucrypto_size_t size = curve->size / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8);

    rucrypto_gost_34_10_big_num_underlying_t* buffer;
    buffer = rucrypto_allocate(11 * size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if (buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }

    rucrypto_result_t status;
    rucrypto_gost_34_10_big_num_t r, s, v;
    rucrypto_gost_34_10_point_t z1P, Q, z2Q, C;

    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 0  * size, &r, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 1  * size, &s, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 2  * size, &v, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 3  * size, &z1P.x, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 4  * size, &z1P.y, size);
    z1P.size = size;
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 5  * size, &Q.x, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 6  * size, &Q.y, size);
    Q.size = size;
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 7  * size, &z2Q.x, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 8  * size, &z2Q.y, size);
    z2Q.size = size;
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 9  * size, &C.x, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 10 * size, &C.y, size);
    C.size = size;

    rucrypto_gost_34_10_big_num_fill_from_buffer(signature, sizeof(rucrypto_gost_34_10_signature_512_t) / 2, &r);
    rucrypto_gost_34_10_big_num_fill_from_buffer(&signature[sizeof(rucrypto_gost_34_10_signature_512_t) / 2], sizeof(rucrypto_gost_34_10_signature_512_t) / 2, &s);

    if (!rucrypto_gost_34_10_is_less(&r, &curve->q) || !rucrypto_gost_34_10_is_less(&s, &curve->q)) {
        rucrypto_deallocate(buffer);
        return rucrypto_result_not_accepted;
    }

    rucrypto_gost_34_10_big_num_fill_from_buffer(hash, sizeof(rucrypto_vec256_t), &v);
    if (rucrypto_gost_34_10_is_zero(&v)) {
        v.buffer[v.size - 1] = 1;
    }
    status = rucrypto_gost_34_10_rebase(&v, &v, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }

    // s = z1, v = z2
    status = rucrypto_gost_34_10_mul_inv(&v, &v, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }
    status = rucrypto_gost_34_10_mul_based(&s, &v, &s, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }
    status = rucrypto_gost_34_10_mul_based(&r, &v, &v, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }
    rucrypto_gost_34_10_sub(&curve->q, &v, &v);

    rucrypto_gost_34_10_big_num_fill_0(&z1P.x);
    rucrypto_gost_34_10_big_num_fill_0(&z1P.y);

    status = rucrypto_gost_34_10_calculate_k_point(curve, &curve->P, &s, &z1P, false);
    if (status != rucrypto_result_ok) {
        if (status == rucrypto_result_invalid_parameters) {
            status = rucrypto_result_not_accepted;
        }
        rucrypto_deallocate(buffer);
        return status;
    }

    rucrypto_gost_34_10_big_num_fill_from_buffer(publicKey, sizeof(rucrypto_vec512_t) / 2, &Q.x);
    rucrypto_gost_34_10_big_num_fill_from_buffer(&publicKey[sizeof(rucrypto_vec512_t) / 2], sizeof(rucrypto_vec512_t) / 2, &Q.y);

    rucrypto_gost_34_10_big_num_fill_0(&z2Q.x);
    rucrypto_gost_34_10_big_num_fill_0(&z2Q.y);

    status = rucrypto_gost_34_10_calculate_k_point(curve, &Q, &v, &z2Q, false);
    if (status != rucrypto_result_ok) {
        if (status == rucrypto_result_invalid_parameters) {
            status = rucrypto_result_not_accepted;
        }
        rucrypto_deallocate(buffer);
        return status;
    }

    rucrypto_gost_34_10_big_num_fill_0(&C.x);
    rucrypto_gost_34_10_big_num_fill_0(&C.y);

    status = rucrypto_gost_34_10_add_points(curve, &z1P, &z2Q, &C, false);
    if (status != rucrypto_result_ok) {
        if (status == rucrypto_result_invalid_parameters) {
            status = rucrypto_result_not_accepted;
        }
        rucrypto_deallocate(buffer);
        return status;
    }
    status = rucrypto_gost_34_10_rebase(&C.x, &C.x, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }
    rucrypto_result_t res = rucrypto_result_not_accepted;
    if (rucrypto_gost_34_10_are_equal(&C.x, &r)) {
        res = rucrypto_result_accepted;
    }

    rucrypto_deallocate(buffer);
    return res;
}

rucrypto_result_t rucrypto_gost_34_10_generate_signature_1024(
        const rucrypto_gost_34_10_elliptic_curve_t* curve,
        const rucrypto_vec512_t hash,
        const rucrypto_vec512_t privateKey,
        rucrypto_gost_34_10_random_generator_func_t random,
        rucrypto_gost_34_10_signature_1024_t signature) {
    RUCRYPTO_CHECK_ARGS(curve->size == 512);
    rucrypto_size_t size = curve->size / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8);

    rucrypto_gost_34_10_big_num_underlying_t* buffer;
    buffer = rucrypto_allocate(5 * size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if (buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }

    rucrypto_result_t status;
    rucrypto_gost_34_10_big_num_t e, k, r, s, d;
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 0 * size, &e, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 1 * size, &k, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 2 * size, &r, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 3 * size, &s, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 4 * size, &d, size);

    rucrypto_gost_34_10_big_num_fill_from_buffer(hash, sizeof(rucrypto_vec512_t), &e);
    status = rucrypto_gost_34_10_rebase(&e, &e, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }
    if (rucrypto_gost_34_10_is_zero(&e)) {
        e.buffer[e.size - 1] = 1;
    }

    rucrypto_gost_34_10_big_num_fill_0(&r);
    rucrypto_gost_34_10_big_num_fill_0(&s);
    rucrypto_gost_34_10_big_num_fill_random(&k, random);
    status = rucrypto_gost_34_10_rebase(&k, &k, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }
    do {
        rucrypto_gost_34_10_point_t C = {
            .size = size,
            .x = r,
            .y = s
        };
        status = rucrypto_gost_34_10_calculate_k_point(curve, &curve->P, &k, &C, false);
        if (status != rucrypto_result_ok) {
            if (status == rucrypto_result_out_of_memory) {
                rucrypto_deallocate(buffer);
                return status;
            }
            rucrypto_gost_34_10_big_num_fill_random(&k, random);
            continue;
        }
        status = rucrypto_gost_34_10_rebase(&r, &r, &curve->q, false);
        if (status != rucrypto_result_ok) {
            rucrypto_deallocate(buffer);
            return status;
        }
        if (rucrypto_gost_34_10_is_zero(&r)) {
            rucrypto_gost_34_10_big_num_fill_random(&k, random);
            continue;
        }
        status = rucrypto_gost_34_10_mul_based(&k, &e, &k, &curve->q, false);
        if (status != rucrypto_result_ok) {
            return status;
        }
        rucrypto_gost_34_10_big_num_copy(&r, &s);
        rucrypto_gost_34_10_big_num_fill_from_buffer(privateKey, sizeof(rucrypto_vec512_t), &d);
        status = rucrypto_gost_34_10_mul_based(&s, &d, &s, &curve->q, true);
        if (status != rucrypto_result_ok) {
            rucrypto_secure_zero(s.buffer, s.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
            rucrypto_secure_zero(d.buffer, d.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
            return status;
        }
        status = rucrypto_gost_34_10_add_based(&s, &k, &s, &curve->q, true);
        if (status != rucrypto_result_ok) {
            rucrypto_secure_zero(s.buffer, s.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
            rucrypto_secure_zero(d.buffer, d.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
            return status;
        }
        if (rucrypto_gost_34_10_is_zero(&s)) {
            rucrypto_gost_34_10_big_num_fill_random(&k, random);
            continue;
        }
        break;
    } while (true);

    rucrypto_gost_34_10_big_num_copy_to_buffer(&r, signature, 512 / 8);
    rucrypto_gost_34_10_big_num_copy_to_buffer(&s, &signature[512 / 8], 512 / 8);

    rucrypto_deallocate(buffer);
    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_gost_34_10_verify_signature_1024(
        const rucrypto_gost_34_10_elliptic_curve_t* curve,
        const rucrypto_vec512_t hash,
        const rucrypto_vec1024_t publicKey,
        const rucrypto_gost_34_10_signature_1024_t signature) {
    RUCRYPTO_CHECK_ARGS(curve->size == 512);
    rucrypto_size_t size = curve->size / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8);

    rucrypto_gost_34_10_big_num_underlying_t* buffer;
    buffer = rucrypto_allocate(11 * size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if (buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }

    rucrypto_result_t status;
    rucrypto_gost_34_10_big_num_t r, s, v;
    rucrypto_gost_34_10_point_t z1P, Q, z2Q, C;

    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 0  * size, &r, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 1  * size, &s, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 2  * size, &v, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 3  * size, &z1P.x, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 4  * size, &z1P.y, size);
    z1P.size = size;
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 5  * size, &Q.x, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 6  * size, &Q.y, size);
    Q.size = size;
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 7  * size, &z2Q.x, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 8  * size, &z2Q.y, size);
    z2Q.size = size;
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 9  * size, &C.x, size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 10 * size, &C.y, size);
    C.size = size;

    rucrypto_gost_34_10_big_num_fill_from_buffer(signature, sizeof(rucrypto_gost_34_10_signature_1024_t) / 2, &r);
    rucrypto_gost_34_10_big_num_fill_from_buffer(&signature[sizeof(rucrypto_gost_34_10_signature_1024_t) / 2], sizeof(rucrypto_gost_34_10_signature_1024_t) / 2, &s);

    if (!rucrypto_gost_34_10_is_less(&r, &curve->q) || !rucrypto_gost_34_10_is_less(&s, &curve->q)) {
        rucrypto_deallocate(buffer);
        return rucrypto_result_not_accepted;
    }

    rucrypto_gost_34_10_big_num_fill_from_buffer(hash, sizeof(rucrypto_vec512_t), &v);
    if (rucrypto_gost_34_10_is_zero(&v)) {
        v.buffer[v.size - 1] = 1;
    }
    status = rucrypto_gost_34_10_rebase(&v, &v, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }

    // s = z1, v = z2
    status = rucrypto_gost_34_10_mul_inv(&v, &v, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }
    status = rucrypto_gost_34_10_mul_based(&s, &v, &s, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }
    status = rucrypto_gost_34_10_mul_based(&r, &v, &v, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }
    rucrypto_gost_34_10_sub(&curve->q, &v, &v);

    rucrypto_gost_34_10_big_num_fill_0(&z1P.x);
    rucrypto_gost_34_10_big_num_fill_0(&z1P.y);

    status = rucrypto_gost_34_10_calculate_k_point(curve, &curve->P, &s, &z1P, false);
    if (status != rucrypto_result_ok) {
        if (status == rucrypto_result_invalid_parameters) {
            status = rucrypto_result_not_accepted;
        }
        rucrypto_deallocate(buffer);
        return status;
    }

    rucrypto_gost_34_10_big_num_fill_from_buffer(publicKey, sizeof(rucrypto_vec1024_t) / 2, &Q.x);
    rucrypto_gost_34_10_big_num_fill_from_buffer(&publicKey[sizeof(rucrypto_vec1024_t) / 2], sizeof(rucrypto_vec1024_t) / 2, &Q.y);

    rucrypto_gost_34_10_big_num_fill_0(&z2Q.x);
    rucrypto_gost_34_10_big_num_fill_0(&z2Q.y);

    status = rucrypto_gost_34_10_calculate_k_point(curve, &Q, &v, &z2Q, false);
    if (status != rucrypto_result_ok) {
        if (status == rucrypto_result_invalid_parameters) {
            status = rucrypto_result_not_accepted;
        }
        rucrypto_deallocate(buffer);
        return status;
    }

    rucrypto_gost_34_10_big_num_fill_0(&C.x);
    rucrypto_gost_34_10_big_num_fill_0(&C.y);

    status = rucrypto_gost_34_10_add_points(curve, &z1P, &z2Q, &C, false);
    if (status != rucrypto_result_ok) {
        if (status == rucrypto_result_invalid_parameters) {
            status = rucrypto_result_not_accepted;
        }
        rucrypto_deallocate(buffer);
        return status;
    }
    status = rucrypto_gost_34_10_rebase(&C.x, &C.x, &curve->q, false);
    if (status != rucrypto_result_ok) {
        rucrypto_deallocate(buffer);
        return status;
    }
    rucrypto_result_t res = rucrypto_result_not_accepted;
    if (rucrypto_gost_34_10_are_equal(&C.x, &r)) {
        res = rucrypto_result_accepted;
    }

    rucrypto_deallocate(buffer);
    return res;
}

void rucrypto_gost_34_10_elliptic_curve_fini(rucrypto_gost_34_10_elliptic_curve_t** curve) {
    rucrypto_deallocate((*curve)->buffer);
    rucrypto_deallocate(*curve);
    *curve = RUCRYPTO_NULL;
}
