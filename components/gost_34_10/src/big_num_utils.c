#include <rucrypto/gost_34_10/big_num_utils.h>

#include <rucrypto/common/secure_zero.h>

rucrypto_result_t rucrypto_gost_34_10_big_num_init_0(
        rucrypto_gost_34_10_big_num_t* var, 
        rucrypto_size_t size) {
    var->buffer = rucrypto_allocate_zeroed(size, sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if (var->buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    var->size = size;
    var->negative = false;
    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_gost_34_10_big_num_init_random(
        rucrypto_gost_34_10_big_num_t* var,
        rucrypto_size_t size,
        rucrypto_gost_34_10_random_generator_func_t random) {
    var->buffer = rucrypto_allocate(size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if (var->buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    var->negative = false;
    var->size = size;
    rucrypto_gost_34_10_big_num_fill_random(var, random);
    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_gost_34_10_big_num_init_copy(
        const rucrypto_gost_34_10_big_num_t* src,
        rucrypto_gost_34_10_big_num_t* dst) {
    RUCRYPTO_ASSERT(src->buffer != RUCRYPTO_NULL);
    RUCRYPTO_ASSERT(src->size != 0);

    dst->buffer = rucrypto_allocate(src->size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if (dst->buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    dst->size = src->size;

    rucrypto_gost_34_10_big_num_copy(src, dst);
    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_gost_34_10_big_num_init_from_buffer(
        const rucrypto_uint8_t* buffer,
        rucrypto_size_t bufferSize,
        rucrypto_gost_34_10_big_num_t* dst) {
    RUCRYPTO_ASSERT(bufferSize % sizeof(rucrypto_gost_34_10_big_num_underlying_t) == 0);

    dst->buffer = rucrypto_allocate(bufferSize);
    if (dst->buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    rucrypto_gost_34_10_big_num_fill_from_buffer(buffer, bufferSize, dst);
    return rucrypto_result_ok;
}

void rucrypto_gost_34_10_big_num_init_no_alloc(
        rucrypto_gost_34_10_big_num_underlying_t* buffer,
        rucrypto_size_t offset,
        rucrypto_gost_34_10_big_num_t* var,
        rucrypto_size_t size) {
    var->negative = false;
    var->buffer = &buffer[offset];
    var->size = size;
}

void rucrypto_gost_34_10_big_num_fini(
        rucrypto_gost_34_10_big_num_t* var,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(var->buffer);

    if (secure) {
        rucrypto_secure_zero(var->buffer, var->size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    }

    rucrypto_deallocate(var->buffer);
    var->buffer = RUCRYPTO_NULL;
    var->size = 0;
}

void rucrypto_gost_34_10_big_num_fill_0(rucrypto_gost_34_10_big_num_t* var) {
    RUCRYPTO_ASSERT(var->buffer);
    RUCRYPTO_ASSERT(var->size != 0);

    var->negative = false;
    RUCRYPTO_MEMSET(var->buffer, 0, var->size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
}

void rucrypto_gost_34_10_big_num_copy(
        const rucrypto_gost_34_10_big_num_t* src,
        rucrypto_gost_34_10_big_num_t* dst) {
    RUCRYPTO_ASSERT(src->buffer != RUCRYPTO_NULL);
    RUCRYPTO_ASSERT(dst->buffer != RUCRYPTO_NULL);
    RUCRYPTO_ASSERT(src->size == dst->size);

    dst->negative = src->negative;
    RUCRYPTO_MEMCPY(dst->buffer, src->buffer, dst->size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
}

void rucrypto_gost_34_10_big_num_fill_random(
        rucrypto_gost_34_10_big_num_t* var,
        rucrypto_gost_34_10_random_generator_func_t random) {
    RUCRYPTO_ASSERT(random);
    RUCRYPTO_ASSERT(var->buffer);
    RUCRYPTO_ASSERT(var->size != 0);

    var->negative = false;
    for (rucrypto_size_t i = 0; i < var->size; ++i) {
        var->buffer[i] = 0;
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
        var->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)random() << 56;
        var->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)random() << 48;
        var->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)random() << 40;
        var->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)random() << 32;
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
        var->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)random() << 24;
        var->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)random() << 16;
        var->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)random() <<  8;
        var->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)random() <<  0;
    }
}

void rucrypto_gost_34_10_big_num_fill_from_buffer(
        const rucrypto_uint8_t* buffer,
        rucrypto_size_t bufferSize,
        rucrypto_gost_34_10_big_num_t* dst) {
    RUCRYPTO_ASSERT(bufferSize % sizeof(rucrypto_gost_34_10_big_num_underlying_t) == 0);
    rucrypto_size_t size = bufferSize / sizeof(rucrypto_gost_34_10_big_num_underlying_t);

    dst->negative = false;
    dst->size = size;
    for (rucrypto_size_t i = 0; i < size; ++i) {
        dst->buffer[i] = 0;
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
        dst->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 0] << 56;
        dst->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 1] << 48;
        dst->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 2] << 40;
        dst->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 3] << 32;
        dst->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 4] << 24;
        dst->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 5] << 16;
        dst->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 6] <<  8;
        dst->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 7] <<  0;
#else
        dst->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 0] << 24;
        dst->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 1] << 16;
        dst->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 2] <<  8;
        dst->buffer[i] |= (rucrypto_gost_34_10_big_num_underlying_t)buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 3] <<  0;
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    }
}

void rucrypto_gost_34_10_big_num_copy_to_buffer(
        const rucrypto_gost_34_10_big_num_t* src,
        rucrypto_uint8_t* buffer,
        rucrypto_size_t bufferSize) {
    RUCRYPTO_ASSERT(src->size * sizeof(rucrypto_gost_34_10_big_num_underlying_t) <= bufferSize);

    /*
     * Buffer size is only used in assert
     */
    (void)bufferSize;

    for (rucrypto_size_t i = 0; i < src->size; ++i) {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
        buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 0] = (rucrypto_uint8_t)(src->buffer[i] >> 56 & 0xFF);
        buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 1] = (rucrypto_uint8_t)(src->buffer[i] >> 48 & 0xFF);
        buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 2] = (rucrypto_uint8_t)(src->buffer[i] >> 40 & 0xFF);
        buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 3] = (rucrypto_uint8_t)(src->buffer[i] >> 32 & 0xFF);
        buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 4] = (rucrypto_uint8_t)(src->buffer[i] >> 24 & 0xFF);
        buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 5] = (rucrypto_uint8_t)(src->buffer[i] >> 16 & 0xFF);
        buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 6] = (rucrypto_uint8_t)(src->buffer[i] >>  8 & 0xFF);
        buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 7] = (rucrypto_uint8_t)(src->buffer[i] >>  0 & 0xFF);
#else
        buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 0] = (rucrypto_uint8_t)(src->buffer[i] >> 24 & 0xFF);
        buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 1] = (rucrypto_uint8_t)(src->buffer[i] >> 16 & 0xFF);
        buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 2] = (rucrypto_uint8_t)(src->buffer[i] >>  8 & 0xFF);
        buffer[i * sizeof(rucrypto_gost_34_10_big_num_underlying_t) + 3] = (rucrypto_uint8_t)(src->buffer[i] >>  0 & 0xFF);
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    }
}

rucrypto_size_t rucrypto_gost_34_10_len(const rucrypto_gost_34_10_big_num_t* a) {
    for (rucrypto_size_t i = 0; i < a->size; ++i) {
        if (a->buffer[i] != 0) {
            return a->size - i;
        }
    }
    return 1;
}

rucrypto_bool_t rucrypto_gost_34_10_is_zero(const rucrypto_gost_34_10_big_num_t* a) {
    for (rucrypto_size_t i = 0; i < a->size; ++i) {
        if (a->buffer[i] != 0) {
            return false;
        }
    }
    return true;
}

rucrypto_bool_t rucrypto_gost_34_10_are_equal(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b) {
    RUCRYPTO_ASSERT(a->size == b->size);
    if (a->negative != b->negative) {
        return false;
    }
    for (rucrypto_size_t i = 0; i < a->size; ++i) {
        if (a->buffer[i] != b->buffer[i]) {
            return false;
        }
    }
    return true;
}

rucrypto_bool_t rucrypto_gost_34_10_are_equal_and_not_zero(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b) {
    RUCRYPTO_ASSERT(a->size == b->size);
    if (a->negative != b->negative) {
        return false;
    }
    rucrypto_bool_t is_zero = false;
    for (rucrypto_size_t i = 0; i < a->size; ++i) {
        if (a->buffer[i] != b->buffer[i]) {
            return false;
        }
        if (a->buffer[i] != 0) {
            is_zero = true;
        }
    }
    return is_zero;
}

rucrypto_bool_t rucrypto_gost_34_10_is_bigger(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b) {
    RUCRYPTO_ASSERT(!a->negative);
    RUCRYPTO_ASSERT(!b->negative);
    RUCRYPTO_ASSERT(a->size == b->size);

    for (rucrypto_size_t i = 0; i < a->size; ++i) {
        if (a->buffer[i] > b->buffer[i]) {
            return true;
        } else if (a->buffer[i] < b->buffer[i]) {
            return false;
        }
    }
    return false;
}

rucrypto_bool_t rucrypto_gost_34_10_is_bigger_or_equal(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b) {
    RUCRYPTO_ASSERT(!a->negative);
    RUCRYPTO_ASSERT(!b->negative);
    RUCRYPTO_ASSERT(a->size == b->size);

    for (rucrypto_size_t i = 0; i < a->size; ++i) {
        if (a->buffer[i] > b->buffer[i]) {
            return true;
        } else if (a->buffer[i] < b->buffer[i]) {
            return false;
        }
    }
    return true;
}

rucrypto_bool_t rucrypto_gost_34_10_is_less(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b) {
    RUCRYPTO_ASSERT(!a->negative);
    RUCRYPTO_ASSERT(!b->negative);
    RUCRYPTO_ASSERT(a->size == b->size);

    for (rucrypto_size_t i = 0; i < a->size; ++i) {
        if (a->buffer[i] > b->buffer[i]) {
            return false;
        } else if (a->buffer[i] < b->buffer[i]) {
            return true;
        }
    }
    return false;
}

rucrypto_bool_t rucrypto_gost_34_10_is_less_or_equal(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b) {
    RUCRYPTO_ASSERT(!a->negative);
    RUCRYPTO_ASSERT(!b->negative);
    RUCRYPTO_ASSERT(a->size == b->size);

    for (rucrypto_size_t i = 0; i < a->size; ++i) {
        if (a->buffer[i] > b->buffer[i]) {
            return false;
        } else if (a->buffer[i] < b->buffer[i]) {
            return true;
        }
    }
    return true;
}

void rucrypto_gost_34_10_shift_left(
        rucrypto_gost_34_10_big_num_t* a,
        rucrypto_size_t shiftCount) {
    if (shiftCount == 0) {
        return;
    }
    if (shiftCount >= a->size) {
        RUCRYPTO_MEMSET(a->buffer, 0, a->size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
        return;
    }
    for (rucrypto_size_t i = 0; i < a->size - shiftCount; ++i) {
        a->buffer[i] = a->buffer[i + shiftCount];
    }
    for (rucrypto_size_t i = a->size - shiftCount; i < a->size; ++i) {
        a->buffer[i] = 0;
    }
}

void rucrypto_gost_34_10_bit_shift_left(
        rucrypto_gost_34_10_big_num_t* a,
        rucrypto_size_t shiftCount) {
    if (shiftCount == 0) {
        return;
    }
    if (shiftCount / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8) >= a->size) {
        RUCRYPTO_MEMSET(a->buffer, 0, a->size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
        return;
    }
    rucrypto_gost_34_10_shift_left(a, shiftCount / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8));
    rucrypto_size_t bitShiftCount = shiftCount % (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8);
    for (rucrypto_size_t i = 0; i < a->size - 1; ++i) {
        a->buffer[i] = (a->buffer[i + 1] >> ((sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8) - bitShiftCount)) | (a->buffer[i] << bitShiftCount);
    }
    a->buffer[a->size - 1] <<= bitShiftCount;
}

void rucrypto_gost_34_10_shift_right(
        rucrypto_gost_34_10_big_num_t* a,
        rucrypto_size_t shiftCount) {
    if (shiftCount == 0) {
        return;
    }
    if (shiftCount >= a->size) {
        RUCRYPTO_MEMSET(a->buffer, 0, a->size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
        return;
    }
    for (rucrypto_size_t i = a->size - 1; i >= shiftCount; --i) {
        a->buffer[i] = a->buffer[i - shiftCount];
    }
    for (rucrypto_size_t i = a->size - shiftCount; i < a->size; ++i) {
        a->buffer[a->size - i - 1] = 0;
    }
}

void rucrypto_gost_34_10_bit_shift_right(
        rucrypto_gost_34_10_big_num_t* a,
        rucrypto_size_t shiftCount) {
    if (shiftCount == 0) {
        return;
    }
    if (shiftCount / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8) >= a->size) {
        RUCRYPTO_MEMSET(a->buffer, 0, a->size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
        return;
    }
    rucrypto_gost_34_10_shift_right(a, shiftCount / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8));
    rucrypto_size_t bitShiftCount = shiftCount % (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8);
    for (rucrypto_size_t i = a->size - 1; i > 0; --i) {
        a->buffer[i] = (a->buffer[i - 1] << ((sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8) - bitShiftCount)) | (a->buffer[i] >> bitShiftCount);
    }
    a->buffer[0] >>= bitShiftCount;
}
