#include <rucrypto/gost_34_10/basic_math_utils.h>

#include <rucrypto/common/secure_zero.h>

#if defined(RUCRYPTO_X86_64_USE_INTRINSICS)
#ifdef _MSC_VER
#include <intrin.h>
#else
#include <x86intrin.h>
#endif // _MSC_VER
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS)

void rucrypto_gost_34_10_mul_by_2(rucrypto_gost_34_10_big_num_t* a) {
    for (rucrypto_size_t i = 0; i < a->size - 1; ++i) {
        a->buffer[i] = (a->buffer[i] << 1) | (a->buffer[i + 1] >> (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8 - 1));
    }
    a->buffer[a->size - 1] <<= 1;
}

void rucrypto_gost_34_10_div_by_2(rucrypto_gost_34_10_big_num_t* a) {
    for (rucrypto_size_t i = a->size - 1; i > 0; --i) {
        a->buffer[i] = (a->buffer[i - 1] << (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8 - 1)) | (a->buffer[i] >> 1);
    }
    a->buffer[0] >>= 1;
}

void rucrypto_gost_34_10_add(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* w) {
    RUCRYPTO_ASSERT(!u->negative);
    RUCRYPTO_ASSERT(!v->negative);
    RUCRYPTO_ASSERT(u->size == v->size);
    RUCRYPTO_ASSERT(w->size > u->size);

#if defined(RUCRYPTO_X86_64_USE_INTRINSICS)
    /*
     * x86_64-linux-gnu-gcc defines uint64_t (aliased to rucrypto_uint64_t) as unsigned long, while intrinsics expect unsigned long long
     */
    RUCRYPTO_STATIC_ASSERT(sizeof(unsigned long long) == sizeof(rucrypto_uint64_t), "Error: unsigned long long must be 64 bits!");

    rucrypto_uint8_t carry = 0;
    for (rucrypto_size_t i = 0; i < u->size; ++i) {
        carry = _addcarry_u64(carry, u->buffer[u->size - i - 1], v->buffer[v->size - i - 1], (unsigned long long*)&w->buffer[w->size - i - 1]);
    }
    w->buffer[w->size - u->size - 1] = carry;
#elif defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    __asm__("movq %0, %%rcx\n"
            "xorq %%rax, %%rax\n"
            "movq %1, %%rax\n"
            "movq %2, %%rbx\n"
            "movq %3, %%rdx\n"
            "loopstart_rucrypto_gost_34_10_add_%=:\n"
            "movq (%%rax), %%r8\n"
            "adcq (%%rbx), %%r8\n"
            "movq %%r8, (%%rdx)\n"
            "leaq -8(%%rax), %%rax\n"
            "leaq -8(%%rbx), %%rbx\n"
            "leaq -8(%%rdx), %%rdx\n"
            "loop loopstart_rucrypto_gost_34_10_add_%=\n"
            "adcq $0, (%%rdx)\n"
            :
            : "g" (u->size), "g" (&u->buffer[u->size - 1]), "g" (&v->buffer[v->size - 1]), "g" (&w->buffer[w->size - 1])
            : "rax", "rbx", "rcx", "rdx", "r8", "cc", "memory"
    );
#else
    rucrypto_uint32_t carry = 0;
    for (rucrypto_size_t i = 0; i < u->size; ++i) {
        rucrypto_uint64_t sum = (rucrypto_uint64_t)u->buffer[u->size - i - 1] + (rucrypto_uint64_t)v->buffer[v->size - i - 1] + carry;
        w->buffer[w->size - i - 1] = (rucrypto_uint32_t)(sum & 0xFFFFFFFF);
        carry = (rucrypto_uint32_t)(sum >> 32);
    }
    w->buffer[w->size - u->size - 1] = carry;
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
}

void rucrypto_gost_34_10_add_no_carry(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* w) {
    RUCRYPTO_ASSERT(!u->negative);
    RUCRYPTO_ASSERT(!v->negative);
    RUCRYPTO_ASSERT(u->size == v->size);
    RUCRYPTO_ASSERT(u->size <= w->size);

#if defined(RUCRYPTO_X86_64_USE_INTRINSICS)
    /*
     * x86_64-linux-gnu-gcc defines uint64_t (aliased to rucrypto_uint64_t) as unsigned long, while intrinsics expect unsigned long long
     */
    RUCRYPTO_STATIC_ASSERT(sizeof(unsigned long long) == sizeof(rucrypto_uint64_t), "Error: unsigned long long must be 64 bits!");

    rucrypto_uint8_t carry = 0;
    for (rucrypto_size_t i = 0; i < u->size; ++i) {
        carry = _addcarry_u64(carry, u->buffer[u->size - i - 1], v->buffer[v->size - i - 1], (unsigned long long*)&w->buffer[w->size - i - 1]);
    }
#elif defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    __asm__("movq %0, %%rcx\n"
            "xorq %%rax, %%rax\n"
            "movq %1, %%rax\n"
            "movq %2, %%rbx\n"
            "movq %3, %%rdx\n"
            "loopstart_rucrypto_gost_34_10_add_no_carry_%=:\n"
            "movq (%%rax), %%r8\n"
            "adcq (%%rbx), %%r8\n"
            "movq %%r8, (%%rdx)\n"
            "leaq -8(%%rax), %%rax\n"
            "leaq -8(%%rbx), %%rbx\n"
            "leaq -8(%%rdx), %%rdx\n"
            "loop loopstart_rucrypto_gost_34_10_add_no_carry_%=\n"
            :
            : "g" (u->size), "g" (&u->buffer[u->size - 1]), "g" (&v->buffer[v->size - 1]), "g" (&w->buffer[w->size - 1])
            : "rax", "rbx", "rcx", "rdx", "r8", "cc", "memory"
    );
#else
    rucrypto_uint32_t carry = 0;
    for (rucrypto_size_t i = 0; i < u->size; ++i) {
        rucrypto_uint64_t sum = (rucrypto_uint64_t)u->buffer[u->size - i - 1] + (rucrypto_uint64_t)v->buffer[v->size - i - 1] + carry;
        w->buffer[w->size - i - 1] = (rucrypto_uint32_t)(sum & 0xFFFFFFFF);
        carry = (rucrypto_uint32_t)(sum >> 32);
    }
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
}

void rucrypto_gost_34_10_sub(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* w) {
    RUCRYPTO_ASSERT(!u->negative);
    RUCRYPTO_ASSERT(!v->negative);
    RUCRYPTO_ASSERT(u->size > 1);
    RUCRYPTO_ASSERT(u->size == v->size);
    RUCRYPTO_ASSERT(w->size >= u->size);

#if defined(RUCRYPTO_X86_64_USE_INTRINSICS)
    /*
     * x86_64-linux-gnu-gcc defines rucrypto_uint64_t as unsigned long, while intrinsics expect unsigned long long
     */
    RUCRYPTO_STATIC_ASSERT(sizeof(unsigned long long) == sizeof(rucrypto_uint64_t), "Error: unsigned long long must be 64 bits!");

    const rucrypto_gost_34_10_big_num_t* a;
    const rucrypto_gost_34_10_big_num_t* b;
    w->negative = rucrypto_gost_34_10_is_bigger(v, u);
    if (w->negative) {
        // v - u
        a = v;
        b = u;
    } else {
        // u - v
        a = u;
        b = v;
    }

    rucrypto_uint8_t borrow = 0;
    for (rucrypto_size_t i = 0; i < a->size; ++i) {
        borrow = _subborrow_u64(borrow, a->buffer[a->size - i - 1], b->buffer[b->size - i - 1], (unsigned long long*)&w->buffer[w->size - i - 1]);
    }
#elif defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    rucrypto_uint64_t* start_a;
    rucrypto_uint64_t* start_b;
    w->negative = rucrypto_gost_34_10_is_bigger(v, u);
    if (w->negative) {
        // v - u
        start_a = &v->buffer[v->size - 1];
        start_b = &u->buffer[u->size - 1];
    } else {
        // u - v
        start_a = &u->buffer[u->size - 1];
        start_b = &v->buffer[v->size - 1];
    }

    __asm__("movq %0, %%rcx\n"
            "xorq %%rax, %%rax\n"
            "movq %1, %%rax\n"
            "movq %2, %%rbx\n"
            "movq %3, %%rdx\n"
            "loopstart_rucrypto_gost_34_10_sub_%=:\n"
            "movq (%%rax), %%r8\n"
            "sbbq (%%rbx), %%r8\n"
            "movq %%r8, (%%rdx)\n"
            "leaq -8(%%rax), %%rax\n"
            "leaq -8(%%rbx), %%rbx\n"
            "leaq -8(%%rdx), %%rdx\n"
            "loop loopstart_rucrypto_gost_34_10_sub_%=\n"
            :
            : "g" (u->size), "g" (start_a), "g" (start_b), "g" (&w->buffer[w->size - 1])
            : "rax", "rbx", "rcx", "rdx", "r8", "cc", "memory"
    );
#else
    const rucrypto_gost_34_10_big_num_t* a;
    const rucrypto_gost_34_10_big_num_t* b;

    w->negative = rucrypto_gost_34_10_is_bigger(v, u);
    if (w->negative) {
        // v - u
        a = v;
        b = u;
    } else {
        // u - v
        a = u;
        b = v;
    }

    rucrypto_uint32_t borrow = 0;
    for (rucrypto_size_t i = 0; i < a->size; ++i) {
        rucrypto_uint64_t sum = (rucrypto_uint64_t)a->buffer[a->size - i - 1] - (rucrypto_uint64_t)b->buffer[b->size - i - 1] - borrow;
        w->buffer[w->size - i - 1] = (rucrypto_uint32_t)(sum & 0xFFFFFFFF);
        borrow = (rucrypto_uint32_t)(sum >> 32) ? 1 : 0;
    }
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
}

void rucrypto_gost_34_10_mul_1(
        const rucrypto_gost_34_10_big_num_t* u,
        rucrypto_gost_34_10_big_num_underlying_t v,
        rucrypto_gost_34_10_big_num_t* w) {
    RUCRYPTO_ASSERT(w->size > u->size);
    RUCRYPTO_ASSERT(w != u);

#if defined(RUCRYPTO_X86_64_USE_INTRINSICS)
    rucrypto_uint64_t high_bits = 0;
    for (rucrypto_size_t j = 0; j < u->size; ++j) {
#ifdef _MSC_VER
        rucrypto_uint64_t tmp = high_bits;
        rucrypto_uint64_t mul = _umul128(u->buffer[u->size - j - 1], v, &high_bits);
        rucrypto_uint8_t carry = _addcarry_u64(0, mul, tmp, &w->buffer[w->size - j - 1]);
        high_bits += carry;
#else
        __uint128_t mul = (__uint128_t)u->buffer[u->size - j - 1] * v + high_bits;
        high_bits = (mul >> 64);
        w->buffer[w->size - j - 1] = (mul & 0xFFFFFFFFFFFFFFFF);
#endif // _MSC_VER
    }
    w->buffer[w->size - u->size - 1] = high_bits;
#elif defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    __asm__("movq %0, %%rcx\n"                              // size -> rcx
            "movq %1, %%rbx\n"                              // v -> rbx
            "movq %2, %%r8\n"                               // &u[len] -> r8
            "movq %3, %%r9\n"                               // &w[len] -> r9
            "xorq %%rdx, %%rdx\n"                           // clear rdx
            "loopstart_rucrypto_gost_34_10_mul_1_%=:\n"     //
            "movq %%rdx, %%r10\n"                           //
            "movq (%%r8), %%rax\n"                          //
            "mulq %%rbx\n"                                  //
            "addq %%r10, %%rax\n"                           //
            "adcq $0, %%rdx\n"                              //
            "movq %%rax, (%%r9)\n"                          //
            "leaq -8(%%r8), %%r8\n"                         //
            "leaq -8(%%r9), %%r9\n"                         //
            "loop loopstart_rucrypto_gost_34_10_mul_1_%=\n" //
            "movq %%rdx, (%%r9)"                            //
            :
            : "g" (u->size), "g" (v), "g" (&u->buffer[u->size - 1]), "g" (&w->buffer[w->size - 1])
            : "rax", "rbx", "rcx", "rdx", "r8", "r9", "r10", "cc", "memory"
    );
#else
    rucrypto_uint32_t c = 0;
    for (rucrypto_size_t j = 0; j < u->size; ++j) {
        rucrypto_uint64_t mul = (rucrypto_uint64_t)u->buffer[u->size - j - 1] * v + c;
        w->buffer[w->size - j - 1] = (rucrypto_uint32_t)(mul & 0xFFFFFFFF);
        c = (rucrypto_uint32_t)(mul >> 32);
    }
    w->buffer[w->size - u->size - 1] = c;
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
}

void rucrypto_gost_34_10_mul(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* w) {
    RUCRYPTO_ASSERT(u->size == v->size);
    RUCRYPTO_ASSERT(w->size == u->size + v->size);
    RUCRYPTO_ASSERT(w != u);
    RUCRYPTO_ASSERT(w != v);

    RUCRYPTO_MEMSET(w->buffer, 0, w->size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS)
    for (rucrypto_size_t i = 0; i < v->size; ++i) {
        rucrypto_uint64_t high_bits = 0;
        for (rucrypto_size_t j = 0; j < u->size; ++j) {
#ifdef _MSC_VER
            rucrypto_uint64_t tmp = high_bits;
            rucrypto_uint64_t mul = _umul128(u->buffer[u->size - j - 1], v->buffer[v->size - i - 1], &high_bits);
            rucrypto_uint8_t carry = _addcarry_u64(0, w->buffer[w->size - i - j - 1], mul, &w->buffer[w->size - i - j - 1]);
            _addcarry_u64(carry, high_bits, 0, &high_bits);
            carry = _addcarry_u64(0, w->buffer[w->size - i - j - 1], tmp, &w->buffer[w->size - i - j - 1]);
            _addcarry_u64(carry, high_bits, 0, &high_bits);
#else
            __uint128_t mul = (__uint128_t) w->buffer[w->size - i - j - 1] +
                              (__uint128_t) u->buffer[u->size - j - 1] * v->buffer[v->size - i - 1] +
                              (__uint128_t) high_bits;
            high_bits = (rucrypto_uint64_t)(mul >> 64);
            w->buffer[w->size - i - j - 1] = (rucrypto_uint64_t)(mul & 0xFFFFFFFFFFFFFFFF);
#endif // _MSC_VER
        }
        w->buffer[w->size - i - u->size - 1] = high_bits;
    }
#elif defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    __asm__("movq %0, %%rbx\n"                               // size -> rbx
            "movq %1, %%r8\n"                                // &u[len] -> r8
            "movq %2, %%r9\n"                                // &v[len] -> r9
            "movq %3, %%r10\n"                               // &w[len] -> r10
            "movq %%rbx, %%rcx\n"                            //
            "loop_start_rucrypto_gost_34_10_mul_1_%=:\n"     // loop j from m to 0
            "movq %%rcx, %%r11\n"                            // (save rcx value, so it can be used for internal loop)
            "xorq %%rdx, %%rdx\n"                            // (clear carry from previous)
            "movq %%rbx, %%rcx\n"                            //
            "loop_start_rucrypto_gost_34_10_mul_2_%=:\n"     //    loop i from n to 0
            "movq %%rdx, %%r12\n"                            //       save rdx to r12
            "movq (%%r8), %%rax\n"                           //       u[i] -> rax
            "mulq (%%r9)\n"                                  //       rax * v[j]
            "addq %%r12, %%rax\n"                            //       rax += r12
            "adcq $0, %%rdx\n"                               //       rdx += carry
            "addq (%%r10), %%rax\n"                          //       rax += w[i + j]
            "adcq $0, %%rdx\n"                               //       rdx += carry
            "movq %%rax, (%%r10)\n"                          //       rax -> w[i + j]
            "leaq -8(%%r8), %%r8\n"                          //
            "leaq -8(%%r10), %%r10\n"                        //       (calculate new addresses for u[i], w[i + j])
            "loop loop_start_rucrypto_gost_34_10_mul_2_%=\n" //
            "movq %%rdx, (%%r10)\n"                          // rdx -> w[j]
            "leaq (%%r8,%%rbx,8), %%r8\n"                    //
            "leaq -8(%%r9), %%r9\n"                          //
            "leaq -8(%%r10,%%rbx,8), %%r10\n"                // (restore u[i] to start and calculate v[j], w[i + j])
            "movq %%r11, %%rcx\n"                            // (restore rcx for external loop)
            "loop loop_start_rucrypto_gost_34_10_mul_1_%=\n" //
            :
            : "g" (u->size), "g" (&u->buffer[u->size - 1]), "g" (&v->buffer[v->size - 1]), "g" (&w->buffer[w->size - 1])
            : "rax", "rbx", "rcx", "rdx", "r8", "r9", "r10", "r11", "r12", "cc", "memory"
    );
#else
    for (rucrypto_size_t i = 0; i < v->size; ++i) {
        rucrypto_uint32_t c = 0;
        for (rucrypto_size_t j = 0; j < u->size; ++j) {
            rucrypto_uint64_t mul = (rucrypto_uint64_t)w->buffer[w->size - i - j - 1] + (rucrypto_uint64_t)u->buffer[u->size - j - 1] * v->buffer[v->size - i - 1] + c;
            w->buffer[w->size - i - j - 1] = (rucrypto_uint32_t)(mul & 0xFFFFFFFF);
            c = (rucrypto_uint32_t)(mul >> 32);
        }
        w->buffer[w->size - i - u->size - 1] = c;
    }
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
}

void rucrypto_gost_34_10_div_1(
        const rucrypto_gost_34_10_big_num_t* u,
        rucrypto_gost_34_10_big_num_underlying_t v,
        rucrypto_gost_34_10_big_num_t* q,
        rucrypto_gost_34_10_big_num_underlying_t* r) {
    RUCRYPTO_ASSERT(!u->negative);
    RUCRYPTO_ASSERT(q->size >= u->size);
    RUCRYPTO_ASSERT(v != 0);

    if (q->size > u->size) {
        RUCRYPTO_MEMSET(q->buffer, 0, (q->size - u->size) * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    }

#if defined(RUCRYPTO_X86_64_USE_INTRINSICS)
    rucrypto_gost_34_10_big_num_underlying_t r_tmp = 0;
    for (rucrypto_size_t i = 0; i < u->size; ++i) {
#ifdef _MSC_VER
        q->buffer[q->size - u->size + i] = _udiv128(r_tmp, u->buffer[i], v, &r_tmp);
#else
        const __uint128_t tmp = (((__uint128_t)(r_tmp) << 64) | (__uint128_t)u->buffer[i]);
        q->buffer[q->size - u->size + i] = (rucrypto_uint64_t)(tmp / v);
        r_tmp = (rucrypto_uint64_t)(tmp % v);
#endif // _MSC_VER
    }
    *r = r_tmp;
#elif defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    *r = 0;
    __asm__("movq %1, %%rcx\n"
            "movq %2, %%r8\n"
            "movq %3, %%r9\n"
            "movq %4, %%rbx\n"
            "movq $0, %%rdx\n"
            "loopstart_rucrypto_gost_34_10_div_1_%=:\n"
            "movq (%%r8), %%rax\n"
            "divq %%rbx\n"
            "movq %%rax, (%%r9)\n"
            "leaq 8(%%r8), %%r8\n"
            "leaq 8(%%r9), %%r9\n"
            "loop loopstart_rucrypto_gost_34_10_div_1_%=\n"
            "mov %%rdx, %0"
            : "+g" (*r)
            : "g" (u->size), "g" (&u->buffer[0]), "g" (&q->buffer[q->size - u->size]), "g" (v)
            : "rax", "rbx", "rcx", "rdx", "r8", "r9", "cc", "memory"
    );
#elif defined(_MSC_VER)
#else
    *r = 0;
    for (rucrypto_size_t i = 0; i < u->size; ++i) {
        rucrypto_uint64_t pair = ((rucrypto_uint64_t)*r << 32) | (u->buffer[i]);
        q->buffer[q->size - u->size + i] = (rucrypto_uint32_t)(pair / (rucrypto_uint64_t)v & 0xFFFFFFFF);
        *r = (rucrypto_uint32_t)(pair % (rucrypto_uint64_t)v & 0xFFFFFFFF);
    }
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
}

rucrypto_result_t rucrypto_gost_34_10_div(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* q,
        rucrypto_gost_34_10_big_num_t* r,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(!u->negative);
    RUCRYPTO_ASSERT(!v->negative);
    RUCRYPTO_ASSERT(u->size == v->size);
    RUCRYPTO_ASSERT(u->size == q->size);
    RUCRYPTO_ASSERT(u->size == r->size);
    rucrypto_size_t size = u->size;

    rucrypto_size_t n = rucrypto_gost_34_10_len(u);
    rucrypto_size_t t = rucrypto_gost_34_10_len(v);
    RUCRYPTO_ASSERT(n >= t);

    RUCRYPTO_MEMSET(q->buffer, 0, size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));

    if (t == 1) {
        RUCRYPTO_MEMSET(r->buffer, 0, size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
        rucrypto_gost_34_10_div_1(u, v->buffer[size - 1], q, &r->buffer[size - 1]);
        return rucrypto_result_ok;
    }

    rucrypto_gost_34_10_big_num_underlying_t* buffer;
    buffer = rucrypto_allocate((3 * size + 1) * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if (buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }

    rucrypto_gost_34_10_big_num_t u_copy = {
        .negative = false,
        .buffer = &buffer[0],
        .size = size
    };
    rucrypto_gost_34_10_big_num_copy(u, &u_copy);

    rucrypto_gost_34_10_big_num_t vnt = {
        .negative = false,
        .buffer = &buffer[size],
        .size = size
    };
    rucrypto_gost_34_10_big_num_copy(v, &vnt);

    rucrypto_gost_34_10_big_num_t tmp = {
        .negative = false,
        .buffer = &buffer[2 * size],
        .size = size + 1
    };
    rucrypto_gost_34_10_big_num_t fake_tmp = {
            .negative = false,
            .buffer = &tmp.buffer[1],
            .size = tmp.size - 1
    };

    rucrypto_gost_34_10_shift_left(&vnt, n - t);
    while (rucrypto_gost_34_10_is_bigger_or_equal(&u_copy, &vnt)) {
        q->buffer[size - (n - t) - 1] += 1;
        rucrypto_gost_34_10_sub(&u_copy, &vnt, &u_copy);
    }

    static rucrypto_gost_34_10_big_num_underlying_t tmp_buffer[12];
    rucrypto_gost_34_10_big_num_underlying_t* uii1_buffer = &tmp_buffer[0];   // 2
    rucrypto_gost_34_10_big_num_t uii1 = {
            .negative = false,
            .buffer = uii1_buffer,
            .size = 2
    };
    rucrypto_gost_34_10_big_num_underlying_t* res_buffer = &tmp_buffer[2];    // 2
    rucrypto_gost_34_10_big_num_t res = {
            .negative = false,
            .buffer = res_buffer,
            .size = 2
    };
    rucrypto_gost_34_10_big_num_underlying_t* vtt1_buffer = &tmp_buffer[4];   // 2
    rucrypto_gost_34_10_big_num_t vtt1 = {
            .negative = false,
            .buffer = vtt1_buffer,
            .size = 2
    };
    rucrypto_gost_34_10_big_num_underlying_t* uii1i2_buffer = &tmp_buffer[6]; // 3
    rucrypto_gost_34_10_big_num_t uii1i2 = {
            .negative = false,
            .buffer = uii1i2_buffer,
            .size = 3
    };
    rucrypto_gost_34_10_big_num_underlying_t* tmp3_buffer = &tmp_buffer[9];   // 3
    rucrypto_gost_34_10_big_num_t tmp3 = {
            .negative = false,
            .buffer = tmp3_buffer,
            .size = 3
    };

    for (rucrypto_size_t i = size - n; i < size - t; ++i) {
        if (u_copy.buffer[i] == v->buffer[size - t]) {
            q->buffer[i + t] = RUCRYPTO_GOST_34_10_BIG_NUM_UNDERLYING_TYPE_MAX_VALUE;
        } else {
            uii1_buffer[0] = u_copy.buffer[i];
            uii1_buffer[1] = u_copy.buffer[i + 1];
            rucrypto_gost_34_10_div_1(&uii1, v->buffer[size - t], &res, &res_buffer[0]);
            q->buffer[i + t] = res_buffer[1];
        }

        vtt1_buffer[0] = v->buffer[size - t];
        vtt1_buffer[1] = v->buffer[size - t + 1];

        uii1i2_buffer[0] = u_copy.buffer[i];
        uii1i2_buffer[1] = u_copy.buffer[i + 1];
        uii1i2_buffer[2] = u_copy.buffer[i + 2];

        rucrypto_gost_34_10_mul_1(&vtt1, q->buffer[i + t], &tmp3);
        while (rucrypto_gost_34_10_is_bigger(&tmp3, &uii1i2)) {
            q->buffer[i + t] -= 1;
            rucrypto_gost_34_10_mul_1(&vtt1, q->buffer[i + t], &tmp3);
        }

        rucrypto_gost_34_10_shift_right(&vnt, 1);
        rucrypto_gost_34_10_big_num_fill_0(&tmp);

        rucrypto_gost_34_10_mul_1(&vnt, q->buffer[i + t], &tmp);
        rucrypto_gost_34_10_sub(&u_copy, &fake_tmp, &u_copy);
        if (u_copy.negative) {
            u_copy.negative = false;
            rucrypto_gost_34_10_sub(&vnt, &u_copy, &u_copy);
            q->buffer[i + t] -= 1;
        }
    }
    RUCRYPTO_MEMCPY(r->buffer, u_copy.buffer, size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));

    if (secure) {
        rucrypto_secure_zero(buffer, (3 * size + 1) * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
        rucrypto_secure_zero(tmp_buffer, 12 * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    }

    rucrypto_deallocate(buffer);

    return rucrypto_result_ok;
}

void rucrypto_gost_34_10_signed_sub(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* w) {
    const rucrypto_gost_34_10_big_num_t u_positive = {
        .negative = false,
        .buffer = u->buffer,
        .size = u->size
    };
    const rucrypto_gost_34_10_big_num_t v_positive = {
        .negative = false,
        .buffer = v->buffer,
        .size = v->size
    };

    if (u->negative != v->negative) {
        bool saved_sign = u->negative;
        rucrypto_gost_34_10_add_no_carry(&u_positive, &v_positive, w);
        w->negative = saved_sign;
    } else {
        bool saved_sign = u->negative;
        rucrypto_gost_34_10_sub(&u_positive, &v_positive, w);
        if (saved_sign) {
            w->negative = !w->negative;
        }
    }
}

void rucrypto_gost_34_10_signed_add(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* w) {
    const rucrypto_gost_34_10_big_num_t u_positive = {
        .negative = false,
        .buffer = u->buffer,
        .size = u->size
    };
    const rucrypto_gost_34_10_big_num_t v_positive = {
        .negative = false,
        .buffer = v->buffer,
        .size = v->size
    };

    if (u->negative == v->negative) {
         rucrypto_gost_34_10_add_no_carry(&u_positive, &v_positive, w);
        w->negative = u->negative;
    } else {
        const rucrypto_gost_34_10_big_num_t* negative_number;
        const rucrypto_gost_34_10_big_num_t* positive_number;

        if (u->negative) {
            negative_number = &u_positive;
            positive_number = &v_positive;
        } else {
            negative_number = &v_positive;
            positive_number = &u_positive;
        }
        rucrypto_gost_34_10_sub(positive_number, negative_number, w);
    }
}

rucrypto_result_t rucrypto_gost_34_10_extended_gcd(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b,
        rucrypto_gost_34_10_big_num_t* d,
        rucrypto_gost_34_10_big_num_t* u,
        rucrypto_gost_34_10_big_num_t* v,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(a->size == b->size);
    RUCRYPTO_ASSERT(a->size == d->size);
    RUCRYPTO_ASSERT(a->size == u->size);
    RUCRYPTO_ASSERT(a->size == v->size);
    rucrypto_size_t size = a->size;
    rucrypto_size_t g = 0;

    rucrypto_gost_34_10_big_num_underlying_t* buffer;
    buffer = rucrypto_allocate_zeroed(8 * (size + 1), sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if (buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }

    rucrypto_gost_34_10_big_num_t a_copy, b_copy, A, B, C, D, rem, u_tmp;
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 0 * (size + 1), &A, size + 1);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 1 * (size + 1), &B, size + 1);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 2 * (size + 1), &C, size + 1);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 3 * (size + 1), &D, size + 1);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 4 * (size + 1), &rem, size + 1);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 5 * (size + 1), &a_copy, size + 1);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 6 * (size + 1), &b_copy, size + 1);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 7 * (size + 1), &u_tmp, size + 1);

    RUCRYPTO_MEMCPY(&a_copy.buffer[1], a->buffer, size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    RUCRYPTO_MEMCPY(&b_copy.buffer[1], b->buffer, size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));

    while (!((a_copy.buffer[size] | b_copy.buffer[size]) & 0x1)) {
        rucrypto_gost_34_10_div_by_2(&a_copy);
        rucrypto_gost_34_10_div_by_2(&b_copy);
        ++g;
    }

    RUCRYPTO_MEMCPY(u_tmp.buffer, a_copy.buffer, (size + 1) * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    RUCRYPTO_MEMCPY(rem.buffer, b_copy.buffer, (size + 1) * sizeof(rucrypto_gost_34_10_big_num_underlying_t));

    A.buffer[size] = 1;
    // B.buffer[size - 1] = 0;
    // C.buffer[size - 1] = 0;
    D.buffer[size] = 1;
    do {
        while (!(u_tmp.buffer[size] & 0x1)) {
            rucrypto_gost_34_10_div_by_2(&u_tmp);
            if (!((A.buffer[size] | B.buffer[size]) & 0x1)) {
                rucrypto_gost_34_10_div_by_2(&A);
                rucrypto_gost_34_10_div_by_2(&B);
            } else {
                rucrypto_gost_34_10_signed_add(&A, &b_copy, &A);
                rucrypto_gost_34_10_div_by_2(&A);
                rucrypto_gost_34_10_signed_sub(&B, &a_copy, &B);
                rucrypto_gost_34_10_div_by_2(&B);
            }
        }

        while (!(rem.buffer[size] & 0x1)) {
            rucrypto_gost_34_10_div_by_2(&rem);
            if (!((C.buffer[size] | D.buffer[size]) & 0x1)) {
                rucrypto_gost_34_10_div_by_2(&C);
                rucrypto_gost_34_10_div_by_2(&D);
            } else {
                rucrypto_gost_34_10_signed_add(&C, &b_copy, &C);
                rucrypto_gost_34_10_div_by_2(&C);
                rucrypto_gost_34_10_signed_sub(&D, &a_copy, &D);
                rucrypto_gost_34_10_div_by_2(&D);
            }
        }

        if (rucrypto_gost_34_10_is_bigger_or_equal(&u_tmp, &rem)) {
            rucrypto_gost_34_10_signed_sub(&u_tmp, &rem, &u_tmp);
            rucrypto_gost_34_10_signed_sub(&A, &C, &A);
            rucrypto_gost_34_10_signed_sub(&B, &D, &B);
        } else {
            rucrypto_gost_34_10_signed_sub(&rem, &u_tmp, &rem);
            rucrypto_gost_34_10_signed_sub(&C, &A, &C);
            rucrypto_gost_34_10_signed_sub(&D, &B, &D);
        }
    } while (!rucrypto_gost_34_10_is_zero(&u_tmp));

    rucrypto_gost_34_10_bit_shift_left(&rem, g);

    RUCRYPTO_MEMCPY(u->buffer, &C.buffer[1], size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    u->negative = C.negative;
    RUCRYPTO_MEMCPY(v->buffer, &D.buffer[1], size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    v->negative = D.negative;
    RUCRYPTO_MEMCPY(d->buffer, &rem.buffer[1], size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    d->negative = rem.negative;

    if (secure) {
        rucrypto_secure_zero(buffer, 8 * (size + 1) * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    }

    rucrypto_deallocate(buffer);

    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_gost_34_10_extended_gcd_no_u(
        const rucrypto_gost_34_10_big_num_t* m,
        const rucrypto_gost_34_10_big_num_t* a,
        rucrypto_gost_34_10_big_num_t* d,
        rucrypto_gost_34_10_big_num_t* v,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(m->size == a->size);
    RUCRYPTO_ASSERT(m->size == d->size);
    RUCRYPTO_ASSERT(m->size == v->size);
    RUCRYPTO_ASSERT(m->buffer[m->size - 1] & 0x1);
    rucrypto_size_t size = m->size;

    rucrypto_gost_34_10_big_num_underlying_t* buffer;
    buffer = rucrypto_allocate_zeroed(5 * (size + 1), sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if (buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }

    rucrypto_gost_34_10_big_num_t B, D, rem, m_copy, u_tmp;
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 0 * (size + 1), &B, size + 1);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 1 * (size + 1), &D, size + 1);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 2 * (size + 1), &rem, size + 1);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 3 * (size + 1), &m_copy, size + 1);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 4 * (size + 1), &u_tmp, size + 1);

    RUCRYPTO_MEMCPY(&m_copy.buffer[1], m->buffer, size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    RUCRYPTO_MEMCPY(&u_tmp.buffer[1], m->buffer, size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    RUCRYPTO_MEMCPY(&rem.buffer[1], a->buffer, size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));

    // B.buffer[size - 1] = 0;
    D.buffer[size] = 1;
    do {
        while (!(u_tmp.buffer[size] & 0x1)) {
            rucrypto_gost_34_10_div_by_2(&u_tmp);
            if (!(B.buffer[size] & 0x1)) {
                rucrypto_gost_34_10_div_by_2(&B);
            } else {
                rucrypto_gost_34_10_signed_sub(&B, &m_copy, &B);
                rucrypto_gost_34_10_div_by_2(&B);
            }
        }

        while (!(rem.buffer[size] & 0x1)) {
            rucrypto_gost_34_10_div_by_2(&rem);
            if (!(D.buffer[size] & 0x1)) {
                rucrypto_gost_34_10_div_by_2(&D);
            } else {
                rucrypto_gost_34_10_signed_sub(&D, &m_copy, &D);
                rucrypto_gost_34_10_div_by_2(&D);
            }
        }

        if (rucrypto_gost_34_10_is_bigger_or_equal(&u_tmp, &rem)) {
            rucrypto_gost_34_10_signed_sub(&u_tmp, &rem, &u_tmp);
            rucrypto_gost_34_10_signed_sub(&B, &D, &B);
        } else {
            rucrypto_gost_34_10_signed_sub(&rem, &u_tmp, &rem);
            rucrypto_gost_34_10_signed_sub(&D, &B, &D);
        }
    } while (!rucrypto_gost_34_10_is_zero(&u_tmp));

    RUCRYPTO_MEMCPY(v->buffer, &D.buffer[1], size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    v->negative = D.negative;
    RUCRYPTO_MEMCPY(d->buffer, &rem.buffer[1], size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    d->negative = rem.negative;

    if (secure) {
        rucrypto_secure_zero(buffer, 5 * (size + 1) * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    }

    rucrypto_deallocate(buffer);

    return rucrypto_result_ok;
}
