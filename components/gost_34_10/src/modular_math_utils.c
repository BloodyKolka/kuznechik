#include <rucrypto/gost_34_10/modular_math_utils.h>

#include <rucrypto/common/secure_zero.h>

rucrypto_result_t rucrypto_gost_34_10_rebase(
        const rucrypto_gost_34_10_big_num_t* a,
        rucrypto_gost_34_10_big_num_t* res,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(!a->negative);
    RUCRYPTO_ASSERT(!base->negative);
    RUCRYPTO_ASSERT(res->size == base->size);
    RUCRYPTO_ASSERT(a->size >= base->size);
    rucrypto_size_t size = base->size;

    if (a->size == base->size) {
        if (rucrypto_gost_34_10_is_bigger_or_equal(a, base)) {
            rucrypto_gost_34_10_big_num_t tmp;
            rucrypto_result_t status = rucrypto_gost_34_10_big_num_init_0(&tmp, size);
            if (status != rucrypto_result_ok) {
                return status;
            }
            status = rucrypto_gost_34_10_div(a, base, &tmp, res, secure);
            rucrypto_gost_34_10_big_num_fini(&tmp, secure);
            return status;
        } else {
            if (a != res) {
                rucrypto_gost_34_10_big_num_copy(a, res);
            }
            return rucrypto_result_ok;
        }
    }

    rucrypto_gost_34_10_big_num_underlying_t* buffer;
    buffer = rucrypto_allocate_zeroed(3 * a->size, sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if (buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    rucrypto_result_t status = rucrypto_result_ok;
    rucrypto_gost_34_10_big_num_t tmp1, tmp2, tmp3;
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 0 * a->size, &tmp1, a->size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 1 * a->size, &tmp2, a->size);
    rucrypto_gost_34_10_big_num_init_no_alloc(buffer, 2 * a->size, &tmp3, a->size);

    RUCRYPTO_MEMSET(tmp1.buffer, 0, (a->size - size) * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    RUCRYPTO_MEMCPY(&tmp1.buffer[a->size - size], base->buffer, size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    if (rucrypto_gost_34_10_is_bigger_or_equal(a, &tmp1)) {
        status = rucrypto_gost_34_10_div(a, &tmp1, &tmp2, &tmp3, secure);
        RUCRYPTO_MEMCPY(res->buffer, &tmp3.buffer[a->size - size], size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    } else {
        RUCRYPTO_MEMCPY(res->buffer, &a->buffer[a->size - size], size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    }

    if (secure) {
        rucrypto_secure_zero(buffer, 3 * a->size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    }
    rucrypto_deallocate(buffer);
    return status;
}

rucrypto_result_t rucrypto_gost_34_10_add_based(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b,
        rucrypto_gost_34_10_big_num_t* res,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(a->size == b->size);
    RUCRYPTO_ASSERT(a->size == base->size);
    RUCRYPTO_ASSERT(a->size == res->size);
    rucrypto_size_t size = a->size;

    rucrypto_gost_34_10_big_num_t tmp;
    rucrypto_result_t status = rucrypto_gost_34_10_big_num_init_0(&tmp, size + 1);
    if (status != rucrypto_result_ok) {
        return status;
    }

    rucrypto_gost_34_10_add(a, b, &tmp);
    status = rucrypto_gost_34_10_rebase(&tmp, res, base, secure);

    rucrypto_gost_34_10_big_num_fini(&tmp, secure);
    return status;
}

void rucrypto_gost_34_10_sub_based(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b,
        rucrypto_gost_34_10_big_num_t* res,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(a->size == b->size);
    RUCRYPTO_ASSERT(a->size == base->size);
    RUCRYPTO_ASSERT(a->size == res->size);

    /*
     * Clearing is not required, because no internal buffers are used
     */
    (void)secure;

    rucrypto_gost_34_10_sub(a, b, res);
    if (res->negative) {
        res->negative = false;
        rucrypto_gost_34_10_sub(base, res, res);
    }
}

rucrypto_result_t rucrypto_gost_34_10_mul_inv(
        const rucrypto_gost_34_10_big_num_t* a,
        rucrypto_gost_34_10_big_num_t* invA,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(base->buffer[base->size - 1] & 0x1);
    RUCRYPTO_ASSERT(a->size == invA->size);
    RUCRYPTO_ASSERT(a->size == base->size);

    rucrypto_gost_34_10_big_num_t tmp1;
    rucrypto_result_t status = rucrypto_gost_34_10_big_num_init_0(&tmp1, a->size);
    if (status != rucrypto_result_ok) {
        return status;
    }

    status = rucrypto_gost_34_10_extended_gcd_no_u(base, a, &tmp1, invA, secure);

    while (invA->negative) {
        invA->negative = false;
        rucrypto_gost_34_10_sub(base, invA, invA);
    }

    rucrypto_gost_34_10_big_num_fini(&tmp1, secure);
    return status;
}

rucrypto_result_t rucrypto_gost_34_10_mul_1_based(
        const rucrypto_gost_34_10_big_num_t* a,
        rucrypto_gost_34_10_big_num_underlying_t b,
        rucrypto_gost_34_10_big_num_t* res,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(a->size == base->size);
    RUCRYPTO_ASSERT(a->size == res->size);
    rucrypto_size_t size = a->size;

    rucrypto_gost_34_10_big_num_t tmp;
    rucrypto_result_t status = rucrypto_gost_34_10_big_num_init_0(&tmp, size + 1);
    if (status != rucrypto_result_ok) {
        return status;
    }

    rucrypto_gost_34_10_mul_1(a, b, &tmp);
    status = rucrypto_gost_34_10_rebase(&tmp, res, base, secure);

    rucrypto_gost_34_10_big_num_fini(&tmp, secure);
    return status;
}

rucrypto_result_t rucrypto_gost_34_10_mul_by_2_based(
        rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(a->size == base->size);
    rucrypto_size_t size = a->size;

#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    if (!(a->buffer[0] & 0x8000000000000000)) {
#else
    if (!(a->buffer[0] & 0x80000000)) {
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
        rucrypto_gost_34_10_mul_by_2(a);
        return rucrypto_gost_34_10_rebase(a, a, base, secure);
    }

    rucrypto_gost_34_10_big_num_t tmp;
    rucrypto_result_t status = rucrypto_gost_34_10_big_num_init_0(&tmp, size + 1);
    if (status != rucrypto_result_ok) {
        return status;
    }
    RUCRYPTO_MEMCPY(&tmp.buffer[1], a->buffer, size);

    rucrypto_gost_34_10_mul_by_2(&tmp);
    status = rucrypto_gost_34_10_rebase(&tmp, a, base, secure);

    rucrypto_gost_34_10_big_num_fini(&tmp, secure);
    return status;
}

rucrypto_result_t rucrypto_gost_34_10_mul_based(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b,
        rucrypto_gost_34_10_big_num_t* res,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(a->size == b->size);
    RUCRYPTO_ASSERT(a->size == base->size);
    RUCRYPTO_ASSERT(a->size == res->size);
    rucrypto_size_t size = a->size;

    rucrypto_gost_34_10_big_num_t tmp;
    rucrypto_result_t status = rucrypto_gost_34_10_big_num_init_0(&tmp, size * 2);
    if (status != rucrypto_result_ok) {
        return status;
    }

    rucrypto_gost_34_10_mul(a, b, &tmp);
    status = rucrypto_gost_34_10_rebase(&tmp, res, base, secure);

    rucrypto_gost_34_10_big_num_fini(&tmp, secure);
    return status;
}

rucrypto_result_t rucrypto_gost_34_10_div_based(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b,
        rucrypto_gost_34_10_big_num_t* res,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(a->size == b->size);
    RUCRYPTO_ASSERT(a->size == base->size);
    RUCRYPTO_ASSERT(a->size == res->size);
    rucrypto_size_t size = a->size;

    rucrypto_gost_34_10_big_num_t tmp;
    rucrypto_result_t status = rucrypto_gost_34_10_big_num_init_0(&tmp, size);
    if (status != rucrypto_result_ok) {
        return status;
    }

    status = rucrypto_gost_34_10_mul_inv(b, &tmp, base, secure);
    if (status != rucrypto_result_ok) {
        return status;
    }
    status = rucrypto_gost_34_10_mul_based(a, &tmp, res, base, secure);

    rucrypto_gost_34_10_big_num_fini(&tmp, secure);
    return status;
}
