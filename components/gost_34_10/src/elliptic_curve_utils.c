#include <rucrypto/gost_34_10/elliptic_curve_utils.h>

#include <rucrypto/common/secure_zero.h>

static rucrypto_result_t rucrypto_gost_34_10_add_points_not_equal(const rucrypto_gost_34_10_elliptic_curve_t* curve, const rucrypto_gost_34_10_point_t* a, const rucrypto_gost_34_10_point_t* b, rucrypto_gost_34_10_point_t* res, rucrypto_bool_t secure) {
    rucrypto_gost_34_10_big_num_t lambda = {
        .negative = false,
        .buffer = rucrypto_allocate(curve->size / 8),
        .size = curve->size / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8)
    };
    if (lambda.buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }

    rucrypto_result_t status;
    // x3 = x2 - x1
    rucrypto_gost_34_10_sub_based(&b->x, &a->x, &res->x, &curve->p, secure);
    // lambda = y2 - y1
    rucrypto_gost_34_10_sub_based(&b->y, &a->y, &lambda, &curve->p, secure);
    // lambda = (y2 - y1) / (x2 - x1)
    status = rucrypto_gost_34_10_div_based(&lambda, &res->x, &lambda, &curve->p, secure);
    if (status != rucrypto_result_ok) {
        return status;
    }

    // x3 = lambda^2
    status = rucrypto_gost_34_10_mul_based(&lambda, &lambda, &res->x, &curve->p, secure);
    if (status != rucrypto_result_ok) {
        return status;
    }
    // x3 = lambda^2 - x1
    rucrypto_gost_34_10_sub_based(&res->x, &a->x, &res->x, &curve->p, secure);
    // x3 = lambda^2 - x1 - x2
    rucrypto_gost_34_10_sub_based(&res->x, &b->x, &res->x, &curve->p, secure);

    // y3 = x1
    rucrypto_gost_34_10_big_num_copy(&a->x, &res->y);
    // y3 = x1 - x3
    rucrypto_gost_34_10_sub_based(&res->y, &res->x, &res->y, &curve->p, secure);
    // y3 = lambda(x1 - x3)
    status = rucrypto_gost_34_10_mul_based(&res->y, &lambda, &res->y, &curve->p, secure);
    if (status != rucrypto_result_ok) {
        return status;
    }
    // y3 = lambda(x1 - x3) - y1
    rucrypto_gost_34_10_sub_based(&res->y, &a->y, &res->y, &curve->p, secure);

    if (secure) {
        rucrypto_secure_zero(lambda.buffer, curve->size / 8);
    }

    rucrypto_deallocate(lambda.buffer);

    return rucrypto_result_ok;
}

static rucrypto_result_t rucrypto_gost_34_10_add_points_equal(const rucrypto_gost_34_10_elliptic_curve_t* curve, const rucrypto_gost_34_10_point_t* a, rucrypto_gost_34_10_point_t* res, rucrypto_bool_t secure) {
    rucrypto_gost_34_10_big_num_t lambda = {
        .negative = false,
        .buffer = rucrypto_allocate(curve->size / 8),
        .size = curve->size / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8)
    };
    if (lambda.buffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }

    rucrypto_result_t status;
    // y3 = y1
    RUCRYPTO_MEMCPY(res->y.buffer, a->y.buffer, curve->size / 8);
    // y3 = 2 * y1
    status = rucrypto_gost_34_10_mul_by_2_based(&res->y, &curve->p, secure);
    if (status != rucrypto_result_ok) {
        return status;
    }

    // x3 = x1^2
    status = rucrypto_gost_34_10_mul_based(&a->x, &a->x, &res->x, &curve->p, secure);
    if (status != rucrypto_result_ok) {
        return status;
    }
    // x3 = 3x1^2
    status = rucrypto_gost_34_10_mul_1_based(&res->x, 3, &res->x, &curve->p, secure);
    if (status != rucrypto_result_ok) {
        return status;
    }
    // x3 = 3x1^2 + a
    status = rucrypto_gost_34_10_add_based(&res->x, &curve->a, &res->x, &curve->p, secure);
    if (status != rucrypto_result_ok) {
        return status;
    }
    // lambda = (3x1^2 + a) / (2 * y1)
    status = rucrypto_gost_34_10_div_based(&res->x, &res->y, &lambda, &curve->p, secure);
    if (status != rucrypto_result_ok) {
        return status;
    }

    // x3 = lambda^2
    status = rucrypto_gost_34_10_mul_based(&lambda, &lambda, &res->x, &curve->p, secure);
    if (status != rucrypto_result_ok) {
        return status;
    }
    // x3 = lambda^2 - x1
    rucrypto_gost_34_10_sub_based(&res->x, &a->x, &res->x, &curve->p, secure);
    // x3 = lambda^2 - 2x1
    rucrypto_gost_34_10_sub_based(&res->x, &a->x, &res->x, &curve->p, secure);

    // y3 = x1
    rucrypto_gost_34_10_big_num_copy(&a->x, &res->y);
    // y3 = x1 - x3
    rucrypto_gost_34_10_sub_based(&res->y, &res->x, &res->y, &curve->p, secure);
    // y3 = lambda(x1 - x3)
    status = rucrypto_gost_34_10_mul_based(&res->y, &lambda, &res->y, &curve->p, secure);
    if (status != rucrypto_result_ok) {
        return status;
    }

    // y3 = lambda(x1 - x3) - y1
    rucrypto_gost_34_10_sub_based(&res->y, &a->y, &res->y, &curve->p, secure);

    if (secure) {
        rucrypto_secure_zero(lambda.buffer, curve->size / 8);
    }

    rucrypto_deallocate(lambda.buffer);

    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_gost_34_10_add_points(
        const rucrypto_gost_34_10_elliptic_curve_t* curve,
        const rucrypto_gost_34_10_point_t* a,
        const rucrypto_gost_34_10_point_t* b,
        rucrypto_gost_34_10_point_t* res,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(curve->size / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8) == a->size);
    RUCRYPTO_ASSERT(curve->size / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8) == b->size);
    RUCRYPTO_ASSERT(curve->size / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8) == res->size);

    if (!rucrypto_gost_34_10_are_equal(&a->x, &b->x)) {
        return rucrypto_gost_34_10_add_points_not_equal(curve, a, b, res, secure);
    } else if (rucrypto_gost_34_10_are_equal_and_not_zero(&a->y, &b->y)) {
        return rucrypto_gost_34_10_add_points_equal(curve, a, res, secure);
    }
    return rucrypto_result_invalid_parameters;
}

rucrypto_result_t rucrypto_gost_34_10_calculate_k_point(
        const rucrypto_gost_34_10_elliptic_curve_t* curve,
        const rucrypto_gost_34_10_point_t* p,
        const rucrypto_gost_34_10_big_num_t* k,
        rucrypto_gost_34_10_point_t* res,
        rucrypto_bool_t secure) {
    RUCRYPTO_ASSERT(curve->size / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8) == p->size);
    RUCRYPTO_ASSERT(curve->size / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8) == k->size);
    RUCRYPTO_ASSERT(curve->size / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8) == res->size);
    rucrypto_size_t size = curve->size / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8);

    if (rucrypto_gost_34_10_is_zero(&p->y)) {
        return rucrypto_result_invalid_parameters;
    }

    // Curve size is in bits. We need (curve size in bits) * (curve size in bytes) bytes
    rucrypto_gost_34_10_big_num_underlying_t* bufferX = rucrypto_allocate_zeroed(curve->size + 1, curve->size / 8);
    rucrypto_gost_34_10_big_num_underlying_t* bufferY = rucrypto_allocate_zeroed(curve->size + 1, curve->size / 8);
    if (bufferX == RUCRYPTO_NULL || bufferY == RUCRYPTO_NULL) {
        rucrypto_deallocate(bufferX);
        rucrypto_deallocate(bufferY);
        return rucrypto_result_out_of_memory;
    }

    rucrypto_gost_34_10_point_t previousPoint = {
        .x = {
            .negative = false,
            .buffer = &bufferX[0],
            .size = size
        },
        .y = {
            .negative = false,
            .buffer = &bufferY[0],
            .size = size
        },
        .size = size
    };
    rucrypto_gost_34_10_point_t currentPoint = {
        .x = {
            .negative = false,
            .buffer = &bufferX[size],
            .size = size
        },
        .y = {
            .negative = false,
            .buffer = &bufferY[size],
            .size = size
        },
        .size = size
    };
    rucrypto_gost_34_10_big_num_copy(&p->x, &previousPoint.x);
    rucrypto_gost_34_10_big_num_copy(&p->y, &previousPoint.y);

    rucrypto_result_t status;
    for (rucrypto_size_t i = 1; i < curve->size; ++i) {
        status = rucrypto_gost_34_10_add_points_equal(curve, &previousPoint, &currentPoint, secure);
        if (status != rucrypto_result_ok) {
            if (secure) {
                rucrypto_secure_zero(bufferX, (curve->size + 1) * curve->size / 8);
                rucrypto_secure_zero(bufferY, (curve->size + 1) * curve->size / 8);
            }
            rucrypto_deallocate(bufferX);
            rucrypto_deallocate(bufferY);
            return status;
        }
        previousPoint.x.buffer = currentPoint.x.buffer;
        previousPoint.y.buffer = currentPoint.y.buffer;
        currentPoint.x.buffer = &bufferX[(i + 1) * size];
        currentPoint.y.buffer = &bufferY[(i + 1) * size];
    }

    rucrypto_size_t i = 0;
    for (; i < curve->size; ++i) {
        if (k->buffer[k->size - i / 64 - 1] & (0x1llu << (i % 64))) {
            RUCRYPTO_MEMCPY(currentPoint.x.buffer, &bufferX[i * curve->size / 64], res->x.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
            RUCRYPTO_MEMCPY(currentPoint.y.buffer, &bufferY[i * curve->size / 64], res->y.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
            ++i;
            break;
        }
    }
    for (; i < curve->size; ++i) {
        if (k->buffer[k->size - i / (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8) - 1] & (0x1llu << (i % (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8)))) {
            previousPoint.x.buffer = currentPoint.x.buffer;
            previousPoint.y.buffer = currentPoint.y.buffer;
            currentPoint.x.buffer = &bufferX[i * size];
            currentPoint.y.buffer = &bufferY[i * size];
            status = rucrypto_gost_34_10_add_points(curve, &previousPoint, &currentPoint, res, secure);
            if (status != rucrypto_result_ok) {
                if (secure) {
                    rucrypto_secure_zero(bufferX, (curve->size + 1) * curve->size / 8);
                    rucrypto_secure_zero(bufferY, (curve->size + 1) * curve->size / 8);
                }
                rucrypto_deallocate(bufferX);
                rucrypto_deallocate(bufferY);
                return status;
            }
            RUCRYPTO_MEMCPY(currentPoint.x.buffer, res->x.buffer, res->x.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
            RUCRYPTO_MEMCPY(currentPoint.y.buffer, res->y.buffer, res->y.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
        }
    }

    if (secure) {
        rucrypto_secure_zero(bufferX, (curve->size + 1) * curve->size / 8);
        rucrypto_secure_zero(bufferY, (curve->size + 1) * curve->size / 8);
    }

    rucrypto_deallocate(bufferX);
    rucrypto_deallocate(bufferY);
    return rucrypto_result_ok;
}
