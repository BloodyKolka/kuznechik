/**
 * @file rucrypto/gost_34_10/gost_34_10.h
 */

#ifndef RUCRYPTO_GOST_34_10_GOST_34_10_H
#define RUCRYPTO_GOST_34_10_GOST_34_10_H

#include <rucrypto/common/common.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * @brief Struct to hold information about an elliptic curve
 */
typedef struct rucrypto_gost_34_10_elliptic_curve_t rucrypto_gost_34_10_elliptic_curve_t;

/** 512 bit signature type */
typedef rucrypto_vec512_t rucrypto_gost_34_10_signature_512_t;
/** 1024 bit signature type */
typedef rucrypto_vec1024_t rucrypto_gost_34_10_signature_1024_t;

/** Random number generator function type */
typedef rucrypto_uint8_t (*rucrypto_gost_34_10_random_generator_func_t)(void);

/**
 * @brief Initialize structure rucrypto_gost_34_10_elliptic_curve_t with
 *        information about the used elliptic curve
 * @param [in, out] curve variable to hold result
 * @param [in] p Prime big number, elliptic curve modulo
 * @param [in] a Coefficient a of the elliptic curve
 * @param [in] b Coefficient b of the elliptic curve
 * @param [in] m Order of the group with elliptic curve points
 * @param [in] q Order of the cyclic group with elliptic curve points such that
 *               m = nq (n >= 1), 2^254 < q < 2^256
 * @param [in] Px X coordinate of point P such that P != O and qP = 0
 * @param [in] Py Y coordinate of point P such that P != O and qP = 0
 * @return rucrypto_result_ok on success, error code otherwise
 */
extern rucrypto_result_t rucrypto_gost_34_10_elliptic_curve_init_256(
        rucrypto_gost_34_10_elliptic_curve_t** curve,
        const rucrypto_vec256_t p,
        const rucrypto_vec256_t a,
        const rucrypto_vec256_t b,
        const rucrypto_vec256_t m,
        const rucrypto_vec256_t q,
        const rucrypto_vec256_t Px,
        const rucrypto_vec256_t Py);

/**
 * @brief Initialize structure rucrypto_gost_34_10_elliptic_curve_t with
 *        information about the used elliptic curve
 * @param [in, out] curve variable to hold result
 * @param [in] p Prime big number, elliptic curve modulo
 * @param [in] a Coefficient a of the elliptic curve
 * @param [in] b Coefficient b of the elliptic curve
 * @param [in] m Order of the group with elliptic curve points
 * @param [in] q Order of the cyclic group with elliptic curve points such that
 *               m = nq (n >= 1), 2^508 < q < 2^512
 * @param [in] Px X coordinate of point P such that P != O and qP = 0
 * @param [in] Py Y coordinate of point P such that P != O and qP = 0
 * @return rucrypto_result_ok on success, error code otherwise
 */
extern rucrypto_result_t rucrypto_gost_34_10_elliptic_curve_init_512(
        rucrypto_gost_34_10_elliptic_curve_t** curve,
        const rucrypto_vec512_t p,
        const rucrypto_vec512_t a,
        const rucrypto_vec512_t b,
        const rucrypto_vec512_t m,
        const rucrypto_vec512_t q,
        const rucrypto_vec512_t Px,
        const rucrypto_vec512_t Py);

/**
 * @brief Generate 512-bit signature
 * @param [in] curve Used elliptic curve, iniatilized with
 *                   rucrypto_gost_34_10_elliptic_curve_init_256
 * @param [in] hash Hash of the message needed to sign
 * @param [in] privateKey Private key, used for signing
 * @param [in] random Function that generates random bytes
 * @param [in, out] signature Buffer to hold resulting signature
 * @return rucrypto_result_ok on success, error code otherwise
 */
extern rucrypto_result_t rucrypto_gost_34_10_generate_signature_512(
        const rucrypto_gost_34_10_elliptic_curve_t* curve,
        const rucrypto_vec256_t hash,
        const rucrypto_vec256_t privateKey,
        rucrypto_gost_34_10_random_generator_func_t random,
        rucrypto_gost_34_10_signature_512_t signature);

/**
 * @brief Verify 512-bit signature
 * @param [in] curve Used elliptic curve, iniatilized with
 *                   rucrypto_gost_34_10_elliptic_curve_init_256
 * @param [in] hash Hash of the signed message
 * @param [in] publicKey Public key (x and y coordinates, concatenated together)
 * @param [in] signature Signature to verify
 * @return Verdict whether or not signature is accepted, or error code
 * @retval rucrypto_result_accepted if signature is accepted
 * @retval rucrypto_result_not_accepted if signature is not accepted
 */
extern rucrypto_result_t rucrypto_gost_34_10_verify_signature_512(
        const rucrypto_gost_34_10_elliptic_curve_t* curve,
        const rucrypto_vec256_t hash,
        const rucrypto_vec512_t publicKey,
        const rucrypto_gost_34_10_signature_512_t signature);

/**
 * @brief Generate 1024-bit signature
 * @param curve Used elliptic curve, iniatilized with
 *              rucrypto_gost_34_10_elliptic_curve_init_512
 * @param hash Hash of the message needed to sign
 * @param privateKey Private key, used for signing
 * @param random Function that generates random bytes
 * @param signature Buffer to hold resulting signature
 * @return rucrypto_result_ok on success, error code otherwise
 */
extern rucrypto_result_t rucrypto_gost_34_10_generate_signature_1024(
        const rucrypto_gost_34_10_elliptic_curve_t* curve,
        const rucrypto_vec512_t hash,
        const rucrypto_vec512_t privateKey,
        rucrypto_gost_34_10_random_generator_func_t random,
        rucrypto_gost_34_10_signature_1024_t signature);

/**
 * @brief Verify 1024-bit signature
 * @param curve Used elliptic curve, iniatilized with
 *              rucrypto_gost_34_10_elliptic_curve_init_512
 * @param hash Hash of the signed message
 * @param publicKey Public key (x and y coordinates, concatenated together)
 * @param signature Signature to verify
 * @return Verdict whether or not signature is accepted, or error code
 * @retval rucrypto_result_accepted if signature is accepted
 * @retval rucrypto_result_not_accepted if signature is not accepted
 */
extern rucrypto_result_t rucrypto_gost_34_10_verify_signature_1024(
        const rucrypto_gost_34_10_elliptic_curve_t* curve,
        const rucrypto_vec512_t hash,
        const rucrypto_vec1024_t publicKey,
        const rucrypto_gost_34_10_signature_1024_t signature);

/**
 * @brief De-initialize structure rucrypto_gost_34_10_elliptic_curve_t, previously
 *        initialized with rucrypto_gost_34_10_elliptic_curve_init_256 or
 *        rucrypto_gost_34_10_elliptic_curve_init_512
 * @param curve variable with structure to de-initialize
 */
extern void rucrypto_gost_34_10_elliptic_curve_fini(rucrypto_gost_34_10_elliptic_curve_t** curve);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_GOST_34_10_GOST_34_10_H
