extern "C" {
#include "../private_include/rucrypto/gost_34_10/modular_math_utils.h"
}

#include "testing_utils.h"

#include <gtest/gtest.h>

TEST(Gost3410ModularMath, RebaseTest) {
    //rucrypto_result_t rucrypto_gost_34_10_rebase(const rucrypto_gost_34_10_big_num_t* a, rucrypto_gost_34_10_big_num_t* res, const rucrypto_gost_34_10_big_num_t* base);
}

TEST(Gost3410ModularMath, AddBasedTest) {
    constexpr size_t size = 128 / (8 * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0071b199aea56570, 0xdaed07c901d92418
#else
      0x0071b199, 0xaea56570, 0xdaed07c9, 0x01d92418
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0015742c79ea54f1, 0x5555f5c8346f175a
#else
      0x0015742c, 0x79ea54f1, 0x5555f5c8, 0x346f175a
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x009343a252c7a465, 0xd39a2599513cb4de
#else
      0x009343a2, 0x52c7a465, 0xd39a2599, 0x513cb4de
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x008725c6288fba62, 0x3042fd9136483b72
#else
      0x008725c6, 0x288fba62, 0x3042fd91, 0x36483b72
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x005b8868146dc1e8, 0xbcf8976be4883126
#else
      0x005b8868, 0x146dc1e8, 0xbcf8976b, 0xe4883126
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x009633ca9c61ebb2, 0xf9a70741ee3beace
#else
      0x009633ca, 0x9c61ebb2, 0xf9a70741, 0xee3beace
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00e5ec374858595d, 0x3f71857133034f16
#else
      0x00e5ec37, 0x4858595d, 0x3f718571, 0x33034f16
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x000bcffb6877543e, 0x772e193c9fc0ccde
#else
      0x000bcffb, 0x6877543e, 0x772e193c, 0x9fc0ccde
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t resBuffer[size];
    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t y = {
        .negative = false,
        .buffer = y1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t base = {
        .negative = false,
        .buffer = base1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = size
    };
    rucrypto_gost_34_10_add_based(&x, &y, &res, &base, false);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expected1Buffer, size));
    x.buffer = x2Buffer;
    y.buffer = y2Buffer;
    base.buffer = base2Buffer;
    rucrypto_gost_34_10_add_based(&x, &y, &res, &base, false);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expected2Buffer, size));
}

TEST(Gost3410ModularMath, SubBasedTest) {
    constexpr size_t size = 128 / (8 * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0071b199aea56570, 0xdaed07c901d92418
#else
      0x0071b199, 0xaea56570, 0xdaed07c9, 0x01d92418
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0015742c79ea54f1, 0x5555f5c8346f175a
#else
      0x0015742c, 0x79ea54f1, 0x5555f5c8, 0x346f175a
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x009343a252c7a465, 0xd39a2599513cb4de
#else
      0x009343a2, 0x52c7a465, 0xd39a2599, 0x513cb4de
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x005c3d6d34bb107f, 0x85971200cd6a0cbe
#else
      0x005c3d6d, 0x34bb107f, 0x85971200, 0xcd6a0cbe
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x005b8868146dc1e8, 0xbcf8976be4883126
#else
      0x005b8868, 0x146dc1e8, 0xbcf8976b, 0xe4883126
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x009633ca9c61ebb2, 0xf9a70741ee3beace
#else
      0x009633ca, 0x9c61ebb2, 0xf9a70741, 0xee3beace
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00e5ec374858595d, 0x3f71857133034f16
#else
      0x00e5ec37, 0x4858595d, 0x3f718571, 0x33034f16
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00ab40d4c0642f93, 0x02c3159b294f956e
#else
      0x00ab40d4, 0xc0642f93, 0x02c3159b, 0x294f956e
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t resBuffer[size];
    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t y = {
        .negative = false,
        .buffer = y1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t base = {
        .negative = false,
        .buffer = base1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = size
    };
    rucrypto_gost_34_10_sub_based(&x, &y, &res, &base, false);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expected1Buffer, size));
    x.buffer = x2Buffer;
    y.buffer = y2Buffer;
    base.buffer = base2Buffer;
    rucrypto_gost_34_10_sub_based(&x, &y, &res, &base, false);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expected2Buffer, size));
}

TEST(Gost3410ModularMath, MulInvTest) {
    constexpr size_t size = 128 / (8 * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    rucrypto_gost_34_10_big_num_underlying_t y1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00d2c235f6b2ef87, 0xc8887bb3941937ed
#else
      0x00d2c235, 0xf6b2ef87, 0xc8887bb3, 0x941937ed
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00dca164d2ead0af, 0xba7909607074c087
#else
      0x00dca164, 0xd2ead0af, 0xba790960, 0x7074c087
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00bb06ac6def501c, 0xa00273b763ace9c0
#else
      0x00bb06ac, 0x6def501c, 0xa00273b7, 0x63ace9c0
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00672bf392d40c81, 0xc298216fac85a83b
#else
      0x00672bf3, 0x92d40c81, 0xc298216f, 0xac85a83b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x006c3e73428c0d15, 0xe124156edcd3912d
#else
      0x006c3e73, 0x428c0d15, 0xe124156e, 0xdcd3912d
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0034c1c24378d315, 0x3997a8b894682d21
#else
      0x0034c1c2, 0x4378d315, 0x3997a8b8, 0x94682d21
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t resBuffer[size];
    rucrypto_gost_34_10_big_num_t y = {
        .negative = false,
        .buffer = y1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t base = {
        .negative = false,
        .buffer = base1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = size
    };
    rucrypto_gost_34_10_mul_inv(&y, &res, &base, false);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expected1Buffer, size));
    y.buffer = y2Buffer;
    base.buffer = base2Buffer;
    rucrypto_gost_34_10_mul_inv(&y, &res, &base, false);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expected2Buffer, size));
}

TEST(Gost3410ModularMath, MulBy2BasedTest) {
    constexpr size_t size = 128 / (8 * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0071b199aea56570, 0xdaed07c901d92418
#else
      0x0071b199, 0xaea56570, 0xdaed07c9, 0x01d92418
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x009343a252c7a465, 0xd39a2599513cb4de
#else
      0x009343a2, 0x52c7a465, 0xd39a2599, 0x513cb4de
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00501f910a83267b, 0xe23fe9f8b2759352
#else
      0x00501f91, 0x0a83267b, 0xe23fe9f8, 0xb2759352
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x005b8868146dc1e8, 0xbcf8976be4883126
#else
      0x005b8868, 0x146dc1e8, 0xbcf8976b, 0xe4883126
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00e5ec374858595d, 0x3f71857133034f16
#else
      0x00e5ec37, 0x4858595d, 0x3f718571, 0x33034f16
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00b710d028db83d1, 0x79f12ed7c910624c
#else
      0x00b710d0, 0x28db83d1, 0x79f12ed7, 0xc910624c
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t base = {
        .negative = false,
        .buffer = base1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_mul_by_2_based(&x, &base, false);
    EXPECT_TRUE(AreBuffersEqual(x.buffer, expected1Buffer, size));
    x.buffer = x2Buffer;
    base.buffer = base2Buffer;
    rucrypto_gost_34_10_mul_by_2_based(&x, &base, false);
    EXPECT_TRUE(AreBuffersEqual(x.buffer, expected2Buffer, size));
}

TEST(Gost3410ModularMath, Mul1BasedTest) {
    constexpr size_t size = 128 / (8 * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0071b199aea56570, 0xdaed07c901d92418
#else
      0x0071b199, 0xaea56570, 0xdaed07c9, 0x01d92418
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x65ee7863efea0936
#else
      0x912353de
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x009343a252c7a465, 0xd39a2599513cb4de
#else
      0x009343a2, 0x52c7a465, 0xd39a2599, 0x513cb4de
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00366b7c1ed7b16f, 0x383427b0cc2f7398
#else
      0x002bde64, 0xa77245ef, 0x05459850, 0xbcdb9fd6
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x005b8868146dc1e8, 0xbcf8976be4883126
#else
      0x005b8868, 0x146dc1e8, 0xbcf8976b, 0xe4883126
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbac5de3834d3b5a4
#else
      0x6478f5e9
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00e5ec374858595d, 0x3f71857133034f16
#else
      0x00e5ec37, 0x4858595d, 0x3f718571, 0x33034f16
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0000132f1df61dd6, 0xad5d391ab50a31ca
#else
      0x008a1fd4, 0x368ce4d8, 0x0709cd20, 0x29ecd704
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t resBuffer[size];
    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t base = {
        .negative = false,
        .buffer = base1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = size
    };
    rucrypto_gost_34_10_mul_1_based(&x, y1, &res, &base, false);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expected1Buffer, size));
    x.buffer = x2Buffer;
    base.buffer = base2Buffer;
    rucrypto_gost_34_10_mul_1_based(&x, y2, &res, &base, false);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expected2Buffer, size));
}

TEST(Gost3410ModularMath, MulBasedTest) {
    constexpr size_t size = 128 / (8 * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0071b199aea56570, 0xdaed07c901d92418
#else
      0x0071b199, 0xaea56570, 0xdaed07c9, 0x01d92418
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0015742c79ea54f1, 0x5555f5c8346f175a
#else
      0x0015742c, 0x79ea54f1, 0x5555f5c8, 0x346f175a
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x009343a252c7a465, 0xd39a2599513cb4de
#else
      0x009343a2, 0x52c7a465, 0xd39a2599, 0x513cb4de
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00803582da2f96f2, 0x6f1fa5ed5321a41e
#else
      0x00803582, 0xda2f96f2, 0x6f1fa5ed, 0x5321a41e
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x005b8868146dc1e8, 0xbcf8976be4883126
#else
      0x005b8868, 0x146dc1e8, 0xbcf8976b, 0xe4883126
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x009633ca9c61ebb2, 0xf9a70741ee3beace
#else
      0x009633ca, 0x9c61ebb2, 0xf9a70741, 0xee3beace
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00e5ec374858595d, 0x3f71857133034f16
#else
      0x00e5ec37, 0x4858595d, 0x3f718571, 0x33034f16
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x007484bc68e67fb1, 0x80be23c097030d9e
#else
      0x007484bc, 0x68e67fb1, 0x80be23c0, 0x97030d9e
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t resBuffer[size];
    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t y = {
        .negative = false,
        .buffer = y1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t base = {
        .negative = false,
        .buffer = base1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = size
    };
    rucrypto_gost_34_10_mul_based(&x, &y, &res, &base, false);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expected1Buffer, size));
    x.buffer = x2Buffer;
    y.buffer = y2Buffer;
    base.buffer = base2Buffer;
    rucrypto_gost_34_10_mul_based(&x, &y, &res, &base, false);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expected2Buffer, size));
}

TEST(Gost3410ModularMath, DivBasedTest) {
    constexpr size_t size = 128 / (8 * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00c42b8c3e641261, 0xfba6cce47c92c6bf
#else
      0x00c42b8c, 0x3e641261, 0xfba6cce4, 0x7c92c6bf
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00d2c235f6b2ef87, 0xc8887bb3941937ed
#else
      0x00d2c235, 0xf6b2ef87, 0xc8887bb3, 0x941937ed
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00dca164d2ead0af, 0xba7909607074c087
#else
      0x00dca164, 0xd2ead0af, 0xba790960, 0x7074c087
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00d5c9902250a378, 0xd1061c50012af683
#else
      0x00d5c990, 0x2250a378, 0xd1061c50, 0x012af683
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x003b0845d787b40e, 0x55473f55cbb57891
#else
      0x003b0845, 0xd787b40e, 0x55473f55, 0xcbb57891
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00672bf392d40c81, 0xc298216fac85a83b
#else
      0x00672bf3, 0x92d40c81, 0xc298216f, 0xac85a83b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t base2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x006c3e73428c0d15, 0xe124156edcd3912d
#else
      0x006c3e73, 0x428c0d15, 0xe124156e, 0xdcd3912d
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t expected2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0054468fa5d9abaa, 0x70d59408968850ed
#else
      0x0054468f, 0xa5d9abaa, 0x70d59408, 0x968850ed
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t resBuffer[size];
    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t y = {
        .negative = false,
        .buffer = y1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t base = {
        .negative = false,
        .buffer = base1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = size
    };
    rucrypto_gost_34_10_div_based(&x, &y, &res, &base, false);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expected1Buffer, size));
    x.buffer = x2Buffer;
    y.buffer = y2Buffer;
    base.buffer = base2Buffer;
    rucrypto_gost_34_10_div_based(&x, &y, &res, &base, false);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expected2Buffer, size));
}
