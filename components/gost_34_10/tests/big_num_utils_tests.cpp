extern "C" {
#include "../private_include/rucrypto/gost_34_10/big_num_utils.h"
}

#include "testing_utils.h"

#include <gtest/gtest.h>

TEST(Gost3410BigNumUtilsTests, Init0Test) {
    static const size_t bufferSize = 10;
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer[bufferSize] = {0};
    static const bool expectedNegative = false;

    rucrypto_gost_34_10_big_num_t var;
    rucrypto_gost_34_10_big_num_init_0(&var, bufferSize);
    EXPECT_EQ(var.negative, expectedNegative);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer, bufferSize));
    EXPECT_EQ(var.size, bufferSize);
    rucrypto_gost_34_10_big_num_fini(&var, false);
}

TEST(Gost3410BigNumUtilsTests, InitRandomTest) {
    static const size_t randomBufferSize = 16;
    static const uint8_t randomNumbers[randomBufferSize] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};
    static const auto getCounter = []() -> size_t& {
        static size_t counter = 0;
        return counter;
    };
    static const auto randomGenFunc = []() {
        return randomNumbers[(getCounter()++) % randomBufferSize];
    };

    static const size_t bufferSize = randomBufferSize / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    static const uint64_t expectedBuffer[bufferSize] = {0x0011223344556677, 0x8899aabbccddeeff};
#else
    static const uint32_t expectedBuffer[bufferSize] = {0x00112233, 0x44556677, 0x8899aabb, 0xccddeeff};
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    static const bool expectedNegative = false;

    rucrypto_gost_34_10_big_num_t var;
    rucrypto_gost_34_10_big_num_init_random(&var, bufferSize, randomGenFunc);
    EXPECT_EQ(var.negative, expectedNegative);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer, bufferSize));
    EXPECT_EQ(var.size, bufferSize);
    rucrypto_gost_34_10_big_num_fini(&var, false);
}

TEST(Gost3410BigNumUtilsTests, InitCopyTest) {
    static const size_t bufferSize = 4;
    static rucrypto_gost_34_10_big_num_underlying_t buffer[bufferSize] = {0x1, 0x2, 0x3, 0x4};
    static const rucrypto_gost_34_10_big_num_t source = {
            .negative = true,
            .buffer = buffer,
            .size = bufferSize
    };

    rucrypto_gost_34_10_big_num_t copy;
    rucrypto_gost_34_10_big_num_init_copy(&source, &copy);

    EXPECT_EQ(copy.negative, source.negative);
    EXPECT_TRUE(AreBuffersEqual(copy.buffer, source.buffer, copy.size));
    EXPECT_EQ(copy.size, source.size);
    EXPECT_NE(copy.buffer, source.buffer);
    rucrypto_gost_34_10_big_num_fini(&copy, false);
}

TEST(Gost3410BigNumUtilsTests, InitFromBufferTest) {
    static const size_t initBufferSize = 16;
    static const uint8_t initBuffer[initBufferSize] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};

    static const size_t bufferSize = initBufferSize / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    static const uint64_t expectedBuffer[bufferSize] = {0x0011223344556677, 0x8899aabbccddeeff};
#else
    static const uint32_t expectedBuffer[bufferSize] = {0x00112233, 0x44556677, 0x8899aabb, 0xccddeeff};
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    static const bool expectedNegative = false;

    rucrypto_gost_34_10_big_num_t var;
    rucrypto_gost_34_10_big_num_init_from_buffer(initBuffer, initBufferSize, &var);
    EXPECT_EQ(var.negative, expectedNegative);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer, bufferSize));
    EXPECT_EQ(var.size, bufferSize);
    rucrypto_gost_34_10_big_num_fini(&var, false);
}

TEST(Gost3410BigNumUtilsTests, Fill0Test) {
    static const size_t bufferSize = 10;
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer[bufferSize] = {0};
    static const bool expectedNegative = false;

    rucrypto_gost_34_10_big_num_underlying_t buffer[bufferSize] = {0x1};
    rucrypto_gost_34_10_big_num_t var = {
            .negative = false,
            .buffer = buffer,
            .size = bufferSize
    };
    rucrypto_gost_34_10_big_num_fill_0(&var);
    EXPECT_EQ(var.negative, expectedNegative);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer, bufferSize));
    EXPECT_EQ(var.size, bufferSize);
}

TEST(Gost3410BigNumUtilsTests, FillRandomTest) {
    static const size_t randomBufferSize = 16;
    static const uint8_t randomNumbers[randomBufferSize] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};
    static const auto getCounter = []() -> size_t& {
        static size_t counter = 0;
        return counter;
    };
    static const auto randomGenFunc = []() {
        return randomNumbers[(getCounter()++) % randomBufferSize];
    };

    static const size_t bufferSize = randomBufferSize / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    static const uint64_t expectedBuffer[bufferSize] = {0x0011223344556677, 0x8899aabbccddeeff};
#else
    static const uint32_t expectedBuffer[bufferSize] = {0x00112233, 0x44556677, 0x8899aabb, 0xccddeeff};
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    static const bool expectedNegative = false;

    rucrypto_gost_34_10_big_num_underlying_t buffer[bufferSize] = {0x1};
    rucrypto_gost_34_10_big_num_t var = {
            .negative = false,
            .buffer = buffer,
            .size = bufferSize
    };
    rucrypto_gost_34_10_big_num_fill_random(&var, randomGenFunc);
    EXPECT_EQ(var.negative, expectedNegative);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer, bufferSize));
    EXPECT_EQ(var.size, bufferSize);
}

TEST(Gost3410BigNumUtilsTests, CopyTest) {
    static const size_t bufferSize = 4;
    static rucrypto_gost_34_10_big_num_underlying_t bufferOriginal[bufferSize] = {0x1, 0x2, 0x3, 0x4};
    static const rucrypto_gost_34_10_big_num_t source = {
            .negative = true,
            .buffer = bufferOriginal,
            .size = bufferSize
    };

    rucrypto_gost_34_10_big_num_underlying_t bufferCopy[bufferSize] = {0x1};
    rucrypto_gost_34_10_big_num_t copy = {
            .negative = false,
            .buffer = bufferCopy,
            .size = bufferSize
    };

    rucrypto_gost_34_10_big_num_copy(&source, &copy);
    EXPECT_EQ(copy.negative, source.negative);
    EXPECT_TRUE(AreBuffersEqual(copy.buffer, source.buffer, copy.size));
    EXPECT_EQ(copy.size, source.size);
    EXPECT_NE(copy.buffer, source.buffer);
}

TEST(Gost3410BigNumUtilsTests, FillFromBufferTest) {
    static const size_t initBufferSize = 16;
    static const uint8_t initBuffer[initBufferSize] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};

    static const size_t bufferSize = initBufferSize / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    static const uint64_t expectedBuffer[bufferSize] = {0x0011223344556677, 0x8899aabbccddeeff};
#else
    static const uint32_t expectedBuffer[bufferSize] = {0x00112233, 0x44556677, 0x8899aabb, 0xccddeeff};
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    static const bool expectedNegative = false;

    rucrypto_gost_34_10_big_num_underlying_t buffer[bufferSize] = {0x1};
    rucrypto_gost_34_10_big_num_t var = {
            .negative = false,
            .buffer = buffer,
            .size = bufferSize
    };
    rucrypto_gost_34_10_big_num_fill_from_buffer(initBuffer, initBufferSize, &var);
    EXPECT_EQ(var.negative, expectedNegative);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer, bufferSize));
    EXPECT_EQ(var.size, bufferSize);
}

TEST(Gost3410BigNumUtilsTests, CopyToBufferTest) {
    static const size_t expectedBufferSize = 16;
    static const uint8_t expectedBuffer[expectedBufferSize] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};

    static const size_t bufferSize = expectedBufferSize / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    static uint64_t bigNumBuffer[bufferSize] = {0x0011223344556677, 0x8899aabbccddeeff};
#else
    static uint32_t bigNumBuffer[bufferSize] = {0x00112233, 0x44556677, 0x8899aabb, 0xccddeeff};
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)

    uint8_t buffer[expectedBufferSize] = {0x0};
    rucrypto_gost_34_10_big_num_t var = {
            .negative = false,
            .buffer = bigNumBuffer,
            .size = bufferSize
    };
    rucrypto_gost_34_10_big_num_copy_to_buffer(&var, buffer, expectedBufferSize);
    EXPECT_TRUE(AreBuffersEqual(buffer, expectedBuffer, expectedBufferSize));
}

TEST(Gost3410BigNumUtilsTests, LenTest) {
    static const size_t bufferSize = 4;
    static rucrypto_gost_34_10_big_num_underlying_t buffer1[bufferSize] = {1, 1, 1, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer2[bufferSize] = {0, 1, 1, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer3[bufferSize] = {0, 0, 1, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer4[bufferSize] = {0, 0, 0, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer5[bufferSize] = {0, 0, 0, 0};
    rucrypto_gost_34_10_big_num_t var = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    EXPECT_EQ(rucrypto_gost_34_10_len(&var), 4);
    var.buffer = buffer2;
    EXPECT_EQ(rucrypto_gost_34_10_len(&var), 3);
    var.buffer = buffer3;
    EXPECT_EQ(rucrypto_gost_34_10_len(&var), 2);
    var.buffer = buffer4;
    EXPECT_EQ(rucrypto_gost_34_10_len(&var), 1);
    var.buffer = buffer5;
    EXPECT_EQ(rucrypto_gost_34_10_len(&var), 1);
}

TEST(Gost3410BigNumUtilsTests, IsZeroTest) {
    static const size_t bufferSize = 4;
    static rucrypto_gost_34_10_big_num_underlying_t buffer1[bufferSize] = {1, 1, 1, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer2[bufferSize] = {0, 1, 1, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer3[bufferSize] = {0, 0, 1, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer4[bufferSize] = {0, 0, 0, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer5[bufferSize] = {0, 0, 0, 0};
    rucrypto_gost_34_10_big_num_t var = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    EXPECT_FALSE(rucrypto_gost_34_10_is_zero(&var));
    var.buffer = buffer2;
    EXPECT_FALSE(rucrypto_gost_34_10_is_zero(&var));
    var.buffer = buffer3;
    EXPECT_FALSE(rucrypto_gost_34_10_is_zero(&var));
    var.buffer = buffer4;
    EXPECT_FALSE(rucrypto_gost_34_10_is_zero(&var));
    var.buffer = buffer5;
    EXPECT_TRUE(rucrypto_gost_34_10_is_zero(&var));
}

TEST(Gost3410BigNumUtilsTests, AreEqualTest) {
    static const size_t bufferSize = 4;
    static rucrypto_gost_34_10_big_num_underlying_t buffer1[bufferSize] = {1, 1, 1, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer2[bufferSize] = {0, 1, 1, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer3[bufferSize] = {0, 0, 1, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer4[bufferSize] = {0, 0, 0, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer5[bufferSize] = {0, 0, 0, 0};
    rucrypto_gost_34_10_big_num_t var1 = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    rucrypto_gost_34_10_big_num_t var2 = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    EXPECT_TRUE(rucrypto_gost_34_10_are_equal(&var1, &var2));
    var2.buffer = buffer2;
    EXPECT_FALSE(rucrypto_gost_34_10_are_equal(&var1, &var2));
    var2.buffer = buffer3;
    EXPECT_FALSE(rucrypto_gost_34_10_are_equal(&var1, &var2));
    var2.buffer = buffer4;
    EXPECT_FALSE(rucrypto_gost_34_10_are_equal(&var1, &var2));
    var2.buffer = buffer5;
    EXPECT_FALSE(rucrypto_gost_34_10_are_equal(&var1, &var2));
    var1.buffer = buffer5;
    EXPECT_TRUE(rucrypto_gost_34_10_are_equal(&var1, &var2));
}

TEST(Gost3410BigNumUtilsTests, AreEqualAndNotZeroTest) {
    static const size_t bufferSize = 4;
    static rucrypto_gost_34_10_big_num_underlying_t buffer1[bufferSize] = {1, 1, 1, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer2[bufferSize] = {0, 1, 1, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer3[bufferSize] = {0, 0, 1, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer4[bufferSize] = {0, 0, 0, 1};
    static rucrypto_gost_34_10_big_num_underlying_t buffer5[bufferSize] = {0, 0, 0, 0};
    rucrypto_gost_34_10_big_num_t var1 = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    rucrypto_gost_34_10_big_num_t var2 = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    EXPECT_TRUE(rucrypto_gost_34_10_are_equal_and_not_zero(&var1, &var2));
    var2.buffer = buffer2;
    EXPECT_FALSE(rucrypto_gost_34_10_are_equal_and_not_zero(&var1, &var2));
    var2.buffer = buffer3;
    EXPECT_FALSE(rucrypto_gost_34_10_are_equal_and_not_zero(&var1, &var2));
    var2.buffer = buffer4;
    EXPECT_FALSE(rucrypto_gost_34_10_are_equal_and_not_zero(&var1, &var2));
    var2.buffer = buffer5;
    EXPECT_FALSE(rucrypto_gost_34_10_are_equal_and_not_zero(&var1, &var2));
    var1.buffer = buffer5;
    EXPECT_FALSE(rucrypto_gost_34_10_are_equal_and_not_zero(&var1, &var2));
}

TEST(Gost3410BigNumUtilsTests, IsBiggerTest) {
    static const size_t bufferSize = 2;
    static rucrypto_gost_34_10_big_num_underlying_t buffer1[bufferSize] = {0xFF, 0xFF};
    static rucrypto_gost_34_10_big_num_underlying_t buffer2[bufferSize] = {0xAA, 0xAA};
    static rucrypto_gost_34_10_big_num_underlying_t buffer3[bufferSize] = {0x00, 0x00};

    rucrypto_gost_34_10_big_num_t var1 = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    rucrypto_gost_34_10_big_num_t var2 = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    EXPECT_FALSE(rucrypto_gost_34_10_is_bigger(&var1, &var2));
    var2.buffer = buffer2;
    EXPECT_TRUE(rucrypto_gost_34_10_is_bigger(&var1, &var2));
    var1.buffer = buffer3;
    EXPECT_FALSE(rucrypto_gost_34_10_is_bigger(&var1, &var2));
}

TEST(Gost3410BigNumUtilsTests, IsBiggerOrEqualTest) {
    static const size_t bufferSize = 2;
    static rucrypto_gost_34_10_big_num_underlying_t buffer1[bufferSize] = {0xFF, 0xFF};
    static rucrypto_gost_34_10_big_num_underlying_t buffer2[bufferSize] = {0xAA, 0xAA};
    static rucrypto_gost_34_10_big_num_underlying_t buffer3[bufferSize] = {0x00, 0x00};

    rucrypto_gost_34_10_big_num_t var1 = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    rucrypto_gost_34_10_big_num_t var2 = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    EXPECT_TRUE(rucrypto_gost_34_10_is_bigger_or_equal(&var1, &var2));
    var2.buffer = buffer2;
    EXPECT_TRUE(rucrypto_gost_34_10_is_bigger_or_equal(&var1, &var2));
    var1.buffer = buffer3;
    EXPECT_FALSE(rucrypto_gost_34_10_is_bigger_or_equal(&var1, &var2));
}

TEST(Gost3410BigNumUtilsTests, IsLessTest) {
    static const size_t bufferSize = 2;
    static rucrypto_gost_34_10_big_num_underlying_t buffer1[bufferSize] = {0xFF, 0xFF};
    static rucrypto_gost_34_10_big_num_underlying_t buffer2[bufferSize] = {0xAA, 0xAA};
    static rucrypto_gost_34_10_big_num_underlying_t buffer3[bufferSize] = {0x00, 0x00};

    rucrypto_gost_34_10_big_num_t var1 = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    rucrypto_gost_34_10_big_num_t var2 = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    EXPECT_FALSE(rucrypto_gost_34_10_is_less(&var1, &var2));
    var2.buffer = buffer2;
    EXPECT_FALSE(rucrypto_gost_34_10_is_less(&var1, &var2));
    var1.buffer = buffer3;
    EXPECT_TRUE(rucrypto_gost_34_10_is_less(&var1, &var2));
}

TEST(Gost3410BigNumUtilsTests, IsLessOrEqualTest) {
    static const size_t bufferSize = 2;
    static rucrypto_gost_34_10_big_num_underlying_t buffer1[bufferSize] = {0xFF, 0xFF};
    static rucrypto_gost_34_10_big_num_underlying_t buffer2[bufferSize] = {0xAA, 0xAA};
    static rucrypto_gost_34_10_big_num_underlying_t buffer3[bufferSize] = {0x00, 0x00};

    rucrypto_gost_34_10_big_num_t var1 = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    rucrypto_gost_34_10_big_num_t var2 = {
            .negative = false,
            .buffer = buffer1,
            .size = bufferSize
    };
    EXPECT_TRUE(rucrypto_gost_34_10_is_less_or_equal(&var1, &var2));
    var2.buffer = buffer2;
    EXPECT_FALSE(rucrypto_gost_34_10_is_less_or_equal(&var1, &var2));
    var1.buffer = buffer3;
    EXPECT_TRUE(rucrypto_gost_34_10_is_less_or_equal(&var1, &var2));
}

TEST(Gost3410BigNumUtilsTests, ShiftLeftTest) {
    static const size_t bufferSize = 16;
    rucrypto_gost_34_10_big_num_underlying_t buffer[bufferSize] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShift1[bufferSize] = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShift5[bufferSize] = {0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShiftBig[bufferSize] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    rucrypto_gost_34_10_big_num_t var = {
            .negative = false,
            .buffer = buffer,
            .size = bufferSize
    };

    rucrypto_gost_34_10_shift_left(&var, 1);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShift1, bufferSize));
    rucrypto_gost_34_10_shift_left(&var, 4);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShift5, bufferSize));
    rucrypto_gost_34_10_shift_left(&var, bufferSize + 1);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShiftBig, bufferSize));
}

TEST(Gost3410BigNumUtilsTests, BitShiftLeftTest) {
    static const size_t bufferSize = 2;
    rucrypto_gost_34_10_big_num_underlying_t buffer[bufferSize] = {0x01, 0xFFFF};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShift1[bufferSize] = {0x02, 0x01FFFE};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShift5[bufferSize] = {0x20, 0x1FFFE0};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShiftCross[bufferSize] = {0xFFFF0, 0x00};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShiftFull[bufferSize] = {0x1FFFE0, 0x00};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShiftBig[bufferSize] = {0x00, 0x00};

    rucrypto_gost_34_10_big_num_t var = {
            .negative = false,
            .buffer = buffer,
            .size = bufferSize
    };

    rucrypto_gost_34_10_bit_shift_left(&var, 1);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShift1, bufferSize));
    rucrypto_gost_34_10_bit_shift_left(&var, 4);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShift5, bufferSize));
    rucrypto_gost_34_10_bit_shift_left(&var, sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8 - 1);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShiftCross, bufferSize));
    rucrypto_gost_34_10_bit_shift_left(&var, 1);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShiftFull, bufferSize));
    rucrypto_gost_34_10_bit_shift_left(&var, bufferSize * sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8 + 1);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShiftBig, bufferSize));
}

TEST(Gost3410BigNumUtilsTests, ShiftRightTest) {
    static const size_t bufferSize = 16;
    rucrypto_gost_34_10_big_num_underlying_t buffer[bufferSize] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShift1[bufferSize] = {0x00, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShift5[bufferSize] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShiftBig[bufferSize] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    rucrypto_gost_34_10_big_num_t var = {
            .negative = false,
            .buffer = buffer,
            .size = bufferSize
    };

    rucrypto_gost_34_10_shift_right(&var, 1);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShift1, bufferSize));
    rucrypto_gost_34_10_shift_right(&var, 4);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShift5, bufferSize));
    rucrypto_gost_34_10_shift_right(&var, bufferSize + 1);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShiftBig, bufferSize));
}

TEST(Gost3410BigNumUtilsTests, BitShiftRightTest) {
    static const size_t bufferSize = 2;
    rucrypto_gost_34_10_big_num_underlying_t buffer[bufferSize] = {0x03, 0xFFFF};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShift1[bufferSize] = {0x01, ((rucrypto_gost_34_10_big_num_underlying_t)1 << (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8 - 1)) | 0x7FFF};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShift5[bufferSize] = {0x00, ((rucrypto_gost_34_10_big_num_underlying_t)3 << (sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8 - 5)) | 0x7FF};
    static const rucrypto_gost_34_10_big_num_underlying_t bufferShiftBig[bufferSize] = {0x00, 0x00};

    rucrypto_gost_34_10_big_num_t var = {
            .negative = false,
            .buffer = buffer,
            .size = bufferSize
    };

    rucrypto_gost_34_10_bit_shift_right(&var, 1);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShift1, bufferSize));
    rucrypto_gost_34_10_bit_shift_right(&var, 4);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShift5, bufferSize));
    rucrypto_gost_34_10_bit_shift_right(&var, bufferSize * sizeof(rucrypto_gost_34_10_big_num_underlying_t) * 8 + 1);
    EXPECT_TRUE(AreBuffersEqual(buffer, bufferShiftBig, bufferSize));
}
