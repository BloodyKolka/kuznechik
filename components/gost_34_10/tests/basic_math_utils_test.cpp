extern "C" {
#include "../private_include/rucrypto/gost_34_10/basic_math_utils.h"
}

#include "testing_utils.h"

#include <gtest/gtest.h>

TEST(Gost3410BasicMathTests, MulBy2Test) {
    constexpr size_t size = 1024 / 8 / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
        0xbfa0905ea10c5a06, 0xd3e3f4296556c105, 0x0b0504443b121027, 0x6ad28c8a8b3a4196, 0xa4b98af9a78be09d, 0x8035864002d9a054, 0x592514d86a847f7e, 0x4121c2ab778ce85a, 0xdee75973cf3859da, 0xfac92ad4e99a9cdc, 0xd4a56d6109aaa8dc, 0x5be860d7ee140ff9, 0x03fab12623ad6b79, 0x4019f7418069e18f, 0xdca88414036b7de3, 0x0f0ebd46145e1252
#else
        0xbfa0905e, 0xa10c5a06, 0xd3e3f429, 0x6556c105, 0x0b050444, 0x3b121027, 0x6ad28c8a, 0x8b3a4196, 0xa4b98af9, 0xa78be09d, 0x80358640, 0x02d9a054, 0x592514d8, 0x6a847f7e, 0x4121c2ab, 0x778ce85a, 0xdee75973, 0xcf3859da, 0xfac92ad4, 0xe99a9cdc, 0xd4a56d61, 0x09aaa8dc, 0x5be860d7, 0xee140ff9, 0x03fab126, 0x23ad6b79, 0x4019f741, 0x8069e18f, 0xdca88414, 0x036b7de3, 0x0f0ebd46, 0x145e1252
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer1[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
        0x7f4120bd4218b40d, 0xa7c7e852caad820a, 0x160a08887624204e, 0xd5a519151674832d, 0x497315f34f17c13b, 0x006b0c8005b340a8, 0xb24a29b0d508fefc, 0x82438556ef19d0b5, 0xbdceb2e79e70b3b5, 0xf59255a9d33539b9, 0xa94adac2135551b8, 0xb7d0c1afdc281ff2, 0x07f5624c475ad6f2, 0x8033ee8300d3c31f, 0xb951082806d6fbc6, 0x1e1d7a8c28bc24a4
#else
        0x7f4120bd, 0x4218b40d, 0xa7c7e852, 0xcaad820a, 0x160a0888, 0x7624204e, 0xd5a51915, 0x1674832d, 0x497315f3, 0x4f17c13b, 0x006b0c80, 0x05b340a8, 0xb24a29b0, 0xd508fefc, 0x82438556, 0xef19d0b5, 0xbdceb2e7, 0x9e70b3b5, 0xf59255a9, 0xd33539b9, 0xa94adac2, 0x135551b8, 0xb7d0c1af, 0xdc281ff2, 0x07f5624c, 0x475ad6f2, 0x8033ee83, 0x00d3c31f, 0xb9510828, 0x06d6fbc6, 0x1e1d7a8c, 0x28bc24a4
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
        0xbe03fdb0ac1ce5ab, 0xd058318fa806b194, 0xc3e270f9f1dd4b04, 0xec29e238cbf84d45, 0x3d3eb648d10c24ab, 0x5b26d87ad2b1f752, 0x6011abccb225a7e0, 0x2b77609e39010149, 0x25f22ae422fb9e8f, 0x0a813ccb047d8176, 0x623b0ec6b0061bdb, 0x151a7db470a14793, 0x67dc005fd1e6836e, 0x61712d35cf7c984e, 0x90ab35bb468753b1, 0x49de18934bda7956
#else
        0xbe03fdb0, 0xac1ce5ab, 0xd058318f, 0xa806b194, 0xc3e270f9, 0xf1dd4b04, 0xec29e238, 0xcbf84d45, 0x3d3eb648, 0xd10c24ab, 0x5b26d87a, 0xd2b1f752, 0x6011abcc, 0xb225a7e0, 0x2b77609e, 0x39010149, 0x25f22ae4, 0x22fb9e8f, 0x0a813ccb, 0x047d8176, 0x623b0ec6, 0xb0061bdb, 0x151a7db4, 0x70a14793, 0x67dc005f, 0xd1e6836e, 0x61712d35, 0xcf7c984e, 0x90ab35bb, 0x468753b1, 0x49de1893, 0x4bda7956
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer2[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x7c07fb615839cb57, 0xa0b0631f500d6329, 0x87c4e1f3e3ba9609, 0xd853c47197f09a8a, 0x7a7d6c91a2184956, 0xb64db0f5a563eea4, 0xc0235799644b4fc0, 0x56eec13c72020292, 0x4be455c845f73d1e, 0x1502799608fb02ec, 0xc4761d8d600c37b6, 0x2a34fb68e1428f26, 0xcfb800bfa3cd06dc, 0xc2e25a6b9ef9309d, 0x21566b768d0ea762, 0x93bc312697b4f2ac
#else
      0x7c07fb61, 0x5839cb57, 0xa0b0631f, 0x500d6329, 0x87c4e1f3, 0xe3ba9609, 0xd853c471, 0x97f09a8a, 0x7a7d6c91, 0xa2184956, 0xb64db0f5, 0xa563eea4, 0xc0235799, 0x644b4fc0, 0x56eec13c, 0x72020292, 0x4be455c8, 0x45f73d1e, 0x15027996, 0x08fb02ec, 0xc4761d8d, 0x600c37b6, 0x2a34fb68, 0xe1428f26, 0xcfb800bf, 0xa3cd06dc, 0xc2e25a6b, 0x9ef9309d, 0x21566b76, 0x8d0ea762, 0x93bc3126, 0x97b4f2ac
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
        0x9fc5b0ca33900a2b, 0xbf4c4e57b6c23c4c, 0x0b4dcc1ebb69bfbd, 0x12f1445648bace8a, 0xc40bfa06a4adca39, 0x57a2057125397f57, 0xb89dfab1e765dc13, 0xcb211b0c581889d4, 0x7f91660294ecdb32, 0x07dbe5de9ba38675, 0xff23ca68e4a2c1bc, 0x38ae76d9beef6cac, 0x784fe5d50478f162, 0x379f0b433a2b19e3, 0xeb7fb1f829b510a3, 0x2886d56f9b3f69a6
#else
        0x9fc5b0ca, 0x33900a2b, 0xbf4c4e57, 0xb6c23c4c, 0x0b4dcc1e, 0xbb69bfbd, 0x12f14456, 0x48bace8a, 0xc40bfa06, 0xa4adca39, 0x57a20571, 0x25397f57, 0xb89dfab1, 0xe765dc13, 0xcb211b0c, 0x581889d4, 0x7f916602, 0x94ecdb32, 0x07dbe5de, 0x9ba38675, 0xff23ca68, 0xe4a2c1bc, 0x38ae76d9, 0xbeef6cac, 0x784fe5d5, 0x0478f162, 0x379f0b43, 0x3a2b19e3, 0xeb7fb1f8, 0x29b510a3, 0x2886d56f, 0x9b3f69a6
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer3[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x3f8b619467201457, 0x7e989caf6d847898, 0x169b983d76d37f7a, 0x25e288ac91759d15, 0x8817f40d495b9472, 0xaf440ae24a72feaf, 0x713bf563cecbb827, 0x96423618b03113a8, 0xff22cc0529d9b664, 0x0fb7cbbd37470ceb, 0xfe4794d1c9458378, 0x715cedb37dded958, 0xf09fcbaa08f1e2c4, 0x6f3e1686745633c7, 0xd6ff63f0536a2146, 0x510daadf367ed34c
#else
      0x3f8b6194, 0x67201457, 0x7e989caf, 0x6d847898, 0x169b983d, 0x76d37f7a, 0x25e288ac, 0x91759d15, 0x8817f40d, 0x495b9472, 0xaf440ae2, 0x4a72feaf, 0x713bf563, 0xcecbb827, 0x96423618, 0xb03113a8, 0xff22cc05, 0x29d9b664, 0x0fb7cbbd, 0x37470ceb, 0xfe4794d1, 0xc9458378, 0x715cedb3, 0x7dded958, 0xf09fcbaa, 0x08f1e2c4, 0x6f3e1686, 0x745633c7, 0xd6ff63f0, 0x536a2146, 0x510daadf, 0x367ed34c
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
        0x30163e90ff6b9c55, 0x6a381e0ed7bdd12a, 0xf1467b2c0a972be1, 0x3bc5af9abb4dde46, 0x5bea5d42a36e2f78, 0xfba76e070acceb0e, 0xbebf5d2b9ce13c15, 0x76e3cc0141f46635, 0xf4fa06221a4d9fc1, 0xb6459df49213fdea, 0x80b494a249d06f8f, 0x3ec8c9d022ee7afe, 0xdacc891bc133fb97, 0xb14a81177e84f07c, 0x90c5def7f86736c0, 0x568c15b7d91d4e9b
#else
        0x30163e90, 0xff6b9c55, 0x6a381e0e, 0xd7bdd12a, 0xf1467b2c, 0x0a972be1, 0x3bc5af9a, 0xbb4dde46, 0x5bea5d42, 0xa36e2f78, 0xfba76e07, 0x0acceb0e, 0xbebf5d2b, 0x9ce13c15, 0x76e3cc01, 0x41f46635, 0xf4fa0622, 0x1a4d9fc1, 0xb6459df4, 0x9213fdea, 0x80b494a2, 0x49d06f8f, 0x3ec8c9d0, 0x22ee7afe, 0xdacc891b, 0xc133fb97, 0xb14a8117, 0x7e84f07c, 0x90c5def7, 0xf86736c0, 0x568c15b7, 0xd91d4e9b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer4[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x602c7d21fed738aa, 0xd4703c1daf7ba255, 0xe28cf658152e57c2, 0x778b5f35769bbc8c, 0xb7d4ba8546dc5ef1, 0xf74edc0e1599d61d, 0x7d7eba5739c2782a, 0xedc7980283e8cc6b, 0xe9f40c44349b3f83, 0x6c8b3be92427fbd5, 0x0169294493a0df1e, 0x7d9193a045dcf5fd, 0xb59912378267f72f, 0x6295022efd09e0f9, 0x218bbdeff0ce6d80, 0xad182b6fb23a9d36
#else
      0x602c7d21, 0xfed738aa, 0xd4703c1d, 0xaf7ba255, 0xe28cf658, 0x152e57c2, 0x778b5f35, 0x769bbc8c, 0xb7d4ba85, 0x46dc5ef1, 0xf74edc0e, 0x1599d61d, 0x7d7eba57, 0x39c2782a, 0xedc79802, 0x83e8cc6b, 0xe9f40c44, 0x349b3f83, 0x6c8b3be9, 0x2427fbd5, 0x01692944, 0x93a0df1e, 0x7d9193a0, 0x45dcf5fd, 0xb5991237, 0x8267f72f, 0x6295022e, 0xfd09e0f9, 0x218bbdef, 0xf0ce6d80, 0xad182b6f, 0xb23a9d36
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_t var = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_mul_by_2(&var);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer1, size));
    var.buffer = x2Buffer;
    rucrypto_gost_34_10_mul_by_2(&var);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer2, size));
    var.buffer = x3Buffer;
    rucrypto_gost_34_10_mul_by_2(&var);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer3, size));
    var.buffer = x4Buffer;
    rucrypto_gost_34_10_mul_by_2(&var);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer4, size));
}

TEST(Gost3410BasicMathTests, DivBy2Test) {
    constexpr size_t size = 1024 / 8 / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbfa0905ea10c5a06, 0xd3e3f4296556c105, 0x0b0504443b121027, 0x6ad28c8a8b3a4196, 0xa4b98af9a78be09d, 0x8035864002d9a054, 0x592514d86a847f7e, 0x4121c2ab778ce85a, 0xdee75973cf3859da, 0xfac92ad4e99a9cdc, 0xd4a56d6109aaa8dc, 0x5be860d7ee140ff9, 0x03fab12623ad6b79, 0x4019f7418069e18f, 0xdca88414036b7de3, 0x0f0ebd46145e1252
#else
      0xbfa0905e, 0xa10c5a06, 0xd3e3f429, 0x6556c105, 0x0b050444, 0x3b121027, 0x6ad28c8a, 0x8b3a4196, 0xa4b98af9, 0xa78be09d, 0x80358640, 0x02d9a054, 0x592514d8, 0x6a847f7e, 0x4121c2ab, 0x778ce85a, 0xdee75973, 0xcf3859da, 0xfac92ad4, 0xe99a9cdc, 0xd4a56d61, 0x09aaa8dc, 0x5be860d7, 0xee140ff9, 0x03fab126, 0x23ad6b79, 0x4019f741, 0x8069e18f, 0xdca88414, 0x036b7de3, 0x0f0ebd46, 0x145e1252
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer1[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x5fd0482f50862d03, 0x69f1fa14b2ab6082, 0x858282221d890813, 0xb5694645459d20cb, 0x525cc57cd3c5f04e, 0xc01ac320016cd02a, 0x2c928a6c35423fbf, 0x2090e155bbc6742d, 0x6f73acb9e79c2ced, 0x7d64956a74cd4e6e, 0x6a52b6b084d5546e, 0x2df4306bf70a07fc, 0x81fd589311d6b5bc, 0xa00cfba0c034f0c7, 0xee54420a01b5bef1, 0x87875ea30a2f0929
#else
      0x5fd0482f, 0x50862d03, 0x69f1fa14, 0xb2ab6082, 0x85828222, 0x1d890813, 0xb5694645, 0x459d20cb, 0x525cc57c, 0xd3c5f04e, 0xc01ac320, 0x016cd02a, 0x2c928a6c, 0x35423fbf, 0x2090e155, 0xbbc6742d, 0x6f73acb9, 0xe79c2ced, 0x7d64956a, 0x74cd4e6e, 0x6a52b6b0, 0x84d5546e, 0x2df4306b, 0xf70a07fc, 0x81fd5893, 0x11d6b5bc, 0xa00cfba0, 0xc034f0c7, 0xee54420a, 0x01b5bef1, 0x87875ea3, 0x0a2f0929
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbe03fdb0ac1ce5ab, 0xd058318fa806b194, 0xc3e270f9f1dd4b04, 0xec29e238cbf84d45, 0x3d3eb648d10c24ab, 0x5b26d87ad2b1f752, 0x6011abccb225a7e0, 0x2b77609e39010149, 0x25f22ae422fb9e8f, 0x0a813ccb047d8176, 0x623b0ec6b0061bdb, 0x151a7db470a14793, 0x67dc005fd1e6836e, 0x61712d35cf7c984e, 0x90ab35bb468753b1, 0x49de18934bda7956
#else
      0xbe03fdb0, 0xac1ce5ab, 0xd058318f, 0xa806b194, 0xc3e270f9, 0xf1dd4b04, 0xec29e238, 0xcbf84d45, 0x3d3eb648, 0xd10c24ab, 0x5b26d87a, 0xd2b1f752, 0x6011abcc, 0xb225a7e0, 0x2b77609e, 0x39010149, 0x25f22ae4, 0x22fb9e8f, 0x0a813ccb, 0x047d8176, 0x623b0ec6, 0xb0061bdb, 0x151a7db4, 0x70a14793, 0x67dc005f, 0xd1e6836e, 0x61712d35, 0xcf7c984e, 0x90ab35bb, 0x468753b1, 0x49de1893, 0x4bda7956
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer2[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x5f01fed8560e72d5, 0xe82c18c7d40358ca, 0x61f1387cf8eea582, 0x7614f11c65fc26a2, 0x9e9f5b2468861255, 0xad936c3d6958fba9, 0x3008d5e65912d3f0, 0x15bbb04f1c8080a4, 0x92f91572117dcf47, 0x85409e65823ec0bb, 0x311d876358030ded, 0x8a8d3eda3850a3c9, 0xb3ee002fe8f341b7, 0x30b8969ae7be4c27, 0x48559adda343a9d8, 0xa4ef0c49a5ed3cab
#else
      0x5f01fed8, 0x560e72d5, 0xe82c18c7, 0xd40358ca, 0x61f1387c, 0xf8eea582, 0x7614f11c, 0x65fc26a2, 0x9e9f5b24, 0x68861255, 0xad936c3d, 0x6958fba9, 0x3008d5e6, 0x5912d3f0, 0x15bbb04f, 0x1c8080a4, 0x92f91572, 0x117dcf47, 0x85409e65, 0x823ec0bb, 0x311d8763, 0x58030ded, 0x8a8d3eda, 0x3850a3c9, 0xb3ee002f, 0xe8f341b7, 0x30b8969a, 0xe7be4c27, 0x48559add, 0xa343a9d8, 0xa4ef0c49, 0xa5ed3cab
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x9fc5b0ca33900a2b, 0xbf4c4e57b6c23c4c, 0x0b4dcc1ebb69bfbd, 0x12f1445648bace8a, 0xc40bfa06a4adca39, 0x57a2057125397f57, 0xb89dfab1e765dc13, 0xcb211b0c581889d4, 0x7f91660294ecdb32, 0x07dbe5de9ba38675, 0xff23ca68e4a2c1bc, 0x38ae76d9beef6cac, 0x784fe5d50478f162, 0x379f0b433a2b19e3, 0xeb7fb1f829b510a3, 0x2886d56f9b3f69a6
#else
      0x9fc5b0ca, 0x33900a2b, 0xbf4c4e57, 0xb6c23c4c, 0x0b4dcc1e, 0xbb69bfbd, 0x12f14456, 0x48bace8a, 0xc40bfa06, 0xa4adca39, 0x57a20571, 0x25397f57, 0xb89dfab1, 0xe765dc13, 0xcb211b0c, 0x581889d4, 0x7f916602, 0x94ecdb32, 0x07dbe5de, 0x9ba38675, 0xff23ca68, 0xe4a2c1bc, 0x38ae76d9, 0xbeef6cac, 0x784fe5d5, 0x0478f162, 0x379f0b43, 0x3a2b19e3, 0xeb7fb1f8, 0x29b510a3, 0x2886d56f, 0x9b3f69a6
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer3[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x4fe2d86519c80515, 0xdfa6272bdb611e26, 0x05a6e60f5db4dfde, 0x8978a22b245d6745, 0x6205fd035256e51c, 0xabd102b8929cbfab, 0xdc4efd58f3b2ee09, 0xe5908d862c0c44ea, 0x3fc8b3014a766d99, 0x03edf2ef4dd1c33a, 0xff91e534725160de, 0x1c573b6cdf77b656, 0x3c27f2ea823c78b1, 0x1bcf85a19d158cf1, 0xf5bfd8fc14da8851, 0x94436ab7cd9fb4d3
#else
      0x4fe2d865, 0x19c80515, 0xdfa6272b, 0xdb611e26, 0x05a6e60f, 0x5db4dfde, 0x8978a22b, 0x245d6745, 0x6205fd03, 0x5256e51c, 0xabd102b8, 0x929cbfab, 0xdc4efd58, 0xf3b2ee09, 0xe5908d86, 0x2c0c44ea, 0x3fc8b301, 0x4a766d99, 0x03edf2ef, 0x4dd1c33a, 0xff91e534, 0x725160de, 0x1c573b6c, 0xdf77b656, 0x3c27f2ea, 0x823c78b1, 0x1bcf85a1, 0x9d158cf1, 0xf5bfd8fc, 0x14da8851, 0x94436ab7, 0xcd9fb4d3
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x30163e90ff6b9c55, 0x6a381e0ed7bdd12a, 0xf1467b2c0a972be1, 0x3bc5af9abb4dde46, 0x5bea5d42a36e2f78, 0xfba76e070acceb0e, 0xbebf5d2b9ce13c15, 0x76e3cc0141f46635, 0xf4fa06221a4d9fc1, 0xb6459df49213fdea, 0x80b494a249d06f8f, 0x3ec8c9d022ee7afe, 0xdacc891bc133fb97, 0xb14a81177e84f07c, 0x90c5def7f86736c0, 0x568c15b7d91d4e9b
#else
      0x30163e90, 0xff6b9c55, 0x6a381e0e, 0xd7bdd12a, 0xf1467b2c, 0x0a972be1, 0x3bc5af9a, 0xbb4dde46, 0x5bea5d42, 0xa36e2f78, 0xfba76e07, 0x0acceb0e, 0xbebf5d2b, 0x9ce13c15, 0x76e3cc01, 0x41f46635, 0xf4fa0622, 0x1a4d9fc1, 0xb6459df4, 0x9213fdea, 0x80b494a2, 0x49d06f8f, 0x3ec8c9d0, 0x22ee7afe, 0xdacc891b, 0xc133fb97, 0xb14a8117, 0x7e84f07c, 0x90c5def7, 0xf86736c0, 0x568c15b7, 0xd91d4e9b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer4[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x180b1f487fb5ce2a, 0xb51c0f076bdee895, 0x78a33d96054b95f0, 0x9de2d7cd5da6ef23, 0x2df52ea151b717bc, 0x7dd3b70385667587, 0x5f5fae95ce709e0a, 0xbb71e600a0fa331a, 0xfa7d03110d26cfe0, 0xdb22cefa4909fef5, 0x405a4a5124e837c7, 0x9f6464e811773d7f, 0x6d66448de099fdcb, 0xd8a5408bbf42783e, 0x4862ef7bfc339b60, 0x2b460adbec8ea74d
#else
      0x180b1f48, 0x7fb5ce2a, 0xb51c0f07, 0x6bdee895, 0x78a33d96, 0x054b95f0, 0x9de2d7cd, 0x5da6ef23, 0x2df52ea1, 0x51b717bc, 0x7dd3b703, 0x85667587, 0x5f5fae95, 0xce709e0a, 0xbb71e600, 0xa0fa331a, 0xfa7d0311, 0x0d26cfe0, 0xdb22cefa, 0x4909fef5, 0x405a4a51, 0x24e837c7, 0x9f6464e8, 0x11773d7f, 0x6d66448d, 0xe099fdcb, 0xd8a5408b, 0xbf42783e, 0x4862ef7b, 0xfc339b60, 0x2b460adb, 0xec8ea74d
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_t var = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_div_by_2(&var);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer1, size));
    var.buffer = x2Buffer;
    rucrypto_gost_34_10_div_by_2(&var);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer2, size));
    var.buffer = x3Buffer;
    rucrypto_gost_34_10_div_by_2(&var);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer3, size));
    var.buffer = x4Buffer;
    rucrypto_gost_34_10_div_by_2(&var);
    EXPECT_TRUE(AreBuffersEqual(var.buffer, expectedBuffer4, size));
}

TEST(Gost3410BasicMathTests, AddTest) {
    constexpr size_t size = 1024 / 8 / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbfa0905ea10c5a06, 0xd3e3f4296556c105, 0x0b0504443b121027, 0x6ad28c8a8b3a4196, 0xa4b98af9a78be09d, 0x8035864002d9a054, 0x592514d86a847f7e, 0x4121c2ab778ce85a, 0xdee75973cf3859da, 0xfac92ad4e99a9cdc, 0xd4a56d6109aaa8dc, 0x5be860d7ee140ff9, 0x03fab12623ad6b79, 0x4019f7418069e18f, 0xdca88414036b7de3, 0x0f0ebd46145e1252
#else
      0xbfa0905e, 0xa10c5a06, 0xd3e3f429, 0x6556c105, 0x0b050444, 0x3b121027, 0x6ad28c8a, 0x8b3a4196, 0xa4b98af9, 0xa78be09d, 0x80358640, 0x02d9a054, 0x592514d8, 0x6a847f7e, 0x4121c2ab, 0x778ce85a, 0xdee75973, 0xcf3859da, 0xfac92ad4, 0xe99a9cdc, 0xd4a56d61, 0x09aaa8dc, 0x5be860d7, 0xee140ff9, 0x03fab126, 0x23ad6b79, 0x4019f741, 0x8069e18f, 0xdca88414, 0x036b7de3, 0x0f0ebd46, 0x145e1252
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x059d0e12c1281578, 0xc21e9bb03762bd95, 0xd0e7184c5b829dcd, 0x4e81e0af14d2a6d9, 0xe6625493f101ba43, 0x5760f89dada25fdd, 0x67cb6bbe0ccd4990, 0xe6fdb1b16e7f2abc, 0x7e00c4fdef9ba51f, 0xc86d47dd9c36e662, 0x8f99c5c9c42922e5, 0x7e1eee506d7127db, 0x204ac3f1706cd566, 0xe411bbb33d4a5c2d, 0xda0feb84b7e46732, 0x17fe4785d9d0b6a3
#else
      0x059d0e12, 0xc1281578, 0xc21e9bb0, 0x3762bd95, 0xd0e7184c, 0x5b829dcd, 0x4e81e0af, 0x14d2a6d9, 0xe6625493, 0xf101ba43, 0x5760f89d, 0xada25fdd, 0x67cb6bbe, 0x0ccd4990, 0xe6fdb1b1, 0x6e7f2abc, 0x7e00c4fd, 0xef9ba51f, 0xc86d47dd, 0x9c36e662, 0x8f99c5c9, 0xc42922e5, 0x7e1eee50, 0x6d7127db, 0x204ac3f1, 0x706cd566, 0xe411bbb3, 0x3d4a5c2d, 0xda0feb84, 0xb7e46732, 0x17fe4785, 0xd9d0b6a3
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer1[size + 1] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0xc53d9e7162346f7f, 0x96028fd99cb97e9a, 0xdbec1c909694adf4, 0xb9546d39a00ce870, 0x8b1bdf8d988d9ae0, 0xd7967eddb07c0031, 0xc0f080967751c90f, 0x281f745ce60c1317, 0x5ce81e71bed3fefa, 0xc33672b285d1833f, 0x643f332acdd3cbc1, 0xda074f285b8537d4, 0x24457517941a40e0, 0x242bb2f4bdb43dbd, 0xb6b86f98bb4fe515, 0x270d04cbee2ec8f5
#else
      0x0, 0xc53d9e71, 0x62346f7f, 0x96028fd9, 0x9cb97e9a, 0xdbec1c90, 0x9694adf4, 0xb9546d39, 0xa00ce870, 0x8b1bdf8d, 0x988d9ae0, 0xd7967edd, 0xb07c0031, 0xc0f08096, 0x7751c90f, 0x281f745c, 0xe60c1317, 0x5ce81e71, 0xbed3fefa, 0xc33672b2, 0x85d1833f, 0x643f332a, 0xcdd3cbc1, 0xda074f28, 0x5b8537d4, 0x24457517, 0x941a40e0, 0x242bb2f4, 0xbdb43dbd, 0xb6b86f98, 0xbb4fe515, 0x270d04cb, 0xee2ec8f5
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbe03fdb0ac1ce5ab, 0xd058318fa806b194, 0xc3e270f9f1dd4b04, 0xec29e238cbf84d45, 0x3d3eb648d10c24ab, 0x5b26d87ad2b1f752, 0x6011abccb225a7e0, 0x2b77609e39010149, 0x25f22ae422fb9e8f, 0x0a813ccb047d8176, 0x623b0ec6b0061bdb, 0x151a7db470a14793, 0x67dc005fd1e6836e, 0x61712d35cf7c984e, 0x90ab35bb468753b1, 0x49de18934bda7956
#else
      0xbe03fdb0, 0xac1ce5ab, 0xd058318f, 0xa806b194, 0xc3e270f9, 0xf1dd4b04, 0xec29e238, 0xcbf84d45, 0x3d3eb648, 0xd10c24ab, 0x5b26d87a, 0xd2b1f752, 0x6011abcc, 0xb225a7e0, 0x2b77609e, 0x39010149, 0x25f22ae4, 0x22fb9e8f, 0x0a813ccb, 0x047d8176, 0x623b0ec6, 0xb0061bdb, 0x151a7db4, 0x70a14793, 0x67dc005f, 0xd1e6836e, 0x61712d35, 0xcf7c984e, 0x90ab35bb, 0x468753b1, 0x49de1893, 0x4bda7956
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x064957b4df057431, 0xb423489b19db106f, 0xe37049d86976c4d3, 0xfcf5eb3482bbf573, 0x2baa4f9e113d4ca2, 0x64044caeee9116d0, 0x3f05f5d1948020f3, 0x8e15c3a24f92bac4, 0x634082bc3318d308, 0xdb05ff152dc60124, 0xfdbcf3d3625af35b, 0x0c473d0ca728102d, 0xe158904417a7410c, 0x5f87b0ffb805d6ad, 0xdf39a75f4d75302e, 0x0fc88661663ae62c
#else
      0x064957b4, 0xdf057431, 0xb423489b, 0x19db106f, 0xe37049d8, 0x6976c4d3, 0xfcf5eb34, 0x82bbf573, 0x2baa4f9e, 0x113d4ca2, 0x64044cae, 0xee9116d0, 0x3f05f5d1, 0x948020f3, 0x8e15c3a2, 0x4f92bac4, 0x634082bc, 0x3318d308, 0xdb05ff15, 0x2dc60124, 0xfdbcf3d3, 0x625af35b, 0x0c473d0c, 0xa728102d, 0xe1589044, 0x17a7410c, 0x5f87b0ff, 0xb805d6ad, 0xdf39a75f, 0x4d75302e, 0x0fc88661, 0x663ae62c
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer2[size + 1] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0xc44d55658b2259dd, 0x847b7a2ac1e1c204, 0xa752bad25b540fd8, 0xe91fcd6d4eb442b8, 0x68e905e6e249714d, 0xbf2b2529c1430e22, 0x9f17a19e46a5c8d3, 0xb98d24408893bc0d, 0x8932ada056147197, 0xe5873be03243829b, 0x5ff8029a12610f36, 0x2161bac117c957c1, 0x493490a3e98dc47a, 0xc0f8de3587826efc, 0x6fe4dd1a93fc83df, 0x59a69ef4b2155f82
#else
      0x0, 0xc44d5565, 0x8b2259dd, 0x847b7a2a, 0xc1e1c204, 0xa752bad2, 0x5b540fd8, 0xe91fcd6d, 0x4eb442b8, 0x68e905e6, 0xe249714d, 0xbf2b2529, 0xc1430e22, 0x9f17a19e, 0x46a5c8d3, 0xb98d2440, 0x8893bc0d, 0x8932ada0, 0x56147197, 0xe5873be0, 0x3243829b, 0x5ff8029a, 0x12610f36, 0x2161bac1, 0x17c957c1, 0x493490a3, 0xe98dc47a, 0xc0f8de35, 0x87826efc, 0x6fe4dd1a, 0x93fc83df, 0x59a69ef4, 0xb2155f82
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x9fc5b0ca33900a2b, 0xbf4c4e57b6c23c4c, 0x0b4dcc1ebb69bfbd, 0x12f1445648bace8a, 0xc40bfa06a4adca39, 0x57a2057125397f57, 0xb89dfab1e765dc13, 0xcb211b0c581889d4, 0x7f91660294ecdb32, 0x07dbe5de9ba38675, 0xff23ca68e4a2c1bc, 0x38ae76d9beef6cac, 0x784fe5d50478f162, 0x379f0b433a2b19e3, 0xeb7fb1f829b510a3, 0x2886d56f9b3f69a6
#else
      0x9fc5b0ca, 0x33900a2b, 0xbf4c4e57, 0xb6c23c4c, 0x0b4dcc1e, 0xbb69bfbd, 0x12f14456, 0x48bace8a, 0xc40bfa06, 0xa4adca39, 0x57a20571, 0x25397f57, 0xb89dfab1, 0xe765dc13, 0xcb211b0c, 0x581889d4, 0x7f916602, 0x94ecdb32, 0x07dbe5de, 0x9ba38675, 0xff23ca68, 0xe4a2c1bc, 0x38ae76d9, 0xbeef6cac, 0x784fe5d5, 0x0478f162, 0x379f0b43, 0x3a2b19e3, 0xeb7fb1f8, 0x29b510a3, 0x2886d56f, 0x9b3f69a6
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0f97e99a1b45970c, 0xc5f2718dad769307, 0xc5a34b66b1e9a62d, 0xbe28ceeb5e0807d2, 0x99fd6d5e1a7e756f, 0x45a26c98df0a4b4e, 0x03d015bfdfd76aef, 0x604f6392ef8b5ed6, 0x0dda9d8e787c8ddf, 0xb87f1719033e07f1, 0xef3b9e3fdef34d7f, 0x83175a589208cb39, 0xeb83158f484d4c41, 0xfd6474fd6867c909, 0xa6110b290c8a336c, 0x2a54e48f3015e9fc
#else
      0x0f97e99a, 0x1b45970c, 0xc5f2718d, 0xad769307, 0xc5a34b66, 0xb1e9a62d, 0xbe28ceeb, 0x5e0807d2, 0x99fd6d5e, 0x1a7e756f, 0x45a26c98, 0xdf0a4b4e, 0x03d015bf, 0xdfd76aef, 0x604f6392, 0xef8b5ed6, 0x0dda9d8e, 0x787c8ddf, 0xb87f1719, 0x033e07f1, 0xef3b9e3f, 0xdef34d7f, 0x83175a58, 0x9208cb39, 0xeb83158f, 0x484d4c41, 0xfd6474fd, 0x6867c909, 0xa6110b29, 0x0c8a336c, 0x2a54e48f, 0x3015e9fc
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer3[size + 1] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0xaf5d9a644ed5a138, 0x853ebfe56438cf53, 0xd0f117856d5365ea, 0xd11a1341a6c2d65d, 0x5e096764bf2c3fa8, 0x9d44720a0443caa5, 0xbc6e1071c73d4703, 0x2b707e9f47a3e8aa, 0x8d6c03910d696911, 0xc05afcf79ee18e67, 0xee5f68a8c3960f3b, 0xbbc5d13250f837e6, 0x63d2fb644cc63da4, 0x35038040a292e2ed, 0x9190bd21363f440f, 0x52dbb9fecb5553a2
#else
      0x0, 0xaf5d9a64, 0x4ed5a138, 0x853ebfe5, 0x6438cf53, 0xd0f11785, 0x6d5365ea, 0xd11a1341, 0xa6c2d65d, 0x5e096764, 0xbf2c3fa8, 0x9d44720a, 0x0443caa5, 0xbc6e1071, 0xc73d4703, 0x2b707e9f, 0x47a3e8aa, 0x8d6c0391, 0x0d696911, 0xc05afcf7, 0x9ee18e67, 0xee5f68a8, 0xc3960f3b, 0xbbc5d132, 0x50f837e6, 0x63d2fb64, 0x4cc63da4, 0x35038040, 0xa292e2ed, 0x9190bd21, 0x363f440f, 0x52dbb9fe, 0xcb5553a2
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x30163e90ff6b9c55, 0x6a381e0ed7bdd12a, 0xf1467b2c0a972be1, 0x3bc5af9abb4dde46, 0x5bea5d42a36e2f78, 0xfba76e070acceb0e, 0xbebf5d2b9ce13c15, 0x76e3cc0141f46635, 0xf4fa06221a4d9fc1, 0xb6459df49213fdea, 0x80b494a249d06f8f, 0x3ec8c9d022ee7afe, 0xdacc891bc133fb97, 0xb14a81177e84f07c, 0x90c5def7f86736c0, 0x568c15b7d91d4e9b
#else
      0x30163e90, 0xff6b9c55, 0x6a381e0e, 0xd7bdd12a, 0xf1467b2c, 0x0a972be1, 0x3bc5af9a, 0xbb4dde46, 0x5bea5d42, 0xa36e2f78, 0xfba76e07, 0x0acceb0e, 0xbebf5d2b, 0x9ce13c15, 0x76e3cc01, 0x41f46635, 0xf4fa0622, 0x1a4d9fc1, 0xb6459df4, 0x9213fdea, 0x80b494a2, 0x49d06f8f, 0x3ec8c9d0, 0x22ee7afe, 0xdacc891b, 0xc133fb97, 0xb14a8117, 0x7e84f07c, 0x90c5def7, 0xf86736c0, 0x568c15b7, 0xd91d4e9b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0e88b400bea04c3c, 0x03fb127d21269b4e, 0x7011a81b4fc71deb, 0xa022722d0bfb9053, 0x70ff915ffe506c39, 0x0c450ace51967535, 0xd1788844721957e5, 0xdaa3b2c1bed90213, 0x73e5e64fb1f2267a, 0x12849cb349804395, 0x659dc313e1b75f1c, 0xa44e4348f828c63e, 0x73e3afba3829e95c, 0x009faec2dd448938, 0x70e93ba65556c360, 0xb733f3228fde0b81
#else
      0x0e88b400, 0xbea04c3c, 0x03fb127d, 0x21269b4e, 0x7011a81b, 0x4fc71deb, 0xa022722d, 0x0bfb9053, 0x70ff915f, 0xfe506c39, 0x0c450ace, 0x51967535, 0xd1788844, 0x721957e5, 0xdaa3b2c1, 0xbed90213, 0x73e5e64f, 0xb1f2267a, 0x12849cb3, 0x49804395, 0x659dc313, 0xe1b75f1c, 0xa44e4348, 0xf828c63e, 0x73e3afba, 0x3829e95c, 0x009faec2, 0xdd448938, 0x70e93ba6, 0x5556c360, 0xb733f322, 0x8fde0b81
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer4[size + 1] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x3e9ef291be0be891, 0x6e33308bf8e46c79, 0x615823475a5e49cc, 0xdbe821c7c7496e99, 0xcce9eea2a1be9bb2, 0x07ec78d55c636044, 0x9037e5700efa93fb, 0x51877ec300cd6849, 0x68dfec71cc3fc63b, 0xc8ca3aa7db94417f, 0xe65257b62b87ceab, 0xe3170d191b17413d, 0x4eb038d5f95de4f3, 0xb1ea2fda5bc979b5, 0x01af1a9e4dbdfa21, 0x0dc008da68fb5a1c
#else
      0x0, 0x3e9ef291, 0xbe0be891, 0x6e33308b, 0xf8e46c79, 0x61582347, 0x5a5e49cc, 0xdbe821c7, 0xc7496e99, 0xcce9eea2, 0xa1be9bb2, 0x07ec78d5, 0x5c636044, 0x9037e570, 0x0efa93fb, 0x51877ec3, 0x00cd6849, 0x68dfec71, 0xcc3fc63b, 0xc8ca3aa7, 0xdb94417f, 0xe65257b6, 0x2b87ceab, 0xe3170d19, 0x1b17413d, 0x4eb038d5, 0xf95de4f3, 0xb1ea2fda, 0x5bc979b5, 0x01af1a9e, 0x4dbdfa21, 0x0dc008da, 0x68fb5a1c
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t resBuffer[size + 1] = {0};
    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t y = {
        .negative = false,
        .buffer = y1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = size + 1
    };

    rucrypto_gost_34_10_add(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(resBuffer, expectedBuffer1, size + 1));
    x.buffer = x2Buffer;
    y.buffer = y2Buffer;
    rucrypto_gost_34_10_add(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(resBuffer, expectedBuffer2, size + 1));
    x.buffer = x3Buffer;
    y.buffer = y3Buffer;
    rucrypto_gost_34_10_add(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(resBuffer, expectedBuffer3, size + 1));
    x.buffer = x4Buffer;
    y.buffer = y4Buffer;
    rucrypto_gost_34_10_add(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(resBuffer, expectedBuffer4, size + 1));
}

TEST(Gost3410BasicMathTests, AddWithOverflowTest) {
    rucrypto_gost_34_10_big_num_underlying_t xBuffer[] = {
        RUCRYPTO_GOST_34_10_BIG_NUM_UNDERLYING_TYPE_MAX_VALUE, 0
    };
    rucrypto_gost_34_10_big_num_underlying_t yBuffer[] = {
        1, 0
    };
    rucrypto_gost_34_10_big_num_underlying_t expectedBuffer[] = {
        1, 0, 0
    };
    rucrypto_gost_34_10_big_num_underlying_t resBuffer[] = {
        0, 0, 0
    };
    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = xBuffer,
        .size = 2
    };
    rucrypto_gost_34_10_big_num_t y = {
        .negative = false,
        .buffer = yBuffer,
        .size = 2
    };
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = 3
    };
    rucrypto_gost_34_10_add(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer, 3));
}

TEST(Gost3410BasicMathTests, AddNoCarryTest) {
    constexpr size_t size = 1024 / 8 / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbfa0905ea10c5a06, 0xd3e3f4296556c105, 0x0b0504443b121027, 0x6ad28c8a8b3a4196, 0xa4b98af9a78be09d, 0x8035864002d9a054, 0x592514d86a847f7e, 0x4121c2ab778ce85a, 0xdee75973cf3859da, 0xfac92ad4e99a9cdc, 0xd4a56d6109aaa8dc, 0x5be860d7ee140ff9, 0x03fab12623ad6b79, 0x4019f7418069e18f, 0xdca88414036b7de3, 0x0f0ebd46145e1252
#else
      0xbfa0905e, 0xa10c5a06, 0xd3e3f429, 0x6556c105, 0x0b050444, 0x3b121027, 0x6ad28c8a, 0x8b3a4196, 0xa4b98af9, 0xa78be09d, 0x80358640, 0x02d9a054, 0x592514d8, 0x6a847f7e, 0x4121c2ab, 0x778ce85a, 0xdee75973, 0xcf3859da, 0xfac92ad4, 0xe99a9cdc, 0xd4a56d61, 0x09aaa8dc, 0x5be860d7, 0xee140ff9, 0x03fab126, 0x23ad6b79, 0x4019f741, 0x8069e18f, 0xdca88414, 0x036b7de3, 0x0f0ebd46, 0x145e1252
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x059d0e12c1281578, 0xc21e9bb03762bd95, 0xd0e7184c5b829dcd, 0x4e81e0af14d2a6d9, 0xe6625493f101ba43, 0x5760f89dada25fdd, 0x67cb6bbe0ccd4990, 0xe6fdb1b16e7f2abc, 0x7e00c4fdef9ba51f, 0xc86d47dd9c36e662, 0x8f99c5c9c42922e5, 0x7e1eee506d7127db, 0x204ac3f1706cd566, 0xe411bbb33d4a5c2d, 0xda0feb84b7e46732, 0x17fe4785d9d0b6a3
#else
      0x059d0e12, 0xc1281578, 0xc21e9bb0, 0x3762bd95, 0xd0e7184c, 0x5b829dcd, 0x4e81e0af, 0x14d2a6d9, 0xe6625493, 0xf101ba43, 0x5760f89d, 0xada25fdd, 0x67cb6bbe, 0x0ccd4990, 0xe6fdb1b1, 0x6e7f2abc, 0x7e00c4fd, 0xef9ba51f, 0xc86d47dd, 0x9c36e662, 0x8f99c5c9, 0xc42922e5, 0x7e1eee50, 0x6d7127db, 0x204ac3f1, 0x706cd566, 0xe411bbb3, 0x3d4a5c2d, 0xda0feb84, 0xb7e46732, 0x17fe4785, 0xd9d0b6a3
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer1[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xc53d9e7162346f7f, 0x96028fd99cb97e9a, 0xdbec1c909694adf4, 0xb9546d39a00ce870, 0x8b1bdf8d988d9ae0, 0xd7967eddb07c0031, 0xc0f080967751c90f, 0x281f745ce60c1317, 0x5ce81e71bed3fefa, 0xc33672b285d1833f, 0x643f332acdd3cbc1, 0xda074f285b8537d4, 0x24457517941a40e0, 0x242bb2f4bdb43dbd, 0xb6b86f98bb4fe515, 0x270d04cbee2ec8f5
#else
      0xc53d9e71, 0x62346f7f, 0x96028fd9, 0x9cb97e9a, 0xdbec1c90, 0x9694adf4, 0xb9546d39, 0xa00ce870, 0x8b1bdf8d, 0x988d9ae0, 0xd7967edd, 0xb07c0031, 0xc0f08096, 0x7751c90f, 0x281f745c, 0xe60c1317, 0x5ce81e71, 0xbed3fefa, 0xc33672b2, 0x85d1833f, 0x643f332a, 0xcdd3cbc1, 0xda074f28, 0x5b8537d4, 0x24457517, 0x941a40e0, 0x242bb2f4, 0xbdb43dbd, 0xb6b86f98, 0xbb4fe515, 0x270d04cb, 0xee2ec8f5
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbe03fdb0ac1ce5ab, 0xd058318fa806b194, 0xc3e270f9f1dd4b04, 0xec29e238cbf84d45, 0x3d3eb648d10c24ab, 0x5b26d87ad2b1f752, 0x6011abccb225a7e0, 0x2b77609e39010149, 0x25f22ae422fb9e8f, 0x0a813ccb047d8176, 0x623b0ec6b0061bdb, 0x151a7db470a14793, 0x67dc005fd1e6836e, 0x61712d35cf7c984e, 0x90ab35bb468753b1, 0x49de18934bda7956
#else
      0xbe03fdb0, 0xac1ce5ab, 0xd058318f, 0xa806b194, 0xc3e270f9, 0xf1dd4b04, 0xec29e238, 0xcbf84d45, 0x3d3eb648, 0xd10c24ab, 0x5b26d87a, 0xd2b1f752, 0x6011abcc, 0xb225a7e0, 0x2b77609e, 0x39010149, 0x25f22ae4, 0x22fb9e8f, 0x0a813ccb, 0x047d8176, 0x623b0ec6, 0xb0061bdb, 0x151a7db4, 0x70a14793, 0x67dc005f, 0xd1e6836e, 0x61712d35, 0xcf7c984e, 0x90ab35bb, 0x468753b1, 0x49de1893, 0x4bda7956
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x064957b4df057431, 0xb423489b19db106f, 0xe37049d86976c4d3, 0xfcf5eb3482bbf573, 0x2baa4f9e113d4ca2, 0x64044caeee9116d0, 0x3f05f5d1948020f3, 0x8e15c3a24f92bac4, 0x634082bc3318d308, 0xdb05ff152dc60124, 0xfdbcf3d3625af35b, 0x0c473d0ca728102d, 0xe158904417a7410c, 0x5f87b0ffb805d6ad, 0xdf39a75f4d75302e, 0x0fc88661663ae62c
#else
      0x064957b4, 0xdf057431, 0xb423489b, 0x19db106f, 0xe37049d8, 0x6976c4d3, 0xfcf5eb34, 0x82bbf573, 0x2baa4f9e, 0x113d4ca2, 0x64044cae, 0xee9116d0, 0x3f05f5d1, 0x948020f3, 0x8e15c3a2, 0x4f92bac4, 0x634082bc, 0x3318d308, 0xdb05ff15, 0x2dc60124, 0xfdbcf3d3, 0x625af35b, 0x0c473d0c, 0xa728102d, 0xe1589044, 0x17a7410c, 0x5f87b0ff, 0xb805d6ad, 0xdf39a75f, 0x4d75302e, 0x0fc88661, 0x663ae62c
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer2[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xc44d55658b2259dd, 0x847b7a2ac1e1c204, 0xa752bad25b540fd8, 0xe91fcd6d4eb442b8, 0x68e905e6e249714d, 0xbf2b2529c1430e22, 0x9f17a19e46a5c8d3, 0xb98d24408893bc0d, 0x8932ada056147197, 0xe5873be03243829b, 0x5ff8029a12610f36, 0x2161bac117c957c1, 0x493490a3e98dc47a, 0xc0f8de3587826efc, 0x6fe4dd1a93fc83df, 0x59a69ef4b2155f82
#else
      0xc44d5565, 0x8b2259dd, 0x847b7a2a, 0xc1e1c204, 0xa752bad2, 0x5b540fd8, 0xe91fcd6d, 0x4eb442b8, 0x68e905e6, 0xe249714d, 0xbf2b2529, 0xc1430e22, 0x9f17a19e, 0x46a5c8d3, 0xb98d2440, 0x8893bc0d, 0x8932ada0, 0x56147197, 0xe5873be0, 0x3243829b, 0x5ff8029a, 0x12610f36, 0x2161bac1, 0x17c957c1, 0x493490a3, 0xe98dc47a, 0xc0f8de35, 0x87826efc, 0x6fe4dd1a, 0x93fc83df, 0x59a69ef4, 0xb2155f82
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x9fc5b0ca33900a2b, 0xbf4c4e57b6c23c4c, 0x0b4dcc1ebb69bfbd, 0x12f1445648bace8a, 0xc40bfa06a4adca39, 0x57a2057125397f57, 0xb89dfab1e765dc13, 0xcb211b0c581889d4, 0x7f91660294ecdb32, 0x07dbe5de9ba38675, 0xff23ca68e4a2c1bc, 0x38ae76d9beef6cac, 0x784fe5d50478f162, 0x379f0b433a2b19e3, 0xeb7fb1f829b510a3, 0x2886d56f9b3f69a6
#else
      0x9fc5b0ca, 0x33900a2b, 0xbf4c4e57, 0xb6c23c4c, 0x0b4dcc1e, 0xbb69bfbd, 0x12f14456, 0x48bace8a, 0xc40bfa06, 0xa4adca39, 0x57a20571, 0x25397f57, 0xb89dfab1, 0xe765dc13, 0xcb211b0c, 0x581889d4, 0x7f916602, 0x94ecdb32, 0x07dbe5de, 0x9ba38675, 0xff23ca68, 0xe4a2c1bc, 0x38ae76d9, 0xbeef6cac, 0x784fe5d5, 0x0478f162, 0x379f0b43, 0x3a2b19e3, 0xeb7fb1f8, 0x29b510a3, 0x2886d56f, 0x9b3f69a6
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0f97e99a1b45970c, 0xc5f2718dad769307, 0xc5a34b66b1e9a62d, 0xbe28ceeb5e0807d2, 0x99fd6d5e1a7e756f, 0x45a26c98df0a4b4e, 0x03d015bfdfd76aef, 0x604f6392ef8b5ed6, 0x0dda9d8e787c8ddf, 0xb87f1719033e07f1, 0xef3b9e3fdef34d7f, 0x83175a589208cb39, 0xeb83158f484d4c41, 0xfd6474fd6867c909, 0xa6110b290c8a336c, 0x2a54e48f3015e9fc
#else
      0x0f97e99a, 0x1b45970c, 0xc5f2718d, 0xad769307, 0xc5a34b66, 0xb1e9a62d, 0xbe28ceeb, 0x5e0807d2, 0x99fd6d5e, 0x1a7e756f, 0x45a26c98, 0xdf0a4b4e, 0x03d015bf, 0xdfd76aef, 0x604f6392, 0xef8b5ed6, 0x0dda9d8e, 0x787c8ddf, 0xb87f1719, 0x033e07f1, 0xef3b9e3f, 0xdef34d7f, 0x83175a58, 0x9208cb39, 0xeb83158f, 0x484d4c41, 0xfd6474fd, 0x6867c909, 0xa6110b29, 0x0c8a336c, 0x2a54e48f, 0x3015e9fc
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer3[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xaf5d9a644ed5a138, 0x853ebfe56438cf53, 0xd0f117856d5365ea, 0xd11a1341a6c2d65d, 0x5e096764bf2c3fa8, 0x9d44720a0443caa5, 0xbc6e1071c73d4703, 0x2b707e9f47a3e8aa, 0x8d6c03910d696911, 0xc05afcf79ee18e67, 0xee5f68a8c3960f3b, 0xbbc5d13250f837e6, 0x63d2fb644cc63da4, 0x35038040a292e2ed, 0x9190bd21363f440f, 0x52dbb9fecb5553a2
#else
      0xaf5d9a64, 0x4ed5a138, 0x853ebfe5, 0x6438cf53, 0xd0f11785, 0x6d5365ea, 0xd11a1341, 0xa6c2d65d, 0x5e096764, 0xbf2c3fa8, 0x9d44720a, 0x0443caa5, 0xbc6e1071, 0xc73d4703, 0x2b707e9f, 0x47a3e8aa, 0x8d6c0391, 0x0d696911, 0xc05afcf7, 0x9ee18e67, 0xee5f68a8, 0xc3960f3b, 0xbbc5d132, 0x50f837e6, 0x63d2fb64, 0x4cc63da4, 0x35038040, 0xa292e2ed, 0x9190bd21, 0x363f440f, 0x52dbb9fe, 0xcb5553a2
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x30163e90ff6b9c55, 0x6a381e0ed7bdd12a, 0xf1467b2c0a972be1, 0x3bc5af9abb4dde46, 0x5bea5d42a36e2f78, 0xfba76e070acceb0e, 0xbebf5d2b9ce13c15, 0x76e3cc0141f46635, 0xf4fa06221a4d9fc1, 0xb6459df49213fdea, 0x80b494a249d06f8f, 0x3ec8c9d022ee7afe, 0xdacc891bc133fb97, 0xb14a81177e84f07c, 0x90c5def7f86736c0, 0x568c15b7d91d4e9b
#else
      0x30163e90, 0xff6b9c55, 0x6a381e0e, 0xd7bdd12a, 0xf1467b2c, 0x0a972be1, 0x3bc5af9a, 0xbb4dde46, 0x5bea5d42, 0xa36e2f78, 0xfba76e07, 0x0acceb0e, 0xbebf5d2b, 0x9ce13c15, 0x76e3cc01, 0x41f46635, 0xf4fa0622, 0x1a4d9fc1, 0xb6459df4, 0x9213fdea, 0x80b494a2, 0x49d06f8f, 0x3ec8c9d0, 0x22ee7afe, 0xdacc891b, 0xc133fb97, 0xb14a8117, 0x7e84f07c, 0x90c5def7, 0xf86736c0, 0x568c15b7, 0xd91d4e9b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0e88b400bea04c3c, 0x03fb127d21269b4e, 0x7011a81b4fc71deb, 0xa022722d0bfb9053, 0x70ff915ffe506c39, 0x0c450ace51967535, 0xd1788844721957e5, 0xdaa3b2c1bed90213, 0x73e5e64fb1f2267a, 0x12849cb349804395, 0x659dc313e1b75f1c, 0xa44e4348f828c63e, 0x73e3afba3829e95c, 0x009faec2dd448938, 0x70e93ba65556c360, 0xb733f3228fde0b81
#else
      0x0e88b400, 0xbea04c3c, 0x03fb127d, 0x21269b4e, 0x7011a81b, 0x4fc71deb, 0xa022722d, 0x0bfb9053, 0x70ff915f, 0xfe506c39, 0x0c450ace, 0x51967535, 0xd1788844, 0x721957e5, 0xdaa3b2c1, 0xbed90213, 0x73e5e64f, 0xb1f2267a, 0x12849cb3, 0x49804395, 0x659dc313, 0xe1b75f1c, 0xa44e4348, 0xf828c63e, 0x73e3afba, 0x3829e95c, 0x009faec2, 0xdd448938, 0x70e93ba6, 0x5556c360, 0xb733f322, 0x8fde0b81
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer4[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x3e9ef291be0be891, 0x6e33308bf8e46c79, 0x615823475a5e49cc, 0xdbe821c7c7496e99, 0xcce9eea2a1be9bb2, 0x07ec78d55c636044, 0x9037e5700efa93fb, 0x51877ec300cd6849, 0x68dfec71cc3fc63b, 0xc8ca3aa7db94417f, 0xe65257b62b87ceab, 0xe3170d191b17413d, 0x4eb038d5f95de4f3, 0xb1ea2fda5bc979b5, 0x01af1a9e4dbdfa21, 0x0dc008da68fb5a1c
#else
      0x3e9ef291, 0xbe0be891, 0x6e33308b, 0xf8e46c79, 0x61582347, 0x5a5e49cc, 0xdbe821c7, 0xc7496e99, 0xcce9eea2, 0xa1be9bb2, 0x07ec78d5, 0x5c636044, 0x9037e570, 0x0efa93fb, 0x51877ec3, 0x00cd6849, 0x68dfec71, 0xcc3fc63b, 0xc8ca3aa7, 0xdb94417f, 0xe65257b6, 0x2b87ceab, 0xe3170d19, 0x1b17413d, 0x4eb038d5, 0xf95de4f3, 0xb1ea2fda, 0x5bc979b5, 0x01af1a9e, 0x4dbdfa21, 0x0dc008da, 0x68fb5a1c
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t resBuffer[size] = {0};
    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t y = {
        .negative = false,
        .buffer = y1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = size
    };

    rucrypto_gost_34_10_add_no_carry(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(resBuffer, expectedBuffer1, size));
    x.buffer = x2Buffer;
    y.buffer = y2Buffer;
    rucrypto_gost_34_10_add_no_carry(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(resBuffer, expectedBuffer2, size));
    x.buffer = x3Buffer;
    y.buffer = y3Buffer;
    rucrypto_gost_34_10_add_no_carry(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(resBuffer, expectedBuffer3, size));
    x.buffer = x4Buffer;
    y.buffer = y4Buffer;
    rucrypto_gost_34_10_add_no_carry(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(resBuffer, expectedBuffer4, size));
}

TEST(Gost3410BasicMathTests, AddNoCarryWithOverflowTest) {
    rucrypto_gost_34_10_big_num_underlying_t xBuffer[] = {
        RUCRYPTO_GOST_34_10_BIG_NUM_UNDERLYING_TYPE_MAX_VALUE, 0
    };
    rucrypto_gost_34_10_big_num_underlying_t yBuffer[] = {
        1, 0
    };
    rucrypto_gost_34_10_big_num_underlying_t expectedBuffer[] = {
        0, 0
    };
    rucrypto_gost_34_10_big_num_underlying_t resBuffer[] = {
        0, 0
    };
    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = xBuffer,
        .size = 2
    };
    rucrypto_gost_34_10_big_num_t y = {
        .negative = false,
        .buffer = yBuffer,
        .size = 2
    };
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = 2
    };
    rucrypto_gost_34_10_add_no_carry(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer, 2));
}

TEST(Gost3410BasicMathTests, SubTest) {
    constexpr size_t size = 1024 / 8 / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbfa0905ea10c5a06, 0xd3e3f4296556c105, 0x0b0504443b121027, 0x6ad28c8a8b3a4196, 0xa4b98af9a78be09d, 0x8035864002d9a054, 0x592514d86a847f7e, 0x4121c2ab778ce85a, 0xdee75973cf3859da, 0xfac92ad4e99a9cdc, 0xd4a56d6109aaa8dc, 0x5be860d7ee140ff9, 0x03fab12623ad6b79, 0x4019f7418069e18f, 0xdca88414036b7de3, 0x0f0ebd46145e1252
#else
      0xbfa0905e, 0xa10c5a06, 0xd3e3f429, 0x6556c105, 0x0b050444, 0x3b121027, 0x6ad28c8a, 0x8b3a4196, 0xa4b98af9, 0xa78be09d, 0x80358640, 0x02d9a054, 0x592514d8, 0x6a847f7e, 0x4121c2ab, 0x778ce85a, 0xdee75973, 0xcf3859da, 0xfac92ad4, 0xe99a9cdc, 0xd4a56d61, 0x09aaa8dc, 0x5be860d7, 0xee140ff9, 0x03fab126, 0x23ad6b79, 0x4019f741, 0x8069e18f, 0xdca88414, 0x036b7de3, 0x0f0ebd46, 0x145e1252
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x059d0e12c1281578, 0xc21e9bb03762bd95, 0xd0e7184c5b829dcd, 0x4e81e0af14d2a6d9, 0xe6625493f101ba43, 0x5760f89dada25fdd, 0x67cb6bbe0ccd4990, 0xe6fdb1b16e7f2abc, 0x7e00c4fdef9ba51f, 0xc86d47dd9c36e662, 0x8f99c5c9c42922e5, 0x7e1eee506d7127db, 0x204ac3f1706cd566, 0xe411bbb33d4a5c2d, 0xda0feb84b7e46732, 0x17fe4785d9d0b6a3
#else
      0x059d0e12, 0xc1281578, 0xc21e9bb0, 0x3762bd95, 0xd0e7184c, 0x5b829dcd, 0x4e81e0af, 0x14d2a6d9, 0xe6625493, 0xf101ba43, 0x5760f89d, 0xada25fdd, 0x67cb6bbe, 0x0ccd4990, 0xe6fdb1b1, 0x6e7f2abc, 0x7e00c4fd, 0xef9ba51f, 0xc86d47dd, 0x9c36e662, 0x8f99c5c9, 0xc42922e5, 0x7e1eee50, 0x6d7127db, 0x204ac3f1, 0x706cd566, 0xe411bbb3, 0x3d4a5c2d, 0xda0feb84, 0xb7e46732, 0x17fe4785, 0xd9d0b6a3
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer1[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xba03824bdfe4448e, 0x11c558792df4036f, 0x3a1debf7df8f725a, 0x1c50abdb76679abc, 0xbe573665b68a265a, 0x28d48da255374076, 0xf159a91a5db735ed, 0x5a2410fa090dbd9e, 0x60e69475df9cb4bb, 0x325be2f74d63b67a, 0x450ba797458185f6, 0xddc9728780a2e81d, 0xe3afed34b3409612, 0x5c083b8e431f8562, 0x0298988f4b8716b0, 0xf71075c03a8d5baf
#else
      0xba03824b, 0xdfe4448e, 0x11c55879, 0x2df4036f, 0x3a1debf7, 0xdf8f725a, 0x1c50abdb, 0x76679abc, 0xbe573665, 0xb68a265a, 0x28d48da2, 0x55374076, 0xf159a91a, 0x5db735ed, 0x5a2410fa, 0x090dbd9e, 0x60e69475, 0xdf9cb4bb, 0x325be2f7, 0x4d63b67a, 0x450ba797, 0x458185f6, 0xddc97287, 0x80a2e81d, 0xe3afed34, 0xb3409612, 0x5c083b8e, 0x431f8562, 0x0298988f, 0x4b8716b0, 0xf71075c0, 0x3a8d5baf
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbe03fdb0ac1ce5ab, 0xd058318fa806b194, 0xc3e270f9f1dd4b04, 0xec29e238cbf84d45, 0x3d3eb648d10c24ab, 0x5b26d87ad2b1f752, 0x6011abccb225a7e0, 0x2b77609e39010149, 0x25f22ae422fb9e8f, 0x0a813ccb047d8176, 0x623b0ec6b0061bdb, 0x151a7db470a14793, 0x67dc005fd1e6836e, 0x61712d35cf7c984e, 0x90ab35bb468753b1, 0x49de18934bda7956
#else
      0xbe03fdb0, 0xac1ce5ab, 0xd058318f, 0xa806b194, 0xc3e270f9, 0xf1dd4b04, 0xec29e238, 0xcbf84d45, 0x3d3eb648, 0xd10c24ab, 0x5b26d87a, 0xd2b1f752, 0x6011abcc, 0xb225a7e0, 0x2b77609e, 0x39010149, 0x25f22ae4, 0x22fb9e8f, 0x0a813ccb, 0x047d8176, 0x623b0ec6, 0xb0061bdb, 0x151a7db4, 0x70a14793, 0x67dc005f, 0xd1e6836e, 0x61712d35, 0xcf7c984e, 0x90ab35bb, 0x468753b1, 0x49de1893, 0x4bda7956
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x064957b4df057431, 0xb423489b19db106f, 0xe37049d86976c4d3, 0xfcf5eb3482bbf573, 0x2baa4f9e113d4ca2, 0x64044caeee9116d0, 0x3f05f5d1948020f3, 0x8e15c3a24f92bac4, 0x634082bc3318d308, 0xdb05ff152dc60124, 0xfdbcf3d3625af35b, 0x0c473d0ca728102d, 0xe158904417a7410c, 0x5f87b0ffb805d6ad, 0xdf39a75f4d75302e, 0x0fc88661663ae62c
#else
      0x064957b4, 0xdf057431, 0xb423489b, 0x19db106f, 0xe37049d8, 0x6976c4d3, 0xfcf5eb34, 0x82bbf573, 0x2baa4f9e, 0x113d4ca2, 0x64044cae, 0xee9116d0, 0x3f05f5d1, 0x948020f3, 0x8e15c3a2, 0x4f92bac4, 0x634082bc, 0x3318d308, 0xdb05ff15, 0x2dc60124, 0xfdbcf3d3, 0x625af35b, 0x0c473d0c, 0xa728102d, 0xe1589044, 0x17a7410c, 0x5f87b0ff, 0xb805d6ad, 0xdf39a75f, 0x4d75302e, 0x0fc88661, 0x663ae62c
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer2[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xb7baa5fbcd17717a, 0x1c34e8f48e2ba124, 0xe072272188668630, 0xef33f704493c57d2, 0x119466aabfced808, 0xf7228bcbe420e082, 0x210bb5fb1da586ec, 0x9d619cfbe96e4684, 0xc2b1a827efe2cb86, 0x2f7b3db5d6b78051, 0x647e1af34dab2880, 0x08d340a7c9793765, 0x8683701bba3f4262, 0x01e97c361776c1a0, 0xb1718e5bf9122383, 0x3a159231e59f932a
#else
      0xb7baa5fb, 0xcd17717a, 0x1c34e8f4, 0x8e2ba124, 0xe0722721, 0x88668630, 0xef33f704, 0x493c57d2, 0x119466aa, 0xbfced808, 0xf7228bcb, 0xe420e082, 0x210bb5fb, 0x1da586ec, 0x9d619cfb, 0xe96e4684, 0xc2b1a827, 0xefe2cb86, 0x2f7b3db5, 0xd6b78051, 0x647e1af3, 0x4dab2880, 0x08d340a7, 0xc9793765, 0x8683701b, 0xba3f4262, 0x01e97c36, 0x1776c1a0, 0xb1718e5b, 0xf9122383, 0x3a159231, 0xe59f932a
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x9fc5b0ca33900a2b, 0xbf4c4e57b6c23c4c, 0x0b4dcc1ebb69bfbd, 0x12f1445648bace8a, 0xc40bfa06a4adca39, 0x57a2057125397f57, 0xb89dfab1e765dc13, 0xcb211b0c581889d4, 0x7f91660294ecdb32, 0x07dbe5de9ba38675, 0xff23ca68e4a2c1bc, 0x38ae76d9beef6cac, 0x784fe5d50478f162, 0x379f0b433a2b19e3, 0xeb7fb1f829b510a3, 0x2886d56f9b3f69a6
#else
      0x9fc5b0ca, 0x33900a2b, 0xbf4c4e57, 0xb6c23c4c, 0x0b4dcc1e, 0xbb69bfbd, 0x12f14456, 0x48bace8a, 0xc40bfa06, 0xa4adca39, 0x57a20571, 0x25397f57, 0xb89dfab1, 0xe765dc13, 0xcb211b0c, 0x581889d4, 0x7f916602, 0x94ecdb32, 0x07dbe5de, 0x9ba38675, 0xff23ca68, 0xe4a2c1bc, 0x38ae76d9, 0xbeef6cac, 0x784fe5d5, 0x0478f162, 0x379f0b43, 0x3a2b19e3, 0xeb7fb1f8, 0x29b510a3, 0x2886d56f, 0x9b3f69a6
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0f97e99a1b45970c, 0xc5f2718dad769307, 0xc5a34b66b1e9a62d, 0xbe28ceeb5e0807d2, 0x99fd6d5e1a7e756f, 0x45a26c98df0a4b4e, 0x03d015bfdfd76aef, 0x604f6392ef8b5ed6, 0x0dda9d8e787c8ddf, 0xb87f1719033e07f1, 0xef3b9e3fdef34d7f, 0x83175a589208cb39, 0xeb83158f484d4c41, 0xfd6474fd6867c909, 0xa6110b290c8a336c, 0x2a54e48f3015e9fc
#else
      0x0f97e99a, 0x1b45970c, 0xc5f2718d, 0xad769307, 0xc5a34b66, 0xb1e9a62d, 0xbe28ceeb, 0x5e0807d2, 0x99fd6d5e, 0x1a7e756f, 0x45a26c98, 0xdf0a4b4e, 0x03d015bf, 0xdfd76aef, 0x604f6392, 0xef8b5ed6, 0x0dda9d8e, 0x787c8ddf, 0xb87f1719, 0x033e07f1, 0xef3b9e3f, 0xdef34d7f, 0x83175a58, 0x9208cb39, 0xeb83158f, 0x484d4c41, 0xfd6474fd, 0x6867c909, 0xa6110b29, 0x0c8a336c, 0x2a54e48f, 0x3015e9fc
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer3[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x902dc730184a731e, 0xf959dcca094ba944, 0x45aa80b80980198f, 0x54c8756aeab2c6b8, 0x2a0e8ca88a2f54ca, 0x11ff98d8462f3409, 0xb4cde4f2078e7124, 0x6ad1b779688d2afe, 0x71b6c8741c704d52, 0x4f5ccec598657e84, 0x0fe82c2905af743c, 0xb5971c812ce6a172, 0x8cccd045bc2ba520, 0x3a3a9645d1c350da, 0x456ea6cf1d2add36, 0xfe31f0e06b297faa
#else
      0x902dc730, 0x184a731e, 0xf959dcca, 0x094ba944, 0x45aa80b8, 0x0980198f, 0x54c8756a, 0xeab2c6b8, 0x2a0e8ca8, 0x8a2f54ca, 0x11ff98d8, 0x462f3409, 0xb4cde4f2, 0x078e7124, 0x6ad1b779, 0x688d2afe, 0x71b6c874, 0x1c704d52, 0x4f5ccec5, 0x98657e84, 0x0fe82c29, 0x05af743c, 0xb5971c81, 0x2ce6a172, 0x8cccd045, 0xbc2ba520, 0x3a3a9645, 0xd1c350da, 0x456ea6cf, 0x1d2add36, 0xfe31f0e0, 0x6b297faa
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x30163e90ff6b9c55, 0x6a381e0ed7bdd12a, 0xf1467b2c0a972be1, 0x3bc5af9abb4dde46, 0x5bea5d42a36e2f78, 0xfba76e070acceb0e, 0xbebf5d2b9ce13c15, 0x76e3cc0141f46635, 0xf4fa06221a4d9fc1, 0xb6459df49213fdea, 0x80b494a249d06f8f, 0x3ec8c9d022ee7afe, 0xdacc891bc133fb97, 0xb14a81177e84f07c, 0x90c5def7f86736c0, 0x568c15b7d91d4e9b
#else
      0x30163e90, 0xff6b9c55, 0x6a381e0e, 0xd7bdd12a, 0xf1467b2c, 0x0a972be1, 0x3bc5af9a, 0xbb4dde46, 0x5bea5d42, 0xa36e2f78, 0xfba76e07, 0x0acceb0e, 0xbebf5d2b, 0x9ce13c15, 0x76e3cc01, 0x41f46635, 0xf4fa0622, 0x1a4d9fc1, 0xb6459df4, 0x9213fdea, 0x80b494a2, 0x49d06f8f, 0x3ec8c9d0, 0x22ee7afe, 0xdacc891b, 0xc133fb97, 0xb14a8117, 0x7e84f07c, 0x90c5def7, 0xf86736c0, 0x568c15b7, 0xd91d4e9b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0e88b400bea04c3c, 0x03fb127d21269b4e, 0x7011a81b4fc71deb, 0xa022722d0bfb9053, 0x70ff915ffe506c39, 0x0c450ace51967535, 0xd1788844721957e5, 0xdaa3b2c1bed90213, 0x73e5e64fb1f2267a, 0x12849cb349804395, 0x659dc313e1b75f1c, 0xa44e4348f828c63e, 0x73e3afba3829e95c, 0x009faec2dd448938, 0x70e93ba65556c360, 0xb733f3228fde0b81
#else
      0x0e88b400, 0xbea04c3c, 0x03fb127d, 0x21269b4e, 0x7011a81b, 0x4fc71deb, 0xa022722d, 0x0bfb9053, 0x70ff915f, 0xfe506c39, 0x0c450ace, 0x51967535, 0xd1788844, 0x721957e5, 0xdaa3b2c1, 0xbed90213, 0x73e5e64f, 0xb1f2267a, 0x12849cb3, 0x49804395, 0x659dc313, 0xe1b75f1c, 0xa44e4348, 0xf828c63e, 0x73e3afba, 0x3829e95c, 0x009faec2, 0xdd448938, 0x70e93ba6, 0x5556c360, 0xb733f322, 0x8fde0b81
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer4[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x218d8a9040cb5019, 0x663d0b91b69735dc, 0x8134d310bad00df5, 0x9ba33d6daf524df2, 0xeaeacbe2a51dc33f, 0xef626338b93675d8, 0xed46d4e72ac7e42f, 0x9c40193f831b6422, 0x81141fd2685b7947, 0xa3c101414893ba55, 0x1b16d18e68191072, 0x9a7a86872ac5b4c0, 0x66e8d961890a123b, 0xb0aad254a1406744, 0x1fdca351a310735f, 0x9f582295493f431a
#else
      0x218d8a90, 0x40cb5019, 0x663d0b91, 0xb69735dc, 0x8134d310, 0xbad00df5, 0x9ba33d6d, 0xaf524df2, 0xeaeacbe2, 0xa51dc33f, 0xef626338, 0xb93675d8, 0xed46d4e7, 0x2ac7e42f, 0x9c40193f, 0x831b6422, 0x81141fd2, 0x685b7947, 0xa3c10141, 0x4893ba55, 0x1b16d18e, 0x68191072, 0x9a7a8687, 0x2ac5b4c0, 0x66e8d961, 0x890a123b, 0xb0aad254, 0xa1406744, 0x1fdca351, 0xa310735f, 0x9f582295, 0x493f431a
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t resBuffer[size];

    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t y = {
        .negative = false,
        .buffer = y1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = size
    };
    rucrypto_gost_34_10_sub(&x, &y, &res);
    EXPECT_FALSE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer1, size));
    x.buffer = x2Buffer;
    y.buffer = y2Buffer;
    rucrypto_gost_34_10_sub(&x, &y, &res);
    EXPECT_FALSE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer2, size));
    x.buffer = x3Buffer;
    y.buffer = y3Buffer;
    rucrypto_gost_34_10_sub(&x, &y, &res);
    EXPECT_FALSE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer3, size));
    x.buffer = x4Buffer;
    y.buffer = y4Buffer;
    rucrypto_gost_34_10_sub(&x, &y, &res);
    EXPECT_FALSE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer4, size));

    x.buffer = x1Buffer;
    y.buffer = y1Buffer;
    rucrypto_gost_34_10_sub(&y, &x, &res);
    EXPECT_TRUE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer1, size));
    x.buffer = x2Buffer;
    y.buffer = y2Buffer;
    rucrypto_gost_34_10_sub(&y, &x, &res);
    EXPECT_TRUE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer2, size));
    x.buffer = x3Buffer;
    y.buffer = y3Buffer;
    rucrypto_gost_34_10_sub(&y, &x, &res);
    EXPECT_TRUE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer3, size));
    x.buffer = x4Buffer;
    y.buffer = y4Buffer;
    rucrypto_gost_34_10_sub(&y, &x, &res);
    EXPECT_TRUE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer4, size));
}

TEST(Gost3410BasicMathTests, Mul1Test) {
    constexpr size_t size = 1024 / 8 / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbfa0905ea10c5a06, 0xd3e3f4296556c105, 0x0b0504443b121027, 0x6ad28c8a8b3a4196, 0xa4b98af9a78be09d, 0x8035864002d9a054, 0x592514d86a847f7e, 0x4121c2ab778ce85a, 0xdee75973cf3859da, 0xfac92ad4e99a9cdc, 0xd4a56d6109aaa8dc, 0x5be860d7ee140ff9, 0x03fab12623ad6b79, 0x4019f7418069e18f, 0xdca88414036b7de3, 0x0f0ebd46145e1252
#else
      0xbfa0905e, 0xa10c5a06, 0xd3e3f429, 0x6556c105, 0x0b050444, 0x3b121027, 0x6ad28c8a, 0x8b3a4196, 0xa4b98af9, 0xa78be09d, 0x80358640, 0x02d9a054, 0x592514d8, 0x6a847f7e, 0x4121c2ab, 0x778ce85a, 0xdee75973, 0xcf3859da, 0xfac92ad4, 0xe99a9cdc, 0xd4a56d61, 0x09aaa8dc, 0x5be860d7, 0xee140ff9, 0x03fab126, 0x23ad6b79, 0x4019f741, 0x8069e18f, 0xdca88414, 0x036b7de3, 0x0f0ebd46, 0x145e1252
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x6c6d8154ce68fc2d
#else
      0x57c5cf92
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer1[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x5129b514c82ca09b, 0xd11742b45a47c7e0, 0x6b4e54bd6deff5dd, 0x813feb2de1ae3fcd, 0x22795deaba72cb80, 0x7ee50b1856840267, 0xb89499a9d8d5694e, 0x0da00510261bf232, 0x3e78806be67eb9ae, 0x7a52ba7c53eb2522, 0x5982829c54355bf9, 0xc5a29e33322058a4, 0x15d09e819d6df9c1, 0x236156363d4baa5e, 0x34a5dc4f3277bc68, 0x1c412811c9d16511, 0xebc78f3b9be1f06a
#else
      0x41b3a2ff, 0x5fdbfb4a, 0x6f1a639f, 0x1fba2fe8, 0x312d5818, 0xcfe43311, 0x1effd7a4, 0x67a61db6, 0x2d2a081e, 0xc33f98d5, 0x082b4e4e, 0x764e79c4, 0xa3f5d7ce, 0x69bde0d7, 0xe6f264ce, 0x853d8dda, 0xcdec257e, 0x62d04581, 0x4f88aa08, 0x0197e59b, 0x0137e15c, 0x2fbf47df, 0x56ad2eb9, 0x38e8db85, 0xeb55b85a, 0x42b04293, 0x7be57cfe, 0x15c8c753, 0x51780a08, 0x8b77abad, 0xe7f0fd55, 0xf94d410d, 0xa590c0c4
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbe03fdb0ac1ce5ab, 0xd058318fa806b194, 0xc3e270f9f1dd4b04, 0xec29e238cbf84d45, 0x3d3eb648d10c24ab, 0x5b26d87ad2b1f752, 0x6011abccb225a7e0, 0x2b77609e39010149, 0x25f22ae422fb9e8f, 0x0a813ccb047d8176, 0x623b0ec6b0061bdb, 0x151a7db470a14793, 0x67dc005fd1e6836e, 0x61712d35cf7c984e, 0x90ab35bb468753b1, 0x49de18934bda7956
#else
      0xbe03fdb0, 0xac1ce5ab, 0xd058318f, 0xa806b194, 0xc3e270f9, 0xf1dd4b04, 0xec29e238, 0xcbf84d45, 0x3d3eb648, 0xd10c24ab, 0x5b26d87a, 0xd2b1f752, 0x6011abcc, 0xb225a7e0, 0x2b77609e, 0x39010149, 0x25f22ae4, 0x22fb9e8f, 0x0a813ccb, 0x047d8176, 0x623b0ec6, 0xb0061bdb, 0x151a7db4, 0x70a14793, 0x67dc005f, 0xd1e6836e, 0x61712d35, 0xcf7c984e, 0x90ab35bb, 0x468753b1, 0x49de1893, 0x4bda7956
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xb83fc45052c9c6f8
#else
      0x15567c16
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer2[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x88c23309171cb73f, 0xdcd3c70e86e4d987, 0x3bd68ba710fdab43, 0xad01fb3c4dc82a54, 0x9e491176d69e7839, 0x176cefff5d688498, 0xd447d805489db54c, 0x207b4693cc7075b8, 0x746fdae73c5bb70d, 0x38e19aa7ec2fa075, 0x56225b83584f7825, 0x1657d2d4cc1d8e60, 0xaac356761f162dd3, 0x84bf05b415326571, 0xd92da4493cf2899e, 0x6a23524f88ad7c36, 0x757207a346040f50
#else
      0x0fd68540, 0xfab19119, 0x098a2f60, 0x0ea0d68c, 0x8aa2b300, 0xf8fd2e20, 0x94619a71, 0x3cfcd76e, 0x700ce5a2, 0xd94b289a, 0x38d8f5af, 0x72bd79af, 0x26a4e0a4, 0x521cd94d, 0x556c6758, 0xfee32ca6, 0xd1652789, 0xaf189515, 0xe2570ac8, 0x941f5d67, 0x61524fce, 0x4f6b24ac, 0x6a58c642, 0xad05451c, 0xef917ce9, 0x797ede1f, 0xe68bc769, 0xcbb8df79, 0xcabdc9bf, 0xc6701f95, 0x44c919a4, 0x43b1dcbf, 0x28701564
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x9fc5b0ca33900a2b, 0xbf4c4e57b6c23c4c, 0x0b4dcc1ebb69bfbd, 0x12f1445648bace8a, 0xc40bfa06a4adca39, 0x57a2057125397f57, 0xb89dfab1e765dc13, 0xcb211b0c581889d4, 0x7f91660294ecdb32, 0x07dbe5de9ba38675, 0xff23ca68e4a2c1bc, 0x38ae76d9beef6cac, 0x784fe5d50478f162, 0x379f0b433a2b19e3, 0xeb7fb1f829b510a3, 0x2886d56f9b3f69a6
#else
      0x9fc5b0ca, 0x33900a2b, 0xbf4c4e57, 0xb6c23c4c, 0x0b4dcc1e, 0xbb69bfbd, 0x12f14456, 0x48bace8a, 0xc40bfa06, 0xa4adca39, 0x57a20571, 0x25397f57, 0xb89dfab1, 0xe765dc13, 0xcb211b0c, 0x581889d4, 0x7f916602, 0x94ecdb32, 0x07dbe5de, 0x9ba38675, 0xff23ca68, 0xe4a2c1bc, 0x38ae76d9, 0xbeef6cac, 0x784fe5d5, 0x0478f162, 0x379f0b43, 0x3a2b19e3, 0xeb7fb1f8, 0x29b510a3, 0x2886d56f, 0x9b3f69a6
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y3 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x6fbc561ca09b3801
#else
      0xd26aeac6
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer3[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x45bc4293c81903c4, 0xc0578bb1552897f5, 0x3003b85cd5f77890, 0xabbd18d5d8eb3d48, 0xb6514daee40b374d, 0xafbcbeb8022f8736, 0xbc21b851280e7c7b, 0xfc7ae8fa5c734f30, 0x5ceeab6cce4739d2, 0x3437114371501ebf, 0x4e8943a098090941, 0x81829555f01b77f4, 0x575f2a1cdba65b4b, 0x35ebff02f146baa8, 0x860b9e3fbc19534a, 0x0f894124ae2fe846, 0x7b2732fa31ddb9a6
#else
      0x8352e567, 0x58851181, 0xa3bda88e, 0x18ddc6c8, 0x3e1aa4cd, 0x95365026, 0x1ae0e182, 0x427513fe, 0x5b8d9695, 0x8da43382, 0xfee40400, 0xf1ac35ce, 0x08a2b5a8, 0xeed1e1d1, 0x70c592e0, 0xb1b4cf4b, 0xf398eed7, 0xecef3801, 0x7cb6e77e, 0xde09da72, 0x5188db55, 0xe7ae9c6e, 0x3966791f, 0x245c6b39, 0x009f0efb, 0xfdbf88ee, 0x8679f7e6, 0x34db9a40, 0x128e9e68, 0x206848ae, 0x7e0d6426, 0x84f16525, 0xf4597264
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x30163e90ff6b9c55, 0x6a381e0ed7bdd12a, 0xf1467b2c0a972be1, 0x3bc5af9abb4dde46, 0x5bea5d42a36e2f78, 0xfba76e070acceb0e, 0xbebf5d2b9ce13c15, 0x76e3cc0141f46635, 0xf4fa06221a4d9fc1, 0xb6459df49213fdea, 0x80b494a249d06f8f, 0x3ec8c9d022ee7afe, 0xdacc891bc133fb97, 0xb14a81177e84f07c, 0x90c5def7f86736c0, 0x568c15b7d91d4e9b
#else
      0x30163e90, 0xff6b9c55, 0x6a381e0e, 0xd7bdd12a, 0xf1467b2c, 0x0a972be1, 0x3bc5af9a, 0xbb4dde46, 0x5bea5d42, 0xa36e2f78, 0xfba76e07, 0x0acceb0e, 0xbebf5d2b, 0x9ce13c15, 0x76e3cc01, 0x41f46635, 0xf4fa0622, 0x1a4d9fc1, 0xb6459df4, 0x9213fdea, 0x80b494a2, 0x49d06f8f, 0x3ec8c9d0, 0x22ee7afe, 0xdacc891b, 0xc133fb97, 0xb14a8117, 0x7e84f07c, 0x90c5def7, 0xf86736c0, 0x568c15b7, 0xd91d4e9b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y4 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x61f95c1f2cab90af
#else
      0xc5d700b8
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer4[] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x126744a5a561f20b, 0x1fb1a50821973fff, 0xdd9bc6606337be20, 0x0e805428024326d4, 0x7908fb31b8b12756, 0xd0b3c7df88d9621f, 0x14cf69f036742bb7, 0x481cab06eca6a012, 0x924801dc8353d35c, 0xedf4e9f10c13915e, 0x017130f68f5b960e, 0x5d972889c33bdc5d, 0x92ae27c0dc4aef2a, 0x3e1efb14260f0d1d, 0x93857f6ac8a40284, 0x210f94781a8f7d0c, 0x13a88ba90cc8ebf5
#else
      0x252980f6, 0xb0514ae5, 0x9bd1c1a7, 0x02d1dd0b, 0xc72a35d7, 0xc0cbc161, 0xc9cfdd0b, 0x832c71a8, 0x71ca4e2a, 0xab2e13f7, 0x6f75503d, 0xb390d80b, 0x85746802, 0xe8e50bbe, 0xbc6754ba, 0xc08a17d0, 0xc47e9b7b, 0x78963ba4, 0x24be7a3f, 0xded998f8, 0xb35b8e96, 0x38e4388b, 0xfa6e6ce3, 0xc8ffe888, 0x85d18e76, 0xc4fecf36, 0xe5310ff0, 0xeca45798, 0xc392b44b, 0x22a14e0b, 0x8851d69a, 0xec2a813e, 0x583d7f68
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t resBuffer[size + 1];
    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = size + 1
    };
    rucrypto_gost_34_10_mul_1(&x, y1, &res);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer1, size + 1));
    x.buffer = x2Buffer;
    rucrypto_gost_34_10_mul_1(&x, y2, &res);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer2, size + 1));
    x.buffer = x3Buffer;
    rucrypto_gost_34_10_mul_1(&x, y3, &res);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer3, size + 1));
    x.buffer = x4Buffer;
    rucrypto_gost_34_10_mul_1(&x, y4, &res);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer4, size + 1));
}

TEST(Gost3410BasicMathTests, MulTest) {
    constexpr size_t size = 1024 / 8 / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbfa0905ea10c5a06, 0xd3e3f4296556c105, 0x0b0504443b121027, 0x6ad28c8a8b3a4196, 0xa4b98af9a78be09d, 0x8035864002d9a054, 0x592514d86a847f7e, 0x4121c2ab778ce85a, 0xdee75973cf3859da, 0xfac92ad4e99a9cdc, 0xd4a56d6109aaa8dc, 0x5be860d7ee140ff9, 0x03fab12623ad6b79, 0x4019f7418069e18f, 0xdca88414036b7de3, 0x0f0ebd46145e1252
#else
      0xbfa0905e, 0xa10c5a06, 0xd3e3f429, 0x6556c105, 0x0b050444, 0x3b121027, 0x6ad28c8a, 0x8b3a4196, 0xa4b98af9, 0xa78be09d, 0x80358640, 0x02d9a054, 0x592514d8, 0x6a847f7e, 0x4121c2ab, 0x778ce85a, 0xdee75973, 0xcf3859da, 0xfac92ad4, 0xe99a9cdc, 0xd4a56d61, 0x09aaa8dc, 0x5be860d7, 0xee140ff9, 0x03fab126, 0x23ad6b79, 0x4019f741, 0x8069e18f, 0xdca88414, 0x036b7de3, 0x0f0ebd46, 0x145e1252
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x059d0e12c1281578, 0xc21e9bb03762bd95, 0xd0e7184c5b829dcd, 0x4e81e0af14d2a6d9, 0xe6625493f101ba43, 0x5760f89dada25fdd, 0x67cb6bbe0ccd4990, 0xe6fdb1b16e7f2abc, 0x7e00c4fdef9ba51f, 0xc86d47dd9c36e662, 0x8f99c5c9c42922e5, 0x7e1eee506d7127db, 0x204ac3f1706cd566, 0xe411bbb33d4a5c2d, 0xda0feb84b7e46732, 0x17fe4785d9d0b6a3
#else
      0x059d0e12, 0xc1281578, 0xc21e9bb0, 0x3762bd95, 0xd0e7184c, 0x5b829dcd, 0x4e81e0af, 0x14d2a6d9, 0xe6625493, 0xf101ba43, 0x5760f89d, 0xada25fdd, 0x67cb6bbe, 0x0ccd4990, 0xe6fdb1b1, 0x6e7f2abc, 0x7e00c4fd, 0xef9ba51f, 0xc86d47dd, 0x9c36e662, 0x8f99c5c9, 0xc42922e5, 0x7e1eee50, 0x6d7127db, 0x204ac3f1, 0x706cd566, 0xe411bbb3, 0x3d4a5c2d, 0xda0feb84, 0xb7e46732, 0x17fe4785, 0xd9d0b6a3
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer1[size * 2] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0433b2d334f363cb, 0xcebb9ac462203055, 0xee53080e681c0388, 0xf9ee0bbb33514568, 0xac0c3acb36620d0b, 0x935f095809fb9598, 0xfda4c7f10d978766, 0xc949da22ac0fa41f, 0x51cf2cd499c955fc, 0x72b25f4491603a9d, 0x672461b827618112, 0x9bb3d1aee1eb26d9, 0xde7192a39f87c87f, 0xf6c9716528ec6222, 0x7b50da7e85571759, 0x40759de33d845d0b, 0x57e9ba311f8cec83, 0x5f10e017404d3f5c, 0xa8d5512032008949, 0x23a684e7965df14d, 0x086503f2a9cca7a0, 0xa10aaeed483c9d14, 0x7d30f269d3d5fecd, 0x00a0b1a8c2231183, 0x6363f20ae8ea3d0d, 0xd97772099efbcf7f, 0x0463cbc4481bd789, 0xf850a03bf281bd9b, 0x41c5fe6179d2318f, 0x104739b71a4e0cd9, 0xc4e321081c7a4ebf, 0x7876e2aa3d8bf636
#else
      0x0433b2d3, 0x34f363cb, 0xcebb9ac4, 0x62203055, 0xee53080e, 0x681c0388, 0xf9ee0bbb, 0x33514568, 0xac0c3acb, 0x36620d0b, 0x935f0958, 0x09fb9598, 0xfda4c7f1, 0x0d978766, 0xc949da22, 0xac0fa41f, 0x51cf2cd4, 0x99c955fc, 0x72b25f44, 0x91603a9d, 0x672461b8, 0x27618112, 0x9bb3d1ae, 0xe1eb26d9, 0xde7192a3, 0x9f87c87f, 0xf6c97165, 0x28ec6222, 0x7b50da7e, 0x85571759, 0x40759de3, 0x3d845d0b, 0x57e9ba31, 0x1f8cec83, 0x5f10e017, 0x404d3f5c, 0xa8d55120, 0x32008949, 0x23a684e7, 0x965df14d, 0x086503f2, 0xa9cca7a0, 0xa10aaeed, 0x483c9d14, 0x7d30f269, 0xd3d5fecd, 0x00a0b1a8, 0xc2231183, 0x6363f20a, 0xe8ea3d0d, 0xd9777209, 0x9efbcf7f, 0x0463cbc4, 0x481bd789, 0xf850a03b, 0xf281bd9b, 0x41c5fe61, 0x79d2318f, 0x104739b7, 0x1a4e0cd9, 0xc4e32108, 0x1c7a4ebf, 0x7876e2aa, 0x3d8bf636
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbe03fdb0ac1ce5ab, 0xd058318fa806b194, 0xc3e270f9f1dd4b04, 0xec29e238cbf84d45, 0x3d3eb648d10c24ab, 0x5b26d87ad2b1f752, 0x6011abccb225a7e0, 0x2b77609e39010149, 0x25f22ae422fb9e8f, 0x0a813ccb047d8176, 0x623b0ec6b0061bdb, 0x151a7db470a14793, 0x67dc005fd1e6836e, 0x61712d35cf7c984e, 0x90ab35bb468753b1, 0x49de18934bda7956
#else
      0xbe03fdb0, 0xac1ce5ab, 0xd058318f, 0xa806b194, 0xc3e270f9, 0xf1dd4b04, 0xec29e238, 0xcbf84d45, 0x3d3eb648, 0xd10c24ab, 0x5b26d87a, 0xd2b1f752, 0x6011abcc, 0xb225a7e0, 0x2b77609e, 0x39010149, 0x25f22ae4, 0x22fb9e8f, 0x0a813ccb, 0x047d8176, 0x623b0ec6, 0xb0061bdb, 0x151a7db4, 0x70a14793, 0x67dc005f, 0xd1e6836e, 0x61712d35, 0xcf7c984e, 0x90ab35bb, 0x468753b1, 0x49de1893, 0x4bda7956
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x064957b4df057431, 0xb423489b19db106f, 0xe37049d86976c4d3, 0xfcf5eb3482bbf573, 0x2baa4f9e113d4ca2, 0x64044caeee9116d0, 0x3f05f5d1948020f3, 0x8e15c3a24f92bac4, 0x634082bc3318d308, 0xdb05ff152dc60124, 0xfdbcf3d3625af35b, 0x0c473d0ca728102d, 0xe158904417a7410c, 0x5f87b0ffb805d6ad, 0xdf39a75f4d75302e, 0x0fc88661663ae62c
#else
      0x064957b4, 0xdf057431, 0xb423489b, 0x19db106f, 0xe37049d8, 0x6976c4d3, 0xfcf5eb34, 0x82bbf573, 0x2baa4f9e, 0x113d4ca2, 0x64044cae, 0xee9116d0, 0x3f05f5d1, 0x948020f3, 0x8e15c3a2, 0x4f92bac4, 0x634082bc, 0x3318d308, 0xdb05ff15, 0x2dc60124, 0xfdbcf3d3, 0x625af35b, 0x0c473d0c, 0xa728102d, 0xe1589044, 0x17a7410c, 0x5f87b0ff, 0xb805d6ad, 0xdf39a75f, 0x4d75302e, 0x0fc88661, 0x663ae62c
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer2[size * 2] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x04aa882f16f8b2a5, 0xe90c7f2b4e549392, 0x9c002364da126a1e, 0xabef7a704de812b9, 0x1f55a88aa1ac2954, 0x6508f4c16cb89253, 0xff22dc8a14a82f3b, 0x5bede97a974bb12d, 0x9c5e73f62a0936f9, 0xf3cb92575c289575, 0x1f15b3babc62049e, 0x0b3893343f0106cb, 0x7a5f7ded88efeb86, 0xe245e8aa319c76f1, 0xac092995f0186414, 0xf3c9b6fb7f18cb69, 0x2ac9bb0d705d618f, 0xfe5eb7459c0cda60, 0xd2410747b63708bf, 0x30e63b8be61aafc8, 0xe34f8794faf17ce6, 0x63378955f0793dfb, 0xf1b4a5035b882311, 0xa7876916d85177c8, 0x6925fdf651ca05a3, 0x55246eeb5c66e364, 0xaf080abf7019f822, 0x87deb8a0e96ca406, 0x8619ecaf926f3652, 0xa16b38cc91754f62, 0xdc2d2a4d4ae64bb5, 0x6c53c8e5140c1ec8
#else
      0x04aa882f, 0x16f8b2a5, 0xe90c7f2b, 0x4e549392, 0x9c002364, 0xda126a1e, 0xabef7a70, 0x4de812b9, 0x1f55a88a, 0xa1ac2954, 0x6508f4c1, 0x6cb89253, 0xff22dc8a, 0x14a82f3b, 0x5bede97a, 0x974bb12d, 0x9c5e73f6, 0x2a0936f9, 0xf3cb9257, 0x5c289575, 0x1f15b3ba, 0xbc62049e, 0x0b389334, 0x3f0106cb, 0x7a5f7ded, 0x88efeb86, 0xe245e8aa, 0x319c76f1, 0xac092995, 0xf0186414, 0xf3c9b6fb, 0x7f18cb69, 0x2ac9bb0d, 0x705d618f, 0xfe5eb745, 0x9c0cda60, 0xd2410747, 0xb63708bf, 0x30e63b8b, 0xe61aafc8, 0xe34f8794, 0xfaf17ce6, 0x63378955, 0xf0793dfb, 0xf1b4a503, 0x5b882311, 0xa7876916, 0xd85177c8, 0x6925fdf6, 0x51ca05a3, 0x55246eeb, 0x5c66e364, 0xaf080abf, 0x7019f822, 0x87deb8a0, 0xe96ca406, 0x8619ecaf, 0x926f3652, 0xa16b38cc, 0x91754f62, 0xdc2d2a4d, 0x4ae64bb5, 0x6c53c8e5, 0x140c1ec8
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x9fc5b0ca33900a2b, 0xbf4c4e57b6c23c4c, 0x0b4dcc1ebb69bfbd, 0x12f1445648bace8a, 0xc40bfa06a4adca39, 0x57a2057125397f57, 0xb89dfab1e765dc13, 0xcb211b0c581889d4, 0x7f91660294ecdb32, 0x07dbe5de9ba38675, 0xff23ca68e4a2c1bc, 0x38ae76d9beef6cac, 0x784fe5d50478f162, 0x379f0b433a2b19e3, 0xeb7fb1f829b510a3, 0x2886d56f9b3f69a6
#else
      0x9fc5b0ca, 0x33900a2b, 0xbf4c4e57, 0xb6c23c4c, 0x0b4dcc1e, 0xbb69bfbd, 0x12f14456, 0x48bace8a, 0xc40bfa06, 0xa4adca39, 0x57a20571, 0x25397f57, 0xb89dfab1, 0xe765dc13, 0xcb211b0c, 0x581889d4, 0x7f916602, 0x94ecdb32, 0x07dbe5de, 0x9ba38675, 0xff23ca68, 0xe4a2c1bc, 0x38ae76d9, 0xbeef6cac, 0x784fe5d5, 0x0478f162, 0x379f0b43, 0x3a2b19e3, 0xeb7fb1f8, 0x29b510a3, 0x2886d56f, 0x9b3f69a6
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0f97e99a1b45970c, 0xc5f2718dad769307, 0xc5a34b66b1e9a62d, 0xbe28ceeb5e0807d2, 0x99fd6d5e1a7e756f, 0x45a26c98df0a4b4e, 0x03d015bfdfd76aef, 0x604f6392ef8b5ed6, 0x0dda9d8e787c8ddf, 0xb87f1719033e07f1, 0xef3b9e3fdef34d7f, 0x83175a589208cb39, 0xeb83158f484d4c41, 0xfd6474fd6867c909, 0xa6110b290c8a336c, 0x2a54e48f3015e9fc
#else
      0x0f97e99a, 0x1b45970c, 0xc5f2718d, 0xad769307, 0xc5a34b66, 0xb1e9a62d, 0xbe28ceeb, 0x5e0807d2, 0x99fd6d5e, 0x1a7e756f, 0x45a26c98, 0xdf0a4b4e, 0x03d015bf, 0xdfd76aef, 0x604f6392, 0xef8b5ed6, 0x0dda9d8e, 0x787c8ddf, 0xb87f1719, 0x033e07f1, 0xef3b9e3f, 0xdef34d7f, 0x83175a58, 0x9208cb39, 0xeb83158f, 0x484d4c41, 0xfd6474fd, 0x6867c909, 0xa6110b29, 0x0c8a336c, 0x2a54e48f, 0x3015e9fc
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer3[size * 2] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x09bb64c23c238553, 0x3bf1539c045fd765, 0xd9a66cd75d7489a4, 0x853bbcd09c788340, 0x8ac0153968e80e5e, 0xbcaa9e3ec8302d3f, 0x3dcc2c9bdfe6ed31, 0xd77041cb72152ec0, 0xefdb233ef245e59b, 0x9b77f2cad9ea0250, 0x7fc887a6290462c1, 0x477b67e6b7ea4d08, 0xd29d6a8495f7a59f, 0x8fc5ba84f5baefa6, 0xe3c6be49f1e2defb, 0xc8c477b33ce5a51f, 0x691ef5d2d6a9ad08, 0x7f136b10b4dee654, 0x1a32a8d8d44aa221, 0x8d21feb5fe0997a1, 0xcbf169363f5727b9, 0xb9cf2e7256876219, 0xe9091fb6d550094f, 0x32857102df2f861b, 0x7597895a5213a62f, 0xdd762e04f17c9629, 0x2430db9fe799868f, 0x127d309a5530934f, 0xc5591b82fb80044c, 0x845a139509512925, 0x72907171bd25175b, 0x253c29e654321568
#else
      0x09bb64c2, 0x3c238553, 0x3bf1539c, 0x045fd765, 0xd9a66cd7, 0x5d7489a4, 0x853bbcd0, 0x9c788340, 0x8ac01539, 0x68e80e5e, 0xbcaa9e3e, 0xc8302d3f, 0x3dcc2c9b, 0xdfe6ed31, 0xd77041cb, 0x72152ec0, 0xefdb233e, 0xf245e59b, 0x9b77f2ca, 0xd9ea0250, 0x7fc887a6, 0x290462c1, 0x477b67e6, 0xb7ea4d08, 0xd29d6a84, 0x95f7a59f, 0x8fc5ba84, 0xf5baefa6, 0xe3c6be49, 0xf1e2defb, 0xc8c477b3, 0x3ce5a51f, 0x691ef5d2, 0xd6a9ad08, 0x7f136b10, 0xb4dee654, 0x1a32a8d8, 0xd44aa221, 0x8d21feb5, 0xfe0997a1, 0xcbf16936, 0x3f5727b9, 0xb9cf2e72, 0x56876219, 0xe9091fb6, 0xd550094f, 0x32857102, 0xdf2f861b, 0x7597895a, 0x5213a62f, 0xdd762e04, 0xf17c9629, 0x2430db9f, 0xe799868f, 0x127d309a, 0x5530934f, 0xc5591b82, 0xfb80044c, 0x845a1395, 0x09512925, 0x72907171, 0xbd25175b, 0x253c29e6, 0x54321568
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x30163e90ff6b9c55, 0x6a381e0ed7bdd12a, 0xf1467b2c0a972be1, 0x3bc5af9abb4dde46, 0x5bea5d42a36e2f78, 0xfba76e070acceb0e, 0xbebf5d2b9ce13c15, 0x76e3cc0141f46635, 0xf4fa06221a4d9fc1, 0xb6459df49213fdea, 0x80b494a249d06f8f, 0x3ec8c9d022ee7afe, 0xdacc891bc133fb97, 0xb14a81177e84f07c, 0x90c5def7f86736c0, 0x568c15b7d91d4e9b
#else
      0x30163e90, 0xff6b9c55, 0x6a381e0e, 0xd7bdd12a, 0xf1467b2c, 0x0a972be1, 0x3bc5af9a, 0xbb4dde46, 0x5bea5d42, 0xa36e2f78, 0xfba76e07, 0x0acceb0e, 0xbebf5d2b, 0x9ce13c15, 0x76e3cc01, 0x41f46635, 0xf4fa0622, 0x1a4d9fc1, 0xb6459df4, 0x9213fdea, 0x80b494a2, 0x49d06f8f, 0x3ec8c9d0, 0x22ee7afe, 0xdacc891b, 0xc133fb97, 0xb14a8117, 0x7e84f07c, 0x90c5def7, 0xf86736c0, 0x568c15b7, 0xd91d4e9b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0e88b400bea04c3c, 0x03fb127d21269b4e, 0x7011a81b4fc71deb, 0xa022722d0bfb9053, 0x70ff915ffe506c39, 0x0c450ace51967535, 0xd1788844721957e5, 0xdaa3b2c1bed90213, 0x73e5e64fb1f2267a, 0x12849cb349804395, 0x659dc313e1b75f1c, 0xa44e4348f828c63e, 0x73e3afba3829e95c, 0x009faec2dd448938, 0x70e93ba65556c360, 0xb733f3228fde0b81
#else
      0x0e88b400, 0xbea04c3c, 0x03fb127d, 0x21269b4e, 0x7011a81b, 0x4fc71deb, 0xa022722d, 0x0bfb9053, 0x70ff915f, 0xfe506c39, 0x0c450ace, 0x51967535, 0xd1788844, 0x721957e5, 0xdaa3b2c1, 0xbed90213, 0x73e5e64f, 0xb1f2267a, 0x12849cb3, 0x49804395, 0x659dc313, 0xe1b75f1c, 0xa44e4348, 0xf828c63e, 0x73e3afba, 0x3829e95c, 0x009faec2, 0xdd448938, 0x70e93ba6, 0x5556c360, 0xb733f322, 0x8fde0b81
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer4[size * 2] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x02bae50cf2cc25f7, 0xfbd631414351058c, 0x093d3a28e3409f82, 0x861701f65b721f92, 0x1e087fa2aa53c32a, 0xe4b7e777df8b628e, 0x5e1c25e849f51dec, 0x8ba8829f835577ea, 0x138a4f2b046127df, 0x8d98cf3f72c30c6f, 0xb50633c5d6491588, 0x0215b252d27b2c4b, 0x8f34010a1d2340e8, 0x09d5bc9d80b5e530, 0x3c207b3f51df6366, 0x6d17af99657b7326, 0x39d1a46999be1483, 0x443f8960533df744, 0x2ff4a374be78a03e, 0x6861cb82f7704ca1, 0xd68fe39faf765250, 0x641ee64640bb1ad5, 0x4614b0b3c0ce78fb, 0x450c02b6dee6a10b, 0x2a080c007201b8ab, 0xa278848d19fab1b9, 0x3b867a089add454e, 0xa4918b1838b46307, 0x2175b390aed721f6, 0x4d19a2e5d2ad8619, 0x1510d89f5a7c4ce9, 0x469e0fe1698f451b
#else
      0x02bae50c, 0xf2cc25f7, 0xfbd63141, 0x4351058c, 0x093d3a28, 0xe3409f82, 0x861701f6, 0x5b721f92, 0x1e087fa2, 0xaa53c32a, 0xe4b7e777, 0xdf8b628e, 0x5e1c25e8, 0x49f51dec, 0x8ba8829f, 0x835577ea, 0x138a4f2b, 0x046127df, 0x8d98cf3f, 0x72c30c6f, 0xb50633c5, 0xd6491588, 0x0215b252, 0xd27b2c4b, 0x8f34010a, 0x1d2340e8, 0x09d5bc9d, 0x80b5e530, 0x3c207b3f, 0x51df6366, 0x6d17af99, 0x657b7326, 0x39d1a469, 0x99be1483, 0x443f8960, 0x533df744, 0x2ff4a374, 0xbe78a03e, 0x6861cb82, 0xf7704ca1, 0xd68fe39f, 0xaf765250, 0x641ee646, 0x40bb1ad5, 0x4614b0b3, 0xc0ce78fb, 0x450c02b6, 0xdee6a10b, 0x2a080c00, 0x7201b8ab, 0xa278848d, 0x19fab1b9, 0x3b867a08, 0x9add454e, 0xa4918b18, 0x38b46307, 0x2175b390, 0xaed721f6, 0x4d19a2e5, 0xd2ad8619, 0x1510d89f, 0x5a7c4ce9, 0x469e0fe1, 0x698f451b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t resBuffer[size * 2];
    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t y = {
        .negative = false,
        .buffer = y1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = size * 2
    };
    rucrypto_gost_34_10_mul(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer1, size * 2));
    x.buffer = x2Buffer;
    y.buffer = y2Buffer;
    rucrypto_gost_34_10_mul(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer2, size * 2));
    x.buffer = x3Buffer;
    y.buffer = y3Buffer;
    rucrypto_gost_34_10_mul(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer3, size * 2));
    x.buffer = x4Buffer;
    y.buffer = y4Buffer;
    rucrypto_gost_34_10_mul(&x, &y, &res);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer4, size * 2));
}

TEST(Gost3410BasicMathTests, Div1Test) {
    constexpr size_t size = 1024 / 8 / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbfa0905ea10c5a06, 0xd3e3f4296556c105, 0x0b0504443b121027, 0x6ad28c8a8b3a4196, 0xa4b98af9a78be09d, 0x8035864002d9a054, 0x592514d86a847f7e, 0x4121c2ab778ce85a, 0xdee75973cf3859da, 0xfac92ad4e99a9cdc, 0xd4a56d6109aaa8dc, 0x5be860d7ee140ff9, 0x03fab12623ad6b79, 0x4019f7418069e18f, 0xdca88414036b7de3, 0x0f0ebd46145e1252
#else
      0xbfa0905e, 0xa10c5a06, 0xd3e3f429, 0x6556c105, 0x0b050444, 0x3b121027, 0x6ad28c8a, 0x8b3a4196, 0xa4b98af9, 0xa78be09d, 0x80358640, 0x02d9a054, 0x592514d8, 0x6a847f7e, 0x4121c2ab, 0x778ce85a, 0xdee75973, 0xcf3859da, 0xfac92ad4, 0xe99a9cdc, 0xd4a56d61, 0x09aaa8dc, 0x5be860d7, 0xee140ff9, 0x03fab126, 0x23ad6b79, 0x4019f741, 0x8069e18f, 0xdca88414, 0x036b7de3, 0x0f0ebd46, 0x145e1252
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x6c6d8154ce68fc2d
#else
      0x57c5cf92
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t qExpectedBuffer1[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0000000000000001, 0xc46f7c0b459ca0a8, 0x1ba732dfe1e02c38, 0x92c016c8d0b63a9d, 0x6ec8c8957150adfa, 0xf2018370c19aeb8f, 0x4a764cec481e219f, 0x6f27a57e20b866d6, 0x9098a9246e55cc12, 0x89bb21ab67374c19, 0xa773e8e1d13c2f1c, 0x4a66d2809beb6000, 0xfaf94e14e317436d, 0x1302f5904a90bbd2, 0x626bc01ea8c41bcb, 0x3eb1b32873949ac9
#else
      0x00000002, 0x2ee7931b, 0xbb667537, 0x9e87c193, 0xdb89dfdc, 0xef6571a8, 0x57982cc5, 0x0971132e, 0xfa5758bc, 0x33017f41, 0xfd181119, 0x214eec24, 0xb850b14e, 0x197941e5, 0xe6001597, 0x3e7eb49a, 0x2b036140, 0x0ed8c288, 0x73a219df, 0xc5c8e1b0, 0x92282c54, 0xf8a51130, 0xa5591a0b, 0xadbd7280, 0x22dab79e, 0xd85e3e08, 0x3f0b5b60, 0x7fef7154, 0x7f1ad0ce, 0x00028363, 0xa7e4dd35, 0x0867ac00
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t rExpected1 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x45aed182db3900fd
#else
      0x1929fa52
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbe03fdb0ac1ce5ab, 0xd058318fa806b194, 0xc3e270f9f1dd4b04, 0xec29e238cbf84d45, 0x3d3eb648d10c24ab, 0x5b26d87ad2b1f752, 0x6011abccb225a7e0, 0x2b77609e39010149, 0x25f22ae422fb9e8f, 0x0a813ccb047d8176, 0x623b0ec6b0061bdb, 0x151a7db470a14793, 0x67dc005fd1e6836e, 0x61712d35cf7c984e, 0x90ab35bb468753b1, 0x49de18934bda7956
#else
      0xbe03fdb0, 0xac1ce5ab, 0xd058318f, 0xa806b194, 0xc3e270f9, 0xf1dd4b04, 0xec29e238, 0xcbf84d45, 0x3d3eb648, 0xd10c24ab, 0x5b26d87a, 0xd2b1f752, 0x6011abcc, 0xb225a7e0, 0x2b77609e, 0x39010149, 0x25f22ae4, 0x22fb9e8f, 0x0a813ccb, 0x047d8176, 0x623b0ec6, 0xb0061bdb, 0x151a7db4, 0x70a14793, 0x67dc005f, 0xd1e6836e, 0x61712d35, 0xcf7c984e, 0x90ab35bb, 0x468753b1, 0x49de1893, 0x4bda7956
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xb83fc45052c9c6f8
#else
      0x15567c16
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t qExpectedBuffer2[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0000000000000001, 0x080319b247426f91, 0xc930f5248906d329, 0x7806794387929ec1, 0x8aa825802b17253b, 0xd9a9b7105bae3ace, 0xf1733ab73014e508, 0x9a265ac216cfa146, 0xe66918d2f353c5b0, 0x5dcf8f9c4cb247fc, 0xb229ddb23206f6a6, 0x65fc5b8b8144a295, 0xae1682cc1b68a81d, 0x81a1e2f17d40c7a0, 0x428f4b85d88c253d, 0x2671a9e1fdd1ca67
#else
      0x00000008, 0xe7b4daa7, 0x12b59556, 0x5a27946c, 0x3568a1ac, 0xe192a5b5, 0x264e804f, 0x44bb638e, 0x5f46706f, 0xad460ee7, 0x6e75b3a4, 0x9b1f4561, 0xf9bec138, 0x27452d44, 0x592d2046, 0xc53dba0a, 0xaab0dec2, 0xbc12841f, 0xefba0e8b, 0x5d4ae131, 0xba2afad2, 0xf5f022bd, 0xc0c883c4, 0x926adb5a, 0x538d83b0, 0x0970b48f, 0x5c97c664, 0xb07ac110, 0x30906c31, 0xdca639a3, 0xc6c651d7, 0x2f78814e
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t rExpected2 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x94c022983d33bb8e
#else
      0x02a994a2
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x9fc5b0ca33900a2b, 0xbf4c4e57b6c23c4c, 0x0b4dcc1ebb69bfbd, 0x12f1445648bace8a, 0xc40bfa06a4adca39, 0x57a2057125397f57, 0xb89dfab1e765dc13, 0xcb211b0c581889d4, 0x7f91660294ecdb32, 0x07dbe5de9ba38675, 0xff23ca68e4a2c1bc, 0x38ae76d9beef6cac, 0x784fe5d50478f162, 0x379f0b433a2b19e3, 0xeb7fb1f829b510a3, 0x2886d56f9b3f69a6
#else
      0x9fc5b0ca, 0x33900a2b, 0xbf4c4e57, 0xb6c23c4c, 0x0b4dcc1e, 0xbb69bfbd, 0x12f14456, 0x48bace8a, 0xc40bfa06, 0xa4adca39, 0x57a20571, 0x25397f57, 0xb89dfab1, 0xe765dc13, 0xcb211b0c, 0x581889d4, 0x7f916602, 0x94ecdb32, 0x07dbe5de, 0x9ba38675, 0xff23ca68, 0xe4a2c1bc, 0x38ae76d9, 0xbeef6cac, 0x784fe5d5, 0x0478f162, 0x379f0b43, 0x3a2b19e3, 0xeb7fb1f8, 0x29b510a3, 0x2886d56f, 0x9b3f69a6
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y3 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x6fbc561ca09b3801
#else
      0xd26aeac6
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t qExpectedBuffer3[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0000000000000001, 0x6e0eba71ed8c2841, 0x713b3ec088366f98, 0x9f6668be722e6903, 0x3c7cb0c134d6b360, 0x81c9c6f7cfec9427, 0xf53d3f3c1ebec879, 0xd4be964bc689ec4f, 0x295de5baf78a04c8, 0x682535d62171b492, 0xa9b3a18cd54e9fad, 0x6e7cfd82672dd49b, 0xfe63fe206962e0d3, 0x53bb1f627d321d48, 0x56f43837580eeb39, 0xfc75e5f4023571ef
#else
      0x0, 0xc262246f, 0x02e91007, 0x1a254afb, 0x8ee42080, 0xaa843334, 0x2f3e07ef, 0x9a35f5e7, 0x757f4391, 0xc74df260, 0x2ab2e3a6, 0x72ea53ac, 0x68fac829, 0xf432d688, 0x3521708a, 0x67ba0782, 0x659dd295, 0x45bd33c5, 0xce75a92a, 0x5d7be32d, 0x1a32e36c, 0x8e91d63d, 0xb28bdc87, 0x47d24af7, 0x6324e47e, 0x1e1c8e8e, 0x23a0eccc, 0x35d2d51e, 0x0995c079, 0xd0d2e980, 0x38249860, 0x89b682d4
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t rExpected3 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x2702114a8c68afb7
#else
      0x30b871ae
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x30163e90ff6b9c55, 0x6a381e0ed7bdd12a, 0xf1467b2c0a972be1, 0x3bc5af9abb4dde46, 0x5bea5d42a36e2f78, 0xfba76e070acceb0e, 0xbebf5d2b9ce13c15, 0x76e3cc0141f46635, 0xf4fa06221a4d9fc1, 0xb6459df49213fdea, 0x80b494a249d06f8f, 0x3ec8c9d022ee7afe, 0xdacc891bc133fb97, 0xb14a81177e84f07c, 0x90c5def7f86736c0, 0x568c15b7d91d4e9b
#else
      0x30163e90, 0xff6b9c55, 0x6a381e0e, 0xd7bdd12a, 0xf1467b2c, 0x0a972be1, 0x3bc5af9a, 0xbb4dde46, 0x5bea5d42, 0xa36e2f78, 0xfba76e07, 0x0acceb0e, 0xbebf5d2b, 0x9ce13c15, 0x76e3cc01, 0x41f46635, 0xf4fa0622, 0x1a4d9fc1, 0xb6459df4, 0x9213fdea, 0x80b494a2, 0x49d06f8f, 0x3ec8c9d0, 0x22ee7afe, 0xdacc891b, 0xc133fb97, 0xb14a8117, 0x7e84f07c, 0x90c5def7, 0xf86736c0, 0x568c15b7, 0xd91d4e9b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y4 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x61f95c1f2cab90af
#else
      0xc5d700b8
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t qExpectedBuffer4[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x7da5e2f8ea2d575c, 0xe165b54e7eaf4fce, 0x15a3445a93e985f8, 0xb4ad7b374f49cf68, 0x7cee0a14f35e7c66, 0xb9b9980c65a9ed60, 0x2575b7b0aa0be068, 0x1bbfbb6c28f6dba3, 0xc0e8c0915b90dbeb, 0x8cbdf728a23a8488, 0xa416abfdc45d90bd, 0x0ff36f1e9c9b7ba9, 0xfd1f088f478a0bd0, 0xbeb50f2849334468, 0x31be1a023fcda886
#else
      0x0, 0x3e3928c9, 0x728dc242, 0x2fbf8942, 0x983ae1ea, 0xeff12be0, 0x7734a80d, 0x892bc98d, 0x78e024d5, 0xd2d24c9c, 0xefb98d6f, 0x9f3e6b0c, 0x94a009e2, 0xea198943, 0xab22e399, 0xbe9ffc47, 0xf3b3d999, 0x66800d6d, 0x5b836666, 0x93b9c334, 0x90c83e53, 0x4b2542e3, 0x8af52870, 0x4920e2bf, 0x3632aa3f, 0x9189ab45, 0x4644b376, 0xdbbb1815, 0x3ac1fdcc, 0x42029e7c, 0x007de874, 0xa3dad3f3
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t rExpected4 = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x51b182b9f339bb01
#else
      0x13bff7f3
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t qBuffer[size];
    rucrypto_gost_34_10_big_num_underlying_t r;

    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t q = {
        .negative = false,
        .buffer = qBuffer,
        .size = size
    };

    rucrypto_gost_34_10_div_1(&x, y1, &q, &r);
    EXPECT_TRUE(AreBuffersEqual(q.buffer, qExpectedBuffer1, size));
    EXPECT_EQ(r, rExpected1);
    x.buffer = x2Buffer;
    rucrypto_gost_34_10_div_1(&x, y2, &q, &r);
    EXPECT_TRUE(AreBuffersEqual(q.buffer, qExpectedBuffer2, size));
    EXPECT_EQ(r, rExpected2);
    x.buffer = x3Buffer;
    rucrypto_gost_34_10_div_1(&x, y3, &q, &r);
    EXPECT_TRUE(AreBuffersEqual(q.buffer, qExpectedBuffer3, size));
    EXPECT_EQ(r, rExpected3);
    x.buffer = x4Buffer;
    rucrypto_gost_34_10_div_1(&x, y4, &q, &r);
    EXPECT_TRUE(AreBuffersEqual(q.buffer, qExpectedBuffer4, size));
    EXPECT_EQ(r, rExpected4);
}

TEST(Gost3410BasicMathTests, DivTest) {
    constexpr size_t size = 1024 / 8 / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbfa0905ea10c5a06, 0xd3e3f4296556c105, 0x0b0504443b121027, 0x6ad28c8a8b3a4196, 0xa4b98af9a78be09d, 0x8035864002d9a054, 0x592514d86a847f7e, 0x4121c2ab778ce85a, 0xdee75973cf3859da, 0xfac92ad4e99a9cdc, 0xd4a56d6109aaa8dc, 0x5be860d7ee140ff9, 0x03fab12623ad6b79, 0x4019f7418069e18f, 0xdca88414036b7de3, 0x0f0ebd46145e1252
#else
      0xbfa0905e, 0xa10c5a06, 0xd3e3f429, 0x6556c105, 0x0b050444, 0x3b121027, 0x6ad28c8a, 0x8b3a4196, 0xa4b98af9, 0xa78be09d, 0x80358640, 0x02d9a054, 0x592514d8, 0x6a847f7e, 0x4121c2ab, 0x778ce85a, 0xdee75973, 0xcf3859da, 0xfac92ad4, 0xe99a9cdc, 0xd4a56d61, 0x09aaa8dc, 0x5be860d7, 0xee140ff9, 0x03fab126, 0x23ad6b79, 0x4019f741, 0x8069e18f, 0xdca88414, 0x036b7de3, 0x0f0ebd46, 0x145e1252
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x059d0e12c1281578, 0xc21e9bb03762bd95, 0xd0e7184c5b829dcd, 0x4e81e0af14d2a6d9, 0xe6625493f101ba43, 0x5760f89dada25fdd, 0x67cb6bbe0ccd4990, 0xe6fdb1b16e7f2abc, 0x7e00c4fdef9ba51f, 0xc86d47dd9c36e662, 0x8f99c5c9c42922e5, 0x7e1eee506d7127db, 0x204ac3f1706cd566, 0xe411bbb33d4a5c2d, 0xda0feb84b7e46732, 0x17fe4785d9d0b6a3
#else
      0x059d0e12, 0xc1281578, 0xc21e9bb0, 0x3762bd95, 0xd0e7184c, 0x5b829dcd, 0x4e81e0af, 0x14d2a6d9, 0xe6625493, 0xf101ba43, 0x5760f89d, 0xada25fdd, 0x67cb6bbe, 0x0ccd4990, 0xe6fdb1b1, 0x6e7f2abc, 0x7e00c4fd, 0xef9ba51f, 0xc86d47dd, 0x9c36e662, 0x8f99c5c9, 0xc42922e5, 0x7e1eee50, 0x6d7127db, 0x204ac3f1, 0x706cd566, 0xe411bbb3, 0x3d4a5c2d, 0xda0feb84, 0xb7e46732, 0x17fe4785, 0xd9d0b6a3
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t qExpectedBuffer1[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0000000000000022
#else
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x00000022
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t rExpectedBuffer1[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x00c4b1e0f9b97ffd, 0x0bd346c20a39931f, 0x4c53ca2013b91ae2, 0xfd92b549c74018a6, 0x0baa4f53a55123ab, 0xe554814ef348e4ec, 0x9020c59ab740ba3f, 0x9370291acaa93b52, 0x22cd2fb9fc8c6ba2, 0x5c459f662a5003c5, 0xc2392894fc340661, 0x9bccba29650cc4de, 0xba0cab15353913ce, 0xf5bf09735c89a378, 0xe68b3c739715c93b, 0xdf493d7f26a5d0ac
#else
      0x00c4b1e0, 0xf9b97ffd, 0x0bd346c2, 0x0a39931f, 0x4c53ca20, 0x13b91ae2, 0xfd92b549, 0xc74018a6, 0x0baa4f53, 0xa55123ab, 0xe554814e, 0xf348e4ec, 0x9020c59a, 0xb740ba3f, 0x9370291a, 0xcaa93b52, 0x22cd2fb9, 0xfc8c6ba2, 0x5c459f66, 0x2a5003c5, 0xc2392894, 0xfc340661, 0x9bccba29, 0x650cc4de, 0xba0cab15, 0x353913ce, 0xf5bf0973, 0x5c89a378, 0xe68b3c73, 0x9715c93b, 0xdf493d7f, 0x26a5d0ac
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xbe03fdb0ac1ce5ab, 0xd058318fa806b194, 0xc3e270f9f1dd4b04, 0xec29e238cbf84d45, 0x3d3eb648d10c24ab, 0x5b26d87ad2b1f752, 0x6011abccb225a7e0, 0x2b77609e39010149, 0x25f22ae422fb9e8f, 0x0a813ccb047d8176, 0x623b0ec6b0061bdb, 0x151a7db470a14793, 0x67dc005fd1e6836e, 0x61712d35cf7c984e, 0x90ab35bb468753b1, 0x49de18934bda7956
#else
      0xbe03fdb0, 0xac1ce5ab, 0xd058318f, 0xa806b194, 0xc3e270f9, 0xf1dd4b04, 0xec29e238, 0xcbf84d45, 0x3d3eb648, 0xd10c24ab, 0x5b26d87a, 0xd2b1f752, 0x6011abcc, 0xb225a7e0, 0x2b77609e, 0x39010149, 0x25f22ae4, 0x22fb9e8f, 0x0a813ccb, 0x047d8176, 0x623b0ec6, 0xb0061bdb, 0x151a7db4, 0x70a14793, 0x67dc005f, 0xd1e6836e, 0x61712d35, 0xcf7c984e, 0x90ab35bb, 0x468753b1, 0x49de1893, 0x4bda7956
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x064957b4df057431, 0xb423489b19db106f, 0xe37049d86976c4d3, 0xfcf5eb3482bbf573, 0x2baa4f9e113d4ca2, 0x64044caeee9116d0, 0x3f05f5d1948020f3, 0x8e15c3a24f92bac4, 0x634082bc3318d308, 0xdb05ff152dc60124, 0xfdbcf3d3625af35b, 0x0c473d0ca728102d, 0xe158904417a7410c, 0x5f87b0ffb805d6ad, 0xdf39a75f4d75302e, 0x0fc88661663ae62c
#else
      0x064957b4, 0xdf057431, 0xb423489b, 0x19db106f, 0xe37049d8, 0x6976c4d3, 0xfcf5eb34, 0x82bbf573, 0x2baa4f9e, 0x113d4ca2, 0x64044cae, 0xee9116d0, 0x3f05f5d1, 0x948020f3, 0x8e15c3a2, 0x4f92bac4, 0x634082bc, 0x3318d308, 0xdb05ff15, 0x2dc60124, 0xfdbcf3d3, 0x625af35b, 0x0c473d0c, 0xa728102d, 0xe1589044, 0x17a7410c, 0x5f87b0ff, 0xb805d6ad, 0xdf39a75f, 0x4d75302e, 0x0fc88661, 0x663ae62c
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t qExpectedBuffer2[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x000000000000001e
#else
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0000001e
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t rExpectedBuffer2[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x016bb67e897947d8, 0xb435af62a05ac478, 0x1cb9c99d95f23a2d, 0x4758521179f189c6, 0x1f4961c2cbdd29a3, 0xa2a5dbfaddb14aea, 0xfd5edd3d4b21cb55, 0x84ea7398e5cf1e45, 0x8462d8d62612e385, 0x5fcd584fa7495f20, 0xa6167c01295d972f, 0xa4c15638d9ef6232, 0xff7b18650c4ce3fb, 0x2f8a6f3e3ecd6fee, 0x67e9989032cbae4b, 0x705e592950f3802e
#else
      0x016bb67e, 0x897947d8, 0xb435af62, 0xa05ac478, 0x1cb9c99d, 0x95f23a2d, 0x47585211, 0x79f189c6, 0x1f4961c2, 0xcbdd29a3, 0xa2a5dbfa, 0xddb14aea, 0xfd5edd3d, 0x4b21cb55, 0x84ea7398, 0xe5cf1e45, 0x8462d8d6, 0x2612e385, 0x5fcd584f, 0xa7495f20, 0xa6167c01, 0x295d972f, 0xa4c15638, 0xd9ef6232, 0xff7b1865, 0x0c4ce3fb, 0x2f8a6f3e, 0x3ecd6fee, 0x67e99890, 0x32cbae4b, 0x705e5929, 0x50f3802e
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x9fc5b0ca33900a2b, 0xbf4c4e57b6c23c4c, 0x0b4dcc1ebb69bfbd, 0x12f1445648bace8a, 0xc40bfa06a4adca39, 0x57a2057125397f57, 0xb89dfab1e765dc13, 0xcb211b0c581889d4, 0x7f91660294ecdb32, 0x07dbe5de9ba38675, 0xff23ca68e4a2c1bc, 0x38ae76d9beef6cac, 0x784fe5d50478f162, 0x379f0b433a2b19e3, 0xeb7fb1f829b510a3, 0x2886d56f9b3f69a6
#else
      0x9fc5b0ca, 0x33900a2b, 0xbf4c4e57, 0xb6c23c4c, 0x0b4dcc1e, 0xbb69bfbd, 0x12f14456, 0x48bace8a, 0xc40bfa06, 0xa4adca39, 0x57a20571, 0x25397f57, 0xb89dfab1, 0xe765dc13, 0xcb211b0c, 0x581889d4, 0x7f916602, 0x94ecdb32, 0x07dbe5de, 0x9ba38675, 0xff23ca68, 0xe4a2c1bc, 0x38ae76d9, 0xbeef6cac, 0x784fe5d5, 0x0478f162, 0x379f0b43, 0x3a2b19e3, 0xeb7fb1f8, 0x29b510a3, 0x2886d56f, 0x9b3f69a6
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0f97e99a1b45970c, 0xc5f2718dad769307, 0xc5a34b66b1e9a62d, 0xbe28ceeb5e0807d2, 0x99fd6d5e1a7e756f, 0x45a26c98df0a4b4e, 0x03d015bfdfd76aef, 0x604f6392ef8b5ed6, 0x0dda9d8e787c8ddf, 0xb87f1719033e07f1, 0xef3b9e3fdef34d7f, 0x83175a589208cb39, 0xeb83158f484d4c41, 0xfd6474fd6867c909, 0xa6110b290c8a336c, 0x2a54e48f3015e9fc
#else
      0x0f97e99a, 0x1b45970c, 0xc5f2718d, 0xad769307, 0xc5a34b66, 0xb1e9a62d, 0xbe28ceeb, 0x5e0807d2, 0x99fd6d5e, 0x1a7e756f, 0x45a26c98, 0xdf0a4b4e, 0x03d015bf, 0xdfd76aef, 0x604f6392, 0xef8b5ed6, 0x0dda9d8e, 0x787c8ddf, 0xb87f1719, 0x033e07f1, 0xef3b9e3f, 0xdef34d7f, 0x83175a58, 0x9208cb39, 0xeb83158f, 0x484d4c41, 0xfd6474fd, 0x6867c909, 0xa6110b29, 0x0c8a336c, 0x2a54e48f, 0x3015e9fc
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t qExpectedBuffer3[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x000000000000000a
#else
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0000000a
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t rExpectedBuffer3[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x03d690c522d823ac, 0x03d3decef0207dfe, 0x52ecda1bc84941f3, 0xa5592f249c6a8050, 0xc025b4599bbd33e0, 0x9f49c7786ed28e4b, 0x927d213328fbaeba, 0x0807374efca6d577, 0xf5073e71e00f5074, 0xd2e4fee47b373702, 0xa6cf9bea2f21bac1, 0x19c4ef640a977c69, 0x45310e3c3173f6ce, 0x51b2795d261d3f83, 0x6ed5425dac4f0e69, 0x8135e7d7ba6445ce
#else
      0x03d690c5, 0x22d823ac, 0x03d3dece, 0xf0207dfe, 0x52ecda1b, 0xc84941f3, 0xa5592f24, 0x9c6a8050, 0xc025b459, 0x9bbd33e0, 0x9f49c778, 0x6ed28e4b, 0x927d2133, 0x28fbaeba, 0x0807374e, 0xfca6d577, 0xf5073e71, 0xe00f5074, 0xd2e4fee4, 0x7b373702, 0xa6cf9bea, 0x2f21bac1, 0x19c4ef64, 0x0a977c69, 0x45310e3c, 0x3173f6ce, 0x51b2795d, 0x261d3f83, 0x6ed5425d, 0xac4f0e69, 0x8135e7d7, 0xba6445ce
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x30163e90ff6b9c55, 0x6a381e0ed7bdd12a, 0xf1467b2c0a972be1, 0x3bc5af9abb4dde46, 0x5bea5d42a36e2f78, 0xfba76e070acceb0e, 0xbebf5d2b9ce13c15, 0x76e3cc0141f46635, 0xf4fa06221a4d9fc1, 0xb6459df49213fdea, 0x80b494a249d06f8f, 0x3ec8c9d022ee7afe, 0xdacc891bc133fb97, 0xb14a81177e84f07c, 0x90c5def7f86736c0, 0x568c15b7d91d4e9b
#else
      0x30163e90, 0xff6b9c55, 0x6a381e0e, 0xd7bdd12a, 0xf1467b2c, 0x0a972be1, 0x3bc5af9a, 0xbb4dde46, 0x5bea5d42, 0xa36e2f78, 0xfba76e07, 0x0acceb0e, 0xbebf5d2b, 0x9ce13c15, 0x76e3cc01, 0x41f46635, 0xf4fa0622, 0x1a4d9fc1, 0xb6459df4, 0x9213fdea, 0x80b494a2, 0x49d06f8f, 0x3ec8c9d0, 0x22ee7afe, 0xdacc891b, 0xc133fb97, 0xb14a8117, 0x7e84f07c, 0x90c5def7, 0xf86736c0, 0x568c15b7, 0xd91d4e9b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0e88b400bea04c3c, 0x03fb127d21269b4e, 0x7011a81b4fc71deb, 0xa022722d0bfb9053, 0x70ff915ffe506c39, 0x0c450ace51967535, 0xd1788844721957e5, 0xdaa3b2c1bed90213, 0x73e5e64fb1f2267a, 0x12849cb349804395, 0x659dc313e1b75f1c, 0xa44e4348f828c63e, 0x73e3afba3829e95c, 0x009faec2dd448938, 0x70e93ba65556c360, 0xb733f3228fde0b81
#else
      0x0e88b400, 0xbea04c3c, 0x03fb127d, 0x21269b4e, 0x7011a81b, 0x4fc71deb, 0xa022722d, 0x0bfb9053, 0x70ff915f, 0xfe506c39, 0x0c450ace, 0x51967535, 0xd1788844, 0x721957e5, 0xdaa3b2c1, 0xbed90213, 0x73e5e64f, 0xb1f2267a, 0x12849cb3, 0x49804395, 0x659dc313, 0xe1b75f1c, 0xa44e4348, 0xf828c63e, 0x73e3afba, 0x3829e95c, 0x009faec2, 0xdd448938, 0x70e93ba6, 0x5556c360, 0xb733f322, 0x8fde0b81
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t qExpectedBuffer4[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0000000000000003
#else
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,0x00000003
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t rExpectedBuffer4[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x047c228ec38ab7a1, 0x5e46e6977449ff3f, 0xa11182da1b41d21e, 0x5b5e5913975b2d4c, 0x08eba922a87ceacd, 0xd6d84d9c16098b6d, 0x4a55c45e46953463, 0xe6f8b3bc05695ffb, 0x9948533304772c53, 0x7eb7c7dab593332a, 0x4fdb4b66a4aa5239, 0x51ddfff53a742843, 0x7f2179ed18b63f83, 0xaf6b74cee6b754d3, 0x3e0a2c04f862ec9e, 0x30f03c5029832c18
#else
      0x047c228e, 0xc38ab7a1, 0x5e46e697, 0x7449ff3f, 0xa11182da, 0x1b41d21e, 0x5b5e5913, 0x975b2d4c, 0x08eba922, 0xa87ceacd, 0xd6d84d9c, 0x16098b6d, 0x4a55c45e, 0x46953463, 0xe6f8b3bc, 0x05695ffb, 0x99485333, 0x04772c53, 0x7eb7c7da, 0xb593332a, 0x4fdb4b66, 0xa4aa5239, 0x51ddfff5, 0x3a742843, 0x7f2179ed, 0x18b63f83, 0xaf6b74ce, 0xe6b754d3, 0x3e0a2c04, 0xf862ec9e, 0x30f03c50, 0x29832c18
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t qBuffer[size];
    rucrypto_gost_34_10_big_num_underlying_t rBuffer[size];
    rucrypto_gost_34_10_big_num_t x {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t y {
        .negative = false,
        .buffer = y1Buffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t q {
        .negative = false,
        .buffer = qBuffer,
        .size = size
    };
    rucrypto_gost_34_10_big_num_t r {
        .negative = false,
        .buffer = rBuffer,
        .size = size
    };

    rucrypto_gost_34_10_div(&x, &y, &q, &r, false);
    EXPECT_TRUE(AreBuffersEqual(q.buffer, qExpectedBuffer1, size));
    EXPECT_TRUE(AreBuffersEqual(r.buffer, rExpectedBuffer1, size));
    x.buffer = x2Buffer;
    y.buffer = y2Buffer;
    rucrypto_gost_34_10_div(&x, &y, &q, &r, false);
    EXPECT_TRUE(AreBuffersEqual(q.buffer, qExpectedBuffer2, size));
    EXPECT_TRUE(AreBuffersEqual(r.buffer, rExpectedBuffer2, size));
    x.buffer = x3Buffer;
    y.buffer = y3Buffer;
    rucrypto_gost_34_10_div(&x, &y, &q, &r, false);
    EXPECT_TRUE(AreBuffersEqual(q.buffer, qExpectedBuffer3, size));
    EXPECT_TRUE(AreBuffersEqual(r.buffer, rExpectedBuffer3, size));
    x.buffer = x4Buffer;
    y.buffer = y4Buffer;
    rucrypto_gost_34_10_div(&x, &y, &q, &r, false);
    EXPECT_TRUE(AreBuffersEqual(q.buffer, qExpectedBuffer4, size));
    EXPECT_TRUE(AreBuffersEqual(r.buffer, rExpectedBuffer4, size));
}

TEST(Gost3410BasicMathTests, SignedSubTest) {
    static rucrypto_gost_34_10_big_num_underlying_t xBuffer[] = {
        0x0, 0x1
    };
    static rucrypto_gost_34_10_big_num_underlying_t yBuffer[] = {
        0x0, 0x2
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer1[] = {
        0x0, 0x1
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer2[] = {
        0x0, 0x3
    };

    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = xBuffer,
        .size = 2
    };

    rucrypto_gost_34_10_big_num_t y = {
        .negative = false,
        .buffer = yBuffer,
        .size = 2
    };

    rucrypto_gost_34_10_big_num_underlying_t resBuffer[2] = {0};
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = 2
    };

    rucrypto_gost_34_10_signed_sub(&x, &y, &res);
    EXPECT_TRUE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer1, 2));
    res.negative = false;
    RUCRYPTO_MEMSET(res.buffer, 0, res.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));

    x.negative = true;
    rucrypto_gost_34_10_signed_sub(&x, &y, &res);
    EXPECT_TRUE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer2, 2));
    res.negative = false;
    RUCRYPTO_MEMSET(res.buffer, 0, res.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));

    y.negative = true;
    rucrypto_gost_34_10_signed_sub(&x, &y, &res);
    EXPECT_FALSE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer1, 2));
    res.negative = false;
    RUCRYPTO_MEMSET(res.buffer, 0, res.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));

    x.negative = false;
    rucrypto_gost_34_10_signed_sub(&x, &y, &res);
    EXPECT_FALSE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer2, 2));
    res.negative = false;
    RUCRYPTO_MEMSET(res.buffer, 0, res.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
}

TEST(Gost3410BasicMathTests, SignedAddTest) {
    static rucrypto_gost_34_10_big_num_underlying_t xBuffer[] = {
        0x0, 0x1
    };
    static rucrypto_gost_34_10_big_num_underlying_t yBuffer[] = {
        0x0, 0x2
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer1[] = {
        0x0, 0x3
    };
    static const rucrypto_gost_34_10_big_num_underlying_t expectedBuffer2[] = {
        0x0, 0x1
    };

    rucrypto_gost_34_10_big_num_t x = {
        .negative = false,
        .buffer = xBuffer,
        .size = 2
    };

    rucrypto_gost_34_10_big_num_t y = {
        .negative = false,
        .buffer = yBuffer,
        .size = 2
    };

    rucrypto_gost_34_10_big_num_underlying_t resBuffer[2] = {0};
    rucrypto_gost_34_10_big_num_t res = {
        .negative = false,
        .buffer = resBuffer,
        .size = 2
    };

    rucrypto_gost_34_10_signed_add(&x, &y, &res);
    EXPECT_FALSE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer1, 2));
    res.negative = false;
    RUCRYPTO_MEMSET(res.buffer, 0, res.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));

    x.negative = true;
    rucrypto_gost_34_10_signed_add(&x, &y, &res);
    EXPECT_FALSE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer2, 2));
    res.negative = false;
    RUCRYPTO_MEMSET(res.buffer, 0, res.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));

    y.negative = true;
    rucrypto_gost_34_10_signed_add(&x, &y, &res);
    EXPECT_TRUE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer1, 2));
    res.negative = false;
    RUCRYPTO_MEMSET(res.buffer, 0, res.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));

    x.negative = false;
    rucrypto_gost_34_10_signed_add(&x, &y, &res);
    EXPECT_TRUE(res.negative);
    EXPECT_TRUE(AreBuffersEqual(res.buffer, expectedBuffer2, 2));
    res.negative = false;
    RUCRYPTO_MEMSET(res.buffer, 0, res.size * sizeof(rucrypto_gost_34_10_big_num_underlying_t));
}

TEST(Gost3410BasicMathTests, ExtendedGCDTest) {
    constexpr size_t size = 1024 / 8 / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x5235e08e4cbcc24c, 0x902bf2cc72ae0c49, 0x4caed49d5e1a45ff, 0xefeb1f4094445b70, 0x7e119dd2552c917a, 0x7ca437282a946049, 0xe3373924ef403119, 0x5f4d9fc3ffd81a76, 0xc73ed682dfc571bc, 0xb737e5c243b8d585, 0x4fd3f29ef0a450bb, 0x6d4a7211e85298c8, 0x20fa7533c945c1b3, 0x770d6f394cd80455, 0xf2fcb76841578cef, 0x58743c010aba764c
#else
      0x5235e08e, 0x4cbcc24c, 0x902bf2cc, 0x72ae0c49, 0x4caed49d, 0x5e1a45ff, 0xefeb1f40, 0x94445b70, 0x7e119dd2, 0x552c917a, 0x7ca43728, 0x2a946049, 0xe3373924, 0xef403119, 0x5f4d9fc3, 0xffd81a76, 0xc73ed682, 0xdfc571bc, 0xb737e5c2, 0x43b8d585, 0x4fd3f29e, 0xf0a450bb, 0x6d4a7211, 0xe85298c8, 0x20fa7533, 0xc945c1b3, 0x770d6f39, 0x4cd80455, 0xf2fcb768, 0x41578cef, 0x58743c01, 0x0aba764c
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xc8604c7308ad565b, 0x67b00499c2ad7795, 0xe2972293413fdd9a, 0xa93d893ade913b4a, 0x8822598ff22cd06f, 0x75297bd4b15c1fbc, 0x58c125b8b3397dbc, 0x74df8812270b2192, 0xa34cdad0f6a84e1e, 0xa468a50b2113f7b9, 0x20ba276f2e54c1c0, 0x45e5a52404827ecb, 0x58268dc2542c6e4f, 0x8fcf7608d22b0c6d, 0xee3487eb6c580981, 0x195a54e980fb2a07
#else
      0xc8604c73, 0x08ad565b, 0x67b00499, 0xc2ad7795, 0xe2972293, 0x413fdd9a, 0xa93d893a, 0xde913b4a, 0x8822598f, 0xf22cd06f, 0x75297bd4, 0xb15c1fbc, 0x58c125b8, 0xb3397dbc, 0x74df8812, 0x270b2192, 0xa34cdad0, 0xf6a84e1e, 0xa468a50b, 0x2113f7b9, 0x20ba276f, 0x2e54c1c0, 0x45e5a524, 0x04827ecb, 0x58268dc2, 0x542c6e4f, 0x8fcf7608, 0xd22b0c6d, 0xee3487eb, 0x6c580981, 0x195a54e9, 0x80fb2a07
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const bool uExpectedNegative1 = false;
    static const rucrypto_gost_34_10_big_num_underlying_t uExpectedBuffer1[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x4994fffd3f9e7fd3, 0x668c221518a208e8, 0xf2259285b7c68dc6, 0xf7549a733213786a, 0x213f5b2a12c45c17, 0x9303991bb3592261, 0x24054ab474c612ec, 0x860d43256f1c4f24, 0x0a56f1ab3c42635b, 0xf1813a90364839ef, 0xcdce64bafb872edf, 0x7c7eec283875ff13, 0x91fc879d11f34476, 0x83b90f35fb0d12d8, 0xa2b95f5c4f814b1b, 0xbe77526e4044a06f
#else
      0x4994fffd, 0x3f9e7fd3, 0x668c2215, 0x18a208e8, 0xf2259285, 0xb7c68dc6, 0xf7549a73, 0x3213786a, 0x213f5b2a, 0x12c45c17, 0x9303991b, 0xb3592261, 0x24054ab4, 0x74c612ec, 0x860d4325, 0x6f1c4f24, 0x0a56f1ab, 0x3c42635b, 0xf1813a90, 0x364839ef, 0xcdce64ba, 0xfb872edf, 0x7c7eec28, 0x3875ff13, 0x91fc879d, 0x11f34476, 0x83b90f35, 0xfb0d12d8, 0xa2b95f5c, 0x4f814b1b, 0xbe77526e, 0x4044a06f
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const bool vExpectedNegative1 = true;
    static const rucrypto_gost_34_10_big_num_underlying_t vExpectedBuffer1[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x1e3074bd02c98d25, 0x9ab9bcb4aa60d83c, 0x909be8a2d5681726, 0xeb79f5bebddcc0c4, 0xafd700cab08f02cf, 0xf13d008ae3203d17, 0xa1155592ad69169e, 0xc4b56bfe48fb9d98, 0x16476ba38c742188, 0x4dcf20d3b3b5879d, 0x704714da42f9c8c2, 0x8910c78a5d184508, 0x54b83fd788ef9da2, 0x3a8bead9056f084d, 0x02111b41be62e689, 0xed274acf7b464cb5
#else
      0x1e3074bd, 0x02c98d25, 0x9ab9bcb4, 0xaa60d83c, 0x909be8a2, 0xd5681726, 0xeb79f5be, 0xbddcc0c4, 0xafd700ca, 0xb08f02cf, 0xf13d008a, 0xe3203d17, 0xa1155592, 0xad69169e, 0xc4b56bfe, 0x48fb9d98, 0x16476ba3, 0x8c742188, 0x4dcf20d3, 0xb3b5879d, 0x704714da, 0x42f9c8c2, 0x8910c78a, 0x5d184508, 0x54b83fd7, 0x88ef9da2, 0x3a8bead9, 0x056f084d, 0x02111b41, 0xbe62e689, 0xed274acf, 0x7b464cb5
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t dExpectedBuffer1[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0000000000000001
#else
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x00000001
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xe2e013f99de467ca, 0x96acba69ab3e614f, 0xb630a644b3dc475f, 0x5ef1d749eb5f0d71, 0xe2691b24144f14c1, 0x23a8b9825df2d27a, 0x4bcda96edf86fdf9, 0xea4d80743faf389b, 0x8af729b5b5cadee5, 0x1a1e068aa2015187, 0x8f6b21f80f0f343b, 0x77d2bfe8b82f7a7b, 0x5bec2f6763ef5987, 0x583c7d408f01aa57, 0x20a52d43cedb4508, 0xbf11c65c0171b66a
#else
      0xe2e013f9, 0x9de467ca, 0x96acba69, 0xab3e614f, 0xb630a644, 0xb3dc475f, 0x5ef1d749, 0xeb5f0d71, 0xe2691b24, 0x144f14c1, 0x23a8b982, 0x5df2d27a, 0x4bcda96e, 0xdf86fdf9, 0xea4d8074, 0x3faf389b, 0x8af729b5, 0xb5cadee5, 0x1a1e068a, 0xa2015187, 0x8f6b21f8, 0x0f0f343b, 0x77d2bfe8, 0xb82f7a7b, 0x5bec2f67, 0x63ef5987, 0x583c7d40, 0x8f01aa57, 0x20a52d43, 0xcedb4508, 0xbf11c65c, 0x0171b66a
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x6d736e005d2821dc, 0x5f0f883eeb10740b, 0x477aebe976a51363, 0x5bff445cd497f2d5, 0x4b66905ff1d9e832, 0x181ae493117c008c, 0xfcc125dfe6a73bd1, 0x2f72814f302dbb1e, 0x02ce49508da4638b, 0x4794fc913d38ce8e, 0xffaf44853b799364, 0x0a492004da14bb73, 0x2186440583d77d3e, 0x10451944080c02b1, 0xc54bf7e385cd4edd, 0xe3f4f257298bffb9
#else
      0x6d736e00, 0x5d2821dc, 0x5f0f883e, 0xeb10740b, 0x477aebe9, 0x76a51363, 0x5bff445c, 0xd497f2d5, 0x4b66905f, 0xf1d9e832, 0x181ae493, 0x117c008c, 0xfcc125df, 0xe6a73bd1, 0x2f72814f, 0x302dbb1e, 0x02ce4950, 0x8da4638b, 0x4794fc91, 0x3d38ce8e, 0xffaf4485, 0x3b799364, 0x0a492004, 0xda14bb73, 0x21864405, 0x83d77d3e, 0x10451944, 0x080c02b1, 0xc54bf7e3, 0x85cd4edd, 0xe3f4f257, 0x298bffb9
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const bool uExpectedNegative2 = false;
    static const rucrypto_gost_34_10_big_num_underlying_t uExpectedBuffer2[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x3d57a3666a6b4d99, 0x33e0acbb1cf83ece, 0x745f71bd76ff373f, 0xc7e8fee6a14823f9, 0x166d4f28805b8334, 0x9a93333de8570f69, 0x1e887b550d59f591, 0x07836f9b67e80ff9, 0xbe70976f96c4e077, 0xd77c3338a0098522, 0xeb180a4958f59611, 0x1de3cdfe6001b952, 0x0a2c2b385c5773cc, 0x5fa21fe7fa08c532, 0xe3d6a6685dd41060, 0xa2d9fcb1d85432ca
#else
      0x3d57a366, 0x6a6b4d99, 0x33e0acbb, 0x1cf83ece, 0x745f71bd, 0x76ff373f, 0xc7e8fee6, 0xa14823f9, 0x166d4f28, 0x805b8334, 0x9a93333d, 0xe8570f69, 0x1e887b55, 0x0d59f591, 0x07836f9b, 0x67e80ff9, 0xbe70976f, 0x96c4e077, 0xd77c3338, 0xa0098522, 0xeb180a49, 0x58f59611, 0x1de3cdfe, 0x6001b952, 0x0a2c2b38, 0x5c5773cc, 0x5fa21fe7, 0xfa08c532, 0xe3d6a668, 0x5dd41060, 0xa2d9fcb1, 0xd85432ca
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const bool vExpectedNegative2 = true;
    static const rucrypto_gost_34_10_big_num_underlying_t vExpectedBuffer2[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x7f2749d47f8f5bb8, 0xda57d26f171a0909, 0x60a447d0e76c1e2b, 0x230fbac9fac336ed, 0x5cc71b44d6075122, 0x30db3de8c2377415, 0x4fa064bf86fc1eb6, 0xeb2cadd14c788ad8, 0xa507ecc94123cedd, 0x1eeda15472c1206e, 0x4e2d0d123a49568b, 0xfd85137311f5416f, 0xba1fa5b9b4c5c7db, 0x753acbe3b38f004c, 0xe7a9736452ee0e56, 0x87172ab847a6543b
#else
      0x7f2749d4, 0x7f8f5bb8, 0xda57d26f, 0x171a0909, 0x60a447d0, 0xe76c1e2b, 0x230fbac9, 0xfac336ed, 0x5cc71b44, 0xd6075122, 0x30db3de8, 0xc2377415, 0x4fa064bf, 0x86fc1eb6, 0xeb2cadd1, 0x4c788ad8, 0xa507ecc9, 0x4123cedd, 0x1eeda154, 0x72c1206e, 0x4e2d0d12, 0x3a49568b, 0xfd851373, 0x11f5416f, 0xba1fa5b9, 0xb4c5c7db, 0x753acbe3, 0xb38f004c, 0xe7a97364, 0x52ee0e56, 0x87172ab8, 0x47a6543b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t dExpectedBuffer2[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0000000000000001
#else
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x00000001
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xeb2b92cbef6cc100, 0x00e93af48ee36b41, 0x453e2e6d83b35ece, 0x2e458d8a5ac2c2b2, 0x58eddbf2a8ee68b5, 0x55b4b938c7eb20a0, 0xdbef698772b34dd7, 0x4f78e861e651a077, 0x026fcfc01b3f3f8f, 0xa29d9f690a04906c, 0x775f064f6a3c80fa, 0x75b879ff64e3ab10, 0x591c3a5f0b2b904d, 0x2931cb48a7e48b83, 0x33de3e0db9f3c7a5, 0x45b9c3e619569538
#else
      0xeb2b92cb, 0xef6cc100, 0x00e93af4, 0x8ee36b41, 0x453e2e6d, 0x83b35ece, 0x2e458d8a, 0x5ac2c2b2, 0x58eddbf2, 0xa8ee68b5, 0x55b4b938, 0xc7eb20a0, 0xdbef6987, 0x72b34dd7, 0x4f78e861, 0xe651a077, 0x026fcfc0, 0x1b3f3f8f, 0xa29d9f69, 0x0a04906c, 0x775f064f, 0x6a3c80fa, 0x75b879ff, 0x64e3ab10, 0x591c3a5f, 0x0b2b904d, 0x2931cb48, 0xa7e48b83, 0x33de3e0d, 0xb9f3c7a5, 0x45b9c3e6, 0x19569538
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xd51b93043ce01c05, 0x31dfe551be46667b, 0xfde7a81073bd6580, 0xd5a5440ff4c9ea37, 0xdbe988848efaf5ca, 0x901b4708be86d381, 0x3dd6d5b10caae102, 0x414f6b6d48f18208, 0x10c00f5d09a3d889, 0x88ed610db622c22b, 0x030782800e7e15f7, 0x82c037d600050db0, 0x8914f48c8586b040, 0xe524ce9a38276b61, 0x9e98a0a80b131039, 0x4a103723b9571940
#else
      0xd51b9304, 0x3ce01c05, 0x31dfe551, 0xbe46667b, 0xfde7a810, 0x73bd6580, 0xd5a5440f, 0xf4c9ea37, 0xdbe98884, 0x8efaf5ca, 0x901b4708, 0xbe86d381, 0x3dd6d5b1, 0x0caae102, 0x414f6b6d, 0x48f18208, 0x10c00f5d, 0x09a3d889, 0x88ed610d, 0xb622c22b, 0x03078280, 0x0e7e15f7, 0x82c037d6, 0x00050db0, 0x8914f48c, 0x8586b040, 0xe524ce9a, 0x38276b61, 0x9e98a0a8, 0x0b131039, 0x4a103723, 0xb9571940
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const bool uExpectedNegative3 = true;
    static const rucrypto_gost_34_10_big_num_underlying_t uExpectedBuffer3[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0312cee2fb543d98, 0x80709f204fe6a1de, 0x0bf257f15e3632cc, 0x4ebec7b44f93c5c9, 0x383a73420f2845a5, 0x8bf257dd8275f2cc, 0x8a48d8e8ea866db8, 0xc59c4891e75fe5ab, 0x0ccb9a62c3444a7a, 0x79cb44ddf549394e, 0x4e9c341cea53a50d, 0xc2811d00f9ab8a41, 0xb812f9051047e988, 0x1b71372f3a4cd9a7, 0xe5669fae33d4a2f7, 0xb6e6e6ec7d76bd99
#else
      0x0312cee2, 0xfb543d98, 0x80709f20, 0x4fe6a1de, 0x0bf257f1, 0x5e3632cc, 0x4ebec7b4, 0x4f93c5c9, 0x383a7342, 0x0f2845a5, 0x8bf257dd, 0x8275f2cc, 0x8a48d8e8, 0xea866db8, 0xc59c4891, 0xe75fe5ab, 0x0ccb9a62, 0xc3444a7a, 0x79cb44dd, 0xf549394e, 0x4e9c341c, 0xea53a50d, 0xc2811d00, 0xf9ab8a41, 0xb812f905, 0x1047e988, 0x1b71372f, 0x3a4cd9a7, 0xe5669fae, 0x33d4a2f7, 0xb6e6e6ec, 0x7d76bd99
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const bool vExpectedNegative3 = false;
    static const rucrypto_gost_34_10_big_num_underlying_t vExpectedBuffer3[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x036443aee57a4cfb, 0x07b0da4a5f5b0742, 0x81823521a97d1fa5, 0xdd9cb0e49f93c213, 0x719c98a41bb8fa65, 0xd6dc9e4f2f1e24a3, 0x7a4105db50a0cc96, 0x9e2b5b8e1cc205ff, 0xa8efb43fbfb10901, 0xa245e650cdaae392, 0x1449f3ce00519e37, 0x965f3ec5a6efbdcc, 0x33c7788ee14fe13a, 0x93fc632230986fbc, 0x125b38605c56413f, 0x363515826ab8db12
#else
      0x036443ae, 0xe57a4cfb, 0x07b0da4a, 0x5f5b0742, 0x81823521, 0xa97d1fa5, 0xdd9cb0e4, 0x9f93c213, 0x719c98a4, 0x1bb8fa65, 0xd6dc9e4f, 0x2f1e24a3, 0x7a4105db, 0x50a0cc96, 0x9e2b5b8e, 0x1cc205ff, 0xa8efb43f, 0xbfb10901, 0xa245e650, 0xcdaae392, 0x1449f3ce, 0x00519e37, 0x965f3ec5, 0xa6efbdcc, 0x33c7788e, 0xe14fe13a, 0x93fc6322, 0x30986fbc, 0x125b3860, 0x5c56413f, 0x36351582, 0x6ab8db12
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t dExpectedBuffer3[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x00000008
#else
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x00000008
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x8125a80405e6c1c1, 0xef732f6a14ccaf8f, 0x29a460a5f874de65, 0xdaf141c89d919f8c, 0x7f4e95636a13615d, 0x2b262502b6d57a6b, 0x9d8d40b9b29de80d, 0x0491e93a2edd420a, 0x8a8f29f4b78240b4, 0x52ded1fd7019c400, 0x798fc8d67314445a, 0x75161f3ceae3f9a7, 0x3f7ba908764c6f74, 0xe82b4704a8e21d52, 0x43ec9c1f8f0eeb1c, 0x7aeb695ac6657cba
#else
      0x8125a804, 0x05e6c1c1, 0xef732f6a, 0x14ccaf8f, 0x29a460a5, 0xf874de65, 0xdaf141c8, 0x9d919f8c, 0x7f4e9563, 0x6a13615d, 0x2b262502, 0xb6d57a6b, 0x9d8d40b9, 0xb29de80d, 0x0491e93a, 0x2edd420a, 0x8a8f29f4, 0xb78240b4, 0x52ded1fd, 0x7019c400, 0x798fc8d6, 0x7314445a, 0x75161f3c, 0xeae3f9a7, 0x3f7ba908, 0x764c6f74, 0xe82b4704, 0xa8e21d52, 0x43ec9c1f, 0x8f0eeb1c, 0x7aeb695a, 0xc6657cba
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xe088759cbcff2172, 0xd72db7afa11e84e9, 0x89818afea10cdbcb, 0x0dfdb58e051f79eb, 0x6c5125b73e531b67, 0x7fd5b5e436754a78, 0x3237421283118c81, 0xe28170364e13aee5, 0x597f788a20cda738, 0xfba1f2f4324c8798, 0x415c9cc5cf9554d1, 0xb2920dfd62aabb3e, 0x81e7ddcaf38e37d5, 0x0f5b4079afb43790, 0xb2328ab77f5da0d9, 0x8114d5698dde6429
#else
      0xe088759c, 0xbcff2172, 0xd72db7af, 0xa11e84e9, 0x89818afe, 0xa10cdbcb, 0x0dfdb58e, 0x051f79eb, 0x6c5125b7, 0x3e531b67, 0x7fd5b5e4, 0x36754a78, 0x32374212, 0x83118c81, 0xe2817036, 0x4e13aee5, 0x597f788a, 0x20cda738, 0xfba1f2f4, 0x324c8798, 0x415c9cc5, 0xcf9554d1, 0xb2920dfd, 0x62aabb3e, 0x81e7ddca, 0xf38e37d5, 0x0f5b4079, 0xafb43790, 0xb2328ab7, 0x7f5da0d9, 0x8114d569, 0x8dde6429
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const bool uExpectedNegative4 = false;
    static const rucrypto_gost_34_10_big_num_underlying_t uExpectedBuffer4[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xb102e08d6e47dbfa, 0x1ae9e9c99bff4022, 0x8adf4f448c86fb2d, 0x475c04f25ce05027, 0x9959701e4bb247b3, 0xe1c1c975d926521a, 0x253b3ad0054ad56d, 0x3211d3b99d6ed2b0, 0xec226008481f20e7, 0x8ec92e1e594110ad, 0xda1153205671d542, 0xca49e8e4f62f3756, 0x0882c66860900db2, 0xf2ef2a0b8acc04dd, 0x2bd4f711117505c0, 0xe7f57ac0507e0c9b
#else
      0xb102e08d, 0x6e47dbfa, 0x1ae9e9c9, 0x9bff4022, 0x8adf4f44, 0x8c86fb2d, 0x475c04f2, 0x5ce05027, 0x9959701e, 0x4bb247b3, 0xe1c1c975, 0xd926521a, 0x253b3ad0, 0x054ad56d, 0x3211d3b9, 0x9d6ed2b0, 0xec226008, 0x481f20e7, 0x8ec92e1e, 0x594110ad, 0xda115320, 0x5671d542, 0xca49e8e4, 0xf62f3756, 0x0882c668, 0x60900db2, 0xf2ef2a0b, 0x8acc04dd, 0x2bd4f711, 0x117505c0, 0xe7f57ac0, 0x507e0c9b
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const bool vExpectedNegative4 = true;
    static const rucrypto_gost_34_10_big_num_underlying_t vExpectedBuffer4[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x65d03f3e09cc1a22, 0x9854049badb49508, 0x54878d298175bec0, 0xcb40d6cf247f1784, 0x2504be7f9ce05593, 0x6edad46b1be5cab6, 0xcde577646a4d5afc, 0xbbe3027de1b012a9, 0x6dafe0750fc210aa, 0x4d1cb7cde53a2e63, 0x93b54f3ba1d16456, 0x6816edacaf1e6508, 0x713b3063312f9561, 0xc823069d58d5a547, 0x4899dacd870a5dc4, 0x86ef826d0a0c9323
#else
      0x65d03f3e, 0x09cc1a22, 0x9854049b, 0xadb49508, 0x54878d29, 0x8175bec0, 0xcb40d6cf, 0x247f1784, 0x2504be7f, 0x9ce05593, 0x6edad46b, 0x1be5cab6, 0xcde57764, 0x6a4d5afc, 0xbbe3027d, 0xe1b012a9, 0x6dafe075, 0x0fc210aa, 0x4d1cb7cd, 0xe53a2e63, 0x93b54f3b, 0xa1d16456, 0x6816edac, 0xaf1e6508, 0x713b3063, 0x312f9561, 0xc823069d, 0x58d5a547, 0x4899dacd, 0x870a5dc4, 0x86ef826d, 0x0a0c9323
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t dExpectedBuffer4[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0000000000000003
#else
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x00000003
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t dBuffer[size];
    rucrypto_gost_34_10_big_num_underlying_t uBuffer[size];
    rucrypto_gost_34_10_big_num_underlying_t vBuffer[size];

    rucrypto_gost_34_10_big_num_t x {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };

    rucrypto_gost_34_10_big_num_t y {
        .negative = false,
        .buffer = y1Buffer,
        .size = size
    };

    rucrypto_gost_34_10_big_num_t d {
        .negative = false,
        .buffer = dBuffer,
        .size = size
    };

    rucrypto_gost_34_10_big_num_t u {
        .negative = false,
        .buffer = uBuffer,
        .size = size
    };

    rucrypto_gost_34_10_big_num_t v {
        .negative = false,
        .buffer = vBuffer,
        .size = size
    };

    rucrypto_gost_34_10_extended_gcd(&x, &y, &d, &u, &v, false);
    EXPECT_FALSE(d.negative);
    EXPECT_TRUE(AreBuffersEqual(d.buffer, dExpectedBuffer1, size));
    EXPECT_EQ(u.negative, uExpectedNegative1);
    EXPECT_TRUE(AreBuffersEqual(u.buffer, uExpectedBuffer1, size));
    EXPECT_EQ(v.negative, vExpectedNegative1);
    EXPECT_TRUE(AreBuffersEqual(v.buffer, vExpectedBuffer1, size));
    x.buffer = x2Buffer;
    y.buffer = y2Buffer;
    rucrypto_gost_34_10_extended_gcd(&x, &y, &d, &u, &v, false);
    EXPECT_FALSE(d.negative);
    EXPECT_TRUE(AreBuffersEqual(d.buffer, dExpectedBuffer2, size));
    EXPECT_EQ(u.negative, uExpectedNegative2);
    EXPECT_TRUE(AreBuffersEqual(u.buffer, uExpectedBuffer2, size));
    EXPECT_EQ(v.negative, vExpectedNegative2);
    EXPECT_TRUE(AreBuffersEqual(v.buffer, vExpectedBuffer2, size));
    x.buffer = x3Buffer;
    y.buffer = y3Buffer;
    rucrypto_gost_34_10_extended_gcd(&x, &y, &d, &u, &v, false);
    EXPECT_FALSE(d.negative);
    EXPECT_TRUE(AreBuffersEqual(d.buffer, dExpectedBuffer3, size));
    EXPECT_EQ(u.negative, uExpectedNegative3);
    EXPECT_TRUE(AreBuffersEqual(u.buffer, uExpectedBuffer3, size));
    EXPECT_EQ(v.negative, vExpectedNegative3);
    EXPECT_TRUE(AreBuffersEqual(v.buffer, vExpectedBuffer3, size));
    x.buffer = x4Buffer;
    y.buffer = y4Buffer;
    rucrypto_gost_34_10_extended_gcd(&x, &y, &d, &u, &v, false);
    EXPECT_FALSE(d.negative);
    EXPECT_TRUE(AreBuffersEqual(d.buffer, dExpectedBuffer4, size));
    EXPECT_EQ(u.negative, uExpectedNegative4);
    EXPECT_TRUE(AreBuffersEqual(u.buffer, uExpectedBuffer4, size));
    EXPECT_EQ(v.negative, vExpectedNegative4);
    EXPECT_TRUE(AreBuffersEqual(v.buffer, vExpectedBuffer4, size));
}

TEST(Gost3410BasicMathTests, ExtendedGcdNoUTest) {
    constexpr size_t size = 1024 / 8 / sizeof(rucrypto_gost_34_10_big_num_underlying_t);
    rucrypto_gost_34_10_big_num_underlying_t x1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x5ce558939f917140, 0xaac7dee31d378d84, 0x6be2529609699dd3, 0x9a2ecb3a90b3ee69, 0xf0e2199cb27546c7, 0x6b137b4a95489787, 0x91896e34a66b198f, 0xea06f3d91feec426, 0x156a013476532817, 0x47e4e83c44d1c098, 0x743dc89e0c2b8817, 0xdd1da785c785a17a, 0x8077a5484133df30, 0x7950487ea172628f, 0x98673197028a1f98, 0x0522c76faf349bc3
#else
      0x5ce55893, 0x9f917140, 0xaac7dee3, 0x1d378d84, 0x6be25296, 0x09699dd3, 0x9a2ecb3a, 0x90b3ee69, 0xf0e2199c, 0xb27546c7, 0x6b137b4a, 0x95489787, 0x91896e34, 0xa66b198f, 0xea06f3d9, 0x1feec426, 0x156a0134, 0x76532817, 0x47e4e83c, 0x44d1c098, 0x743dc89e, 0x0c2b8817, 0xdd1da785, 0xc785a17a, 0x8077a548, 0x4133df30, 0x7950487e, 0xa172628f, 0x98673197, 0x028a1f98, 0x0522c76f, 0xaf349bc3
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y1Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xab824f9218f3d476, 0x8b4d9929cb46f643, 0xb57bfbb7e99cebec, 0x32d5f2fe2ad8e2d6, 0x030fee29f1202e2d, 0xedb03932649e44e5, 0x9000886ef3e9bd1c, 0xf909f609eb4c6160, 0xd33c6742437eeba4, 0xdd45d2a2f6d0b97f, 0x4925227fde9165b3, 0xfc4d819e99325305, 0x5b7c82745b51a84a, 0x0a120c19e63578e3, 0x460ac459f1494da6, 0x10c8912ab93f4f29
#else
      0xab824f92, 0x18f3d476, 0x8b4d9929, 0xcb46f643, 0xb57bfbb7, 0xe99cebec, 0x32d5f2fe, 0x2ad8e2d6, 0x030fee29, 0xf1202e2d, 0xedb03932, 0x649e44e5, 0x9000886e, 0xf3e9bd1c, 0xf909f609, 0xeb4c6160, 0xd33c6742, 0x437eeba4, 0xdd45d2a2, 0xf6d0b97f, 0x4925227f, 0xde9165b3, 0xfc4d819e, 0x99325305, 0x5b7c8274, 0x5b51a84a, 0x0a120c19, 0xe63578e3, 0x460ac459, 0xf1494da6, 0x10c8912a, 0xb93f4f29
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const bool vExpectedNegative1 = true;
    static const rucrypto_gost_34_10_big_num_underlying_t vExpectedBuffer1[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x4381b1ea0c60cb14, 0x1881b4a1bf785c62, 0xa28ffe8845e00e41, 0x3650d3c93aaa7488, 0xecd67e882db4eec3, 0x33b19909e4b78ca0, 0xde6a12e0e57043bf, 0x42c8599a7237de38, 0x2f7ba6a9d71c4f68, 0x85a8e900008c8766, 0xafe236e83b41047d, 0x1710b86b5f0cb960, 0xbe45ef1f4fb01fe8, 0xfa6a1d61f6d8cf62, 0xded5c1b2a9a26a2d, 0xa787a4ddceaec8e6
#else
      0x4381b1ea, 0x0c60cb14, 0x1881b4a1, 0xbf785c62, 0xa28ffe88, 0x45e00e41, 0x3650d3c9, 0x3aaa7488, 0xecd67e88, 0x2db4eec3, 0x33b19909, 0xe4b78ca0, 0xde6a12e0, 0xe57043bf, 0x42c8599a, 0x7237de38, 0x2f7ba6a9, 0xd71c4f68, 0x85a8e900, 0x008c8766, 0xafe236e8, 0x3b41047d, 0x1710b86b, 0x5f0cb960, 0xbe45ef1f, 0x4fb01fe8, 0xfa6a1d61, 0xf6d8cf62, 0xded5c1b2, 0xa9a26a2d, 0xa787a4dd, 0xceaec8e6
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t dExpectedBuffer1[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0000000000000001
#else
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x00000001
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x30f9ccee74bb5d93, 0x968578a91fa76ac0, 0x0c660fc509fd4ce9, 0x2b51e8a1f65e912a, 0xe7078eadbd4f4505, 0x5305e19bee25ccdc, 0x9ecef1bf552a6c7a, 0x22514fae2c70227d, 0xef5e9e9a26ad8388, 0xc12fe4e717033c4d, 0x3fdfeab0fa5dd75f, 0x9b4cac3e7096d068, 0xa02d0c694249890e, 0x833108cd623f360f, 0xe3a0db3381f4d6f7, 0x44c029f896c4f2c7
#else
      0x30f9ccee, 0x74bb5d93, 0x968578a9, 0x1fa76ac0, 0x0c660fc5, 0x09fd4ce9, 0x2b51e8a1, 0xf65e912a, 0xe7078ead, 0xbd4f4505, 0x5305e19b, 0xee25ccdc, 0x9ecef1bf, 0x552a6c7a, 0x22514fae, 0x2c70227d, 0xef5e9e9a, 0x26ad8388, 0xc12fe4e7, 0x17033c4d, 0x3fdfeab0, 0xfa5dd75f, 0x9b4cac3e, 0x7096d068, 0xa02d0c69, 0x4249890e, 0x833108cd, 0x623f360f, 0xe3a0db33, 0x81f4d6f7, 0x44c029f8, 0x96c4f2c7
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y2Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x357c5846ea5949a7, 0x32f876d29c7a156d, 0x9d9c475a1d9c7ff1, 0x34184e65100648d4, 0x96ebf27fdb3cec5a, 0x345532f89a332498, 0xabc8eaf65c4ed54f, 0xdd99bdfd7a511448, 0x437655798cda1654, 0xf2ca1cea419d880f, 0x03689c9e97547941, 0xe5353732cbdeeb61, 0xcf5dab2faecc0ee4, 0xdfa365f32c04c8c3, 0x09331a1228dda925, 0x90fa04951dc0f4a5
#else
      0x357c5846, 0xea5949a7, 0x32f876d2, 0x9c7a156d, 0x9d9c475a, 0x1d9c7ff1, 0x34184e65, 0x100648d4, 0x96ebf27f, 0xdb3cec5a, 0x345532f8, 0x9a332498, 0xabc8eaf6, 0x5c4ed54f, 0xdd99bdfd, 0x7a511448, 0x43765579, 0x8cda1654, 0xf2ca1cea, 0x419d880f, 0x03689c9e, 0x97547941, 0xe5353732, 0xcbdeeb61, 0xcf5dab2f, 0xaecc0ee4, 0xdfa365f3, 0x2c04c8c3, 0x09331a12, 0x28dda925, 0x90fa0495, 0x1dc0f4a5
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const bool vExpectedNegative2 = true;
    static const rucrypto_gost_34_10_big_num_underlying_t vExpectedBuffer2[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x020c37cb884e6195, 0x5119b9b29cb2613c, 0x2d3954d96592cf2a, 0xe49cea742104055b, 0xc8a4ffd6ffef6bd6, 0xe53143625cbd12d9, 0xbb8db442cf7d2807, 0x4360c525476061a5, 0x3611f1295742d11b, 0x858934dba4cb82cf, 0x792178a484c79e9f, 0xe2af97861462b4b9, 0x1636ffba3a47ee84, 0xbcab8a5a56bd0a12, 0x0308c491d0fb2e02, 0x70a50e79f29eeabb
#else
      0x020c37cb, 0x884e6195, 0x5119b9b2, 0x9cb2613c, 0x2d3954d9, 0x6592cf2a, 0xe49cea74, 0x2104055b, 0xc8a4ffd6, 0xffef6bd6, 0xe5314362, 0x5cbd12d9, 0xbb8db442, 0xcf7d2807, 0x4360c525, 0x476061a5, 0x3611f129, 0x5742d11b, 0x858934db, 0xa4cb82cf, 0x792178a4, 0x84c79e9f, 0xe2af9786, 0x1462b4b9, 0x1636ffba, 0x3a47ee84, 0xbcab8a5a, 0x56bd0a12, 0x0308c491, 0xd0fb2e02, 0x70a50e79, 0xf29eeabb
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t dExpectedBuffer2[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0000000000000001
#else
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x00000001
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x1ac192336594db72, 0x0817189f61606d7c, 0x04c5bde888844264, 0x05e32577b41fa34f, 0xe964d9ce91ed5666, 0x7dd9407891529391, 0xa29c11052d0ce473, 0x0e17a8e08e793050, 0x15383e40cac95b90, 0x452b018290718db8, 0xf438498f1aa49a37, 0x6c558737ae594a7e, 0x36d62a038e9587ea, 0xc3fdaf08ce1c8140, 0xfecf120920c4dac5, 0xd7226ae9b02555c7
#else
      0x1ac19233, 0x6594db72, 0x0817189f, 0x61606d7c, 0x04c5bde8, 0x88844264, 0x05e32577, 0xb41fa34f, 0xe964d9ce, 0x91ed5666, 0x7dd94078, 0x91529391, 0xa29c1105, 0x2d0ce473, 0x0e17a8e0, 0x8e793050, 0x15383e40, 0xcac95b90, 0x452b0182, 0x90718db8, 0xf438498f, 0x1aa49a37, 0x6c558737, 0xae594a7e, 0x36d62a03, 0x8e9587ea, 0xc3fdaf08, 0xce1c8140, 0xfecf1209, 0x20c4dac5, 0xd7226ae9, 0xb02555c7
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y3Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x6365f139f236cd9e, 0x1599a69135d714f1, 0xd1397eb08b0b6d6f, 0x12199ed347e99d7e, 0xce00f986fa0f9a44, 0xe653db8919e0f2c5, 0x72e36e25ef74580f, 0xf8cd954549b94bd1, 0xab333898cbd2523a, 0xfa8cf6fdeff08b2d, 0xb5e46c5e57b509c2, 0xa1f285d5e4271dcc, 0xe0a2369440daed9e, 0x0c28c58190d2776c, 0xa0c8542c9107069e, 0x6ee28feb44d7ea92
#else
      0x6365f139, 0xf236cd9e, 0x1599a691, 0x35d714f1, 0xd1397eb0, 0x8b0b6d6f, 0x12199ed3, 0x47e99d7e, 0xce00f986, 0xfa0f9a44, 0xe653db89, 0x19e0f2c5, 0x72e36e25, 0xef74580f, 0xf8cd9545, 0x49b94bd1, 0xab333898, 0xcbd2523a, 0xfa8cf6fd, 0xeff08b2d, 0xb5e46c5e, 0x57b509c2, 0xa1f285d5, 0xe4271dcc, 0xe0a23694, 0x40daed9e, 0x0c28c581, 0x90d2776c, 0xa0c8542c, 0x9107069e, 0x6ee28feb, 0x44d7ea92
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const bool vExpectedNegative3 = true;
    static const rucrypto_gost_34_10_big_num_underlying_t vExpectedBuffer3[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0877b8d6849dfa35, 0xfbdb9621ed7dbb39, 0x6757daecb9a73d5b, 0xac137832d7d4a653, 0xfb02ad0583c5fb48, 0x42645f8577e2ff43, 0xcd7fab01f758df4e, 0x27dd8e4df07a04d6, 0xaa95414ee338573f, 0x9d846b6ea6469a24, 0x0436b0f465abd965, 0x0a655f232aa71238, 0x8aa57dc2346c2bd8, 0x31b54b2304f8e946, 0x37a2eb0f2aa62dc3, 0xec5606087a083939
#else
      0x0877b8d6, 0x849dfa35, 0xfbdb9621, 0xed7dbb39, 0x6757daec, 0xb9a73d5b, 0xac137832, 0xd7d4a653, 0xfb02ad05, 0x83c5fb48, 0x42645f85, 0x77e2ff43, 0xcd7fab01, 0xf758df4e, 0x27dd8e4d, 0xf07a04d6, 0xaa95414e, 0xe338573f, 0x9d846b6e, 0xa6469a24, 0x0436b0f4, 0x65abd965, 0x0a655f23, 0x2aa71238, 0x8aa57dc2, 0x346c2bd8, 0x31b54b23, 0x04f8e946, 0x37a2eb0f, 0x2aa62dc3, 0xec560608, 0x7a083939
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t dExpectedBuffer3[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x00000007
#else
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x00000007
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t x4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0xd69dcfe9eafa2fc5, 0x3007163f6eef368d, 0x6ee7e45b91b791f9, 0xc2782abab223340f, 0x6a596dff88eb9dd6, 0xe14c03870d9011be, 0x3d8906dbdb660f4b, 0x300aa576499a756c, 0xbce59a20a49f9371, 0x7771d37d38ac973f, 0xd9ceb8ed57d3c679, 0x74db1d9eefb0ad86, 0x80755f12d940ed43, 0x489c6b62877eb93f, 0x82cd2f0a35e1a07d, 0x30d3892ca047d8b3
#else
      0xd69dcfe9, 0xeafa2fc5, 0x3007163f, 0x6eef368d, 0x6ee7e45b, 0x91b791f9, 0xc2782aba, 0xb223340f, 0x6a596dff, 0x88eb9dd6, 0xe14c0387, 0x0d9011be, 0x3d8906db, 0xdb660f4b, 0x300aa576, 0x499a756c, 0xbce59a20, 0xa49f9371, 0x7771d37d, 0x38ac973f, 0xd9ceb8ed, 0x57d3c679, 0x74db1d9e, 0xefb0ad86, 0x80755f12, 0xd940ed43, 0x489c6b62, 0x877eb93f, 0x82cd2f0a, 0x35e1a07d, 0x30d3892c, 0xa047d8b3
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    rucrypto_gost_34_10_big_num_underlying_t y4Buffer[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x4062398b10bc6cca, 0x4645fd491cc5c905, 0x6930aae28139a3d3, 0x1d19393ff43db224, 0x4ab563c96c141e67, 0x53c9e0103af2efd5, 0xd7e69f8010a6581d, 0x9fb45bcd20a6fd94, 0x76a5341df3962598, 0x59072891dc6c8e9a, 0xdd95d8d0ad5dee68, 0x6535c52874aadc0e, 0x9730b35a2fb3dd7a, 0x85eb0784bca42784, 0xee6df6457c98aea8, 0xbb49107708b1c0fb
#else
      0x4062398b, 0x10bc6cca, 0x4645fd49, 0x1cc5c905, 0x6930aae2, 0x8139a3d3, 0x1d19393f, 0xf43db224, 0x4ab563c9, 0x6c141e67, 0x53c9e010, 0x3af2efd5, 0xd7e69f80, 0x10a6581d, 0x9fb45bcd, 0x20a6fd94, 0x76a5341d, 0xf3962598, 0x59072891, 0xdc6c8e9a, 0xdd95d8d0, 0xad5dee68, 0x6535c528, 0x74aadc0e, 0x9730b35a, 0x2fb3dd7a, 0x85eb0784, 0xbca42784, 0xee6df645, 0x7c98aea8, 0xbb491077, 0x08b1c0fb
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const bool vExpectedNegative4 = true;
    static const rucrypto_gost_34_10_big_num_underlying_t vExpectedBuffer4[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x43cd9f8bee5c2b20, 0xa46503190f5a6ba2, 0x80667ae5ca39c6e1, 0x001866187e5da6b9, 0xb98f5265d31a5367, 0x45ccc1de54fb191c, 0x8af7cc5de036631c, 0x8b9f86bdd4b948bf, 0x982b8698b3abf763, 0xeb266b06ac2c4b55, 0xf45011ee44ea6611, 0x6f64f9f8b5079597, 0xd0504f64b4d8b655, 0x7de565d6dab876af, 0x680cebd0823b31d9, 0x7f7e5e67141b0dc5
#else
      0x43cd9f8b, 0xee5c2b20, 0xa4650319, 0x0f5a6ba2, 0x80667ae5, 0xca39c6e1, 0x00186618, 0x7e5da6b9, 0xb98f5265, 0xd31a5367, 0x45ccc1de, 0x54fb191c, 0x8af7cc5d, 0xe036631c, 0x8b9f86bd, 0xd4b948bf, 0x982b8698, 0xb3abf763, 0xeb266b06, 0xac2c4b55, 0xf45011ee, 0x44ea6611, 0x6f64f9f8, 0xb5079597, 0xd0504f64, 0xb4d8b655, 0x7de565d6, 0xdab876af, 0x680cebd0, 0x823b31d9, 0x7f7e5e67, 0x141b0dc5
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };
    static const rucrypto_gost_34_10_big_num_underlying_t dExpectedBuffer4[size] = {
#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0000000000000039
#else
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x00000039
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
    };

    rucrypto_gost_34_10_big_num_underlying_t dBuffer[size];
    rucrypto_gost_34_10_big_num_underlying_t vBuffer[size];

    rucrypto_gost_34_10_big_num_t x {
        .negative = false,
        .buffer = x1Buffer,
        .size = size
    };

    rucrypto_gost_34_10_big_num_t y {
        .negative = false,
        .buffer = y1Buffer,
        .size = size
    };

    rucrypto_gost_34_10_big_num_t d {
        .negative = false,
        .buffer = dBuffer,
        .size = size
    };

    rucrypto_gost_34_10_big_num_t v {
        .negative = false,
        .buffer = vBuffer,
        .size = size
    };

    rucrypto_gost_34_10_extended_gcd_no_u(&x, &y, &d, &v, false);
    EXPECT_FALSE(d.negative);
    EXPECT_TRUE(AreBuffersEqual(d.buffer, dExpectedBuffer1, size));
    EXPECT_EQ(v.negative, vExpectedNegative1);
    EXPECT_TRUE(AreBuffersEqual(v.buffer, vExpectedBuffer1, size));
    x.buffer = x2Buffer;
    y.buffer = y2Buffer;
    rucrypto_gost_34_10_extended_gcd_no_u(&x, &y, &d, &v, false);
    EXPECT_FALSE(d.negative);
    EXPECT_TRUE(AreBuffersEqual(d.buffer, dExpectedBuffer2, size));
    EXPECT_EQ(v.negative, vExpectedNegative2);
    EXPECT_TRUE(AreBuffersEqual(v.buffer, vExpectedBuffer2, size));
    x.buffer = x3Buffer;
    y.buffer = y3Buffer;
    rucrypto_gost_34_10_extended_gcd_no_u(&x, &y, &d, &v, false);
    EXPECT_FALSE(d.negative);
    EXPECT_TRUE(AreBuffersEqual(d.buffer, dExpectedBuffer3, size));
    EXPECT_EQ(v.negative, vExpectedNegative3);
    EXPECT_TRUE(AreBuffersEqual(v.buffer, vExpectedBuffer3, size));
    x.buffer = x4Buffer;
    y.buffer = y4Buffer;
    rucrypto_gost_34_10_extended_gcd_no_u(&x, &y, &d, &v, false);
    EXPECT_FALSE(d.negative);
    EXPECT_TRUE(AreBuffersEqual(d.buffer, dExpectedBuffer4, size));
    EXPECT_EQ(v.negative, vExpectedNegative4);
    EXPECT_TRUE(AreBuffersEqual(v.buffer, vExpectedBuffer4, size));
}
