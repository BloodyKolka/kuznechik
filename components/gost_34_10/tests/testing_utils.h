#pragma once

#include <cstddef>
#include <cstdint>
#include <string>

std::string PrintBuffer(const uint8_t a[], size_t size);

std::string PrintBuffer(const uint64_t a[], size_t size);

std::string PrintBuffer(const uint32_t a[], size_t size);

bool AreBuffersEqual(const uint8_t a[], const uint8_t b[], size_t size);

bool AreBuffersEqual(const uint64_t a[], const uint64_t b[], size_t size);

bool AreBuffersEqual(const uint32_t a[], const uint32_t b[], size_t size);
