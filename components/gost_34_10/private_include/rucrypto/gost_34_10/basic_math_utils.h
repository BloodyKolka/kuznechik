#ifndef RUCRYPTO_GOST_34_10_BASIC_MATH_UTILS_H
#define RUCRYPTO_GOST_34_10_BASIC_MATH_UTILS_H

#include <rucrypto/gost_34_10/gost_34_10.h>

#include <rucrypto/gost_34_10/big_num_utils.h>

/**
 * @brief Multiply big number 'a' by 2
 * @param [in, out] a bit number to multiply
 * @details Does not account for overflow
 */
void rucrypto_gost_34_10_mul_by_2(rucrypto_gost_34_10_big_num_t* a);

/**
 * @brief Divide big number 'a' by 2
 * @param [in, out] a bit number to divide'
 * @details Does not account for remainder
 */
void rucrypto_gost_34_10_div_by_2(rucrypto_gost_34_10_big_num_t* a);

/**
 * @brief Add big number 'v' to big number 'u' and store result in 'w'
 * @param [in] u big number u
 * @param [in] v big number v
 * @param [in, out] w big number w to store result
 * @details Buffers allocated to store 'a' and 'b' must be of the same size, and
 *          buffer of 'w' must be bigger at least by 1
 */
void rucrypto_gost_34_10_add(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* w);

/**
 * @brief Add big number 'v' to big number 'u' and store result in 'w'. Wraps
 *        around 2^size
 * @param [in] u big number u
 * @param [in] v big number v
 * @param [in, out] w big number w to store result
 * @return Has the number overflowed
 * @details Buffers, allocated to store these numbers must be of the same size
 */
void rucrypto_gost_34_10_add_no_carry(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* w);

/**
 * @brief Subtract big number 'v' from big number 'u' and store result in 'w'
 * @param [in] u big number u
 * @param [in] v big number v
 * @param [in, out] w big number w to store result
 * @details Buffers allocated to store these numbers must be of the same size
 */
void rucrypto_gost_34_10_sub(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* w);

/**
 * @brief Multiply big number 'u' by 64-bit number 'v' and store result in 'w'
 * @param [in] u big number u
 * @param [in] v number v
 * @param [in, out] w big number w to store result
 * @details Buffer, allocated to store 'w' must be bigger than for 'u' at
 *          least by 1
 */
void rucrypto_gost_34_10_mul_1(
        const rucrypto_gost_34_10_big_num_t* u,
        rucrypto_gost_34_10_big_num_underlying_t v,
        rucrypto_gost_34_10_big_num_t* w);

/**
 * @brief Multiply big number 'u' by big number 'v' and store result in 'w'
 * @param [in] u big number u
 * @param [in] v big number v
 * @param [in, out] w big number w to store result
 * @details Buffers, allocated to store 'u' and 'w' must be of the same size,
 *          buffer, allocated to store 'w' must be twice the size
 */
void rucrypto_gost_34_10_mul(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* w);

/**
 * @brief Divide big number 'u' by 64-bit number 'v' and store quotient in 'q'
 *        and remainder in 'r'
 * @param [in] u big number u
 * @param [in] v number v
 * @param [in, out] q bit number q to store quotient
 * @param [in, out] r pointer to a variable to store remainder
 * @details Buffers, allocated to store these numbers must be of the same size
 */
void rucrypto_gost_34_10_div_1(
        const rucrypto_gost_34_10_big_num_t* u,
        rucrypto_gost_34_10_big_num_underlying_t v,
        rucrypto_gost_34_10_big_num_t* q,
        rucrypto_gost_34_10_big_num_underlying_t *r);

/**
 * @brief Divide big number 'u' by big number 'v' and store quotient in 'q' and
 *        remainder in 'r'
 * @param [in] u big number u
 * @param [in] v big number v
 * @param [in, out] q big number q to store quotient
 * @param [in, out] r big number r to store remainder
 * @param [in] secure securely clear temporary buffers before freeing
 * @details Buffers, allocated to store these numbers must be of the same size
 */
rucrypto_result_t rucrypto_gost_34_10_div(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* q,
        rucrypto_gost_34_10_big_num_t* r,
        rucrypto_bool_t secure);

/**
 * @brief Subtract 'u' from 'v' accounting for signs
 * @param [in] u big number u
 * @param [in] v big number v
 * @param [in, out] w big number w to store result
 * @return Has the number overflowed
 */
void rucrypto_gost_34_10_signed_sub(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* w);

/**
 * @brief Add 'u' to 'v' accounting for signs
 * @param [in] u big number u
 * @param [in] v big number v
 * @param [in, out] w big number w, initialized with 0, to store result
 * @return Has the number overflowed
 */
void rucrypto_gost_34_10_signed_add(
        const rucrypto_gost_34_10_big_num_t* u,
        const rucrypto_gost_34_10_big_num_t* v,
        rucrypto_gost_34_10_big_num_t* w);

/**
 * @brief Calculate d, u and v such that av + bu = d = GCD(a, b) using binary
 *        gcd algorithm
 * @param [in] a big number a
 * @param [in] b big number b
 * @param [in, out] d big number d
 * @param [in, out] u big number u
 * @param [in, out] v big number v
 * @param [in] secure securely clear temporary buffers before freeing
 */
rucrypto_result_t rucrypto_gost_34_10_extended_gcd(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b,
        rucrypto_gost_34_10_big_num_t* d,
        rucrypto_gost_34_10_big_num_t* u,
        rucrypto_gost_34_10_big_num_t* v,
        rucrypto_bool_t secure);

/**
 * @brief Calculate d and v such that mv + au = d = GCD(a, b) using binary gcd
 *        algorithm. Useful for calculating a^-1 mod m
 * @param [in] m big number m
 * @param [in] a big number a
 * @param [in, out] d big number d
 * @param [in, out] v big number v
 * @param [in] secure securely clear temporary buffers before freeing
 * @details Same as rucrypto_gost_34_10_extended_gcd, except a bit cheaper,
 *          because it does not calculate u. m must be odd
 */
rucrypto_result_t rucrypto_gost_34_10_extended_gcd_no_u(
        const rucrypto_gost_34_10_big_num_t* m,
        const rucrypto_gost_34_10_big_num_t* a,
        rucrypto_gost_34_10_big_num_t* d,
        rucrypto_gost_34_10_big_num_t* v,
        rucrypto_bool_t secure);

#endif // RUCRYPTO_GOST_34_10_BASIC_MATH_UTILS_H
