#ifndef RUCRYPTO_GOST_34_10_ELLIPTIC_CURVE_UTILS_H
#define RUCRYPTO_GOST_34_10_ELLIPTIC_CURVE_UTILS_H

#include <rucrypto/gost_34_10/gost_34_10.h>

#include <rucrypto/gost_34_10/modular_math_utils.h>

typedef struct {
    rucrypto_gost_34_10_big_num_t x;
    rucrypto_gost_34_10_big_num_t y;
    rucrypto_size_t size;
} rucrypto_gost_34_10_point_t;

struct rucrypto_gost_34_10_elliptic_curve_t {
    rucrypto_gost_34_10_big_num_t p;
    rucrypto_gost_34_10_big_num_t a;
    rucrypto_gost_34_10_big_num_t b;
    rucrypto_gost_34_10_big_num_t m;
    rucrypto_gost_34_10_big_num_t q;
    rucrypto_gost_34_10_point_t P;
    rucrypto_size_t size;
    rucrypto_gost_34_10_big_num_underlying_t* buffer;
};

/**
 * @brief Add two points of elliptic curve
 * @param [in] curve struct rucrypto_gost_34_10_elliptic_curve_t with information
 *              about the elliptic curve
 * @param [in] a point a
 * @param [in] b point b
 * @param [in, out] res point to hold result
 * @param [in] secure securely clear temporary buffers before freeing
 * @return rucrypto_result_ok on success, error code otherwise
 */
rucrypto_result_t rucrypto_gost_34_10_add_points(
        const rucrypto_gost_34_10_elliptic_curve_t* curve,
        const rucrypto_gost_34_10_point_t* a,
        const rucrypto_gost_34_10_point_t* b,
        rucrypto_gost_34_10_point_t* res,
        rucrypto_bool_t secure);

/**
 * @brief For point P, calculate point kP = P + P + ... + P (k times)
 * @param [in] curve struct rucrypto_gost_34_10_elliptic_curve_t with information
*               about the elliptic curve
 * @param [in] p point P
 * @param [in] k big number k
 * @param [in, out] res point to hold result
 * @param [in] secure securely clear temporary buffers before freeing
 * @return rucrypto_result_ok on success, error code otherwise
 */
rucrypto_result_t rucrypto_gost_34_10_calculate_k_point(
        const rucrypto_gost_34_10_elliptic_curve_t* curve,
        const rucrypto_gost_34_10_point_t* p,
        const rucrypto_gost_34_10_big_num_t* k,
        rucrypto_gost_34_10_point_t* res,
        rucrypto_bool_t secure);

#endif // RUCRYPTO_GOST_34_10_ELLIPTIC_CURVE_UTILS_H
