#ifndef RUCRYPTO_GOST_34_10_BIG_NUM_UTILS_H
#define RUCRYPTO_GOST_34_10_BIG_NUM_UTILS_H

#include <rucrypto/gost_34_10/gost_34_10.h>

#if defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)
typedef rucrypto_uint64_t rucrypto_gost_34_10_big_num_underlying_t;
#define RUCRYPTO_GOST_34_10_BIG_NUM_UNDERLYING_TYPE_MAX_VALUE 0xFFFFFFFFFFFFFFFF
#else
typedef rucrypto_uint32_t rucrypto_gost_34_10_big_num_underlying_t;
#define RUCRYPTO_GOST_34_10_BIG_NUM_UNDERLYING_TYPE_MAX_VALUE 0xFFFFFFFF
#endif // defined(RUCRYPTO_X86_64_USE_INTRINSICS) || defined(RUCRYPTO_X86_64_USE_GCC_ASM)

typedef struct {
    rucrypto_bool_t negative;
    rucrypto_gost_34_10_big_num_underlying_t* buffer;
    rucrypto_size_t size;
} rucrypto_gost_34_10_big_num_t;

/**
 * @brief Initialize big number of size 'size' with 0
 * @param [in, out] var variable to hold number
 * @param [in] size size of buffer, allocated for the number in 64 bit blocks
 */
rucrypto_result_t rucrypto_gost_34_10_big_num_init_0(
        rucrypto_gost_34_10_big_num_t* var,
        rucrypto_size_t size);

/**
 * @brief Initialize big number of size 'size' with random bytes
 * @param [in, out] var variable to hold number
 * @param [in] size size of buffer, allocated for the number in 64 bit blocks
 * @param [in] random function to generate random byte
 */
rucrypto_result_t rucrypto_gost_34_10_big_num_init_random(
        rucrypto_gost_34_10_big_num_t* var,
        rucrypto_size_t size,
        rucrypto_gost_34_10_random_generator_func_t random);

/**
 * @brief Initialize big number with another big number
 * @param [in] src bit number to copy
 * @param [in, out] dst variable to hold new number
 */
rucrypto_result_t rucrypto_gost_34_10_big_num_init_copy(
        const rucrypto_gost_34_10_big_num_t* src,
        rucrypto_gost_34_10_big_num_t* dst);

/**
 * @brief Initialize big number from buffer
 * @param [in] buffer buffer to iniatilize big number from
 * @param [in] bufferSize size of buffer in bytes
 * @param [in, out] dst variable to hold new number
 */
rucrypto_result_t rucrypto_gost_34_10_big_num_init_from_buffer(
        const rucrypto_uint8_t* buffer,
        rucrypto_size_t bufferSize,
        rucrypto_gost_34_10_big_num_t* dst);

/**
 * @brief Initialize big number without allocating memory
 * @param [in] buffer big buffer to use
 * @param [in] offset offset in buffer (index of first element of used
 *                    sub-buffer)
 * @param [in] size size of buffer for 1 number
 * @param [in, out] dst variable to hold big number
 */
void rucrypto_gost_34_10_big_num_init_no_alloc(
        rucrypto_gost_34_10_big_num_underlying_t* buffer,
        rucrypto_size_t offset,
        rucrypto_gost_34_10_big_num_t* var,
        rucrypto_size_t size);

/**
 * @brief De-initialize big number
 * @param [in] var variable that holds big number, initialized with rucrypto_gost_34_10_big_num_init_*
 * @param [in] secure securely clear buffer before freeing
 */
void rucrypto_gost_34_10_big_num_fini(
        rucrypto_gost_34_10_big_num_t* var,
        rucrypto_bool_t secure);

/**
 * @brief Fill big number with 0
 * @param [in, out] var big number to overwrite
 */
void rucrypto_gost_34_10_big_num_fill_0(rucrypto_gost_34_10_big_num_t* var);

/**
 * @brief Fill big number with random bytes
 * @param [in, out] var big number to fill
 * @param [in] random function to generate random byte
 */
void rucrypto_gost_34_10_big_num_fill_random(
        rucrypto_gost_34_10_big_num_t* var,
        rucrypto_gost_34_10_random_generator_func_t random);

/**
 * @brief Overwrite one big number with another
 * @param [in] src big number to copy
 * @param [in, out] dst big number to overwrite
 * @details Does not initialize 'dst', it must be already iniatiaalized
 */
void rucrypto_gost_34_10_big_num_copy(
        const rucrypto_gost_34_10_big_num_t* src,
        rucrypto_gost_34_10_big_num_t* dst);

/**
 * @brief Fill big number from buffer
 * @param [in] buffer buffer
 * @param [in] bufferSize size of buffer
 * @param [in, out] dst big number to fill
 */
void rucrypto_gost_34_10_big_num_fill_from_buffer(
        const rucrypto_uint8_t* buffer,
        rucrypto_size_t bufferSize,
        rucrypto_gost_34_10_big_num_t* dst);

/**
 * @brief Copy big number to buffer
 * @param [in] src big number to copy
 * @param [in, out] buffer to copy number to
 * @param [in] bufferSize size of buffer
 */
void rucrypto_gost_34_10_big_num_copy_to_buffer(
        const rucrypto_gost_34_10_big_num_t* src,
        rucrypto_uint8_t* buffer,
        rucrypto_size_t bufferSize);

/**
 * @brief Get number of significant 32- or 64-bit digits
 * @param [in] a big number
 * @return number of significant 32- or 64-bit digits
 * @details If a = 0, return 1
 */
rucrypto_size_t rucrypto_gost_34_10_len(const rucrypto_gost_34_10_big_num_t* a);

/**
 * @brief Test if big number is zero
 * @param [in] a big number
 * @return true if a = 0, false otherwise
 */
rucrypto_bool_t rucrypto_gost_34_10_is_zero(const rucrypto_gost_34_10_big_num_t* a);

/**
 * @brief Test if big number a is equal to big number b
 * @param [in] a big number a
 * @param [in] b big number b
 * @return true if (a == b), false otherwise
 */
rucrypto_bool_t rucrypto_gost_34_10_are_equal(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b);

/**
 * @brief Test if big number a is equal to big number b
 * @param [in] a big number a
 * @param [in] b big number b
 * @return true if (a == b), false otherwise
 */
rucrypto_bool_t rucrypto_gost_34_10_are_equal_and_not_zero(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b);

/**
 * @brief Test if big number a is bigger than big number b
 * @param [in] a big number a
 * @param [in] b big number b
 * @return true if (a > b), false otherwise
 */
rucrypto_bool_t rucrypto_gost_34_10_is_bigger(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b);

/**
 * @brief Test if big number a is bigger or equal to big number b
 * @param [in] a big number a
 * @param [in] b big number b
 * @return true if (a >= b), false otherwise
 */
rucrypto_bool_t rucrypto_gost_34_10_is_bigger_or_equal(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b);

/**
 * @brief Test if big number a is less than big number b
 * @param [in] a big number a
 * @param [in] b big number b
 * @return true if (a < b), false otherwise
 */
rucrypto_bool_t rucrypto_gost_34_10_is_less(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b);

/**
 * @brief Test if big number a is less or equal to big number b
 * @param [in] a big number a
 * @param [in] b big number b
 * @return true if (a <= b), false otherwise
 */
rucrypto_bool_t rucrypto_gost_34_10_is_less_or_equal(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b);

/**
 * @brief Shift big number 'a' left by 'shiftCount' 32- or 64-bit blocks
 * @param [in, out] a big number to shift
 * @param [in] shiftCount shift count
 * @details Effectively means multiplying by (2^32)^shiftCount or
 *          (2^64)^shiftCount
 */
void rucrypto_gost_34_10_shift_left(
        rucrypto_gost_34_10_big_num_t* a,
        rucrypto_size_t shiftCount);

/**
 * @brief Shift big number 'a' left by 'shiftCount' bits
 * @param [in, out] a big number to shift
 * @param [in] shiftCount shift count
 * @details Effectively means multiplying by 2^shiftCount
 */
void rucrypto_gost_34_10_bit_shift_left(
        rucrypto_gost_34_10_big_num_t* a,
        rucrypto_size_t shiftCount);

/**
 * @brief Shift big number 'a' right by 'shiftCount' 32- or 64-bit blocks
 * @param [in, out] a big number to shift
 * @param [in] shiftCount shift count
 * @details Effectively means dividing by (2^32)^shiftCount or (2^64)^shiftCount
 */
void rucrypto_gost_34_10_shift_right(
        rucrypto_gost_34_10_big_num_t* a,
        rucrypto_size_t shiftCount);

/**
 * @brief Shift big number 'a' right by 'shiftCount' bits
 * @param [in, out] a big number to shift
 * @param [in] shiftCount shift count
 * @details Effectively means dividing by 2^shiftCount
 */
void rucrypto_gost_34_10_bit_shift_right(
        rucrypto_gost_34_10_big_num_t* a,
        rucrypto_size_t shiftCount);

#endif // RUCRYPTO_GOST_34_10_BIG_NUM_UTILS_H
