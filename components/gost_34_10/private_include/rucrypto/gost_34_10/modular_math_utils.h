#ifndef RUCRYPTO_GOST_34_10_MODULAR_MATH_UTILS_H
#define RUCRYPTO_GOST_34_10_MODULAR_MATH_UTILS_H

#include <rucrypto/gost_34_10/basic_math_utils.h>

/**
 * @brief Calculate 'a' mod 'base' and store result in 'res'
 * @param [in] a big number a
 * @param [in, out] res big number to store result
 * @param [in] base big number with modulo
 * @param [in] secure securely clear temporary buffers before freeing
 * @return rucrypto_result_ok on success, error code otherwise
 */
rucrypto_result_t rucrypto_gost_34_10_rebase(
        const rucrypto_gost_34_10_big_num_t* a, 
        rucrypto_gost_34_10_big_num_t* res,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure);

/**
 * @brief Calculate ('a' + 'b') mod 'base' and store result in 'res'
 * @param [in] a big number a
 * @param [in] b big number b
 * @param [in, out] res big number to store result
 * @param [in] base big number with modulo
 * @param [in] secure securely clear temporary buffers before freeing
 * @return rucrypto_result_ok on success, error code otherwise
 */
rucrypto_result_t rucrypto_gost_34_10_add_based(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b,
        rucrypto_gost_34_10_big_num_t* res,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure);

/**
 * @brief Calculate ('a' + 'b') mod 'base' and store result in 'res'
 * @param [in] a big number a
 * @param [in] b big number b
 * @param [in, out] res big number to store result
 * @param [in] base big number with modulo
 * @param [in] secure securely clear temporary buffers before freeing
 */
void rucrypto_gost_34_10_sub_based(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b,
        rucrypto_gost_34_10_big_num_t* res,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure);

/**
 * @brief Calculate multiplicative inverse for 'a' in mod 'base' and store
 *        result in 'invA'
 * @param [in] a big number a
 * @param [in, out] invA big number to store result
 * @param [in] base big number with modulo
 * @param [in] secure securely clear temporary buffers before freeing
 * @return rucrypto_result_ok on success, error code otherwise
 * @details Multiplicative inverse for a in mod b is such a^-1, that
 *          (a * a^-1) mod b = 1. b must be coprime with a, otherwise such a^-1
 *          does not exist
 */
rucrypto_result_t rucrypto_gost_34_10_mul_inv(
        const rucrypto_gost_34_10_big_num_t* a,
        rucrypto_gost_34_10_big_num_t* invA,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure);

/**
 * @brief Calculate ('a' * 2) mod 'base' and store result in 'a'
 * @param [in, out] a big number a
 * @param [in] base big number with modulo
 * @param [in] secure securely clear temporary buffers before freeing
 * @return rucrypto_result_ok on success, error code otherwise
 */
rucrypto_result_t rucrypto_gost_34_10_mul_by_2_based(
        rucrypto_gost_34_10_big_num_t* a, 
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure);

/**
 * @brief Calculate ('a' * 'b') mod 'base' and store result in 'res'
 * @param [in] a big number a
 * @param [in] b number b
 * @param [in, out] res big number to store result
 * @param [in] base big number with modulo
 * @param [in] secure securely clear temporary buffers before freeing
 * @return rucrypto_result_ok on success, error code otherwise
 */
rucrypto_result_t rucrypto_gost_34_10_mul_1_based(
        const rucrypto_gost_34_10_big_num_t* a,
        rucrypto_gost_34_10_big_num_underlying_t b,
        rucrypto_gost_34_10_big_num_t* res,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure);

/**
 * @brief Calculate ('a' * 'b') mod 'base' and store result in 'res'
 * @param [in] a big number a
 * @param [in] b big number b
 * @param [in, out] res big number to store result
 * @param [in] base big number with modulo
 * @param [in] secure securely clear temporary buffers before freeing
 * @return rucrypto_result_ok on success, error code otherwise
 */
rucrypto_result_t rucrypto_gost_34_10_mul_based(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b,
        rucrypto_gost_34_10_big_num_t* res,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure);

/**
 * @brief Calculate ('a' / 'b') mod 'base' and store result in 'res'
 * @param [in] a big number a
 * @param [in] b big number b
 * @param [in, out] res big number to store result
 * @param [in] base big number with modulo
 * @param [in] secure securely clear temporary buffers before freeing
 * @return rucrypto_result_ok on success, error code otherwise
 * @details 'b' and 'base' must be coprime, otherwise b^-1 does not exist
 */
rucrypto_result_t rucrypto_gost_34_10_div_based(
        const rucrypto_gost_34_10_big_num_t* a,
        const rucrypto_gost_34_10_big_num_t* b,
        rucrypto_gost_34_10_big_num_t* res,
        const rucrypto_gost_34_10_big_num_t* base,
        rucrypto_bool_t secure);

#endif // RUCRYPTO_GOST_34_10_MODULAR_MATH_UTILS_H
