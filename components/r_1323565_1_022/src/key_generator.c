#include "rucrypto/common/allocator_provider.h"
#include "rucrypto/common/types.h"
#include "rucrypto/gost_34_13/common.h"
#include <rucrypto/r_1323565_1_022/key_generator.h>
#include <rucrypto/r_1323565_1_022/key_generator_context.h>
#include <rucrypto/r_1323565_1_022/mac_functions.h>

#include <rucrypto/gost_34_13/mac.h>
#include <rucrypto/gost_34_12/gost_34_12.h>

#include <rucrypto/common/secure_zero.h>

rucrypto_result_t rucrypto_r_1323565_1_022_gen_intermediate_key(rucrypto_r_1323565_1_022_intermediate_key_gen_type_t genType, rucrypto_uint8_t sourceKeyData[], rucrypto_size_t sourceKeyDataSize, rucrypto_uint8_t salt[], rucrypto_size_t saltSize, rucrypto_r_1323565_1_022_intermediate_key_t key) {
    switch (genType) {
        case rucrypto_r_1323565_1_022_intermediate_key_gen_type_nmac: {
            rucrypto_r_1323565_1_022_mac_key_t macKey = {
                .buffer = salt,
                .size = saltSize,
            };

            rucrypto_r_1323565_1_022_nmac_context_t* ctx;
            rucrypto_result_t res = rucrypto_r_1323565_1_022_nmac_context_init(&ctx, &macKey);
            if (res != rucrypto_result_ok) {
                return res;
            }

            rucrypto_size_t i = 0;
            for (; i < sourceKeyDataSize / 64; ++i) {
                rucrypto_r_1323565_1_022_nmac_part(ctx, &sourceKeyData[i * 64]);
            }

            rucrypto_r_1323565_1_022_nmac_fini(&ctx, &sourceKeyData[i * 64], sourceKeyDataSize % 64, key);

            return rucrypto_result_ok;
        }
        case rucrypto_r_1323565_1_022_intermediate_key_gen_type_hmac: {
            rucrypto_r_1323565_1_022_mac_key_t macKey = {
                .buffer = salt,
                .size = saltSize,
            };

            rucrypto_r_1323565_1_022_hmac_context_t* ctx;
            rucrypto_result_t res = rucrypto_r_1323565_1_022_hmac_512_context_init(&ctx, &macKey);
            if (res != rucrypto_result_ok) {
                return res;
            }

            rucrypto_size_t i = 0;
            for (; i < sourceKeyDataSize / 64; ++i) {
                rucrypto_r_1323565_1_022_hmac_part(ctx, &sourceKeyData[i * 64]);
            }
            
            rucrypto_r_1323565_1_022_hmac_512_t hmac;
            rucrypto_r_1323565_1_022_hmac_512_fini(&ctx, &sourceKeyData[i * 64], sourceKeyDataSize % 64, hmac);

            RUCRYPTO_MEMCPY(key, hmac, 32);

            return rucrypto_result_ok;
        }
        case rucrypto_r_1323565_1_022_intermediate_key_gen_type_xor: {
            if (sourceKeyDataSize != 32 || saltSize != 32) {
                return rucrypto_result_invalid_parameters;
            }
            for (rucrypto_size_t i = 0; i < 32; ++i) {
                key[i] = sourceKeyData[i] ^ salt[i];
            }

            return rucrypto_result_ok;
        }
        default: 
            return rucrypto_result_invalid_parameters;
    }
}

#define START_FLAG       (                 0) 
#define START_COUNTER    (START_FLAG    +  1)
#define START_ZPART      (START_COUNTER + 32)
#define START_LENGTH     (START_ZPART   + 64)
#define START_USAGE      (START_LENGTH  + 31)
#define START_USERS      (START_USAGE   + 32)
#define START_ADDITIONAL (START_USERS   + 16)

void rucrypto_r_1323565_1_022_format(
        rucrypto_r_1323565_1_022_format_flags_t flags,
        const rucrypto_r_1323565_1_022_counter_t* counter,
        const rucrypto_r_1323565_1_022_zpart_t* zpart,
        const rucrypto_r_1323565_1_022_length_t* length,
        const rucrypto_r_1323565_1_022_usage_t* usage,
        const rucrypto_r_1323565_1_022_users_t* users,
        const rucrypto_r_1323565_1_022_additional_t* additional,
        rucrypto_uint8_t* format,
        rucrypto_size_t* formatSize) {
    RUCRYPTO_ASSERT(sizeof(flags) == 1);
    RUCRYPTO_ASSERT(flags.counter || flags.zpart);
    RUCRYPTO_ASSERT(zpart->size == 64 || zpart->size == 32 || zpart->size == 16);
    
    // f + C_i + z_(i-1) + L  + P  + U  + A
    // 1 + 32  + 64      + 31 + 32 + 16 + 16 = 192    
    if (*formatSize == 0) {
        *formatSize = 192;
        return;
    }

    RUCRYPTO_MEMCPY(&format[START_FLAG], (uint8_t*)&flags, 1);
    
    if (flags.counter) {
        RUCRYPTO_MEMCPY(&format[START_COUNTER], counter->buffer, 32);
    } else {
        RUCRYPTO_MEMSET(&format[START_COUNTER], 0, 32);
    }
    
    if (flags.zpart) {
        RUCRYPTO_MEMCPY(&format[START_ZPART], zpart->buffer, zpart->size);
        if (zpart->size < 64) {
            RUCRYPTO_MEMSET(&format[START_ZPART + zpart->size], 0, 64 - zpart->size);
        }
    } else {
        RUCRYPTO_MEMSET(&format[START_ZPART], 0, 64);
    }

    if (flags.length) {
        RUCRYPTO_MEMCPY(&format[START_LENGTH], length->buffer, 31);
    } else {
        RUCRYPTO_MEMSET(&format[START_LENGTH], 0, 31);
    }

    if (flags.usage) {
        RUCRYPTO_MEMCPY(&format[START_USAGE], usage->buffer, 32);
    } else {
        RUCRYPTO_MEMSET(&format[START_USAGE], 0, 32);
    }

    if (flags.users) {
        RUCRYPTO_MEMCPY(&format[START_USERS], users->buffer, 16);
    } else {
        RUCRYPTO_MEMSET(&format[START_USERS], 0, 16);
    }
   
    if (flags.additional) {
        RUCRYPTO_MEMCPY(&format[START_ADDITIONAL], additional->buffer, 16);
    } else {
        RUCRYPTO_MEMSET(&format[START_ADDITIONAL], 0, 16);
    }
}

static void rucrypto_r_1323565_1_022_add_1(const rucrypto_r_1323565_1_022_counter_t* counter) {
    if (counter == RUCRYPTO_NULL) {
        return;
    }

    rucrypto_uint32_t c = (rucrypto_uint32_t)counter->buffer[counter->size - 1] + 1;
    rucrypto_uint32_t carry = (c >> 8) & 0xFF;
    counter->buffer[counter->size - 1] = (rucrypto_uint8_t)(c & 0xFF);
    for (rucrypto_size_t i = 1; (i < 64) && (carry != 0); ++i) {
        c = (rucrypto_uint32_t)counter->buffer[counter->size - 1 - i] + carry;
        carry = (c >> 8) & 0xFF;
        counter->buffer[counter->size - 1 - i] = (rucrypto_uint8_t)(c & 0xFF);
    }
}

rucrypto_result_t rucrypto_r_1323565_1_022_generator_init(
        rucrypto_r_1323565_1_022_key_generator_context_t** ctx,
        rucrypto_r_1323565_1_022_key_data_gen_type_t genType,
        const rucrypto_r_1323565_1_022_intermediate_key_t key,
        rucrypto_r_1323565_1_022_format_func_t formatFunc,
        rucrypto_r_1323565_1_022_format_flags_t flags,
        rucrypto_r_1323565_1_022_counter_t* counter,
        const rucrypto_r_1323565_1_022_iv_t* iv,
        rucrypto_r_1323565_1_022_length_t* length,
        rucrypto_r_1323565_1_022_usage_t* usage,
        rucrypto_r_1323565_1_022_users_t* users,
        rucrypto_r_1323565_1_022_additional_t* additional) {
    RUCRYPTO_ASSERT(ctx != RUCRYPTO_NULL);

    switch (genType) {
        case rucrypto_r_1323565_1_022_key_data_gen_type_cmac128: { 
            if (iv->size != 16) {
                return rucrypto_result_invalid_parameters;
            }
            break;
        }
        case rucrypto_r_1323565_1_022_key_data_gen_type_hmac256: { 
            if (iv->size != 32) {
                return rucrypto_result_invalid_parameters;
            }
            break;
        }
        case rucrypto_r_1323565_1_022_key_data_gen_type_hmac512: { 
            if (iv->size != 64) {
                return rucrypto_result_invalid_parameters;
            }
            break;
        }
        case rucrypto_r_1323565_1_022_key_data_gen_type_nmac: {
            if (iv->size != 32) {
                return rucrypto_result_invalid_parameters;
            }
            break;
        }
        default: {
            return rucrypto_result_invalid_parameters;
        }
    }

    *ctx = rucrypto_allocate(sizeof(rucrypto_r_1323565_1_022_key_generator_context_t));
    if (*ctx == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }

    (*ctx)->zpart.buffer = rucrypto_allocate(iv->size);
    if ((*ctx)->zpart.buffer == RUCRYPTO_NULL) {
        rucrypto_deallocate(*ctx);
        return rucrypto_result_out_of_memory;
    }
    (*ctx)->zpart.size = iv->size;

    if (genType == rucrypto_r_1323565_1_022_key_data_gen_type_cmac128) {
        rucrypto_result_t res = rucrypto_gost_34_13_base_cipher_init(&(*ctx)->baseCipher, rucrypto_gost_34_12_encrypt_128, rucrypto_gost_34_12_decrypt_128, 16, (*ctx)->key, 32);
        if (res != rucrypto_result_ok) {
            rucrypto_deallocate((*ctx)->zpart.buffer);
            rucrypto_deallocate(*ctx);
            return res;
        }
    }

    (*ctx)->genType = genType;
    RUCRYPTO_MEMCPY((*ctx)->key, key, 32);
    (*ctx)->flags = flags;
    if (flags.counter) {
        RUCRYPTO_ASSERT(counter != RUCRYPTO_NULL);
        (*ctx)->counter = counter;
    } else {
        (*ctx)->counter = RUCRYPTO_NULL;
    }
    RUCRYPTO_MEMCPY((*ctx)->zpart.buffer, iv->buffer, iv->size);
    if (flags.length) {
        RUCRYPTO_ASSERT(length != RUCRYPTO_NULL);
        (*ctx)->length = length;
    } else {
        (*ctx)->length = RUCRYPTO_NULL;
    }
    if (flags.usage) {
        RUCRYPTO_ASSERT(usage != RUCRYPTO_NULL);
        (*ctx)->usage = usage;
    } else {
        (*ctx)->usage = RUCRYPTO_NULL;
    }
    if (flags.users) {
        RUCRYPTO_ASSERT(users != RUCRYPTO_NULL);
        (*ctx)->users = users;
    } else {
        (*ctx)->users = RUCRYPTO_NULL;
    }
    if (flags.additional) {
        RUCRYPTO_ASSERT(additional != RUCRYPTO_NULL);
        (*ctx)->additional = additional;
    } else {
        (*ctx)->additional = RUCRYPTO_NULL;
    }

    if (formatFunc == RUCRYPTO_NULL) {
        (*ctx)->formatFunc = rucrypto_r_1323565_1_022_format;
        (*ctx)->formatSize = 192;
    } else {
        (*ctx)->formatFunc = formatFunc;
        (*ctx)->formatSize = 0;
        (*ctx)->formatFunc((*ctx)->flags, RUCRYPTO_NULL, RUCRYPTO_NULL, RUCRYPTO_NULL, RUCRYPTO_NULL, RUCRYPTO_NULL, RUCRYPTO_NULL, RUCRYPTO_NULL, &(*ctx)->formatSize);
    }

    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_r_1323565_1_022_gen_next_key_data_part(
        rucrypto_r_1323565_1_022_key_generator_context_t* ctx,
        rucrypto_uint8_t part[],
        rucrypto_size_t partSize) {
    if (ctx->zpart.size != partSize) {
        return rucrypto_result_invalid_parameters;
    }
    rucrypto_r_1323565_1_022_add_1(ctx->counter);

    rucrypto_uint8_t* format = rucrypto_allocate(ctx->formatSize);
    if (format == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    ctx->formatFunc(ctx->flags, ctx->counter, &ctx->zpart, ctx->length, ctx->usage, ctx->users, ctx->additional, format, &ctx->formatSize);

    rucrypto_result_t res = rucrypto_result_ok;
    switch (ctx->genType) {
        case rucrypto_r_1323565_1_022_key_data_gen_type_cmac128: {
            rucrypto_gost_34_13_mac_context_t* macCtx;
            res = rucrypto_gost_34_13_mac_context_init(&macCtx, ctx->baseCipher);
            if (res != rucrypto_result_ok) {
                break;
            }
            rucrypto_size_t i = 0;
            for (; i < ctx->formatSize / 16 - 1; ++i) {
                rucrypto_gost_34_13_mac_calculate_part(macCtx, &format[16 * i]);
            }
            rucrypto_gost_34_13_mac_calculate_last(macCtx, &format[16 * i], ctx->formatSize - 16 * i);
            rucrypto_gost_34_13_mac_context_fini(&macCtx, ctx->zpart.buffer, ctx->zpart.size);
            RUCRYPTO_MEMCPY(part, ctx->zpart.buffer, ctx->zpart.size);
            break;
        }
        case rucrypto_r_1323565_1_022_key_data_gen_type_hmac256: {
            rucrypto_r_1323565_1_022_mac_key_t key = {
                .buffer = ctx->key,
                .size = 32
            };
            rucrypto_r_1323565_1_022_hmac_context_t* hmacCtx;
            res = rucrypto_r_1323565_1_022_hmac_256_context_init(&hmacCtx, &key);
            if (res != rucrypto_result_ok) {
                break;
            }
            rucrypto_size_t i = 0;
            for (; i < ctx->formatSize / 64; ++i) {
                rucrypto_r_1323565_1_022_hmac_part(hmacCtx, &format[64 * i]);
            }
            rucrypto_r_1323565_1_022_hmac_256_fini(&hmacCtx, &format[64 * i], ctx->formatSize - 64 * i, ctx->zpart.buffer);
            break;
        }
        case rucrypto_r_1323565_1_022_key_data_gen_type_hmac512: {
            rucrypto_r_1323565_1_022_mac_key_t key = {
                .buffer = ctx->key,
                .size = 32
            };
            rucrypto_r_1323565_1_022_hmac_context_t* hmacCtx;
            res = rucrypto_r_1323565_1_022_hmac_512_context_init(&hmacCtx, &key);
            if (res != rucrypto_result_ok) {
                break;
            }
            rucrypto_size_t i = 0;
            for (; i < ctx->formatSize / 64; ++i) {
                rucrypto_r_1323565_1_022_hmac_part(hmacCtx, &format[64 * i]);
            }
            rucrypto_r_1323565_1_022_hmac_512_fini(&hmacCtx, &format[64 * i], ctx->formatSize - 64 * i, ctx->zpart.buffer);
            break;
        }
        case rucrypto_r_1323565_1_022_key_data_gen_type_nmac: {
            rucrypto_r_1323565_1_022_mac_key_t key = {
                .buffer = ctx->key,
                .size = 32
            };
            rucrypto_r_1323565_1_022_nmac_context_t* nmacCtx;
            res = rucrypto_r_1323565_1_022_nmac_context_init(&nmacCtx, &key);
            if (res != rucrypto_result_ok) {
                break;
            }
            rucrypto_size_t i = 0;
            for (; i < 192 / 64; ++i) {
                rucrypto_r_1323565_1_022_nmac_part(nmacCtx, &format[64 * i]);
            }
            rucrypto_r_1323565_1_022_nmac_fini(&nmacCtx, &format[64 * i], ctx->formatSize - 64 * i, ctx->zpart.buffer);
            break;
        }
        default: {
            res = rucrypto_result_invalid_parameters;
            break;
        }
    }
    rucrypto_secure_zero(format, ctx->formatSize);
    rucrypto_deallocate(format);
    return res;
}

void rucrypto_r_1323565_1_022_generator_fini(rucrypto_r_1323565_1_022_key_generator_context_t** ctx) {
    if ((*ctx)->genType == rucrypto_r_1323565_1_022_key_data_gen_type_cmac128) {
        rucrypto_gost_34_13_base_cipher_fini(&(*ctx)->baseCipher);
    }
    rucrypto_secure_zero((*ctx)->zpart.buffer, (*ctx)->zpart.size);
    rucrypto_deallocate((*ctx)->zpart.buffer);
    rucrypto_deallocate(*ctx);
    *ctx = RUCRYPTO_NULL;
}
