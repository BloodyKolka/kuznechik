#include "rucrypto/common/allocator_provider.h"
#include <rucrypto/r_1323565_1_022/mac_functions.h>
#include <rucrypto/r_1323565_1_022/mac_context.h>

#include <rucrypto/gost_34_11/gost_34_11.h>

rucrypto_result_t rucrypto_r_1323565_1_022_hmac_512_context_init(rucrypto_r_1323565_1_022_hmac_context_t **ctx, const rucrypto_r_1323565_1_022_mac_key_t *key) {
    rucrypto_gost_34_11_hash_context_t* gost_34_11_ctx;
    rucrypto_result_t res = rucrypto_gost_34_11_hash_512_context_init(&gost_34_11_ctx);
    if (res != rucrypto_result_ok) {
        return res;
    }

    *ctx = rucrypto_allocate(sizeof(rucrypto_r_1323565_1_022_hmac_context_t));
    if (*ctx == RUCRYPTO_NULL) {
        rucrypto_gost_34_11_hash_context_fini(&gost_34_11_ctx);
        return rucrypto_result_out_of_memory;
    }

    (*ctx)->key = key;
    (*ctx)->size = rucrypto_r_1323565_1_022_size_512;
    (*ctx)->gost_34_11_ctx = gost_34_11_ctx;
    (*ctx)->iter = 0;

    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_r_1323565_1_022_hmac_256_context_init(rucrypto_r_1323565_1_022_hmac_context_t **ctx, const rucrypto_r_1323565_1_022_mac_key_t *key) {
    rucrypto_gost_34_11_hash_context_t* gost_34_11_ctx;
    rucrypto_result_t res = rucrypto_gost_34_11_hash_256_context_init(&gost_34_11_ctx);
    if (res != rucrypto_result_ok) {
        return res;
    }

    *ctx = rucrypto_allocate(sizeof(rucrypto_r_1323565_1_022_hmac_context_t));
    if (*ctx == RUCRYPTO_NULL) {
        rucrypto_gost_34_11_hash_context_fini(&gost_34_11_ctx);
        return rucrypto_result_out_of_memory;
    }

    (*ctx)->key = key;
    (*ctx)->size = rucrypto_r_1323565_1_022_size_256;
    (*ctx)->gost_34_11_ctx = gost_34_11_ctx;
    (*ctx)->iter = 0;

    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_r_1323565_1_022_nmac_context_init(rucrypto_r_1323565_1_022_nmac_context_t **ctx, const rucrypto_r_1323565_1_022_mac_key_t *key) {
    rucrypto_gost_34_11_hash_context_t* gost_34_11_ctx;
    rucrypto_result_t res = rucrypto_gost_34_11_hash_512_context_init(&gost_34_11_ctx);
    if (res != rucrypto_result_ok) {
        return res;
    }

    *ctx = rucrypto_allocate(sizeof(rucrypto_r_1323565_1_022_nmac_context_t));
    if (*ctx == RUCRYPTO_NULL) {
        rucrypto_gost_34_11_hash_context_fini(&gost_34_11_ctx);
        return rucrypto_result_out_of_memory;
    }

    (*ctx)->key = key;
    (*ctx)->gost_34_11_ctx = gost_34_11_ctx;
    (*ctx)->iter = 0;

    return rucrypto_result_ok;
}

static void rucrypto_r_1323565_1_022_do_keyed_part(rucrypto_gost_34_11_hash_context_t* ctx, const rucrypto_r_1323565_1_022_mac_key_t* key, rucrypto_bool_t internal) {
    RUCRYPTO_ASSERT(key->size >= 32);
    RUCRYPTO_ASSERT(key->size <= 64);

    rucrypto_vec512_t keyBuffer;
    if (internal) {
        for (rucrypto_size_t i = 0; i < key->size; ++i) {
            keyBuffer[i] = key->buffer[i] ^ 0x36;
        }
        if (key->size < 64) {
            RUCRYPTO_MEMSET(&keyBuffer[key->size], 0x36, 64 - key->size);
        }
    } else {
        for (rucrypto_size_t i = 0; i < key->size; ++i) {
            keyBuffer[i] = key->buffer[i] ^ 0x5c;
        }
        if (key->size < 64) {
            RUCRYPTO_MEMSET(&keyBuffer[key->size], 0x5c, 64 - key->size);
        }
    }
    rucrypto_gost_34_11_hash_part(ctx, keyBuffer);
}

void rucrypto_r_1323565_1_022_hmac_part(rucrypto_r_1323565_1_022_hmac_context_t* ctx, const rucrypto_vec512_t m) {
    if (ctx->iter == 0) {
        rucrypto_r_1323565_1_022_do_keyed_part(ctx->gost_34_11_ctx, ctx->key, true);
    }
    rucrypto_gost_34_11_hash_part(ctx->gost_34_11_ctx, m);
    ++ctx->iter;
}

void rucrypto_r_1323565_1_022_nmac_part(rucrypto_r_1323565_1_022_nmac_context_t* ctx, const rucrypto_vec512_t m) {
    if (ctx->iter == 0) {
        rucrypto_r_1323565_1_022_do_keyed_part(ctx->gost_34_11_ctx, ctx->key, true);
    }
    rucrypto_gost_34_11_hash_part(ctx->gost_34_11_ctx, m);
    ++ctx->iter;
}

rucrypto_result_t rucrypto_r_1323565_1_022_hmac_512_fini(rucrypto_r_1323565_1_022_hmac_context_t** ctx, const rucrypto_uint8_t data[], rucrypto_size_t dataSize, rucrypto_r_1323565_1_022_hmac_512_t hmac) {
    RUCRYPTO_ASSERT(dataSize < 64);

    if ((*ctx)->iter == 0) {
        rucrypto_r_1323565_1_022_do_keyed_part((*ctx)->gost_34_11_ctx, (*ctx)->key, true);
    }

    rucrypto_gost_34_11_hash_512_t internalHash;
    rucrypto_gost_34_11_hash_512_fini(&(*ctx)->gost_34_11_ctx, data, dataSize, internalHash);

    rucrypto_result_t res = rucrypto_gost_34_11_hash_512_context_init(&(*ctx)->gost_34_11_ctx);
    if (res != rucrypto_result_ok) {
        return res;
    }

    rucrypto_r_1323565_1_022_do_keyed_part((*ctx)->gost_34_11_ctx, (*ctx)->key, false);

    rucrypto_gost_34_11_hash_part((*ctx)->gost_34_11_ctx, internalHash);
    rucrypto_gost_34_11_hash_512_fini(&(*ctx)->gost_34_11_ctx, internalHash, 0, hmac);

    rucrypto_deallocate(*ctx);
    *ctx = RUCRYPTO_NULL;

    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_r_1323565_1_022_hmac_256_fini(rucrypto_r_1323565_1_022_hmac_context_t** ctx, const rucrypto_uint8_t data[], rucrypto_size_t dataSize, rucrypto_r_1323565_1_022_hmac_256_t hmac) {
    RUCRYPTO_ASSERT(dataSize < 64);

    if ((*ctx)->iter == 0) {
        rucrypto_r_1323565_1_022_do_keyed_part((*ctx)->gost_34_11_ctx, (*ctx)->key, true);
    }

    rucrypto_gost_34_11_hash_256_t internalHash;
    rucrypto_gost_34_11_hash_256_fini(&(*ctx)->gost_34_11_ctx, data, dataSize, internalHash);

    rucrypto_result_t res = rucrypto_gost_34_11_hash_256_context_init(&(*ctx)->gost_34_11_ctx);
    if (res != rucrypto_result_ok) {
        return res;
    }
 
    rucrypto_r_1323565_1_022_do_keyed_part((*ctx)->gost_34_11_ctx, (*ctx)->key, false);
    rucrypto_gost_34_11_hash_256_fini(&(*ctx)->gost_34_11_ctx, internalHash, 32, hmac); 

    rucrypto_deallocate(*ctx);
    *ctx = RUCRYPTO_NULL;

    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_r_1323565_1_022_nmac_fini(rucrypto_r_1323565_1_022_nmac_context_t** ctx, const rucrypto_uint8_t data[], rucrypto_size_t dataSize, rucrypto_r_1323565_1_022_nmac_t nmac) {
    RUCRYPTO_ASSERT(dataSize < 64);

    if ((*ctx)->iter == 0) {
        rucrypto_r_1323565_1_022_do_keyed_part((*ctx)->gost_34_11_ctx, (*ctx)->key, true);
    }

    rucrypto_gost_34_11_hash_512_t internalHash;
    rucrypto_gost_34_11_hash_512_fini(&(*ctx)->gost_34_11_ctx, data, dataSize, internalHash);

    rucrypto_result_t res = rucrypto_gost_34_11_hash_256_context_init(&(*ctx)->gost_34_11_ctx);
    if (res != rucrypto_result_ok) {
        return res;
    }

    rucrypto_r_1323565_1_022_do_keyed_part((*ctx)->gost_34_11_ctx, (*ctx)->key, false);

    rucrypto_gost_34_11_hash_part((*ctx)->gost_34_11_ctx, internalHash);
    rucrypto_gost_34_11_hash_256_fini(&(*ctx)->gost_34_11_ctx, internalHash, 0, nmac);

    rucrypto_deallocate(*ctx);
    *ctx = RUCRYPTO_NULL;

    return rucrypto_result_ok;
}

