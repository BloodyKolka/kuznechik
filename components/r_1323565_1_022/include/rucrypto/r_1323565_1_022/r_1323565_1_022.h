/**
 * @file rucrypto/r_1323565_1_022/r_1323565_1_022.h
 */

#ifndef RUCRYPTO_R_1323565_1_022_R_1323565_1_022_H
#define RUCRYPTO_R_1323565_1_022_R_1323565_1_022_H

#include <rucrypto/r_1323565_1_022/mac_functions.h>
#include <rucrypto/r_1323565_1_022/key_generator.h>

#endif // RUCRYPTO_R_1323565_1_022_R_1323565_1_022_H
