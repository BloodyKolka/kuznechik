/**
 * @file rucrypto/r_1323565_1_022/mac_functions.h
 */

#ifndef RUCRYPTO_R_1323565_1_022_MAC_FUNCTIONS_H
#define RUCRYPTO_R_1323565_1_022_MAC_FUNCTIONS_H

#include <rucrypto/common/common.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/** Struct to hold HMAC context */
typedef struct rucrypto_r_1323565_1_022_hmac_context_t rucrypto_r_1323565_1_022_hmac_context_t;
/** Struct to hold NMAC context */
typedef struct rucrypto_r_1323565_1_022_nmac_context_t rucrypto_r_1323565_1_022_nmac_context_t;

/** MAC key type */
typedef rucrypto_variable_buffer_const_t rucrypto_r_1323565_1_022_mac_key_t;

/** 256 bit hmac buffer */
typedef rucrypto_vec256_t rucrypto_r_1323565_1_022_hmac_256_t;
/** 512 bit hmac buffer */
typedef rucrypto_vec512_t rucrypto_r_1323565_1_022_hmac_512_t;
/** nmac buffer */
typedef rucrypto_vec256_t rucrypto_r_1323565_1_022_nmac_t;

/**
 * @brief Initialize context for 512-bit hmac function from R 1323565.1.022
 * @param [out] ctx pointer to rucrypto_r_1323565_1_022_hmac_context_t
 * @param [in] key pointer to key information
 * @return rucrypto_result_ok on success, error code otherwise
 * @details Must be called first to initialize context
 */
extern rucrypto_result_t rucrypto_r_1323565_1_022_hmac_512_context_init(rucrypto_r_1323565_1_022_hmac_context_t** ctx, const rucrypto_r_1323565_1_022_mac_key_t* key);

/**
 * @brief Initialize context for 256-bit hmac function from R 1323565.1.022
 * @param [out] ctx pointer to rucrypto_r_1323565_1_022_hmac_context_t
 * @param [in] key pointer to key information
 * @return rucrypto_result_ok on success, error code otherwise
 * @details Must be called first to initialize context
 */
extern rucrypto_result_t rucrypto_r_1323565_1_022_hmac_256_context_init(rucrypto_r_1323565_1_022_hmac_context_t** ctx, const rucrypto_r_1323565_1_022_mac_key_t* key);

/**
 * @brief Initialize context for nmac function from R 1323565.1.022
 * @param [out] ctx pointer to rucrypto_r_1323565_1_022_nmac_context_t
 * @param [in] key pointer to key information
 * @return rucrypto_result_ok on success, error code otherwise
 * @details Must be called first to initialize context
 */
extern rucrypto_result_t rucrypto_r_1323565_1_022_nmac_context_init(rucrypto_r_1323565_1_022_nmac_context_t** ctx, const rucrypto_r_1323565_1_022_mac_key_t* key);

/**
 * @brief Calculate intermediate values for 512 data block for nmac function from R 1323565.1.022
 * @param [in, out] ctx pointer to rucrypto_r_1323565_1_022_hmac_context_t, initialized with rucrypto_r_1323565_1_022_hmac_256_context_init
 *        or rucrypto_r_1323565_1_022_hmac_512_context_init
 * @param [in] m next 512 bits of input data
 * @details Must be called for all parts of data with size >= 512
 */
extern void rucrypto_r_1323565_1_022_hmac_part(rucrypto_r_1323565_1_022_hmac_context_t* ctx, const rucrypto_vec512_t m);

/**
 * @brief Calculate intermediate values for 512 data block for nmac function from R 1323565.1.022
 * @param [in, out] ctx pointer to rucrypto_r_1323565_1_022_nmac_context_t, initialized with rucrypto_r_1323565_1_022_hash_256_context_init
 * @param [in] m next 512 bits of input data
 * @details Must be called for all parts of data with size >= 512
 */
extern void rucrypto_r_1323565_1_022_nmac_part(rucrypto_r_1323565_1_022_nmac_context_t* ctx, const rucrypto_vec512_t m);

/**
 * @brief Calculate final value of 512-bit hmac function from R 1323565.1.022
 * @param [in] ctx pointer to rucrypto_r_1323565_1_022_hmac_context_t
 * @param [in] data remaining input data
 * @param [in] dataSize size of remaining data in bytes (must be strictly less than 512)
 * @param [out] hmac resulting hmac hmac
 * @return rucrypto_result_ok on success, error code otherwise
 * @details Must be called last with the last part of data with size < 512
 */
extern rucrypto_result_t rucrypto_r_1323565_1_022_hmac_512_fini(rucrypto_r_1323565_1_022_hmac_context_t** ctx, const rucrypto_uint8_t data[], rucrypto_size_t dataSize, rucrypto_r_1323565_1_022_hmac_512_t hmac);

/**
 * @brief Calculate final value of 256-bit hmac function from R 1323565.1.022
 * @param [in] ctx pointer to rucrypto_r_1323565_1_022_hmac_context_t
 * @param [in] data remaining input data
 * @param [in] dataSize size of remaining data in bytes (must be strictly less than 512)
 * @param [out] hmac resulting hmac
 * @return rucrypto_result_ok on success, error code otherwise
 * @details Must be called last with the last part of data with size < 512
 */
extern rucrypto_result_t rucrypto_r_1323565_1_022_hmac_256_fini(rucrypto_r_1323565_1_022_hmac_context_t** ctx, const rucrypto_uint8_t data[], rucrypto_size_t dataSize, rucrypto_r_1323565_1_022_hmac_256_t hmac);

/**
 * @brief Calculate final value of nmac function from R 1323565.1.022
 * @param [in] ctx pointer to rucrypto_r_1323565_1_022_nmac_context_t
 * @param [in] data remaining input data
 * @param [in] dataSize size of remaining data in bytes (must be strictly less than 512)
 * @param [out] nmac resulting nmac
 * @return rucrypto_result_ok on success, error code otherwise
 * @details Must be called last with the last part of data with size < 512
 */
extern rucrypto_result_t rucrypto_r_1323565_1_022_nmac_fini(rucrypto_r_1323565_1_022_nmac_context_t** ctx, const rucrypto_uint8_t data[], rucrypto_size_t dataSize, rucrypto_r_1323565_1_022_nmac_t nmac);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_R_1323565_1_022_MAC_FUNCTIONS_H
