/**
 * @file rucrypto/r_1323565_1_022/key_generator.h
 */

#ifndef RUCRYPTO_R_1323565_1_022_KEY_GENERATOR_H
#define RUCRYPTO_R_1323565_1_022_KEY_GENERATOR_H

#include <rucrypto/common/common.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/** Intermediate key type */
typedef rucrypto_vec256_t rucrypto_r_1323565_1_022_intermediate_key_t;

/** Generation type for intermediate key */
typedef enum rucrypto_r_1323565_1_022_intermediate_key_gen_type_t {
    rucrypto_r_1323565_1_022_intermediate_key_gen_type_hmac,
    rucrypto_r_1323565_1_022_intermediate_key_gen_type_nmac,
    rucrypto_r_1323565_1_022_intermediate_key_gen_type_xor,
} rucrypto_r_1323565_1_022_intermediate_key_gen_type_t;

/**
 * @brief Format flags, an 8 bit (1 byte) flags
 */
typedef struct rucrypto_r_1323565_1_022_format_flags_t {
    /** Should C_i be used in format function */
    rucrypto_bool_t counter    : 1; // C_i     }
    /** Should z_(i-1) be used in format function */
    rucrypto_bool_t zpart      : 1; // z_(i-1) } at least 1 must be true
    /** Should L be used in format function */
    rucrypto_bool_t length     : 1; // L
    /** Should P be used in format function */
    rucrypto_bool_t usage      : 1; // P
    /** Should U be used in format function */
    rucrypto_bool_t users      : 1; // U
    /** Should A be used in format function */
    rucrypto_bool_t additional : 1; // A
    /** Unused, must be 0 */
    rucrypto_bool_t _zero1     : 1; // Unused, must be 0
    /** Unused, must be 0 */
    rucrypto_bool_t _zero2     : 1; // Unused, must be 0
} rucrypto_r_1323565_1_022_format_flags_t;

/** Counter buffer type (is 32 bits in R 1323565.1.022 format function) */
typedef rucrypto_variable_buffer_t rucrypto_r_1323565_1_022_counter_t;
/** Z buffer type (is 64 bits in R 1323565.1.022 format function) */
typedef rucrypto_variable_buffer_t rucrypto_r_1323565_1_022_zpart_t;
/** Length buffer type (is 31 bits in R 1323565.1.022 format function) */
typedef rucrypto_variable_buffer_t rucrypto_r_1323565_1_022_length_t;
/** Usage buffer type (is 32 bits in R 1323565.1.022 format function) */
typedef rucrypto_variable_buffer_t rucrypto_r_1323565_1_022_usage_t;
/** User buffer type (is 16 bits in R 1323565.1.022 format function) */
typedef rucrypto_variable_buffer_t rucrypto_r_1323565_1_022_users_t;
/** Additional buffer type (is 16 bits in R 1323565.1.022 format function) */
typedef rucrypto_variable_buffer_t rucrypto_r_1323565_1_022_additional_t;

/** Initializing value buffer type (is 64 bits in R 1323565.1.022 format function) */
typedef rucrypto_variable_buffer_t rucrypto_r_1323565_1_022_iv_t;

/** Generation type for key data */
typedef enum rucrypto_r_1323565_1_022_key_data_gen_type_t {
   rucrypto_r_1323565_1_022_key_data_gen_type_cmac128,
   rucrypto_r_1323565_1_022_key_data_gen_type_hmac256,
   rucrypto_r_1323565_1_022_key_data_gen_type_hmac512,
   rucrypto_r_1323565_1_022_key_data_gen_type_nmac,
} rucrypto_r_1323565_1_022_key_data_gen_type_t;

/** Struct to hold generator context */
typedef struct rucrypto_r_1323565_1_022_key_generator_context_t rucrypto_r_1323565_1_022_key_generator_context_t;

/**
 * @brief Generate intermediate key K(1) using kdf(1) from R 1323565.1.022
 * @param [in] genType Type of kdf(1) function to use
 * @param [in] sourceKeyData Source key material
 * @param [in] sourceKeyDataSize Source key material size
 * @param [in] salt Salt
 * @param [in] saltSize Salt size
 * @param [out] key Intermediate key K(1)
 * @return rucrypto_result_ok on success, error code otherwise
 * @details Note that saltSize must be <= 512 bits. If xor type is used, saltSize and sourceKeyDataSize must both be 256 bits
 */
extern rucrypto_result_t rucrypto_r_1323565_1_022_gen_intermediate_key(
        rucrypto_r_1323565_1_022_intermediate_key_gen_type_t genType, 
        rucrypto_uint8_t sourceKeyData[], 
        rucrypto_size_t sourceKeyDataSize, 
        rucrypto_uint8_t salt[], 
        rucrypto_size_t saltSize, 
        rucrypto_r_1323565_1_022_intermediate_key_t key);

/**
 * @brief Format function type
 * @brief Format info for key data generation
 * @param [in] flags Flags
 * @param [in] counter Counter variable
 * @param [in] zpart Previous part
 * @param [in] length Length of requested key data
 * @param [in] usage Key data usage mark
 * @param [in] users Key data user information
 * @param [in] additional Additional information
 * @param [out] format Resulting buffer
 * @param [in, out] formatSize Buffer size
 * @details Setting all parameters, except for formatSize to zero must set formatSize to the required size of format buffer
 */
typedef void (*rucrypto_r_1323565_1_022_format_func_t)(
        rucrypto_r_1323565_1_022_format_flags_t flags,
        const rucrypto_r_1323565_1_022_counter_t* counter,
        const rucrypto_r_1323565_1_022_zpart_t* zpart,
        const rucrypto_r_1323565_1_022_length_t* length,
        const rucrypto_r_1323565_1_022_usage_t* usage,
        const rucrypto_r_1323565_1_022_users_t* users,
        const rucrypto_r_1323565_1_022_additional_t* additional,
        rucrypto_uint8_t format[],
        rucrypto_size_t* formatSize); 

/**
 * @brief Format info for key data generation
 * @param [in] flags Flags
 * @param [in] counter Counter variable
 * @param [in] zpart Previous part
 * @param [in] length Length of requested key data
 * @param [in] usage Key data usage mark
 * @param [in] users Key data user information
 * @param [in] additional Additional information
 * @param [out] format Resulting buffer
 * @param [in, out] formatSize Buffer size
 * @details Formating of usage, users and additional info are not defined in R 1323565.1.022, so are left to the user's discretion
 * @details Although part size may vary, the allocated block for size is 512 bits. If it is less, the less is filled with 0
 * @details Buffer must be 192 bytes or bigger. If not, formatSize will be set to 192 and function will return rucrypto_result_invalid_parameters
 */
extern void rucrypto_r_1323565_1_022_format(
        rucrypto_r_1323565_1_022_format_flags_t flags,
        const rucrypto_r_1323565_1_022_counter_t* counter,
        const rucrypto_r_1323565_1_022_zpart_t* zpart,
        const rucrypto_r_1323565_1_022_length_t* length,
        const rucrypto_r_1323565_1_022_usage_t* usage,
        const rucrypto_r_1323565_1_022_users_t* users,
        const rucrypto_r_1323565_1_022_additional_t* additional,
        rucrypto_uint8_t format[],
        rucrypto_size_t* formatSize); 

/**
 * @brief Init key data generator
 * @param [out] ctx Pointer to rucrypto_r_1323565_1_022_key_generator_context_t 
 * @param [in] genType Type of MAC function to use
 * @param [in] key Intermediate key, generated with rucrypto_r_1323565_1_022_gen_intermediate_key
 * @param [in] formatFunc Formatting function
 * @param [in] flags Flags to use for format function rucrypto_r_1323565_1_022_format
 * @param [in] counter Buffer for counter, must be initialized with zeroes
 * @param [in] iv Initializing value for z0 (as per R 1323565.1.022)
 * @param [in] length Length of key data to generate (may be NULL if relevant flag is false)
 * @param [in] usage Usage information (may be NULL if relevant flag is false)
 * @param [in] users Users inforamtion (may be NULL if relevant flafs is false)
 * @param [in] additional Additional information (may be NULL if relevant flag is false)
 * @return rucrypto_result_ok on success, error code otherwise
 * @details Buffers for length, usage, users and additional should be valid until generator is de-initialized
 */
extern rucrypto_result_t rucrypto_r_1323565_1_022_generator_init(
        rucrypto_r_1323565_1_022_key_generator_context_t** ctx,
        rucrypto_r_1323565_1_022_key_data_gen_type_t genType,
        const rucrypto_r_1323565_1_022_intermediate_key_t key,
        rucrypto_r_1323565_1_022_format_func_t formatFunc,
        rucrypto_r_1323565_1_022_format_flags_t flags,
        rucrypto_r_1323565_1_022_counter_t* counter,
        const rucrypto_r_1323565_1_022_iv_t* iv,
        rucrypto_r_1323565_1_022_length_t* length,
        rucrypto_r_1323565_1_022_usage_t* usage,
        rucrypto_r_1323565_1_022_users_t* users,
        rucrypto_r_1323565_1_022_additional_t* additional);

/**
 * @brief Generate next part of key data rucrypto_r_1323565_1_022_init_generator
 * @param [in] ctx Key generator context, initialized with 
 * @param [out] part Buffer to write key data to
 * @param [in] partSize Size of part buffer
 * @return rucrypto_result_ok on success, error code otherwise
 * @details According to selected MAC-function in rucrypto_r_1323565_1_022_init_generator, partSize must be 16, 32 or 64 
 *          (cmac128, hmac256/nmac, hmac512) 
 */
extern rucrypto_result_t rucrypto_r_1323565_1_022_gen_next_key_data_part(
        rucrypto_r_1323565_1_022_key_generator_context_t* ctx,
        rucrypto_uint8_t part[],
        rucrypto_size_t partSize);

/**
 * @brief Deallocate memory and securely clear buffers with leftover information
 * @param [in, out] ctx Key generator context
 */
extern void rucrypto_r_1323565_1_022_generator_fini(rucrypto_r_1323565_1_022_key_generator_context_t** ctx);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_R_1323565_1_022_KEY_GENERATOR_H
