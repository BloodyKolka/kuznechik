#ifndef RUCRYPTO_R_1323565_1_022_MAC_CONTEXT_H
#define RUCRYPTO_R_1323565_1_022_MAC_CONTEXT_H

#include <rucrypto/common/common.h>

#include <rucrypto/gost_34_11/gost_34_11.h>

#include <rucrypto/r_1323565_1_022/mac_functions.h>

typedef enum rucrypto_r_1323565_1_022_hmac_size {
    rucrypto_r_1323565_1_022_size_256,
    rucrypto_r_1323565_1_022_size_512,
} rucrypto_r_1323565_1_022_hmac_size;

struct rucrypto_r_1323565_1_022_hmac_context_t {
    rucrypto_gost_34_11_hash_context_t* gost_34_11_ctx;
    rucrypto_r_1323565_1_022_hmac_size size;
    const rucrypto_r_1323565_1_022_mac_key_t* key;
    rucrypto_size_t iter;
};

struct rucrypto_r_1323565_1_022_nmac_context_t {
    rucrypto_gost_34_11_hash_context_t* gost_34_11_ctx;
    const rucrypto_r_1323565_1_022_mac_key_t* key;
    rucrypto_size_t iter;
};

#endif // RUCRYPTO_R_1323565_1_022_MAC_CONTEXT_H

