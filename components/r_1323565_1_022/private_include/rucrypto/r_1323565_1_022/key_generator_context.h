#ifndef RUCRYPTO_R_1323565_1_022_KEY_GENERATOR_CONTEXT_H
#define RUCRYPTO_R_1323565_1_022_KEY_GENERATOR_CONTEXT_H

#include <rucrypto/r_1323565_1_022/key_generator.h>
#include <rucrypto/gost_34_13/common.h>

struct rucrypto_r_1323565_1_022_key_generator_context_t {
    rucrypto_r_1323565_1_022_key_data_gen_type_t genType;
    rucrypto_r_1323565_1_022_intermediate_key_t key;
    rucrypto_r_1323565_1_022_format_func_t formatFunc;
    rucrypto_size_t formatSize;
    rucrypto_r_1323565_1_022_format_flags_t flags; 
    rucrypto_r_1323565_1_022_counter_t* counter;
    rucrypto_r_1323565_1_022_zpart_t zpart;
    rucrypto_r_1323565_1_022_length_t* length;
    rucrypto_r_1323565_1_022_usage_t* usage;
    rucrypto_r_1323565_1_022_users_t* users;
    rucrypto_r_1323565_1_022_additional_t* additional;
    rucrypto_gost_34_13_base_cipher_t* baseCipher;
    void* internalBuffer;
};

#endif // RUCRYPTO_R_1323565_1_022_KEY_GENERATOR_CONTEXT_H

