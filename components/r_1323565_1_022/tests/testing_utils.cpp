#include "testing_utils.h"

#include <gtest/gtest.h>

#include <iomanip>
#include <sstream>

std::string PrintBuffer(const uint8_t a[], size_t size) {
    std::stringstream ss;
    for (size_t i = 0; i < size; ++i) {
        ss << std::hex << std::setfill('0') << std::setw(2) << (int)a[i];
    }
    return ss.str();
}

std::string PrintBuffer(const uint64_t a[], size_t size) {
    std::stringstream ss;
    for (size_t i = 0; i < size; ++i) {
        ss << std::hex << std::setfill('0') << std::setw(16) << a[i];
    }
    return ss.str();
}

std::string PrintBuffer(const uint32_t a[], size_t size) {
    std::stringstream ss;
    for (size_t i = 0; i < size; ++i) {
        ss << std::hex << std::setfill('0') << std::setw(8) << a[i];
    }
    return ss.str();
}

bool AreBuffersEqual(const uint8_t a[], const uint8_t b[], size_t size) {
    for (size_t i = 0; i < size; ++i) {
        if (a[i] != b[i]) {
            GTEST_LOG_(INFO) << PrintBuffer(a, size) << " - " << PrintBuffer(b, size);
            GTEST_LOG_(INFO) << std::hex << "a[" << i << "](" << a[i] << ") != " << "b[" << i << "](" << b[i] << ")";
            return false;
        }
    }
    return true;
}

bool AreBuffersEqual(const uint64_t a[], const uint64_t b[], size_t size) {
    for (size_t i = 0; i < size; ++i) {
        if (a[i] != b[i]) {
            GTEST_LOG_(INFO) << PrintBuffer(a, size) << " - " << PrintBuffer(b, size);
            GTEST_LOG_(INFO) << std::hex << "a[" << i << "](" << a[i] << ") != " << "b[" << i << "](" << b[i] << ")";
            return false;
        }
    }
    return true;
}

bool AreBuffersEqual(const uint32_t a[], const uint32_t b[], size_t size) {
    for (size_t i = 0; i < size; ++i) {
        if (a[i] != b[i]) {
            GTEST_LOG_(INFO) << PrintBuffer(a, size) << " - " << PrintBuffer(b, size);
            GTEST_LOG_(INFO) << std::hex << "a[" << i << "](" << a[i] << ") != " << "b[" << i << "](" << b[i] << ")";
            return false;
        }
    }
    return true;
}
