#ifndef RUCRYPTO_GOST_34_13_CTR_CONTEXT_H
#define RUCRYPTO_GOST_34_13_CTR_CONTEXT_H

#include <rucrypto/gost_34_13/common.h>

struct rucrypto_gost_34_13_ctr_context_t {
    const rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_uint8_t* ctr;
    rucrypto_size_t gammaSize;
    rucrypto_uint8_t* tmpBuffer;
};

#endif // RUCRYPTO_GOST_34_13_CTR_CONTEXT_H
