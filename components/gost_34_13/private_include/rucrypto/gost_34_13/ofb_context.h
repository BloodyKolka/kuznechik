#ifndef RUCRYPTO_GOST_34_12_OFB_CONTEXT_H
#define RUCRYPTO_GOST_34_12_OFB_CONTEXT_H

#include <rucrypto/gost_34_13/common.h>

struct rucrypto_gost_34_13_ofb_context_t {
    const rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_size_t gammaSize;
    rucrypto_uint8_t* reg;
    rucrypto_size_t regSize;
};

#endif // RUCRYPTO_GOST_34_12_OFB_CONTEXT_H
