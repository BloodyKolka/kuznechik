#ifndef RUCRYPTO_GOST_34_13_MAC_CONTEXT_H
#define RUCRYPTO_GOST_34_13_MAC_CONTEXT_H

#include <rucrypto/gost_34_13/common.h>

struct rucrypto_gost_34_13_mac_context_t {
    const rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_uint8_t* buffer;
    rucrypto_uint8_t* key;
};

#endif // RUCRYPTO_GOST_34_13_MAC_CONTEXT_H
