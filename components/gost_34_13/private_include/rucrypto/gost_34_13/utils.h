#ifndef RUCRYPTO_GOST_34_13_UTILS_H
#define RUCRYPTO_GOST_34_13_UTILS_H

#include <rucrypto/common/types.h>

/**
 * @brief Perform addition in ring Z_2^n
 * @param a first operand, used to store result
 * @param b second operand
 * @param operandSize size of first operand
 */
void rucrypto_gost_34_13_add(rucrypto_uint8_t a[], rucrypto_uint8_t b, rucrypto_size_t operandSize);

/**
 * @brief Perform cyclic shift of buffer in the direction of most significant bytes
 * @param buffer buffer
 * @param bufferSize size of buffer
 * @param shiftCount times to shift
 */
void rucrypto_gost_34_13_shift_left(rucrypto_uint8_t buffer[], rucrypto_size_t bufferSize, rucrypto_size_t shiftCount);

#endif // RUCRYPTO_GOST_34_13_UTILS_H
