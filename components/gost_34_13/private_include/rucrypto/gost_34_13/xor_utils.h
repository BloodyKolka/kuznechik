#ifndef RUCRYPTO_GOST_34_13_XOR_UTILS_H
#define RUCRYPTO_GOST_34_13_XOR_UTILS_H

#include <rucrypto/gost_34_13/common.h>

/**
 * @brief Perform X (XOR) operation from GOST 34.13-2018
 * @param [in, out] a first operand, used to store result
 * @param [in] b second operand
 * @param [in] size operands' size
 */
void rucrypto_gost_34_13_xor(rucrypto_uint8_t* a, const rucrypto_uint8_t* b, rucrypto_size_t size);

#endif // RUCRYPTO_GOST_34_13_XOR_UTILS_H
