#ifndef RUCRYPTO_GOST_34_13_CBC_CONTEXT_H
#define RUCRYPTO_GOST_34_13_CBC_CONTEXT_H

#include <rucrypto/gost_34_13/common.h>

struct rucrypto_gost_34_13_cbc_context_t {
    const rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_uint8_t* reg;
    rucrypto_size_t regSize;
    rucrypto_uint8_t* tmpBuffer;
};

#endif // RUCRYPTO_GOST_34_13_CBC_CONTEXT_H
