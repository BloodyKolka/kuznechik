#ifndef RUCRYPTO_GOST_34_13_BASE_CIPHER_H
#define RUCRYPTO_GOST_34_13_BASE_CIPHER_H

#include <rucrypto/gost_34_13/common.h>

struct rucrypto_gost_34_13_base_cipher_t {
    rucrypto_gost_34_13_base_cipher_func_t encryptFunc;
    rucrypto_gost_34_13_base_cipher_func_t decryptFunc;
    rucrypto_size_t blockSize;
    const rucrypto_uint8_t* key;
    rucrypto_size_t keySize;
};

#endif // RUCRYPTO_GOST_34_13_BASE_CIPHER_H
