#include <rucrypto/gost_34_13/common.h>

#include <gtest/gtest.h>

TEST(Gost3413Padding, Padding1Test) {
    // Data is 0xFF, 0xFF. 0x11, 0x11 is garbage to test padding
    uint8_t input1[] {
        0xFF, 0xFF, 0x11, 0x11
    };

    static const uint8_t expectedOutput1[] = {
        0xFF, 0xFF, 0x00, 0x00
    };

    rucrypto_gost_34_13_pad_1(input1, 2, 4);
    for (size_t i = 0; i < 4; ++i) {
        EXPECT_EQ(input1[i], expectedOutput1[i]);
    }

    uint8_t input2[] {
        0xFF, 0xFF, 0x11, 0x11
    };

    static const uint8_t expectedOutput2[] = {
        0xFF, 0xFF, 0x11, 0x11
    };

    rucrypto_gost_34_13_pad_1(input2, 4, 4);
    for (size_t i = 0; i < 4; ++i) {
        EXPECT_EQ(input2[i], expectedOutput2[i]);
    }
}

TEST(Gost3413Padding, Padding2Test) {
    // Data is 0xFF, 0xFF. 0x11, 0x11 is garbage to test padding
    uint8_t input1[] {
        0xFF, 0xFF, 0x11, 0x11
    };

    static const uint8_t expectedOutput1[] = {
        0xFF, 0xFF, 0x80, 0x00
    };

    rucrypto_gost_34_13_pad_2(input1, 2, 4);
    for (size_t i = 0; i < 4; ++i) {
        EXPECT_EQ(input1[i], expectedOutput1[i]);
    }

    uint8_t input2[] {
        0xFF, 0xFF, 0x11, 0x11
    };

    static const uint8_t expectedOutput2[] = {
        0xFF, 0xFF, 0x11, 0x11
    };

    rucrypto_gost_34_13_pad_2(input2, 4, 4);
    for (size_t i = 0; i < 4; ++i) {
        EXPECT_EQ(input2[i], expectedOutput2[i]);
    }

    // Edge case, original message is padded, buffer with garbage is passed, dataSize = 0
    uint8_t input3[] {
        0xFF, 0xFF, 0xFF, 0xFF
    };

    static const uint8_t expectedOutput3[] = {
        0x80, 0x00, 0x00, 0x00
    };

    rucrypto_gost_34_13_pad_2(input3, 0, 4);
    for (size_t i = 0; i < 4; ++i) {
        EXPECT_EQ(input3[i], expectedOutput3[i]);
    }
}

TEST(Gost3413Padding, Padding3Test) {
    // Data is 0xFF, 0xFF. 0x11, 0x11 is garbage to test padding
    uint8_t input1[] {
        0xFF, 0xFF, 0x11, 0x11
    };

    static const uint8_t expectedOutput1[] = {
        0xFF, 0xFF, 0x80, 0x00
    };

    rucrypto_gost_34_13_pad_3(input1, 2, 4);
    for (size_t i = 0; i < 4; ++i) {
        EXPECT_EQ(input1[i], expectedOutput1[i]);
    }

    uint8_t input2[] {
        0xFF, 0xFF, 0x11, 0x11
    };

    static const uint8_t expectedOutput2[] = {
        0xFF, 0xFF, 0x11, 0x11
    };

    rucrypto_gost_34_13_pad_3(input2, 4, 4);
    for (size_t i = 0; i < 4; ++i) {
        EXPECT_EQ(input2[i], expectedOutput2[i]);
    }

    // Edge case, original message is padded, buffer with garbage is passed, dataSize = 0
    uint8_t input3[] {
        0xFF, 0xFF, 0xFF, 0xFF
    };

    static const uint8_t expectedOutput3[] = {
        0xFF, 0xFF, 0xFF, 0xFF
    };

    rucrypto_gost_34_13_pad_3(input3, 0, 4);
    for (size_t i = 0; i < 4; ++i) {
        EXPECT_EQ(input3[i], expectedOutput3[i]);
    }
}
