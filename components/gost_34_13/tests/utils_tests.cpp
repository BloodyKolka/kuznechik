extern "C" {
#include "../private_include/rucrypto/gost_34_13/utils.h"
}

#include <gtest/gtest.h>

TEST(Gost3413Utils, ShiftLeftTest) {
    static const uint8_t inputInit[] = {
        0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff
    };

    uint8_t input[16];

    uint8_t expectedOutputs[16][16] = {
            {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff},
            {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00},
            {0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00, 0x11},
            {0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00, 0x11, 0x22},
            {0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00, 0x11, 0x22, 0x33},
            {0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00, 0x11, 0x22, 0x33, 0x44},
            {0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55},
            {0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66},
            {0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77},
            {0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88},
            {0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99},
            {0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa},
            {0xcc, 0xdd, 0xee, 0xff, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb},
            {0xdd, 0xee, 0xff, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc},
            {0xee, 0xff, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd},
            {0xff, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee},
    };

    for (size_t i = 0; i <= 2 * sizeof(input); ++i) {
        for (size_t j = 0; j < sizeof(input); ++j) {
            input[j] = inputInit[j];
        }
        rucrypto_gost_34_13_shift_left(input, sizeof(input), i);
        for (size_t j = 0; j < sizeof(input); ++j) {
            EXPECT_EQ(input[j], expectedOutputs[i % 16][j]) << "Failed on " << i;
        }
    }
}

TEST(Gost3413Utils, AddTest) {
    uint8_t input1[] = {
        0x00, 0x00
    };

    static const uint8_t output1[] = {
        0x00, 0x01
    };

    rucrypto_gost_34_13_add(input1, 0x1, sizeof(input1));
    for (size_t i = 0; i < sizeof(input1); ++i) {
        EXPECT_EQ(input1[i], output1[i]);
    }

    uint8_t input2[] = {
        0x00, 0xFF, 0xFF
    };

    static const uint8_t output2[] = {
        0x01, 0x00, 0x00
    };

    rucrypto_gost_34_13_add(input2, 0x1, sizeof(input2));
    for (size_t i = 0; i < sizeof(input2); ++i) {
        EXPECT_EQ(input2[i], output2[i]);
    }
}
