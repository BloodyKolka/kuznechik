#include <rucrypto/gost_34_13/common.h>

#include <rucrypto/gost_34_13/base_cipher.h>

rucrypto_result_t rucrypto_gost_34_13_base_cipher_init(
        rucrypto_gost_34_13_base_cipher_t** baseCipher, 
        rucrypto_gost_34_13_base_cipher_func_t encryptFunc, 
        rucrypto_gost_34_13_base_cipher_func_t decryptFunc, 
        rucrypto_size_t blockSize, 
        const rucrypto_uint8_t* key, 
        rucrypto_size_t keySize) {
    *baseCipher = rucrypto_allocate(sizeof(rucrypto_gost_34_13_base_cipher_t));
    if (*baseCipher == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    (*baseCipher)->encryptFunc = encryptFunc;
    (*baseCipher)->decryptFunc = decryptFunc;
    (*baseCipher)->blockSize = blockSize;
    (*baseCipher)->key = key;
    (*baseCipher)->keySize = keySize;
    return rucrypto_result_ok;
}

void rucrypto_gost_34_13_base_cipher_fini(rucrypto_gost_34_13_base_cipher_t** baseCipher) {
    rucrypto_deallocate(*baseCipher);
}

rucrypto_result_t rucrypto_gost_34_13_pad_1(rucrypto_uint8_t buffer[], rucrypto_size_t dataSize, rucrypto_size_t padSize) {
    RUCRYPTO_CHECK_ARGS(dataSize <= padSize);
    for (rucrypto_size_t i = dataSize; i < padSize; ++i) {
        buffer[i] = 0;
    }
    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_gost_34_13_pad_2(rucrypto_uint8_t buffer[], rucrypto_size_t dataSize, rucrypto_size_t padSize) {
    RUCRYPTO_CHECK_ARGS(dataSize <= padSize);
    buffer[dataSize] = 0x80;
    for (rucrypto_size_t i = dataSize + 1; i < padSize; ++i) {
        buffer[i] = 0;
    }
    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_gost_34_13_pad_3(rucrypto_uint8_t buffer[], rucrypto_size_t dataSize, rucrypto_size_t padSize) {
    RUCRYPTO_CHECK_ARGS(dataSize <= padSize);
    if (dataSize == 0)
        return rucrypto_result_ok;
    return rucrypto_gost_34_13_pad_2(buffer, dataSize, padSize);
}
