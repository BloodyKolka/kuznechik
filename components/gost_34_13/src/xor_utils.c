#include <rucrypto/gost_34_13/xor_utils.h>

void rucrypto_gost_34_13_xor(rucrypto_uint8_t* a, const rucrypto_uint8_t* b, rucrypto_size_t size) {
    for (rucrypto_size_t i = 0; i < size; ++i) {
        a[i] ^= b[i];
    }
}
