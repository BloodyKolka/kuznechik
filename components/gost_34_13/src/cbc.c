#include <rucrypto/gost_34_13/cbc.h>

#include <rucrypto/gost_34_13/base_cipher.h>
#include <rucrypto/gost_34_13/cbc_context.h>
#include <rucrypto/gost_34_13/xor_utils.h>
#include <rucrypto/gost_34_13/utils.h>

#include <rucrypto/common/secure_zero.h>

rucrypto_result_t rucrypto_gost_34_13_cbc_context_init(
        rucrypto_gost_34_13_cbc_context_t** ctx, 
        const rucrypto_gost_34_13_base_cipher_t* baseCipher, 
        rucrypto_size_t regSize, 
        const rucrypto_uint8_t iv[]) {
    RUCRYPTO_CHECK_ARGS(regSize % baseCipher->blockSize == 0);
    *ctx = rucrypto_allocate(sizeof(rucrypto_gost_34_13_cbc_context_t));
    if (*ctx == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    (*ctx)->baseCipher = baseCipher;
    (*ctx)->regSize = regSize;
    (*ctx)->reg = rucrypto_allocate(regSize + baseCipher->blockSize);
    if ((*ctx)->reg == RUCRYPTO_NULL) {
        rucrypto_deallocate(*ctx);
        return rucrypto_result_out_of_memory;
    }
    (*ctx)->tmpBuffer = &(*ctx)->reg[regSize];
    RUCRYPTO_MEMCPY((*ctx)->reg, iv, regSize);
    return rucrypto_result_ok;
}

void rucrypto_gost_34_13_cbc_encrypt_block(rucrypto_gost_34_13_cbc_context_t* ctx, rucrypto_uint8_t block[]) {
    rucrypto_gost_34_13_xor(block, ctx->reg, ctx->baseCipher->blockSize);
    ctx->baseCipher->encryptFunc(block, ctx->baseCipher->key);
    rucrypto_gost_34_13_shift_left(ctx->reg, ctx->regSize, ctx->baseCipher->blockSize);
    RUCRYPTO_MEMCPY(&ctx->reg[ctx->regSize - ctx->baseCipher->blockSize], block, ctx->baseCipher->blockSize);
}

void rucrypto_gost_34_13_cbc_decrypt_block(rucrypto_gost_34_13_cbc_context_t* ctx, rucrypto_uint8_t block[]) {
    RUCRYPTO_MEMCPY(ctx->tmpBuffer, block, ctx->baseCipher->blockSize);

    ctx->baseCipher->decryptFunc(block, ctx->baseCipher->key);
    rucrypto_gost_34_13_xor(block, ctx->reg, ctx->baseCipher->blockSize);
    rucrypto_gost_34_13_shift_left(ctx->reg, ctx->regSize, ctx->baseCipher->blockSize);
    RUCRYPTO_MEMCPY(&ctx->reg[ctx->regSize - ctx->baseCipher->blockSize], ctx->tmpBuffer, ctx->baseCipher->blockSize);
}

void rucrypto_gost_34_13_cbc_context_fini(rucrypto_gost_34_13_cbc_context_t** ctx) {
    rucrypto_secure_zero((*ctx)->reg, (*ctx)->regSize);
    rucrypto_secure_zero((*ctx)->tmpBuffer, (*ctx)->baseCipher->blockSize);
    rucrypto_deallocate((*ctx)->reg);
    rucrypto_deallocate(*ctx);
    *ctx = RUCRYPTO_NULL;
}
