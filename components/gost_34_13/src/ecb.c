#include <rucrypto/gost_34_13/ecb.h>

#include <rucrypto/gost_34_13/base_cipher.h>

void rucrypto_gost_34_13_ecb_encrypt_block(const rucrypto_gost_34_13_base_cipher_t* baseCipher, rucrypto_uint8_t block[]) {
    baseCipher->encryptFunc(block, baseCipher->key);
}

void rucrypto_gost_34_13_ecb_decrypt_block(const rucrypto_gost_34_13_base_cipher_t* baseCipher, rucrypto_uint8_t block[]) {
    baseCipher->decryptFunc(block, baseCipher->key);
}
