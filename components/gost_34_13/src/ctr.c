#include <rucrypto/gost_34_13/ctr.h>

#include <rucrypto/gost_34_13/base_cipher.h>
#include <rucrypto/gost_34_13/ctr_context.h>
#include <rucrypto/gost_34_13/xor_utils.h>
#include <rucrypto/gost_34_13/utils.h>

#include <rucrypto/common/secure_zero.h>

rucrypto_result_t rucrypto_gost_34_13_ctr_context_init(
        rucrypto_gost_34_13_ctr_context_t** ctx,
        const rucrypto_gost_34_13_base_cipher_t* baseCipher,
        rucrypto_size_t gammaSize,
        const rucrypto_uint8_t iv[]) {
    RUCRYPTO_CHECK_ARGS(gammaSize <= baseCipher->blockSize);
    *ctx = rucrypto_allocate(sizeof(rucrypto_gost_34_13_ctr_context_t));
    if (*ctx == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    (*ctx)->baseCipher = baseCipher;
    (*ctx)->gammaSize = gammaSize;
    (*ctx)->ctr = rucrypto_allocate(baseCipher->blockSize * 2);
    if ((*ctx)->ctr == RUCRYPTO_NULL) {
        rucrypto_deallocate(*ctx);
        return rucrypto_result_out_of_memory;
    }
    (*ctx)->tmpBuffer = &(*ctx)->ctr[baseCipher->blockSize];
    RUCRYPTO_MEMCPY((*ctx)->ctr, iv, baseCipher->blockSize / 2);
    RUCRYPTO_MEMSET(&((*ctx)->ctr[baseCipher->blockSize / 2]), 0, baseCipher->blockSize / 2);
    return rucrypto_result_ok;
}

static void rucrypto_gost_34_13_ctr(rucrypto_gost_34_13_ctr_context_t* ctx, rucrypto_uint8_t block[], rucrypto_size_t blockSize) {
    RUCRYPTO_ASSERT(blockSize <= ctx->gammaSize);
    RUCRYPTO_MEMCPY(ctx->tmpBuffer, ctx->ctr, ctx->baseCipher->blockSize);

    ctx->baseCipher->encryptFunc(ctx->tmpBuffer, ctx->baseCipher->key);
    rucrypto_gost_34_13_xor(block, ctx->tmpBuffer, blockSize);
    rucrypto_gost_34_13_add(ctx->ctr, 0x1, ctx->baseCipher->blockSize);
}

void rucrypto_gost_34_13_ctr_encrypt_block(rucrypto_gost_34_13_ctr_context_t* ctx, rucrypto_uint8_t block[], rucrypto_size_t blockSize) {
    rucrypto_gost_34_13_ctr(ctx, block, blockSize);
}

void rucrypto_gost_34_13_ctr_decrypt_block(rucrypto_gost_34_13_ctr_context_t* ctx, rucrypto_uint8_t block[], rucrypto_size_t blockSize) {
    rucrypto_gost_34_13_ctr(ctx, block, blockSize);
}

void rucrypto_gost_34_13_ctr_context_fini(rucrypto_gost_34_13_ctr_context_t** ctx) {
    RUCRYPTO_ASSERT(*ctx);
    rucrypto_secure_zero((*ctx)->ctr, (*ctx)->baseCipher->blockSize);
    rucrypto_secure_zero((*ctx)->tmpBuffer, (*ctx)->baseCipher->blockSize);
    rucrypto_deallocate((*ctx)->ctr);
    rucrypto_deallocate(*ctx);
    *ctx =NULL;
}
