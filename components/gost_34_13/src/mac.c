#include <rucrypto/gost_34_13/mac.h>

#include <rucrypto/gost_34_13/base_cipher.h>
#include <rucrypto/gost_34_13/mac_context.h>
#include <rucrypto/gost_34_13/xor_utils.h>

#include <rucrypto/common/secure_zero.h>

rucrypto_result_t rucrypto_gost_34_13_mac_context_init(rucrypto_gost_34_13_mac_context_t** ctx, const rucrypto_gost_34_13_base_cipher_t* baseCipher) {
    RUCRYPTO_CHECK_ARGS(baseCipher->blockSize == 8 || baseCipher->blockSize == 16);
    *ctx = rucrypto_allocate(sizeof(rucrypto_gost_34_13_mac_context_t));
    if (*ctx == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    (*ctx)->baseCipher = baseCipher;
    (*ctx)->buffer = rucrypto_allocate(2 * baseCipher->blockSize);
    if ((*ctx)->buffer == RUCRYPTO_NULL) {
        rucrypto_deallocate(*ctx);
        return rucrypto_result_out_of_memory;
    }
    (*ctx)->key = &(*ctx)->buffer[baseCipher->blockSize];
    RUCRYPTO_MEMSET((*ctx)->buffer, 0, baseCipher->blockSize);
    return rucrypto_result_ok;
}

void rucrypto_gost_34_13_mac_calculate_part(rucrypto_gost_34_13_mac_context_t* ctx, const rucrypto_uint8_t block[]) {
    rucrypto_gost_34_13_xor(ctx->buffer, block, ctx->baseCipher->blockSize);
    ctx->baseCipher->encryptFunc(ctx->buffer, ctx->baseCipher->key);
}

static void rucrypto_gost_34_13_create_key(rucrypto_gost_34_13_mac_context_t* ctx) {
    rucrypto_uint8_t msb = ctx->key[0] >> 7;
    for (rucrypto_size_t i = 0; i < ctx->baseCipher->blockSize - 1; ++i) {
        ctx->key[i] = (ctx->key[i] << 1) | (ctx->key[i + 1] >> 7);
    }
    ctx->key[ctx->baseCipher->blockSize - 1] = ctx->key[ctx->baseCipher->blockSize - 1] << 1;
    if (msb) {
        if (ctx->baseCipher->blockSize == 16) {
            ctx->key[ctx->baseCipher->blockSize - 1] ^= 0x87;
        } else {
            ctx->key[ctx->baseCipher->blockSize - 1] ^= 0x1b;
        }
    }
}

static void rucrypto_gost_34_13_create_key_1(rucrypto_gost_34_13_mac_context_t* ctx) {
    RUCRYPTO_MEMSET(ctx->key, 0, ctx->baseCipher->blockSize);
    ctx->baseCipher->encryptFunc(ctx->key, ctx->baseCipher->key);

    rucrypto_gost_34_13_create_key(ctx);
}

static void rucrypto_gost_34_13_create_key_2(rucrypto_gost_34_13_mac_context_t* ctx) {
    RUCRYPTO_MEMSET(ctx->key, 0, ctx->baseCipher->blockSize);
    ctx->baseCipher->encryptFunc(ctx->key, ctx->baseCipher->key);

    rucrypto_gost_34_13_create_key(ctx);
    rucrypto_gost_34_13_create_key(ctx);
}

void rucrypto_gost_34_13_mac_calculate_last(rucrypto_gost_34_13_mac_context_t* ctx, rucrypto_uint8_t block[], rucrypto_size_t blockSize) {
    RUCRYPTO_ASSERT(blockSize <= ctx->baseCipher->blockSize);
    if (blockSize < ctx->baseCipher->blockSize) {
        rucrypto_gost_34_13_pad_3(block, blockSize, ctx->baseCipher->blockSize);
        rucrypto_gost_34_13_create_key_2(ctx);
    } else {
        rucrypto_gost_34_13_create_key_1(ctx);
    }

    rucrypto_gost_34_13_xor(ctx->buffer, block, ctx->baseCipher->blockSize);
    rucrypto_gost_34_13_xor(ctx->buffer, ctx->key, ctx->baseCipher->blockSize);
    ctx->baseCipher->encryptFunc(ctx->buffer, ctx->baseCipher->key);
}


void rucrypto_gost_34_13_mac_context_fini(rucrypto_gost_34_13_mac_context_t** ctx, rucrypto_uint8_t macBuffer[], rucrypto_size_t macSize) {
    RUCRYPTO_ASSERT(macSize <= (*ctx)->baseCipher->blockSize);
    RUCRYPTO_MEMCPY(macBuffer, (*ctx)->buffer, macSize);
    rucrypto_secure_zero((*ctx)->buffer, (*ctx)->baseCipher->blockSize);
    rucrypto_secure_zero((*ctx)->key, (*ctx)->baseCipher->blockSize);
    rucrypto_deallocate((*ctx)->buffer);
    rucrypto_deallocate(*ctx);
    *ctx = RUCRYPTO_NULL;
}
