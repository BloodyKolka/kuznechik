#include <rucrypto/gost_34_13/cfb.h>

#include <rucrypto/gost_34_13/base_cipher.h>
#include <rucrypto/gost_34_13/cfb_context.h>
#include <rucrypto/gost_34_13/xor_utils.h>
#include <rucrypto/gost_34_13/utils.h>

#include <rucrypto/common/secure_zero.h>

rucrypto_result_t rucrypto_gost_34_13_cfb_context_init(
        rucrypto_gost_34_13_cfb_context_t** ctx, 
        const rucrypto_gost_34_13_base_cipher_t* baseCipher, 
        rucrypto_size_t gammaSize, 
        rucrypto_size_t regSize, 
        const rucrypto_uint8_t iv[]) {
    RUCRYPTO_CHECK_ARGS(baseCipher->blockSize <= regSize);
    RUCRYPTO_CHECK_ARGS(gammaSize <= baseCipher->blockSize);
    *ctx = rucrypto_allocate(sizeof(rucrypto_gost_34_13_cfb_context_t));
    if (*ctx == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    (*ctx)->baseCipher = baseCipher;
    (*ctx)->gammaSize = gammaSize;
    (*ctx)->regSize = regSize;
    (*ctx)->reg = rucrypto_allocate(regSize + baseCipher->blockSize + gammaSize);
    if ((*ctx)->reg == RUCRYPTO_NULL) {
        rucrypto_deallocate(*ctx);
        return rucrypto_result_out_of_memory;
    }
    (*ctx)->tmpBuffer = &(*ctx)->reg[regSize];
    (*ctx)->cipherBlock = &(*ctx)->reg[regSize + baseCipher->blockSize];
    RUCRYPTO_MEMCPY((*ctx)->reg, iv, regSize);
    return rucrypto_result_ok;
}

void rucrypto_gost_34_13_cfb_encrypt_block(rucrypto_gost_34_13_cfb_context_t* ctx, rucrypto_uint8_t block[], rucrypto_size_t blockSize) {
    RUCRYPTO_ASSERT(blockSize <= ctx->gammaSize);
    RUCRYPTO_MEMCPY(ctx->tmpBuffer, ctx->reg, ctx->baseCipher->blockSize);

    ctx->baseCipher->encryptFunc(ctx->tmpBuffer, ctx->baseCipher->key);
    rucrypto_gost_34_13_xor(block, ctx->tmpBuffer, blockSize);
    rucrypto_gost_34_13_shift_left(ctx->reg, ctx->regSize, blockSize);
    RUCRYPTO_MEMCPY(&ctx->reg[ctx->regSize - blockSize], block, blockSize);
}

void rucrypto_gost_34_13_cfb_decrypt_block(rucrypto_gost_34_13_cfb_context_t* ctx, rucrypto_uint8_t block[], rucrypto_size_t blockSize) {
    RUCRYPTO_ASSERT(blockSize <= ctx->gammaSize);
    RUCRYPTO_MEMCPY(ctx->cipherBlock, block, blockSize);
    RUCRYPTO_MEMCPY(ctx->tmpBuffer, ctx->reg, ctx->baseCipher->blockSize);

    ctx->baseCipher->encryptFunc(ctx->tmpBuffer, ctx->baseCipher->key);
    rucrypto_gost_34_13_xor(block, ctx->tmpBuffer, blockSize);
    rucrypto_gost_34_13_shift_left(ctx->reg, ctx->regSize, blockSize);
    RUCRYPTO_MEMCPY(&ctx->reg[ctx->regSize - blockSize], ctx->cipherBlock, blockSize);
}

void rucrypto_gost_34_13_cfb_context_fini(rucrypto_gost_34_13_cfb_context_t** ctx) {
    rucrypto_secure_zero((*ctx)->reg, (*ctx)->regSize);
    rucrypto_secure_zero((*ctx)->tmpBuffer, (*ctx)->baseCipher->blockSize);
    rucrypto_secure_zero((*ctx)->cipherBlock, (*ctx)->gammaSize);
    rucrypto_deallocate((*ctx)->reg);
    rucrypto_deallocate(*ctx);
    *ctx = RUCRYPTO_NULL;
}
