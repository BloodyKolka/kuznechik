#include <rucrypto/gost_34_13/utils.h>

#include <rucrypto/common/allocator_provider.h>
#include <rucrypto/common/secure_zero.h>

void rucrypto_gost_34_13_add(rucrypto_uint8_t a[], rucrypto_uint8_t b, rucrypto_size_t operandSize) {
    rucrypto_uint8_t carry;
    rucrypto_uint32_t c = (rucrypto_uint32_t)a[operandSize - 1] + (rucrypto_uint32_t)b;
    carry = c >> 8;
    a[operandSize - 1] = (rucrypto_uint8_t)(c & 0xFF);

    for (rucrypto_size_t i = 1; (i < operandSize) && (carry != 0); ++i) {
        c = (rucrypto_uint32_t)a[operandSize - i - 1] + carry;
        carry = c >> 8;
        a[operandSize - i - 1] = (rucrypto_uint8_t)(c & 0xFF);
    }
}

void rucrypto_gost_34_13_shift_left(rucrypto_uint8_t buffer[], rucrypto_size_t bufferSize, rucrypto_size_t shiftCount) {
    shiftCount = shiftCount % bufferSize;
    rucrypto_uint8_t* tmpBuffer = rucrypto_allocate(shiftCount);
    RUCRYPTO_MEMCPY(tmpBuffer, buffer, shiftCount);
    for (rucrypto_size_t i = 0; i < bufferSize - shiftCount; ++i) {
        buffer[i] = buffer[i + shiftCount];
    }
    RUCRYPTO_MEMCPY(&buffer[bufferSize - shiftCount], tmpBuffer, shiftCount);
    rucrypto_secure_zero(tmpBuffer, shiftCount);
    rucrypto_deallocate(tmpBuffer);
}
