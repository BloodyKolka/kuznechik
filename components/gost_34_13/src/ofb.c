#include <rucrypto/gost_34_13/ofb.h>

#include <rucrypto/gost_34_13/base_cipher.h>
#include <rucrypto/gost_34_13/ofb_context.h>
#include <rucrypto/gost_34_13/xor_utils.h>
#include <rucrypto/gost_34_13/utils.h>

#include <rucrypto/common/secure_zero.h>

rucrypto_result_t rucrypto_gost_34_13_ofb_context_init(
        rucrypto_gost_34_13_ofb_context_t** ctx,
        const rucrypto_gost_34_13_base_cipher_t* baseCipher,
        rucrypto_size_t gammaSize,
        rucrypto_size_t regSize,
        const rucrypto_uint8_t iv[]) {
    RUCRYPTO_ASSERT(regSize % baseCipher->blockSize == 0);
    RUCRYPTO_ASSERT(gammaSize <= baseCipher->blockSize);
    *ctx = rucrypto_allocate(sizeof(rucrypto_gost_34_13_ofb_context_t));
    if (*ctx == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    (*ctx)->baseCipher = baseCipher;
    (*ctx)->gammaSize = gammaSize;
    (*ctx)->regSize = regSize;
    (*ctx)->reg = rucrypto_allocate(regSize);
    if ((*ctx)->reg == RUCRYPTO_NULL) {
        rucrypto_deallocate(*ctx);
        return rucrypto_result_out_of_memory;
    }
    RUCRYPTO_MEMCPY((*ctx)->reg, iv, regSize);
    return rucrypto_result_ok;
}

static void rucrypto_gost_34_13_ofb(rucrypto_gost_34_13_ofb_context_t* ctx, rucrypto_uint8_t block[], rucrypto_size_t blockSize) {
    RUCRYPTO_ASSERT(blockSize <= ctx->gammaSize);
    ctx->baseCipher->encryptFunc(ctx->reg, ctx->baseCipher->key);
    rucrypto_gost_34_13_xor(block, ctx->reg, blockSize);
    rucrypto_gost_34_13_shift_left(ctx->reg, ctx->regSize, ctx->baseCipher->blockSize);
}

void rucrypto_gost_34_13_ofb_encrypt_block(rucrypto_gost_34_13_ofb_context_t* ctx, rucrypto_uint8_t block[], rucrypto_size_t blockSize) {
    rucrypto_gost_34_13_ofb(ctx, block, blockSize);
}

void rucrypto_gost_34_13_ofb_decrypt_block(rucrypto_gost_34_13_ofb_context_t* ctx, rucrypto_uint8_t block[], rucrypto_size_t blockSize) {
    rucrypto_gost_34_13_ofb(ctx, block, blockSize);
}

void rucrypto_gost_34_13_ofb_context_fini(rucrypto_gost_34_13_ofb_context_t** ctx) {
    rucrypto_secure_zero((*ctx)->reg, (*ctx)->regSize);
    rucrypto_deallocate((*ctx)->reg);
    rucrypto_deallocate(*ctx);
    *ctx = RUCRYPTO_NULL;
}
