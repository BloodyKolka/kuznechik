/**
 * @file rucrypto/gost_34_13/common.h
 */

#ifndef RUCRYPTO_GOST_34_13_COMMON_H
#define RUCRYPTO_GOST_34_13_COMMON_H

#include <rucrypto/common/common.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * @brief Basic block cipher function
 * @param [in, out] buffer Buffer to encrypt
 * @param [in] key Key used in encryption
 */
typedef void (*rucrypto_gost_34_13_base_cipher_func_t)(rucrypto_uint8_t* buffer, const rucrypto_uint8_t* key);

/**
 * @brief Struct with information about basic block cipher
 */
typedef struct rucrypto_gost_34_13_base_cipher_t rucrypto_gost_34_13_base_cipher_t;

/**
 * @brief Create rucrypto_gost_34_13_base_cipher_t storing information about cipher
 * @param [in, out] baseCipher pointer to rucrypto_gost_34_13_base_cipher_t* where to store information about the cipher
 * @param [in] encryptFunc pointer to base cipher encryption function
 * @param [in] decryptFunc pointer to base cipher decryption function
 * @param [in] blockSize cipher block size in bytes
 * @param [in] key key used (note: key is not copied, pointer must be kept alive)
 * @param [in] keySize size of key used in base cipher
 * @return rucrypto_result_ok on success, error code overwise
 */
extern rucrypto_result_t rucrypto_gost_34_13_base_cipher_init(
        rucrypto_gost_34_13_base_cipher_t** baseCipher,
        rucrypto_gost_34_13_base_cipher_func_t encryptFunc,
        rucrypto_gost_34_13_base_cipher_func_t decryptFunc,
        rucrypto_size_t blockSize,
        const rucrypto_uint8_t* key,
        rucrypto_size_t keySize);

/**
 * @brief Destroy rucrypto_gost_34_13_base_cipher_t storing information about cipher
 * @param [in] baseCipher pointer to rucrypto_gost_34_13_base_cipher_t* used in rucrypto_gost_34_13_base_cipher_init
 */
extern void rucrypto_gost_34_13_base_cipher_fini(rucrypto_gost_34_13_base_cipher_t** baseCipher);

/**
 * @brief Padding method 1 (see GOST 34.13-2018)
 * @param [in, out] buffer buffer of size padSize filled with data in most significant bytes (staring with buffer[0])
 * @param [in] dataSize size of data
 * @param [in] padSize size to pad to
 * @return rucrypto_result_ok on success, error code overwise
 * @details Fills remainder of buffer with zeros. If already padded - does nothing
 */
extern rucrypto_result_t rucrypto_gost_34_13_pad_1(rucrypto_uint8_t buffer[], rucrypto_size_t dataSize, rucrypto_size_t padSize);

/**
 * @brief Padding method 2 (see GOST 34.13-2018)
 * @param [in, out] buffer buffer of size padSize filled with data in most significant bytes (staring with buffer[0]) or empty
 * @param [in] dataSize size of data
 * @param [in] padSize size to pad to
 * @return rucrypto_result_ok on success, error code overwise
 * @details Fills remainder of buffer with 100...0. If last remaining block is already padded - this function needs to be used with empty buffer and dataSize = 0
 */
extern rucrypto_result_t rucrypto_gost_34_13_pad_2(rucrypto_uint8_t buffer[], rucrypto_size_t dataSize, rucrypto_size_t padSize);

/**
 * @brief Padding method 3 (see GOST 34.13-2018)
 * @param [in, out] buffer buffer of size padSize filled with data in most significant bytes (staring with buffer[0])
 * @param [in] dataSize size of data
 * @param [in] padSize size to pad to
 * @return rucrypto_result_ok on success, error code overwise
 * @details Fills remainder of buffer with 100...0. If already padded - does nothing
 */
extern rucrypto_result_t rucrypto_gost_34_13_pad_3(rucrypto_uint8_t buffer[], rucrypto_size_t dataSize, rucrypto_size_t padSize);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_GOST_34_13_COMMON_H
