/**
 * @file rucrypto/gost_34_13/mac.h
 */

#ifndef RUCRYPTO_GOST_34_13_MAC_H
#define RUCRYPTO_GOST_34_13_MAC_H

#include <rucrypto/gost_34_13/common.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/** Struct to hold MAC context */
typedef struct rucrypto_gost_34_13_mac_context_t rucrypto_gost_34_13_mac_context_t;

/**
 * @brief Create context for calculating MAC (Message Authentication Code)
 * @param [in, out] ctx pointer to rucrypto_gost_34_13_mac_context_t* to store context
 * @param [in] baseCipher pointer to rucrypto_gost_34_13_base_cipher_t* created with rucrypto_gost_34_13_base_cipher_init
 * @return rucrypto_result_ok on success, error code otherwise
 * @details Only use ciphers with block size of 64 or 128 bits are supported
 */
extern rucrypto_result_t rucrypto_gost_34_13_mac_context_init(rucrypto_gost_34_13_mac_context_t** ctx, const rucrypto_gost_34_13_base_cipher_t* baseCipher);

/**
 * @brief Process current block of message
 * @param [in] ctx pointer to context, received from rucrypto_gost_34_13_mac_context_init
 * @param [in] block current message block (size must be equal to cipher block size)
 */
extern void rucrypto_gost_34_13_mac_calculate_part(rucrypto_gost_34_13_mac_context_t* ctx, const rucrypto_uint8_t block[]);

/**
 * @brief Process last block of message
 * @param [in] ctx pointer to context, received from rucrypto_gost_34_13_mac_context_init
 * @param [in, out] block last block of message (will be padded if needed)
 * @param [in] blockSize size of last block (must be <= cipher block size)
 */
extern void rucrypto_gost_34_13_mac_calculate_last(rucrypto_gost_34_13_mac_context_t* ctx, rucrypto_uint8_t block[], rucrypto_size_t blockSize);

/**
 * @brief Destroy context for calculating MAC (Message Authentication Code), created with rucrypto_gost_34_13_mac_context_init
 * @param [in] ctx pointer to rucrypto_gost_34_13_mac_context_t* used in rucrypto_gost_34_13_mac_context_init
 * @param [in, out] macBuffer buffer to store MAC
 * @param [in] macSize required MAC size
 */
extern void rucrypto_gost_34_13_mac_context_fini(rucrypto_gost_34_13_mac_context_t** ctx, rucrypto_uint8_t macBuffer[], rucrypto_size_t macSize);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_GOST_34_13_MAC_H
