/**
 * @file rucrypto/gost_34_13/cbc.h
 */

#ifndef RUCRYPTO_GOST_34_13_CBC_H
#define RUCRYPTO_GOST_34_13_CBC_H

#include <rucrypto/gost_34_13/common.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/** Struct to hold CBC context */
typedef struct rucrypto_gost_34_13_cbc_context_t rucrypto_gost_34_13_cbc_context_t;

/**
 * @brief Create context for encryption/decryption using CBC (Cipher Block Chaining) method
 * @param [in, out] ctx pointer to rucrypto_gost_34_13_cbc_context_t* to store context
 * @param [in] baseCipher pointer to rucrypto_gost_34_13_base_cipher_t* created with rucrypto_gost_34_13_base_cipher_init
 * @param [in] regSize size of register R (see GOST 34.13-2018), must be multiple of cipher block size
 * @param [in] iv initializing value of size equal to size of R register
 * @return rucrypto_result_ok on success, error code otherwise
 */
extern rucrypto_result_t rucrypto_gost_34_13_cbc_context_init(
        rucrypto_gost_34_13_cbc_context_t** ctx,
        const rucrypto_gost_34_13_base_cipher_t* baseCipher,
        rucrypto_size_t regSize,
        const rucrypto_uint8_t iv[]);

/**
 * @brief Encrypt block using CBC (Cipher Block Chaining) method
 * @param [in] ctx pointer to context, received from rucrypto_gost_34_13_cbc_context_init
 * @param [in, out] block block to encrypt (size must be equal to cipher block size, using padding for smaller blocks)
 */
extern void rucrypto_gost_34_13_cbc_encrypt_block(rucrypto_gost_34_13_cbc_context_t* ctx, rucrypto_uint8_t block[]);

/**
 * @brief Decrypt block using CBC (Cipher Block Chaining) method
 * @param [in] ctx pointer to context, received from rucrypto_gost_34_13_cbc_context_init
 * @param [in, out] block block to decrypt (size must be equal to cipher block size, note the padding using when encrypting)
 */
extern void rucrypto_gost_34_13_cbc_decrypt_block(rucrypto_gost_34_13_cbc_context_t* ctx, rucrypto_uint8_t block[]);

/**
 * @brief Destroy context for encryption/decryption using CBC (Cipher Block Chaining) method, created with rucrypto_gost_34_13_cbc_context_init
 * @param [in] ctx pointer to rucrypto_gost_34_13_cbc_context_t* used in rucrypto_gost_34_13_cbc_context_init
 */
extern void rucrypto_gost_34_13_cbc_context_fini(rucrypto_gost_34_13_cbc_context_t** ctx);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_GOST_34_13_CBC_H
