/**
 * @file rucrypto/gost_34_13/ecb.h
 */

#ifndef RUCRYPTO_GOST_34_13_ECB_H
#define RUCRYPTO_GOST_34_13_ECB_H

#include <rucrypto/gost_34_13/common.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * @brief Encrypt block with base cipher using ECB (Electronic Codebook) method
 * @param [in] baseCipher struct rucrypto_gost_34_13_base_cipher_t* initialized with rucrypto_gost_34_13_base_cipher_init
 * @param [in, out] block block of data with size matching cipher block size
 */
extern void rucrypto_gost_34_13_ecb_encrypt_block(const rucrypto_gost_34_13_base_cipher_t* baseCipher, rucrypto_uint8_t block[]);

/**
 * @brief Decrypt block with base cipher using ECB (Electronic Codebook) method
 * @param [in] baseCipher struct rucrypto_gost_34_13_base_cipher_t* initialized with rucrypto_gost_34_13_base_cipher_init
 * @param [in, out] block block of data with size matching cipher block size
 */
extern void rucrypto_gost_34_13_ecb_decrypt_block(const rucrypto_gost_34_13_base_cipher_t* baseCipher, rucrypto_uint8_t block[]);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_GOST_34_13_ECB_H
