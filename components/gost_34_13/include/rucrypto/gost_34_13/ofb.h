/**
 * @file rucrypto/gost_34_13/ofb.h
 */

#ifndef RUCRYPTO_GOST_34_13_OFB_H
#define RUCRYPTO_GOST_34_13_OFB_H

#include <rucrypto/gost_34_13/common.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/** Struct to hold OFB context */
typedef struct rucrypto_gost_34_13_ofb_context_t rucrypto_gost_34_13_ofb_context_t;

/**
 * @brief Create context for encryption/decryption using OFB (Output Feedback) method
 * @param [in, out] ctx pointer to rucrypto_gost_34_13_ofb_context_t* to store context
 * @param [in] baseCipher pointer to rucrypto_gost_34_13_base_cipher_t* created with rucrypto_gost_34_13_base_cipher_init
 * @param [in] gammaSize size of gamma (must be less than or equal to cipher block size)
 * @param [in] regSize size of R register (see GOST 34.13-2018), must be multiple of cipher block size
 * @param [in] iv initializing value of size equal to size of R register
 * @return rucrypto_result_ok on success, error code otherwise
 */
extern rucrypto_result_t rucrypto_gost_34_13_ofb_context_init(
        rucrypto_gost_34_13_ofb_context_t** ctx,
        const rucrypto_gost_34_13_base_cipher_t* baseCipher,
        rucrypto_size_t gammaSize,
        rucrypto_size_t regSize,
        const rucrypto_uint8_t iv[]);

/**
 * @brief Encrypt block using OFB (Output Feedback) method
 * @param [in] ctx pointer to context, received from rucrypto_gost_34_13_ctr_context_init
 * @param [in, out] block block to encrypt
 * @param [in] blockSize size of block (must be <= cipher block size)
 */
extern void rucrypto_gost_34_13_ofb_encrypt_block(rucrypto_gost_34_13_ofb_context_t* ctx, rucrypto_uint8_t block[], rucrypto_size_t blockSize);

/**
 * @brief Decrypt block using OFB (Output Feedback) method
 * @param [in] ctx pointer to context, received from rucrypto_gost_34_13_ctr_context_init
 * @param [in, out] block block to decrypt
 * @param [in] blockSize size of block (must be <= cipher block size)
 */
extern void rucrypto_gost_34_13_ofb_decrypt_block(rucrypto_gost_34_13_ofb_context_t* ctx, rucrypto_uint8_t block[], rucrypto_size_t blockSize);

/**
 * @brief Destroy context for encryption/decryption using OFB (Output Feedback) method, created with rucrypto_gost_34_13_ctr_context_init
 * @param [in] ctx pointer to rucrypto_gost_34_13_ofb_context_t* used in rucrypto_gost_34_13_ctr_context_init
 */
extern void rucrypto_gost_34_13_ofb_context_fini(rucrypto_gost_34_13_ofb_context_t** ctx);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_GOST_34_13_OFB_H
