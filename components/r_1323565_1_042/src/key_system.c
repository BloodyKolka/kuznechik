#include <rucrypto/r_1323565_1_042/key_system.h>
#include <rucrypto/r_1323565_1_042/format_func.h>
#include <rucrypto/r_1323565_1_022/key_generator.h>

#include <rucrypto/gost_34_13/mac.h>
#include <rucrypto/gost_34_13/base_cipher.h>
#include <rucrypto/gost_34_13/mac_context.h>

#include <rucrypto/gost_34_12/gost_34_12.h>

#include <rucrypto/common/common.h>

static rucrypto_result_t rucrypto_r_1323565_1_042_gen_key(
        const rucrypto_vec256_t macKey,
        const rucrypto_vec128_t iv,
        rucrypto_vec128_t usage,
        rucrypto_vec256_t key) {
    const rucrypto_gost_34_13_base_cipher_t baseCipher = {
        .encryptFunc = rucrypto_gost_34_12_encrypt_128,
        .decryptFunc = rucrypto_gost_34_12_decrypt_128,
        .blockSize = 128 / 8,
        .key = macKey,
        .keySize = 256 / 8,
    };
    RUCRYPTO_MEMSET(key, 0, 32);
    
    rucrypto_vec128_t tmpBuffer;
    rucrypto_gost_34_13_mac_context_t macCtx = {
        .baseCipher = &baseCipher,
        .buffer = &key[0],
        .key = tmpBuffer,
    };

    rucrypto_gost_34_13_mac_calculate_part(&macCtx, iv); 
    rucrypto_gost_34_13_mac_calculate_last(&macCtx, usage, 16);
    
    macCtx.buffer = &key[16];
    rucrypto_gost_34_13_mac_calculate_part(&macCtx, &key[0]); 
    rucrypto_gost_34_13_mac_calculate_last(&macCtx, usage, 16);

    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_r_1323565_1_042_gen_partition_key(
        rucrypto_uint64_t partitionNum,
        rucrypto_uint64_t partitionKeyGenInfo,
        const rucrypto_r_1323565_1_042_secret_key_t secretKey,
        rucrypto_r_1323565_1_042_partition_key_t partitionKey) {
    rucrypto_vec128_t ivBuffer;
    RUCRYPTO_MEMSET(ivBuffer, 0, 16);
    
    rucrypto_vec128_t usageBuffer;
    usageBuffer[ 0] = (partitionKeyGenInfo >> 56) & 0xFF;
    usageBuffer[ 1] = (partitionKeyGenInfo >> 48) & 0xFF;
    usageBuffer[ 2] = (partitionKeyGenInfo >> 40) & 0xFF;
    usageBuffer[ 3] = (partitionKeyGenInfo >> 32) & 0xFF;
    usageBuffer[ 4] = (partitionKeyGenInfo >> 24) & 0xFF;
    usageBuffer[ 5] = (partitionKeyGenInfo >> 16) & 0xFF;
    usageBuffer[ 6] = (partitionKeyGenInfo >>  8) & 0xFF;
    usageBuffer[ 7] = (partitionKeyGenInfo >>  0) & 0xFF;

    usageBuffer[ 8] = (partitionNum >> 56) & 0xFF;
    usageBuffer[ 9] = (partitionNum >> 48) & 0xFF;
    usageBuffer[10] = (partitionNum >> 40) & 0xFF;
    usageBuffer[11] = (partitionNum >> 32) & 0xFF;
    usageBuffer[12] = (partitionNum >> 24) & 0xFF;
    usageBuffer[13] = (partitionNum >> 16) & 0xFF;
    usageBuffer[14] = (partitionNum >>  8) & 0xFF;
    usageBuffer[15] = (partitionNum >>  0) & 0xFF;

    return rucrypto_r_1323565_1_042_gen_key(secretKey, ivBuffer, usageBuffer, partitionKey);
}

rucrypto_result_t rucrypto_r_1323565_1_042_gen_sector_key(
        rucrypto_uint64_t partitionNum,
        const rucrypto_r_1323565_1_042_partition_key_t partitionKey,
        rucrypto_uint64_t sectorNum,
        rucrypto_uint64_t sectorKeyGenInfo,
        rucrypto_uint64_t keyChangeFrequency,
        rucrypto_r_1323565_1_042_sector_key_t sectorKey) {
    rucrypto_vec128_t ivBuffer;
    ivBuffer[ 0] = (partitionNum >> 56) & 0xFF;
    ivBuffer[ 1] = (partitionNum >> 48) & 0xFF;
    ivBuffer[ 2] = (partitionNum >> 40) & 0xFF;
    ivBuffer[ 3] = (partitionNum >> 32) & 0xFF;
    ivBuffer[ 4] = (partitionNum >> 24) & 0xFF;
    ivBuffer[ 5] = (partitionNum >> 16) & 0xFF;
    ivBuffer[ 6] = (partitionNum >>  8) & 0xFF;
    ivBuffer[ 7] = (partitionNum >>  0) & 0xFF;
    RUCRYPTO_MEMSET(&ivBuffer[8], 0, 8);

    rucrypto_uint64_t usagePart1 = sectorKeyGenInfo / keyChangeFrequency;

    rucrypto_vec128_t usageBuffer;
    usageBuffer[ 0] = (usagePart1 >> 56) & 0xFF;
    usageBuffer[ 1] = (usagePart1 >> 48) & 0xFF;
    usageBuffer[ 2] = (usagePart1 >> 40) & 0xFF;
    usageBuffer[ 3] = (usagePart1 >> 32) & 0xFF;
    usageBuffer[ 4] = (usagePart1 >> 24) & 0xFF;
    usageBuffer[ 5] = (usagePart1 >> 16) & 0xFF;
    usageBuffer[ 6] = (usagePart1 >>  8) & 0xFF;
    usageBuffer[ 7] = (usagePart1 >>  0) & 0xFF;
    usageBuffer[ 8] = (sectorNum >> 56) & 0xFF;
    usageBuffer[ 9] = (sectorNum >> 48) & 0xFF;
    usageBuffer[10] = (sectorNum >> 40) & 0xFF;
    usageBuffer[11] = (sectorNum >> 32) & 0xFF;
    usageBuffer[12] = (sectorNum >> 24) & 0xFF;
    usageBuffer[13] = (sectorNum >> 16) & 0xFF;
    usageBuffer[14] = (sectorNum >>  8) & 0xFF;
    usageBuffer[15] = (sectorNum >>  0) & 0xFF;

    return rucrypto_r_1323565_1_042_gen_key(partitionKey, ivBuffer, usageBuffer, sectorKey);
}

