#include <rucrypto/r_1323565_1_042/disk_functions.h>
#include <rucrypto/r_1323565_1_042/key_system.h>

#include <rucrypto/gost_34_12/gost_34_12.h>
#include <rucrypto/gost_34_12/xor_utils.h>

#include <rucrypto/common/secure_zero.h>

static void rucrypto_r_1323565_1_042_xor_sector(
        rucrypto_uint64_t sectorKeyGenInfo,
        rucrypto_uint64_t sectorNum,
        const rucrypto_r_1323565_1_042_sector_key_t sectorKey,
        rucrypto_r_1323565_1_042_sector_t sector) {
    rucrypto_uint64_t highPart;

    rucrypto_gost_34_12_block_t ctr;
    for (rucrypto_size_t i = 0; i < sector.size / 16; ++i) {
        highPart = sectorKeyGenInfo * sector.size / 16 + i;
        ctr[ 0] = (sectorNum >> 56) & 0xFF;
        ctr[ 1] = (sectorNum >> 48) & 0xFF;
        ctr[ 2] = (sectorNum >> 40) & 0xFF;
        ctr[ 3] = (sectorNum >> 32) & 0xFF;
        ctr[ 4] = (sectorNum >> 24) & 0xFF;
        ctr[ 5] = (sectorNum >> 16) & 0xFF;
        ctr[ 6] = (sectorNum >>  8) & 0xFF;
        ctr[ 7] = (sectorNum >>  0) & 0xFF;
        ctr[ 8] = (highPart >> 56) & 0xFF;
        ctr[ 9] = (highPart >> 48) & 0xFF;
        ctr[10] = (highPart >> 40) & 0xFF;
        ctr[11] = (highPart >> 32) & 0xFF;
        ctr[12] = (highPart >> 24) & 0xFF;
        ctr[13] = (highPart >> 16) & 0xFF;
        ctr[14] = (highPart >>  8) & 0xFF;
        ctr[15] = (highPart >>  0) & 0xFF;

        rucrypto_gost_34_12_encrypt_128(ctr, sectorKey);
        rucrypto_gost_34_12_xor(&sector.buffer[16 * i], ctr);
    }
}

static rucrypto_result_t rucrypto_r_1323565_1_042_encrypted_sector_common(
        const rucrypto_r_1323565_1_042_block_device_info_t* diskInfo,
        rucrypto_uint64_t partitionNum,
        rucrypto_uint64_t sectorNum,
        rucrypto_r_1323565_1_042_sector_t sector) {
    rucrypto_result_t res;

    rucrypto_uint64_t partitionKeyGenInfo;
    res = diskInfo->getPartitionKeyGenInfo(partitionNum, &partitionKeyGenInfo);
    if (res != rucrypto_result_ok) {
        return res;
    }
    rucrypto_uint64_t sectorKeyGenInfo;
    res = diskInfo->getSectorKeyGenInfo(partitionNum, sectorNum, &sectorKeyGenInfo);
    if (res != rucrypto_result_ok) {
        return res;
    }

    rucrypto_r_1323565_1_042_partition_key_t partitionKey;
    rucrypto_r_1323565_1_042_sector_key_t sectorKey;

    res = rucrypto_r_1323565_1_042_gen_partition_key(partitionNum, partitionKeyGenInfo, diskInfo->secretKey, partitionKey);
    if (res != rucrypto_result_ok) {
        return res;
    }
    res = rucrypto_r_1323565_1_042_gen_sector_key(partitionNum, partitionKey, sectorNum, sectorKeyGenInfo, diskInfo->keyChangeFrequency, sectorKey);
    if (res != rucrypto_result_ok) {
        return res;
    }

    rucrypto_r_1323565_1_042_xor_sector(sectorKeyGenInfo, sectorNum, sectorKey, sector);

    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_r_1323565_1_042_read_encrypted_sector(
        const rucrypto_r_1323565_1_042_block_device_info_t* diskInfo,
        rucrypto_uint64_t partitionNum,
        rucrypto_uint64_t sectorNum,
        rucrypto_r_1323565_1_042_sector_t sector) {
    RUCRYPTO_CHECK_ARGS(diskInfo != RUCRYPTO_NULL)
    RUCRYPTO_CHECK_ARGS(partitionNum < diskInfo->partitionCount)
    RUCRYPTO_CHECK_ARGS(sectorNum < diskInfo->partitionSectorCounts[partitionNum])
    RUCRYPTO_CHECK_ARGS(sector.size == diskInfo->sectorSize);

    rucrypto_result_t res = diskInfo->readSector(partitionNum, sectorNum, sector);
    if (res != rucrypto_result_ok) {
        return res;
    }

    return rucrypto_r_1323565_1_042_encrypted_sector_common(diskInfo, partitionNum, sectorNum, sector);
}

rucrypto_result_t rucrypto_r_1323565_1_042_write_encrypted_sector(
        const rucrypto_r_1323565_1_042_block_device_info_t* diskInfo,
        rucrypto_uint64_t partitionNum,
        rucrypto_uint64_t sectorNum,
        rucrypto_r_1323565_1_042_sector_t sector) {
    RUCRYPTO_CHECK_ARGS(diskInfo != RUCRYPTO_NULL)
    RUCRYPTO_CHECK_ARGS(partitionNum < diskInfo->partitionCount)
    RUCRYPTO_CHECK_ARGS(sectorNum < diskInfo->partitionSectorCounts[partitionNum])
    RUCRYPTO_CHECK_ARGS(sector.size == diskInfo->sectorSize);

    rucrypto_result_t res;

    rucrypto_uint64_t sectorKeyGenInfo;
    res = diskInfo->getSectorKeyGenInfo(partitionNum, sectorNum, &sectorKeyGenInfo);
    if (res != rucrypto_result_ok) {
        return res;
    }

    if (sectorKeyGenInfo + 1 == 0) {
        res = rucrypto_r_1323565_1_042_re_encrypt_partition(diskInfo, partitionNum);
        if (res != rucrypto_result_ok) {
            return res;
        }
    }

    sectorKeyGenInfo += 1;
    res = diskInfo->setSectorKeyGenInfo(partitionNum, sectorNum, sectorKeyGenInfo);
    if (res != rucrypto_result_ok) {
        return res;
    }

    res = rucrypto_r_1323565_1_042_encrypted_sector_common(diskInfo, partitionNum, sectorNum, sector);
    if (res != rucrypto_result_ok) {
        return res;
    }

    return diskInfo->writeSector(partitionNum, sectorNum, sector);
}

rucrypto_result_t rucrypto_r_1323565_1_042_re_encrypt_partition(
        const rucrypto_r_1323565_1_042_block_device_info_t* diskInfo, 
        rucrypto_uint64_t partitionNum) {
    RUCRYPTO_CHECK_ARGS(diskInfo != RUCRYPTO_NULL)
    RUCRYPTO_CHECK_ARGS(partitionNum < diskInfo->partitionCount)

    rucrypto_result_t res;

    rucrypto_uint64_t partitionKeyGenInfo;
    res = diskInfo->getPartitionKeyGenInfo(partitionNum, &partitionKeyGenInfo);
    if (res != rucrypto_result_ok) {
        return res;
    }

    if (partitionKeyGenInfo + 1 == 0) {
        return rucrypto_result_cannot_continue;
    }

    rucrypto_r_1323565_1_042_partition_key_t partitionKey;
    res = rucrypto_r_1323565_1_042_gen_partition_key(partitionNum, partitionKeyGenInfo, diskInfo->secretKey, partitionKey);
    if (res != rucrypto_result_ok) {
        return res;
    }

    partitionKeyGenInfo += 1;
    rucrypto_r_1323565_1_042_partition_key_t newPartitionKey;
    res = rucrypto_r_1323565_1_042_gen_partition_key(partitionNum, partitionKeyGenInfo, diskInfo->secretKey, newPartitionKey);
    if (res != rucrypto_result_ok) {
        return res;
    }

    rucrypto_uint8_t* sectorBuffer = rucrypto_allocate(diskInfo->sectorSize);
    if (sectorBuffer == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    rucrypto_r_1323565_1_042_sector_t sector = {
        .buffer = sectorBuffer,
        .size = diskInfo->sectorSize,
    };

    rucrypto_r_1323565_1_042_sector_key_t sectorKey;
    for (size_t i = 0; i < diskInfo->partitionSectorCounts[partitionNum]; ++i) {
        // 1. Generate sector key
        rucrypto_uint64_t sectorKeyGenInfo;
        res = diskInfo->getSectorKeyGenInfo(partitionNum, i, &sectorKeyGenInfo);
        if (res != rucrypto_result_ok) {
            return res;
        }
        res = rucrypto_r_1323565_1_042_gen_sector_key(partitionNum, partitionKey, i, sectorKeyGenInfo, diskInfo->keyChangeFrequency, sectorKey);
        if (res != rucrypto_result_ok) {
            return res;
        }

        // 2. Read sector from disk and decrypt it
        diskInfo->readSector(partitionNum, i, sector);
        rucrypto_r_1323565_1_042_xor_sector(sectorKeyGenInfo, i, sectorKey, sector);

        // 3. Update sector key generation info to 0 and generate new sector key with new partition key generation info
        sectorKeyGenInfo = 0;
        res = diskInfo->setSectorKeyGenInfo(partitionNum, i, sectorKeyGenInfo);
        if (res != rucrypto_result_ok) {
            return res;
        }
        res = rucrypto_r_1323565_1_042_gen_sector_key(partitionNum, newPartitionKey, i, sectorKeyGenInfo, diskInfo->keyChangeFrequency, sectorKey);
        if (res != rucrypto_result_ok) {
            return res;
        }

        // 4. Encrypt sector and write it on disk
        rucrypto_r_1323565_1_042_xor_sector(sectorKeyGenInfo, i, sectorKey, sector);
        diskInfo->writeSector(partitionNum, i, sector);
    }

    rucrypto_secure_zero(sectorBuffer, diskInfo->sectorSize);
    rucrypto_deallocate(sectorBuffer);

    return diskInfo->setPartitionKeyGenInfo(partitionNum, partitionKeyGenInfo);
}
