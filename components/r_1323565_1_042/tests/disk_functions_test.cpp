#include <rucrypto/r_1323565_1_042/r_1323565_1_042.h>

#include <gtest/gtest.h>

#include <random>
#include <cstring>

#include "testing_utils.h"

constexpr size_t SectorSize = 16 * 4;

constexpr size_t PartitionCount = 2;
constexpr size_t SectorCount = 10;

constexpr size_t PartitionSize = SectorCount * SectorSize;

constexpr size_t DiskSize = PartitionCount * PartitionSize;

class DiskEmulator {
public:
    DiskEmulator()
        : m_diskData(DiskSize, 0) {
        std::mt19937_64 generator; // NOLINT: Predictability is actually good in tests
        for (uint8_t& val : m_diskData) {
            val = static_cast<uint8_t>(generator());
        }
        m_originalDiskData = m_diskData;
        for (uint64_t& val : m_partitionKeyGenInfos) {
            val = 0;
        }
        for (uint64_t& val : m_sectorKeyGenInfos) {
            val = 0;
        }
    }

    std::vector<uint8_t> ReadOriginalSector(size_t partitionNum, size_t sectorNum) {
        if (partitionNum > PartitionCount || sectorNum > SectorCount) {
            throw std::invalid_argument("Partition or sector number is incorrect!");
        }

        auto itBegin = m_originalDiskData.begin() + static_cast<std::iter_difference_t<std::vector<uint8_t>>>(partitionNum * PartitionSize + sectorNum * SectorSize);
        auto itEnd = itBegin + static_cast<std::iter_difference_t<std::vector<uint8_t>>>(SectorSize);
        return {itBegin, itEnd};
    }

    std::vector<uint8_t> ReadSector(size_t partitionNum, size_t sectorNum) {
        if (partitionNum > PartitionCount || sectorNum > SectorCount) {
            throw std::invalid_argument("Partition or sector number is incorrect!");
        }

        auto itBegin = m_diskData.begin() + static_cast<std::iter_difference_t<std::vector<uint8_t>>>(partitionNum * PartitionSize + sectorNum * SectorSize);
        auto itEnd = itBegin + static_cast<std::iter_difference_t<std::vector<uint8_t>>>(SectorSize);
        return {itBegin, itEnd};
    }

    void WriteSector(size_t partitionNum, size_t sectorNum, std::vector<uint8_t> sector) {
        if (partitionNum > PartitionCount || sectorNum > SectorCount) {
            throw std::invalid_argument("Partition or sector number is incorrect!");
        }

        auto itBegin = m_diskData.begin() + static_cast<std::iter_difference_t<std::vector<uint8_t>>>(partitionNum * PartitionSize + sectorNum * SectorSize);

        if (sector.size() != SectorSize) {
            sector.resize(SectorSize);
        }

        std::copy(sector.begin(), sector.end(), itBegin);
    }

    size_t GetPartitionKeyGenInfo(size_t partitionNum) {
        return m_partitionKeyGenInfos[partitionNum];
    }

    void SetPartitionKeyGenInfo(size_t partitionNum, uint64_t val) {
        m_partitionKeyGenInfos[partitionNum] = val;
    }

    size_t GetSectorKeyGenInfo(size_t partitionNum, size_t sectorNum) {
        return m_sectorKeyGenInfos[partitionNum * SectorCount + sectorNum];
    }

    void SetSectorKeyGenInfo(size_t partitionNum, size_t sectorNum, uint64_t val) {
        m_sectorKeyGenInfos[partitionNum * SectorCount + sectorNum] = val;
    }

private:
    std::vector<uint8_t> m_originalDiskData;
    std::vector<uint8_t> m_diskData;

    std::array<uint64_t, PartitionCount> m_partitionKeyGenInfos{};
    std::array<uint64_t, PartitionCount * SectorCount> m_sectorKeyGenInfos{};
};

DiskEmulator& GetDiskEmulator() {
    static DiskEmulator instance;
    return instance;
}

auto read_func(rucrypto_uint64_t partitionNum, rucrypto_uint64_t sectorNum, rucrypto_r_1323565_1_042_sector_t sector) -> rucrypto_result_t {
    auto tmp = GetDiskEmulator().ReadSector(partitionNum, sectorNum);
    if (sector.size != tmp.size()) {
        throw std::invalid_argument("Wrong sector size!");
    }
    std::memcpy(sector.buffer, tmp.data(), tmp.size());
    return rucrypto_result_ok;
}

auto write_func(rucrypto_uint64_t partitionNum, rucrypto_uint64_t sectorNum, rucrypto_r_1323565_1_042_sector_t sector) -> rucrypto_result_t {
    std::vector<uint8_t> tmp;
    tmp.resize(SectorSize);
    std::memcpy(tmp.data(), sector.buffer, tmp.size());
    GetDiskEmulator().WriteSector(partitionNum, sectorNum, tmp);
    return rucrypto_result_ok;
}

auto get_par_keygen_info(rucrypto_uint64_t partitionNum, rucrypto_uint64_t* partitionKeyGenInfo) -> rucrypto_result_t {
    *partitionKeyGenInfo = GetDiskEmulator().GetPartitionKeyGenInfo(partitionNum);
    return rucrypto_result_ok;
}

auto get_sec_keygen_info(rucrypto_uint64_t partitionNum, rucrypto_uint64_t sectorNum, rucrypto_uint64_t* sectorKeyGenInfo) -> rucrypto_result_t {
    *sectorKeyGenInfo = GetDiskEmulator().GetSectorKeyGenInfo(partitionNum, sectorNum);
    return rucrypto_result_ok;
}

auto set_par_keygen_info(rucrypto_uint64_t partitionNum, rucrypto_uint64_t partitionKeyGenInfo) -> rucrypto_result_t {
    GetDiskEmulator().SetPartitionKeyGenInfo(partitionNum, partitionKeyGenInfo);
    return rucrypto_result_ok;
}

auto set_sec_keygen_info(rucrypto_uint64_t partitionNum, rucrypto_uint64_t sectorNum, rucrypto_uint64_t sectorKeyGenInfo) -> rucrypto_result_t {
    GetDiskEmulator().SetSectorKeyGenInfo(partitionNum, sectorNum, sectorKeyGenInfo);
    return rucrypto_result_ok;
}

TEST(DiskOperationTest, UsageSimulation) {
    std::array<uint8_t, sizeof(rucrypto_r_1323565_1_042_secret_key_t)> secretKey = {
        0xa0, 0x0f, 0xde, 0x1d, 0x4a, 0x83, 0x00, 0xb6, 0xa0, 0x0f, 0xde, 0x1d, 0x4a, 0x83, 0x00, 0xb6,
        0xa0, 0x0f, 0xde, 0x1d, 0x4a, 0x83, 0x00, 0xb6, 0xa0, 0x0f, 0xde, 0x1d, 0x4a, 0x83, 0x00, 0xb6,
    };

    std::array<uint64_t, PartitionCount> partitionInfos{};
    partitionInfos[0] = SectorCount;
    partitionInfos[1] = SectorCount;

    rucrypto_r_1323565_1_042_block_device_info_t blockDeviceInfo = {
        .partitionCount = 2,
        .partitionSectorCounts = partitionInfos.data(),

        .sectorSize = SectorSize,
        .readSector = read_func,
        .writeSector = write_func,

        .getPartitionKeyGenInfo = get_par_keygen_info,
        .getSectorKeyGenInfo = get_sec_keygen_info,
        .setPartitionKeyGenInfo = set_par_keygen_info,
        .setSectorKeyGenInfo = set_sec_keygen_info,

        .keyChangeFrequency = 10,
    };
    std::memcpy(blockDeviceInfo.secretKey, secretKey.data(), secretKey.size());

    for (size_t partitionNum = 0; partitionNum < PartitionCount; ++partitionNum) {
        for (size_t sectorNum = 0; sectorNum < SectorCount; ++sectorNum) {
            auto tmpBuffer = GetDiskEmulator().ReadSector(partitionNum, sectorNum);
            rucrypto_r_1323565_1_042_sector_t tmpSector = {
                .buffer = tmpBuffer.data(),
                .size = tmpBuffer.size(),
            };
            rucrypto_r_1323565_1_042_write_encrypted_sector(&blockDeviceInfo, partitionNum, sectorNum, tmpSector);
        }
    }

    for (size_t partitionNum = 0; partitionNum < PartitionCount; ++partitionNum) {
        for (size_t sectorNum = 0; sectorNum < SectorCount; ++sectorNum) {
            auto originalBuffer = GetDiskEmulator().ReadOriginalSector(partitionNum, sectorNum);

            std::array<uint8_t, SectorSize> tmpBuffer{};
            rucrypto_r_1323565_1_042_sector_t tmpSector = {
                    .buffer = tmpBuffer.data(),
                    .size = tmpBuffer.size(),
            };
            rucrypto_r_1323565_1_042_read_encrypted_sector(&blockDeviceInfo, partitionNum, sectorNum, tmpSector);
            GTEST_LOG_(INFO) << \
                "\nOriginal:  " << \
                            PrintBuffer(originalBuffer.data(), originalBuffer.size()) << \
                "\nDecrypted: " << \
                            PrintBuffer(tmpBuffer.data(), tmpBuffer.size());
            EXPECT_TRUE(AreBuffersEqual(originalBuffer.data(), tmpBuffer.data(), originalBuffer.size()));
        }
    }

    rucrypto_r_1323565_1_042_re_encrypt_partition(&blockDeviceInfo, 0);
    rucrypto_r_1323565_1_042_re_encrypt_partition(&blockDeviceInfo, 1);

    for (size_t partitionNum = 0; partitionNum < PartitionCount; ++partitionNum) {
        for (size_t sectorNum = 0; sectorNum < SectorCount; ++sectorNum) {
            auto originalBuffer = GetDiskEmulator().ReadOriginalSector(partitionNum, sectorNum);

            std::array<uint8_t, SectorSize> tmpBuffer{};
            rucrypto_r_1323565_1_042_sector_t tmpSector = {
                    .buffer = tmpBuffer.data(),
                    .size = tmpBuffer.size(),
            };
            rucrypto_r_1323565_1_042_read_encrypted_sector(&blockDeviceInfo, partitionNum, sectorNum, tmpSector);
            GTEST_LOG_(INFO) << \
                "\nOriginal:  " << \
                            PrintBuffer(originalBuffer.data(), originalBuffer.size()) << \
                "\nDecrypted: " << \
                            PrintBuffer(tmpBuffer.data(), tmpBuffer.size());
            EXPECT_TRUE(AreBuffersEqual(originalBuffer.data(), tmpBuffer.data(), originalBuffer.size()));
        }
    }
}
