/**
 * @file rucrypto/r_1323565_1_042/disk_functions.h
 */

#ifndef RUCRYPTO_R_1323565_1_042_SECTOR_FUNCTIONS_H
#define RUCRYPTO_R_1323565_1_042_SECTOR_FUNCTIONS_H

#include <rucrypto/r_1323565_1_042/key_system.h>

#include <rucrypto/common/common.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * @brief Function type to get partition key generation info
 * @param [in] partitionNum Partition number
 * @param [out] partitionKeyGenInfo Partition key generation info
 * @return rucrypto_result_ok on success, error code otherwise
 */
typedef rucrypto_result_t (*rucrypto_r_1323565_1_042_get_partition_key_gen_info_func_t)(rucrypto_uint64_t partitionNum, rucrypto_uint64_t* partitionKeyGenInfo);
/**
 * @brief Function type to get sector key generation info
 * @param [in] partitionNum Partition number
 * @param [in] sectorNum Sector number
 * @param [out] sectorKeyGenInfo Sector key generation info
 * @return rucrypto_result_ok on success, error code otherwise
 */
typedef rucrypto_result_t (*rucrypto_r_1323565_1_042_get_sector_key_gen_info_func_t)(rucrypto_uint64_t partitionNum, rucrypto_uint64_t sectorNum, rucrypto_uint64_t* sectorKeyGenInfo);

/**
 * @brief Function type to set partition key generation info
 * @param [in] partitionNum Partition number
 * @param [in] partitionKeyGenInfo New partition key generation info
 * @return rucrypto_result_ok on success, error code otherwise
 */
typedef rucrypto_result_t (*rucrypto_r_1323565_1_042_set_partition_key_gen_info_func_t)(rucrypto_uint64_t partitionNum, rucrypto_uint64_t partitionKeyGenInfo);
/**
 * @brief Function type to set sector key generation info
 * @param [in] partitionNum Partition number
 * @param [in] sectorNum Sector number
 * @param [in] sectorKeyGenInfo New sector key generation info
 * @return rucrypto_result_ok on success, error code otherwise
 */
typedef rucrypto_result_t (*rucrypto_r_1323565_1_042_set_sector_key_gen_info_func_t)(rucrypto_uint64_t partitionNum, rucrypto_uint64_t sectorNum, rucrypto_uint64_t sectorKeyGenInfo);

/** Sector buffer type */
typedef rucrypto_variable_buffer_t rucrypto_r_1323565_1_042_sector_t;

/**
 * @brief Function type to read disk sector
 * @param [in] partitionNum Partition number
 * @param [in] sectorNum Sector number
 * @param [out] sector Buffer for read sector
 * @return rucrypto_result_ok on success, error code otherwise
 */
typedef rucrypto_result_t (*rucrypto_r_1323565_1_042_read_sector)(rucrypto_uint64_t partitionNum, rucrypto_uint64_t sectorNum, rucrypto_r_1323565_1_042_sector_t sector);
/**
 * @brief Function type to write disk sector
 * @param [in] partitionNum Partition number
 * @param [in] sectorNum Sector number
 * @param [in] sector Buffer to write
 * @return rucrypto_result_ok on success, error code otherwise
 */
typedef rucrypto_result_t (*rucrypto_r_1323565_1_042_write_sector)(rucrypto_uint64_t partitionNum, rucrypto_uint64_t sectorNum, rucrypto_r_1323565_1_042_sector_t sector);

/**
 * @brief Structure with information about block device
 */
typedef struct {
    /** Number of partitions on block device */
    rucrypto_uint64_t partitionCount;
    /** Number of sectors for each partition */
    rucrypto_uint64_t* partitionSectorCounts;

    /** Size of sector */
    rucrypto_uint64_t sectorSize;
    /** Function to read from sector */
    rucrypto_r_1323565_1_042_read_sector readSector;
    /** Function to write to sector */
    rucrypto_r_1323565_1_042_write_sector writeSector;

    /** Function to get partition key info by partition number */
    rucrypto_r_1323565_1_042_get_partition_key_gen_info_func_t getPartitionKeyGenInfo;
    /** Function to get sector key info by partition number and sector number */
    rucrypto_r_1323565_1_042_get_sector_key_gen_info_func_t getSectorKeyGenInfo;
    /** Function to set partition key info */
    rucrypto_r_1323565_1_042_set_partition_key_gen_info_func_t setPartitionKeyGenInfo;
    /** Function to set sector key info */
    rucrypto_r_1323565_1_042_set_sector_key_gen_info_func_t setSectorKeyGenInfo;

    rucrypto_r_1323565_1_042_secret_key_t secretKey; ///< Secret key for the whole device
    uint64_t keyChangeFrequency; ///< Key change frequency parameter of DEC algorithm
} rucrypto_r_1323565_1_042_block_device_info_t;

/**
 * @brief Read and decrypt encrypted sector
 * @param [in] diskInfo Structure with disk information
 * @param [in] partitionNum Number of partition to read from
 * @param [in] sectorNum Number of sector to read from
 * @param [out] sector Buffer to store read sector
 * @return rucrypto_result_ok on success, error code otherwise
 */
extern rucrypto_result_t rucrypto_r_1323565_1_042_read_encrypted_sector(
        const rucrypto_r_1323565_1_042_block_device_info_t* diskInfo, 
        rucrypto_uint64_t partitionNum, 
        rucrypto_uint64_t sectorNum, 
        rucrypto_r_1323565_1_042_sector_t sector);

/**
 * @brief Encrypt and write to encrypted sector
 * @param [in] diskInfo Structure with disk information
 * @param [in] partitionNum Number of partition to read from
 * @param [in] sectorNum Number of sector to read from
 * @param [in] sector Buffer with unencrypted info to write
 * @return rucrypto_result_ok on success, error code otherwise
 */
extern rucrypto_result_t rucrypto_r_1323565_1_042_write_encrypted_sector(
        const rucrypto_r_1323565_1_042_block_device_info_t* diskInfo,
        rucrypto_uint64_t partitionNum,
        rucrypto_uint64_t sectorNum,
        rucrypto_r_1323565_1_042_sector_t sector);

/**
 * @brief Re-encrypt disk partition 
 * @param [in] diskInfo Structure with disk information
 * @param [in] partitionNum Number of partition to re-encrypt 
 * @return rucrypto_result_ok on success, error code otherwise
 */
extern rucrypto_result_t rucrypto_r_1323565_1_042_re_encrypt_partition(
        const rucrypto_r_1323565_1_042_block_device_info_t* diskInfo,
        rucrypto_uint64_t partitionNum);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_R_1323565_1_042_SECTOR_FUNCTIONS_H
