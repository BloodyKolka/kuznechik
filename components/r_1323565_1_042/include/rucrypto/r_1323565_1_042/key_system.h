#ifndef RUCRYPTO_R_1323565_1_042_KEY_SYSTEM_H
#define RUCRYPTO_R_1323565_1_042_KEY_SYSTEM_H

#include <rucrypto/common/common.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef rucrypto_vec256_t rucrypto_r_1323565_1_042_secret_key_t;
typedef rucrypto_vec256_t rucrypto_r_1323565_1_042_partition_key_t;
typedef rucrypto_vec256_t rucrypto_r_1323565_1_042_sector_key_t;

/**
 * @brief Generate partition key 
 * @param [in] partitionNum Partition number
 * @param [in] partitionKeyGenInfo Partition key generation info
 * @param [in] secretKey Secret key for disk
 * @param [out] partitionKey Resulting partition key
 */
extern rucrypto_result_t rucrypto_r_1323565_1_042_gen_partition_key(
        rucrypto_uint64_t partitionNum,
        rucrypto_uint64_t partitionKeyGenInfo,
        const rucrypto_r_1323565_1_042_secret_key_t secretKey,
        rucrypto_r_1323565_1_042_partition_key_t partitionKey);
/**
 * @brief Generate sector key 
 * @param [in] partitionNum Partition number
 * @param [in] partitionKey Partition key
 * @param [in] sectorNum Sector number
 * @param [in] sectorKeyGenInfo Sector key generation info
 * @param [in] keyChangeFrequency Key change frequency (defined by R 1323565.1.042)
 * @param [out] secretKey Resulting sector key
 */
extern rucrypto_result_t rucrypto_r_1323565_1_042_gen_sector_key(
        rucrypto_uint64_t partitionNum,
        const rucrypto_r_1323565_1_042_partition_key_t partitionKey,
        rucrypto_uint64_t sectorNum,
        rucrypto_uint64_t sectorKeyGenInfo,
        rucrypto_uint64_t keyChangeFrequency,
        rucrypto_r_1323565_1_042_sector_key_t sectorKey);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_R_1323565_1_042_KEY_SYSTEM_H
