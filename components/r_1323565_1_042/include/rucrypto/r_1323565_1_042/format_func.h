/**
 * @file rucrypto/r_1323565_1_042/format_func.h
 */

#ifndef RUCRYPTO_R_1323565_1_042_FORMAT_FUNC_H
#define RUCRYPTO_R_1323565_1_042_FORMAT_FUNC_H

#include <rucrypto/r_1323565_1_022/r_1323565_1_022.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * @brief Format function from R 1323565.1.042
 * @param [in] flags unused
 * @param [in] counter unused
 * @param [in] zpart Previous z part
 * @param [in] length unused
 * @param [in] usage Usage parameter P (see R 1323565.1.042)
 * @param [in] users unused
 * @param [in] additional unused
 * @param [in, out] format Format output buffer
 * @param [in, out] formatSize Size of format output buffer (must be 32 bytes for z || p, or 0 to request it)
 * @details Is defined as format(z, C, P, U, A, L) = z || P (|| - is concatination)
 */
extern void rucrypto_r_1323565_1_042_format(
        rucrypto_r_1323565_1_022_format_flags_t flags,
        const rucrypto_r_1323565_1_022_counter_t* counter,
        const rucrypto_r_1323565_1_022_zpart_t* zpart,
        const rucrypto_r_1323565_1_022_length_t* length,
        const rucrypto_r_1323565_1_022_usage_t* usage,
        const rucrypto_r_1323565_1_022_users_t* users,
        const rucrypto_r_1323565_1_022_additional_t* additional,
        rucrypto_uint8_t format[],
        rucrypto_size_t* formatSize); 

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_R_1323565_1_042_FORMAT_FUNC_H
