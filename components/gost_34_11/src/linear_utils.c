#include <rucrypto/gost_34_11/linear_utils.h>

rucrypto_uint64_t rucrypto_gost_34_11_linear_part_l_little_endian(rucrypto_uint64_t vec64) {
    static const rucrypto_uint64_t matrixLines[64] = {
            0x8e20faa72ba0b470, 0x47107ddd9b505a38, 0xad08b0e0c3282d1c, 0xd8045870ef14980e,
            0x6c022c38f90a4c07, 0x3601161cf205268d, 0x1b8e0b0e798c13c8, 0x83478b07b2468764,
            0xa011d380818e8f40, 0x5086e740ce47c920, 0x2843fd2067adea10, 0x14aff010bdd87508,
            0x0ad97808d06cb404, 0x05e23c0468365a02, 0x8c711e02341b2d01, 0x46b60f011a83988e,
            0x90dab52a387ae76f, 0x486dd4151c3dfdb9, 0x24b86a840e90f0d2, 0x125c354207487869,
            0x092e94218d243cba, 0x8a174a9ec8121e5d, 0x4585254f64090fa0, 0xaccc9ca9328a8950,
            0x9d4df05d5f661451, 0xc0a878a0a1330aa6, 0x60543c50de970553, 0x302a1e286fc58ca7,
            0x18150f14b9ec46dd, 0x0c84890ad27623e0, 0x0642ca05693b9f70, 0x0321658cba93c138,
            0x86275df09ce8aaa8, 0x439da0784e745554, 0xafc0503c273aa42a, 0xd960281e9d1d5215,
            0xe230140fc0802984, 0x71180a8960409a42, 0xb60c05ca30204d21, 0x5b068c651810a89e,
            0x456c34887a3805b9, 0xac361a443d1c8cd2, 0x561b0d22900e4669, 0x2b838811480723ba,
            0x9bcf4486248d9f5d, 0xc3e9224312c8c1a0, 0xeffa11af0964ee50, 0xf97d86d98a327728,
            0xe4fa2054a80b329c, 0x727d102a548b194e, 0x39b008152acb8227, 0x9258048415eb419d,
            0x492c024284fbaec0, 0xaa16012142f35760, 0x550b8e9e21f7a530, 0xa48b474f9ef5dc18,
            0x70a6a56e2440598e, 0x3853dc371220a247, 0x1ca76e95091051ad, 0x0edd37c48a08a6d8,
            0x07e095624504536c, 0x8d70c431ac02a736, 0xc83862965601dd1b, 0x641c314b2b8ee083
    };
    rucrypto_uint64_t res = 0;
    for (int i = 63; i >= 0; --i) {
        if (vec64 & 0x1) {
            res ^= matrixLines[i];
        }
        vec64 >>= 1;
    }
    return res;
}

rucrypto_uint64_t rucrypto_gost_34_11_linear_part_l(rucrypto_uint64_t vec64) {
    static const rucrypto_uint64_t matrixLines[64] = {
            0x70b4a02ba7fa208e, 0x385a509bdd7d1047, 0x1c2d28c3e0b008ad, 0x0e9814ef705804d8,
            0x074c0af9382c026c, 0x8d2605f21c160136, 0xc8138c790e0b8e1b, 0x648746b2078b4783,
            0x408f8e8180d311a0, 0x20c947ce40e78650, 0x10eaad6720fd4328, 0x0875d8bd10f0af14,
            0x04b46cd00878d90a, 0x025a3668043ce205, 0x012d1b34021e718c, 0x8e98831a010fb646,
            0x6fe77a382ab5da90, 0xb9fd3d1c15d46d48, 0xd2f0900e846ab824, 0x6978480742355c12,
            0xba3c248d21942e09, 0x5d1e12c89e4a178a, 0xa00f09644f258545, 0x50898a32a99cccac,
            0x5114665f5df04d9d, 0xa60a33a1a078a8c0, 0x530597de503c5460, 0xa78cc56f281e2a30,
            0xdd46ecb9140f1518, 0xe02376d20a89840c, 0x709f3b6905ca4206, 0x38c193ba8c652103,
            0xa8aae89cf05d2786, 0x5455744e78a09d43, 0x2aa43a273c50c0af, 0x15521d9d1e2860d9,
            0x842980c00f1430e2, 0x429a4060890a1871, 0x214d2030ca050cb6, 0x9ea81018658c065b,
            0xb905387a88346c45, 0xd28c1c3d441a36ac, 0x69460e90220d1b56, 0xba2307481188832b,
            0x5d9f8d248644cf9b, 0xa0c1c8124322e9c3, 0x50ee6409af11faef, 0x2877328ad9867df9,
            0x9c320ba85420fae4, 0x4e198b542a107d72, 0x2782cb2a1508b039, 0x9d41eb1584045892,
            0xc0aefb8442022c49, 0x6057f342210116aa, 0x30a5f7219e8e0b55, 0x18dcf59e4f478ba4,
            0x8e5940246ea5a670, 0x47a2201237dc5338, 0xad511009956ea71c, 0xd8a6088ac437dd0e,
            0x6c5304456295e007, 0x36a702ac31c4708d, 0x1bdd0156966238c8, 0x83e08e2b4b311c64,
    };
    rucrypto_uint64_t res = 0;
    for (rucrypto_size_t i = 0; i < 8; ++i) {
        for (rucrypto_size_t j = 0; j < 8; ++j) {
            if (vec64 & 0x1) {
                res ^= matrixLines[i * 8 + (7 - j)];
            }
            vec64 >>= 1;
        }
    }
    return res;
}

void rucrypto_gost_34_11_linear_transform(rucrypto_vec512_t vec512) {
    rucrypto_uint64_t* vec8 = (rucrypto_uint64_t*)&vec512[0];

    for (rucrypto_size_t i = 0; i < 8; ++i) {
#ifdef __ORDER_LITTLE_ENDIAN__
        vec8[i] = rucrypto_gost_34_11_linear_part_l_little_endian(vec8[i]);
#else
        vec8[i] = rucrypto_gost_34_11_linear_part_l(vec8[i]);
#endif
    }
}
