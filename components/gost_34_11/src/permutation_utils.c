#include <rucrypto/gost_34_11/permutation_utils.h>

void rucrypto_gost_34_11_permutation(rucrypto_gost_34_11_hash_512_t vec512) {
    /* t = {
     *         0 , 8, 16, 24, 32, 40, 48, 56,
     *         1 , 9, 17, 25, 33, 41, 49, 57,
     *         2, 10, 18, 26, 34, 42, 50, 58,
     *         3, 11, 19, 27, 35, 43, 51, 59,
     *         4, 12, 20, 28, 36, 44, 52, 60,
     *         5, 13, 21, 29, 37, 45, 53, 61,
     *         6, 14, 22, 30, 38, 46, 54, 62,
     *         7, 15, 23, 31, 39, 47, 55, 63
     * };
     */
    for (rucrypto_size_t i = 0; i < 8; ++i) {
        for (rucrypto_size_t j = i; j < 8; ++j) {
            rucrypto_uint8_t tmp = vec512[i * 8 + j];
            vec512[i * 8 + j] = vec512[j * 8 + i];
            vec512[j * 8 + i] = tmp;
        }
    }
}
