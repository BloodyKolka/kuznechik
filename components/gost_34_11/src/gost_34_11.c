#include "rucrypto/common/types.h"
#include <rucrypto/gost_34_11/gost_34_11.h>

#include <rucrypto/gost_34_11/context.h>
#include <rucrypto/gost_34_11/compression_utils.h>

#include <rucrypto/common/secure_zero.h>

rucrypto_result_t rucrypto_gost_34_11_hash_512_context_init(rucrypto_gost_34_11_hash_context_t** ctx) {
    *ctx = rucrypto_allocate(sizeof(rucrypto_gost_34_11_hash_context_t));
    if (*ctx == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    for (rucrypto_size_t i = 0; i < 64; ++i) {
        (*ctx)->h[i] = 0x0;
        (*ctx)->n[i] = 0x0;
        (*ctx)->sum[i] = 0x0;
    }
    return rucrypto_result_ok;
}

rucrypto_result_t rucrypto_gost_34_11_hash_256_context_init(rucrypto_gost_34_11_hash_context_t** ctx) {
    *ctx = rucrypto_allocate(sizeof(rucrypto_gost_34_11_hash_context_t));
    if (*ctx == RUCRYPTO_NULL) {
        return rucrypto_result_out_of_memory;
    }
    for (rucrypto_size_t i = 0; i < 64; ++i) {
        (*ctx)->h[i] = 0x1;
        (*ctx)->n[i] = 0x0;
        (*ctx)->sum[i] = 0x0;
    }
    return rucrypto_result_ok;
}

static void rucrypto_gost_34_11_add_512(rucrypto_vec512_t a, const rucrypto_vec512_t b) {
    rucrypto_uint32_t carry = 0;
    for (rucrypto_size_t i = 0; i < 64; ++i) {
        rucrypto_uint32_t c = (rucrypto_uint32_t)a[i] + (rucrypto_uint32_t)b[i] + carry;
        carry = c >> 8;
        a[i] = (rucrypto_uint8_t)(c & 0xFF);
    }
}

void rucrypto_gost_34_11_hash_part(rucrypto_gost_34_11_hash_context_t* ctx, const rucrypto_vec512_t m) {
    static const rucrypto_vec512_t value512 = {
            0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };

    rucrypto_gost_34_11_g_transform(ctx->h, m, ctx->n);
    rucrypto_gost_34_11_add_512(ctx->n, value512);
    rucrypto_gost_34_11_add_512(ctx->sum, m);
}

static void rucrypto_gost_34_11_hash_common_fini(
        rucrypto_gost_34_11_hash_context_t** ctx,
        const rucrypto_uint8_t data[],
        rucrypto_size_t dataSize) {
    static const rucrypto_vec512_t zeroVec512 = {0x0};

    RUCRYPTO_ASSERT(dataSize < 64);
    rucrypto_vec512_t m;
    RUCRYPTO_MEMCPY(m, data, dataSize);
    m[dataSize] = 1;
    RUCRYPTO_MEMSET(&m[dataSize + 1], 0, 64 - dataSize - 1);

    dataSize <<= 3;

    rucrypto_vec512_t dataSizeVec512 = {0x0};
    dataSizeVec512[1] = (rucrypto_uint8_t)((dataSize & 0xFF00) >> 8);
    dataSizeVec512[0] = (rucrypto_uint8_t)(dataSize & 0xFF);

    rucrypto_gost_34_11_g_transform((*ctx)->h, m, (*ctx)->n);
    rucrypto_gost_34_11_add_512((*ctx)->n, dataSizeVec512);
    rucrypto_gost_34_11_add_512((*ctx)->sum, m);
    rucrypto_gost_34_11_g_transform((*ctx)->h, (*ctx)->n, zeroVec512);
    rucrypto_gost_34_11_g_transform((*ctx)->h, (*ctx)->sum, zeroVec512);
}

void rucrypto_gost_34_11_hash_context_fini(rucrypto_gost_34_11_hash_context_t** ctx) {
    rucrypto_secure_zero((*ctx)->h, 64);
    rucrypto_secure_zero((*ctx)->n, 64);
    rucrypto_secure_zero((*ctx)->sum, 64);
    rucrypto_deallocate(*ctx);
    *ctx = RUCRYPTO_NULL;
}

void rucrypto_gost_34_11_hash_512_fini(
        rucrypto_gost_34_11_hash_context_t** ctx,
        const rucrypto_uint8_t data[],
        rucrypto_size_t dataSize,
        rucrypto_gost_34_11_hash_512_t hash) {
    rucrypto_gost_34_11_hash_common_fini(ctx, data, dataSize);
    RUCRYPTO_MEMCPY(hash, (*ctx)->h, 64);
    rucrypto_gost_34_11_hash_context_fini(ctx);
}

void rucrypto_gost_34_11_hash_256_fini(
        rucrypto_gost_34_11_hash_context_t** ctx,
        const rucrypto_uint8_t data[],
        rucrypto_size_t dataSize,
        rucrypto_gost_34_11_hash_256_t hash) {
    rucrypto_gost_34_11_hash_common_fini(ctx, data, dataSize);
    RUCRYPTO_MEMCPY(hash, &(*ctx)->h[32], 32);
    rucrypto_gost_34_11_hash_context_fini(ctx);
}
