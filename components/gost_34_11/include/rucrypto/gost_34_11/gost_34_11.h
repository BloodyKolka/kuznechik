/**
 * @file rucrypto/gost_34_11/gost_34_11.h
 */

#ifndef RUCRYPTO_GOST_34_11_GOST_34_11_H
#define RUCRYPTO_GOST_34_11_GOST_34_11_H

#include <rucrypto/common/common.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/** 256 bit hash buffer type */
typedef rucrypto_vec256_t rucrypto_gost_34_11_hash_256_t;
/** 512 bit hash buffer type */
typedef rucrypto_vec512_t rucrypto_gost_34_11_hash_512_t;

/** Struct to hold hash context */
typedef struct rucrypto_gost_34_11_hash_context_t rucrypto_gost_34_11_hash_context_t;

/**
 * @brief Perform step 1 of 512-bit hash function computation from GOST 34.11-2012
 * @param [out] ctx pointer to rucrypto_gost_34_11_hash_context_t
 * @return rucrypto_result_ok on success, error code otherwise
 * @details Must be called first to initialize context
 */
extern rucrypto_result_t rucrypto_gost_34_11_hash_512_context_init(rucrypto_gost_34_11_hash_context_t** ctx);

/**
 * @brief Perform step 1 of 256-bit hash function computation from GOST 34.11-2012
 * @param [out] ctx pointer to rucrypto_gost_34_11_hash_context_t
 * @return rucrypto_result_ok on success, error code otherwise
 * @details Must be called first to initialize context
 */
extern rucrypto_result_t rucrypto_gost_34_11_hash_256_context_init(rucrypto_gost_34_11_hash_context_t** ctx);

/**
 * @brief Perform step 2 of hash function computation from GOST 34.11-2012
 * @param [in, out] ctx pointer to rucrypto_gost_34_11_hash_context_t, initialized with rucrypto_gost_34_11_hash_256_context_init
 *        or rucrypto_gost_34_11_hash_512_context_init
 * @param [in] m last 512 bits of input data
 * @details Must be called for all parts of data with size >= 512
 */
extern void rucrypto_gost_34_11_hash_part(rucrypto_gost_34_11_hash_context_t* ctx, const rucrypto_vec512_t m);

/**
 * @brief Finalize context and deallocate memory
 * @param [in, out] ctx pointer to rucrypto_gost_34_11_hash_context_t, initialized with rucrypto_gost_34_11_hash_256_context_init
 *        or rucrypto_gost_34_11_hash_512_context_init
 */
extern void rucrypto_gost_34_11_hash_context_fini(rucrypto_gost_34_11_hash_context_t** ctx);

/**
 * @brief Perform step 3 of 512-bit hash function computation from GOST 34.11-2012
 * @param [in] ctx pointer to rucrypto_gost_34_11_hash_context_t
 * @param [in] data remaining input data
 * @param [in] dataSize size of remaining data in bytes (must be strictly less than 512)
 * @param [out] hash resulting hash
 * @details Must be called last with the last part of data with size < 512, calls rucrypto_gost_34_11_context_fini 
 */
extern void rucrypto_gost_34_11_hash_512_fini(
        rucrypto_gost_34_11_hash_context_t** ctx,
        const rucrypto_uint8_t data[],
        rucrypto_size_t dataSize,
        rucrypto_gost_34_11_hash_512_t hash);

/**
 * @brief Perform step 3 of 256-bit hash function computation from GOST 34.11-2012
 * @param [in] ctx pointer to rucrypto_gost_34_11_hash_context_t
 * @param [in] data remaining input data
 * @param [in] dataSize size of remaining data in bytes (must be strictly less than 512)
 * @param [out] hash resulting hash
 * @details Must be called last with the last part of data with size < 512, calls rucrypto_gost_34_11_context_fini 
 */
extern void rucrypto_gost_34_11_hash_256_fini(
        rucrypto_gost_34_11_hash_context_t** ctx,
        const rucrypto_uint8_t data[],
        rucrypto_size_t dataSize,
        rucrypto_gost_34_11_hash_256_t hash);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_GOST_34_11_GOST_34_11_H
