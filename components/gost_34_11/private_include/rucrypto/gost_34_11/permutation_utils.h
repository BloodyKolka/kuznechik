#ifndef RUCRYPTO_GOST_34_11_PERMUTATION_UTILS_H
#define RUCRYPTO_GOST_34_11_PERMUTATION_UTILS_H

#include <rucrypto/gost_34_11/gost_34_11.h>

/**
 * @brief Perform permutation P from GOST 34.11-2012
 * @param [in, out] vec512 512-bit block
 */
void rucrypto_gost_34_11_permutation(rucrypto_gost_34_11_hash_512_t vec512);

#endif // RUCRYPTO_GOST_34_11_PERMUTATION_UTILS_H
