#ifndef RUCRYPTO_GOST_34_11_CONTEXT_H
#define RUCRYPTO_GOST_34_11_CONTEXT_H

#include <rucrypto/common/common.h>

struct rucrypto_gost_34_11_hash_context_t {
    rucrypto_vec512_t h;
    rucrypto_vec512_t n;
    rucrypto_vec512_t sum;
};

#endif // RUCRYPTO_GOST_34_11_CONTEXT_H
