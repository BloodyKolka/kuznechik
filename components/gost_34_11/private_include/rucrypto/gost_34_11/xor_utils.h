#ifndef GOST_34_11_XOR_UTILS_H
#define GOST_34_11_XOR_UTILS_H

#include <rucrypto/gost_34_11/gost_34_11.h>

/**
 * @brief Perform X (XOR) operation from GOST 34.11-2012
 * @param [in, out] a first 512-bit operand, used to store result
 * @param [in] b second 512-bit operand
 */
void rucrypto_gost_34_11_xor(rucrypto_vec512_t a, const rucrypto_vec512_t b);

#endif // GOST_34_11_XOR_UTILS_H
