#ifndef GOST_34_11_SUBSTITUTION_UTILS_H
#define GOST_34_11_SUBSTITUTION_UTILS_H

#include <rucrypto/gost_34_11/gost_34_11.h>

/**
 * @brief Perform substitution S from GOST 34.11-2012
 * @param [in, out] vec512 512-bit block
 */
void rucrypto_gost_34_11_substitution(rucrypto_vec512_t vec512);

#endif // GOST_34_11_SUBSTITUTION_UTILS_H
