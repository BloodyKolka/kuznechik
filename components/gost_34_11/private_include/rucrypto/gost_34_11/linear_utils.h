#ifndef RUCRYPTO_GOST_34_11_LINEAR_UTILS_H
#define RUCRYPTO_GOST_34_11_LINEAR_UTILS_H

#include <rucrypto/gost_34_11/gost_34_11.h>

/**
 * @brief Perform linear tranformation l from GOST 34.11-2012
 * @param [in] vec64 64-bit vector
 * @return result
 */
rucrypto_uint64_t rucrypto_gost_34_11_linear_part_l(rucrypto_uint64_t vec64);

/**
 * @brief Perform linear tranformation l from GOST 34.11-2012 (designed for usage on uint8_t buffers on little endian
 *        systems)
 * @param [in] vec64 64-bit vector
 * @return result
 * @details On little endian systems when reading uint8_t buffer as uin64_t buffer, byte order is reversed so a special
 *          'reversed' implementation is required
 */
rucrypto_uint64_t rucrypto_gost_34_11_linear_part_l_little_endian(rucrypto_uint64_t vec64);

/**
 * @brief Perform linear transformation L from GOST 34.11-2012
 * @param [in, out] vec512
 */
void rucrypto_gost_34_11_linear_transform(rucrypto_vec512_t vec512);

#endif // RUCRYPTO_GOST_34_11_LINEAR_UTILS_H
