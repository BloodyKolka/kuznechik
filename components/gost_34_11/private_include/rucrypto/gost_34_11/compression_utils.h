#ifndef RUCRYPTO_GOST_34_11_COMPRESSION_UTILS_H
#define RUCRYPTO_GOST_34_11_COMPRESSION_UTILS_H

#include <rucrypto/gost_34_11/gost_34_11.h>

/**
 * @brief Perform E transformation from GOST 34.11-2012
 * @param [in] k parameter K (see GOST 34.11-2012)
 * @param [in] m argument m (see GOST 34.11-2012)
 * @param [out] res result of tranformation
 * @details k and res are allowed to be the same buffer, because k is copied before it is used, but in that case k
 *          const-ness is, obviously, not preserved
 */
void rucrypto_gost_34_11_e_transform(const rucrypto_vec512_t k, const rucrypto_vec512_t m, rucrypto_vec512_t res);

/**
 * @brief Perform g_N transformation from GOST 34.11-2012
 * @param [in, out] h argument h (see GOST 34.11-2012)
 * @param [in] m argument m (see GOST 34.11-2012)
 * @param [in] n parameter N (see GOST 34.11-2012)
 */
void rucrypto_gost_34_11_g_transform(rucrypto_vec512_t h, const rucrypto_vec512_t m, const rucrypto_vec512_t n);

#endif // RUCRYPTO_GOST_34_11_COMPRESSION_UTILS_H
