#include <rucrypto/common/secure_zero.h>

void rucrypto_secure_zero(void* buffer, rucrypto_size_t bufferSize)
{
    volatile unsigned char *volatile volatileBuffer = (volatile unsigned char *volatile)buffer;
    for (rucrypto_size_t i = 0; i < bufferSize; ++i) {
        volatileBuffer[i] = 0;
    }
}

