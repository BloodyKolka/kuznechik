#include <rucrypto/common/allocator_provider.h>

#ifndef RUCRYPTO_KERNEL_MODE
#include <stdlib.h>
#endif // RUCRYPTO_KERNEL_MODE

static rucrypto_allocator_t* rucrypto_get_allocator(void) {
    static rucrypto_allocator_t allocator = {
#ifndef RUCRYPTO_KERNEL_MODE
        .allocateFunc = malloc,
        .allocateZeroedFunc = calloc,
        .deallocateFunc = free,
#else
        .allocateFunc = RUCRYPTO_NULL,
        .allocateZeroedFunc = RUCRYPTO_NULL,
        .deallocateFunc = RUCRYPTO_NULL,
#endif // RUCRYPTO_KERNEL_MODE
    };
    return &allocator;
}

void rucrypto_init_allocator(rucrypto_allocator_t allocator) {
    *rucrypto_get_allocator() = allocator;
}

void* rucrypto_allocate(rucrypto_size_t size) {
#ifdef RUCRYPTO_ENABLE_ARGS_CHECKS
    if (rucrypto_get_allocator()->allocateFunc == RUCRYPTO_NULL) {
        return RUCRYPTO_NULL;
    }
#endif // RUCRYPTO_ENABLE_ARGS_CHECKS
    return rucrypto_get_allocator()->allocateFunc(size);
}

void* rucrypto_allocate_zeroed(rucrypto_size_t count, rucrypto_size_t size) {
#ifdef RUCRYPTO_ENABLE_ARGS_CHECKS
    if (rucrypto_get_allocator()->allocateZeroedFunc == RUCRYPTO_NULL) {
        return RUCRYPTO_NULL;
    }
#endif // RUCRYPTO_ENABLE_ARGS_CHECKS
    return rucrypto_get_allocator()->allocateZeroedFunc(count, size);
}

void rucrypto_deallocate(void* ptr) {
#ifdef RUCRYPTO_ENABLE_ARGS_CHECKS
    if (rucrypto_get_allocator()->deallocateFunc == RUCRYPTO_NULL) {
        return;
    }
#endif // RUCRYPTO_ENABLE_ARGS_CHECKS
    rucrypto_get_allocator()->deallocateFunc(ptr);
}

