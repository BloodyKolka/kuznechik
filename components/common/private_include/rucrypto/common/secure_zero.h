#ifndef RUCRYPTO_COMMON_SECURE_ZERO_H
#define RUCRYPTO_COMMON_SECURE_ZERO_H

#include <rucrypto/common/types.h>

/**
 * @brief Securely clear buffer with zeroes
 * @param [in] buffer
 * @param [out] bufferSize
 */
void rucrypto_secure_zero(void* buffer, rucrypto_size_t bufferSize);

#endif // RUCRYPTO_COMMON_SECURE_ZERO_H
