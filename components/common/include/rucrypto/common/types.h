/**
 * @file rucrypto/common/types.h
 */

#ifndef RUCRYPTO_COMMON_TYPES_H
#define RUCRYPTO_COMMON_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#ifndef RUCRYPTO_KERNEL_MODE
#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

/** 8 bit unsigned int */
typedef uint8_t  rucrypto_uint8_t;
/** 16 bit unsigned int */
typedef uint16_t rucrypto_uint16_t;
/** 32 bit unsigned int */
typedef uint32_t rucrypto_uint32_t;
/** 64 bit unsigned int */
typedef uint64_t rucrypto_uint64_t;

/** 8 bit signed int */
typedef int8_t  rucrypto_int8_t;
/** 16 bit signed int */
typedef int16_t rucrypto_int16_t;
/** 32 bit signed int */
typedef int32_t rucrypto_int32_t;
/** 64 bit signed int */
typedef int64_t rucrypto_int64_t;

/** boolean type */
typedef bool rucrypto_bool_t;
/** true constant */
#define RUCRYPTO_TRUE true
/** false constant */
#define RUCRYPTO_FALSE false

/** size type */
typedef size_t rucrypto_size_t;

/** null constant */
#define RUCRYPTO_NULL NULL

/**
 * @brief Debug assert 
 * @param [in] expr Expression to assert
 */
#define RUCRYPTO_ASSERT(expr) assert(expr)
/**
 * @brief Static assert
 * @param [in] expr Expression to assert
 * @param [in] msg Message to print on assertion failure
 */
#define RUCRYPTO_STATIC_ASSERT(expr, msg) static_assert(expr, msg)

/**
 * @brief Memset function
 * @param [out] dst Destination buffer
 * @param [in] c Value to set
 * @param [in] count Size of buffer
 */
#define RUCRYPTO_MEMSET(dst, c, count) memset(dst, c, count)
/**
 * @brief Memcpy function
 * @param [out] dst Destination buffer
 * @param [in] src Source buffer
 * @param [in] n Buffer size
 */
#define RUCRYPTO_MEMCPY(dst, src, n) memcpy(dst, src, n)

#else
#include <linux/stddef.h>
#include <linux/string.h>

/** 8 bit unsigned int */
typedef uint8_t  rucrypto_uint8_t;
/** 16 bit unsigned int */
typedef uint16_t rucrypto_uint16_t;
/** 32 bit unsigned int */
typedef uint32_t rucrypto_uint32_t;
/** 64 bit unsigned int */
typedef uint64_t rucrypto_uint64_t;

/** 8 bit signed int */
typedef int8_t  rucrypto_int8_t;
/** 16 bit signed int */
typedef int16_t rucrypto_int16_t;
/** 32 bit signed int */
typedef int32_t rucrypto_int32_t;
/** 64 bit signed int */
typedef int64_t rucrypto_int64_t;

/** boolean type */
typedef bool rucrypto_bool_t;
/** true constant */
#define RUCRYPTO_TRUE true
/** false constant */
#define RUCRYPTO_FALSE false

/** size type */
typedef size_t rucrypto_size_t;

/** null constant */
#define RUCRYPTO_NULL NULL

/**
 * @brief Debug assert 
 * @param [in] expr Expression to assert
 */
#define RUCRYPTO_ASSERT(expr)
/**
 * @brief Static assert
 * @param [in] expr Expression to assert
 * @param [in] msg Message to print on assertion failure
 */
#define RUCRYPTO_STATIC_ASSERT(expr, msg)

/**
 * @brief Memset function
 * @param [out] dst Destination buffer
 * @param [in] c Value to set
 * @param [in] count Size of buffer
 */
#define RUCRYPTO_MEMSET(dst, c, count) memset(dst, c, count)
/**
 * @brief Memcpy function
 * @param [out] dst Destination buffer
 * @param [in] src Source buffer
 * @param [in] n Buffer size
 */
#define RUCRYPTO_MEMCPY(dst, src, n) memcpy(dst, src, n)

#endif // RUCRYPTO_KERNEL_MODE

/** 128 bit buffer */
typedef rucrypto_uint8_t rucrypto_vec128_t[128 / 8];
/** 256 bit buffer */
typedef rucrypto_uint8_t rucrypto_vec256_t[256 / 8];
/** 512 bit buffer */
typedef rucrypto_uint8_t rucrypto_vec512_t[512 / 8];
/** 1024 bit buffer */
typedef rucrypto_uint8_t rucrypto_vec1024_t[1024 / 8];

/**
 * @brief Buffer of non-fixed length
 */
typedef struct {
    /** Buffer */
    rucrypto_uint8_t* buffer;
    /** Size of buffer */
    rucrypto_size_t size;
} rucrypto_variable_buffer_t;

/** @brief Const buffer of non-fixed length */
typedef struct {
    /** Buffer */
    rucrypto_uint8_t const* buffer;
    /** Size of buffer */
    rucrypto_size_t size;
} rucrypto_variable_buffer_const_t;

/**
 * @brief Result code for functions that can fail or return status
 */
typedef enum {
    rucrypto_result_ok,
    rucrypto_result_accepted,
    rucrypto_result_not_accepted,
    rucrypto_result_out_of_memory,
    rucrypto_result_invalid_parameters,
    rucrypto_result_cannot_continue,
} rucrypto_result_t;

#ifdef RUCRYPTO_ENABLE_ARGS_CHECKS
/** @undocumented */
#define __RUCRYPTO_CHECK_ARGS_IMPL(expr) if (!(expr)) { return rucrypto_result_invalid_parameters; }
#else
/** @undocumented */
#define __RUCRYPTO_CHECK_ARGS_IMPL(expr)
#endif // RUCRYPTO_ENABLE_ARGS_CHECKS

/**
 * @brief Check arguments
 * @param [in] expr Validation expression
 * @details Returns rucrypto_result_invalid_parameters if validation fails
 */
#define RUCRYPTO_CHECK_ARGS(expr) __RUCRYPTO_CHECK_ARGS_IMPL(expr)

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_COMMON_TYPES_H
