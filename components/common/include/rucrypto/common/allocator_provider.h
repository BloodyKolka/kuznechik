/**
 * @file rucrypto/common/allocator_provider.h
 */

#ifndef RUCRYPTO_COMMON_ALLOCATOR_PROVIDER_H
#define RUCRYPTO_COMMON_ALLOCATOR_PROVIDER_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include <rucrypto/common/types.h>

/**
 * @brief Allocate block of size
 * @param [in] size Size of allocated block
 * @return Pointer to allocated block or RUCRYPTO_NULL
 */
typedef void* (*rucrypto_allocate_func_t)(rucrypto_size_t size);

/**
 * @brief Allocate block, filled with zeroes for count elements of size
 * @param [in] count Count of elements
 * @param [in] size Size of 1 element
 * @return Pointer to allocated block or RUCRYPTO_NULL
 */
typedef void* (*rucrypto_allocate_zeroed_func_t)(rucrypto_size_t count, rucrypto_size_t size);

/**
 * @brief Free block, allocated with rucrypto_allocate_func_t or rucrypto_allocate_zeroed_func_t
 * @param ptr Pointer to previously allocated block
 */
typedef void (*rucrypto_deallocate_func_t)(void* ptr);

/** Allocator functons */
typedef struct {
    /** Function to allocate block of fixed size (basically - malloc) */
    rucrypto_allocate_func_t allocateFunc;
    /** Function to allocate block of elements with fixed size, filled with zeroes (basically - calloc) */
    rucrypto_allocate_zeroed_func_t allocateZeroedFunc;
    /** Function to deallocate block, allocated with allocateFunc or allocateZeroedFunc (basically - free) */
    rucrypto_deallocate_func_t deallocateFunc;
} rucrypto_allocator_t;

/**
 * @brief Initialize allocator
 * @param allocator Struct with allocator information
 * @details If malloc is available, allocator is already initialized with
 *          malloc/free. Otherwise the pointers are RUCRYPTO_NULL
 */
extern void rucrypto_init_allocator(rucrypto_allocator_t allocator);

/**
 * @brief Allocate buffer using previously provided allocator
 * @param size Size of buffer to allocate
 * @return Pointer to allocated buffer
 * @details Used internally by all functions so they can be used than malloc is
 *          not available
 */
extern void* rucrypto_allocate(rucrypto_size_t size);

/**
 * @brief Allocate zeroed buffer
 * @param count Count of elements
 * @param size Size of 1 element
 * @return Pointer to allocated buffer
 * @details Used internally by all functions so they can be used than malloc is
 *          not available
 */
extern void* rucrypto_allocate_zeroed(rucrypto_size_t count, rucrypto_size_t size);

/**
 * @brief Deallocate buffer, previously allocated via rucrypto_allocate using
 *        previously provided allocator
 * @param ptr Pointer to allocated buffer
 * @details Used internally by all functions so they can be used than malloc is
 *          not available
 */
extern void rucrypto_deallocate(void* ptr);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // RUCRYPTO_COMMON_ALLOCATOR_PROVIDER_H
