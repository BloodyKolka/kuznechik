include(FetchContent)

set(BUILD_GMOCK ON CACHE BOOL "" FORCE)
set(BUILD_GTEST ON CACHE BOOL "" FORCE)
set(INSTALL_GTEST OFF CACHE BOOL "" FORCE)

FetchContent_Declare(
        GTest
        GIT_REPOSITORY https://github.com/google/googletest.git
        GIT_TAG        v1.15.2
        EXCLUDE_FROM_ALL
        FIND_PACKAGE_ARGS NAMES GTest
)

FetchContent_MakeAvailable(GTest)
