#!/bin/bash

USE_DOCKERFILE="${1:-dockerfile}"
if [ "${USE_DOCKERFILE}" = "no_dockerfile" ]; then
    DEBIAN_FRONTEND=noninteractive apt update
    DEBIAN_FRONTEND=noninteractive apt -y install gcc g++ lcov libgtest-dev
fi

# Initialize zero coverage
echo "=== Initializing zero coverage ==="
lcov -c -i -d .test_build -o coverage_report_base.info --exclude '/usr/*' --exclude '*/tests/*' || exit 1

# Run tests
echo "=== Running tests ==="
res=0
[ -f ".test_build/components/gost_34_10/tests/gost_34_10_tests" ] && .test_build/components/gost_34_10/tests/gost_34_10_tests --gtest_output="xml:report_gost_34_10_tests.xml" || res=1
[ -f ".test_build/components/gost_34_11/tests/gost_34_11_tests" ] && .test_build/components/gost_34_11/tests/gost_34_11_tests --gtest_output="xml:report_gost_34_11_tests.xml" || res=1
[ -f ".test_build/components/gost_34_12/tests/gost_34_12_tests" ] && .test_build/components/gost_34_12/tests/gost_34_12_tests --gtest_output="xml:report_gost_34_12_tests.xml" || res=1
[ -f ".test_build/components/gost_34_13/tests/gost_34_13_tests" ] && .test_build/components/gost_34_13/tests/gost_34_13_tests --gtest_output="xml:report_gost_34_13_tests.xml" || res=1
[ -f ".test_build/components/r_1323565_1_022/tests/r_1323565_1_022_tests" ] && .test_build/components/r_1323565_1_022/tests/r_1323565_1_022_tests --gtest_output="xml:report_r_1323565_1_022_tests.xml" || res=1
[ -f ".test_build/components/r_1323565_1_042/tests/r_1323565_1_042_tests" ] && .test_build/components/r_1323565_1_042/tests/r_1323565_1_042_tests --gtest_output="xml:report_r_1323565_1_042_tests.xml" || res=1

# Collect coverage
echo "=== Collecting coverage info ==="
lcov -c -d .test_build -o coverage_report_test.info --exclude '/usr/*' --exclude '*/tests/*' || exit 1
lcov -a coverage_report_base.info -a coverage_report_test.info -o coverage_report.info || exit 1

# Generate HTML report
echo "=== Generating HTML report ==="
mkdir coverage_report || exit 1
genhtml coverage_report.info --output coverage_report || exit 1

echo "=== Compressing HTML report ==="
tar czfv coverage_report.tar.gz coverage_report

echo "=== Done ==="
exit ${res}
