#!/bin/bash

USE_DOCKERFILE="${1:-dockerfile}"
if [ "${USE_DOCKERFILE}" = "no_dockerfile" ]; then
    DEBIAN_FRONTEND=noninteractive apt update
    DEBIAN_FRONTEND=noninteractive apt -y install valgrind gcc g++ libgtest-dev
fi

# Run tests
echo "=== Running tests ==="
res=0
VALGRIND_CMD="valgrind -s --leak-check=full --leak-resolution=med --track-origins=yes --vgdb=no --error-exitcode=1"
[ -f ".test_build/components/gost_34_10/tests/gost_34_10_tests" ] && $VALGRIND_CMD .test_build/components/gost_34_10/tests/gost_34_10_tests || res=1
[ -f ".test_build/components/gost_34_11/tests/gost_34_11_tests" ] && $VALGRIND_CMD .test_build/components/gost_34_11/tests/gost_34_11_tests || res=1
[ -f ".test_build/components/gost_34_12/tests/gost_34_12_tests" ] && $VALGRIND_CMD .test_build/components/gost_34_12/tests/gost_34_12_tests || res=1
[ -f ".test_build/components/gost_34_13/tests/gost_34_13_tests" ] && $VALGRIND_CMD .test_build/components/gost_34_13/tests/gost_34_13_tests || res=1
[ -f ".test_build/components/r_1323565_1_022/tests/r_1323565_1_022_tests" ] && $VALGRIND_CMD .test_build/components/r_1323565_1_022/tests/r_1323565_1_022_tests || res=1
[ -f ".test_build/components/r_1323565_1_042/tests/r_1323565_1_042_tests" ] && $VALGRIND_CMD .test_build/components/r_1323565_1_042/tests/r_1323565_1_042_tests || res=1

echo "=== Done ==="
exit ${res}
