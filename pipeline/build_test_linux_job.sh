#!/bin/bash

USE_DOCKERFILE="${1:-dockerfile}"
if [ "${USE_DOCKERFILE}" = "no_dockerfile" ]; then
    DEBIAN_FRONTEND=noninteractive apt update
    DEBIAN_FRONTEND=noninteractive apt -y install cmake make gcc g++ clang-tidy libgtest-dev
fi

# Configuration
echo "=== Configuring project ==="
mkdir .test_build || exit 1
cmake -S . -B .test_build -DCMAKE_BUILD_TYPE=Debug -DRUCRYPTO_ENABLE_COVERAGE=ON -DRUCRYPTO_ENABLE_TESTS=ON || exit 1

# Build
echo "=== Building project ==="
cmake --build .test_build --target all || exit 1

echo "=== Done ==="
exit 0
