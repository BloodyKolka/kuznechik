#!/bin/bash

USE_DOCKERFILE="${1:-dockerfile}"
if [ "${USE_DOCKERFILE}" = "no_dockerfile" ]; then
    DEBIAN_FRONTEND=noninteractive apt update
    DEBIAN_FRONTEND=noninteractive apt -y install cmake make gcc-x86-64-linux-gnu g++-x86-64-linux-gnu gcc-mingw-w64-x86-64-win32 g++-mingw-w64-x86-64-win32 clang-tidy nsis
fi

# Create build directories
echo "=== Creating build directories ==="
mkdir .release || exit 1
mkdir .release/x86_64-linux-gnu || exit 1
mkdir .release/x86_64-w64-mingw || exit 1

# Configure project
echo "=== Configuring project ==="
cmake -S . -B .release/x86_64-linux-gnu -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=cmake/x86_64-linux-gnu-toolchain.cmake || exit 1
cmake -S . -B .release/x86_64-w64-mingw -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=cmake/x86_64-w64-mingw32-toolchain.cmake || exit 1

# Build project
echo "=== Building project ==="
cmake --build .release/x86_64-linux-gnu --target all || exit 1
cmake --build .release/x86_64-w64-mingw --target all || exit 1

# Pack project (Linux DEB)
echo "=== Packing project for x86_64-linux-gnu (DEB) ==="
cd .release/x86_64-linux-gnu || exit 1
cpack -G DEB || exit 1
cd ../.. || exit 1

# Pack project (Windows NSIS)
echo "=== Packing project for x86_64-w64-mingw (NSIS) ==="
cd .release/x86_64-w64-mingw || exit 1
cpack -G NSIS || exit 1
cd ../.. || exit 1

echo "=== Done ==="
exit 0
