#include <Python.h>

#include <rucrypto/gost_34_12/gost_34_12.h>
#include <rucrypto/gost_34_13/gost_34_13.h>

static PyObject* method_encrypt_128_ecb(PyObject *self, PyObject *args) {
    Py_buffer inputBuffer, keyBuffer;

    /* Parse arguments */
    if (!PyArg_ParseTuple(args, "y*y*", &inputBuffer, &keyBuffer)) {
        return NULL;
    }

    if (keyBuffer.len != RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES) {
        PyErr_SetString(PyExc_ValueError, "Key is not of the expected 256-bit size!");
        return NULL;
    }

    rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_gost_34_13_base_cipher_init(&baseCipher, rucrypto_gost_34_12_encrypt_128, rucrypto_gost_34_12_decrypt_128, RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES, keyBuffer.buf, RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES);

    size_t size = (inputBuffer.len / RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES) * RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES + ((inputBuffer.len % RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES == 0) ? 0 : RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES);
    uint8_t* buffer = malloc(size);
    if (buffer == NULL) {
        return PyErr_NoMemory();
    }
    RUCRYPTO_MEMCPY(buffer, inputBuffer.buf, inputBuffer.len);
    if (inputBuffer.len % RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES != 0) {
        rucrypto_gost_34_13_pad_2(&buffer[size - RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES], inputBuffer.len % RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES, RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES);
    }
    for (size_t i = 0; i < size; i += RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES) {
        rucrypto_gost_34_13_ecb_encrypt_block(baseCipher, &buffer[i]);
    }

    rucrypto_gost_34_13_base_cipher_fini(&baseCipher);
    PyObject* res = PyBytes_FromStringAndSize((char*)buffer, (Py_ssize_t)size);
    free(buffer);

    return res;
}

static PyObject* method_decrypt_128_ecb(PyObject *self, PyObject *args) {
    Py_buffer inputBuffer, keyBuffer;

    /* Parse arguments */
    if (!PyArg_ParseTuple(args, "y*y*", &inputBuffer, &keyBuffer)) {
        return NULL;
    }

    if (keyBuffer.len != RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES) {
        PyErr_SetString(PyExc_ValueError, "Key is not of the expected 256-bit size!");
        return NULL;
    }

    rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_gost_34_13_base_cipher_init(&baseCipher, rucrypto_gost_34_12_encrypt_128, rucrypto_gost_34_12_decrypt_128, RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES, keyBuffer.buf, RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES);

    size_t size = inputBuffer.len;
    uint8_t* buffer = malloc(size);
    if (buffer == NULL) {
        return PyErr_NoMemory();
    }
    RUCRYPTO_MEMCPY(buffer, inputBuffer.buf, inputBuffer.len);
    for (size_t i = 0; i < size; i += RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES) {
        rucrypto_gost_34_13_ecb_decrypt_block(baseCipher, &buffer[i]);
    }

    size_t paddedBytes = 1;
    uint8_t cur = buffer[size - 1];
    while (cur == 0x00 && paddedBytes < size) {
        cur = buffer[size - paddedBytes];
        paddedBytes++;
    }
    if (paddedBytes == size || cur != 0x80) {
        paddedBytes = 0;
    }

    rucrypto_gost_34_13_base_cipher_fini(&baseCipher);
    PyObject* res = PyBytes_FromStringAndSize((char*)buffer, (Py_ssize_t)(size - paddedBytes));
    free(buffer);

    return res;
}

static PyObject* method_encrypt_128_ctr(PyObject *self, PyObject *args) {
    Py_buffer inputBuffer, keyBuffer, ivBuffer;
    Py_ssize_t gammaSize;

    /* Parse arguments */
    if (!PyArg_ParseTuple(args, "y*y*ny*", &inputBuffer, &keyBuffer, &gammaSize, &ivBuffer)) {
        return NULL;
    }

    if (keyBuffer.len != RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES || ivBuffer.len != RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES / 2 || gammaSize == 0 || gammaSize > RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES) {
        PyErr_SetString(PyExc_ValueError, "Some arguments are invalid! Please make sure that the key is 256 bits, initializing vector (IV) is half the block size (128 bits / 2) and gamma size is greater than 0 and less than block size (128 bits)");
        return NULL;
    }

    rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_gost_34_13_base_cipher_init(&baseCipher, rucrypto_gost_34_12_encrypt_128, rucrypto_gost_34_12_decrypt_128, RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES, keyBuffer.buf, RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES);

    rucrypto_gost_34_13_ctr_context_t* ctx;
    rucrypto_gost_34_13_ctr_context_init(&ctx, baseCipher, gammaSize, ivBuffer.buf);

    size_t size = inputBuffer.len;
    uint8_t* buffer = malloc(size);
    if (buffer == NULL) {
        return PyErr_NoMemory();
    }
    RUCRYPTO_MEMCPY(buffer, inputBuffer.buf, inputBuffer.len);

    size_t i = 0;
    for (; i + gammaSize < size; i += gammaSize) {
        rucrypto_gost_34_13_ctr_encrypt_block(ctx, &buffer[i], gammaSize);
    }
    rucrypto_gost_34_13_ctr_encrypt_block(ctx, &buffer[i], (size % gammaSize == 0) ? gammaSize : size % gammaSize);

    rucrypto_gost_34_13_ctr_context_fini(&ctx);
    rucrypto_gost_34_13_base_cipher_fini(&baseCipher);
    PyObject* res = PyBytes_FromStringAndSize((char*)buffer, (Py_ssize_t)size);
    free(buffer);

    return res;
}

static PyObject* method_decrypt_128_ctr(PyObject *self, PyObject *args) {
    Py_buffer inputBuffer, keyBuffer, ivBuffer;
    Py_ssize_t gammaSize;

    /* Parse arguments */
    if (!PyArg_ParseTuple(args, "y*y*ny*", &inputBuffer, &keyBuffer, &gammaSize, &ivBuffer)) {
        return NULL;
    }

    if (keyBuffer.len != RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES || ivBuffer.len != RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES / 2 || gammaSize == 0 || gammaSize > RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES) {
        PyErr_SetString(PyExc_ValueError, "Some arguments are invalid! Please make sure that the key is 256 bits, initializing vector (IV) is half the block size (128 bits / 2) and gamma size is greater than 0 and less than block size (128 bits)");
        return NULL;
    }

    rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_gost_34_13_base_cipher_init(&baseCipher, rucrypto_gost_34_12_encrypt_128, rucrypto_gost_34_12_decrypt_128, RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES, keyBuffer.buf, RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES);

    rucrypto_gost_34_13_ctr_context_t* ctx;
    rucrypto_gost_34_13_ctr_context_init(&ctx, baseCipher, gammaSize, ivBuffer.buf);

    size_t size = inputBuffer.len;
    uint8_t* buffer = malloc(size);
    if (buffer == NULL) {
        return PyErr_NoMemory();
    }
    RUCRYPTO_MEMCPY(buffer, inputBuffer.buf, inputBuffer.len);

    size_t i = 0;
    for (; i + gammaSize < size; i += gammaSize) {
        rucrypto_gost_34_13_ctr_decrypt_block(ctx, &buffer[i], gammaSize);
    }
    rucrypto_gost_34_13_ctr_decrypt_block(ctx, &buffer[i], (size % gammaSize == 0) ? gammaSize : size % gammaSize);

    rucrypto_gost_34_13_ctr_context_fini(&ctx);
    rucrypto_gost_34_13_base_cipher_fini(&baseCipher);
    PyObject* res = PyBytes_FromStringAndSize((char*)buffer, (Py_ssize_t)size);
    free(buffer);

    return res;
}

static PyObject* method_encrypt_128_ofb(PyObject *self, PyObject *args) {
    Py_buffer inputBuffer, keyBuffer, ivBuffer;
    Py_ssize_t gammaSize, regSize;

    /* Parse arguments */
    if (!PyArg_ParseTuple(args, "y*y*ny*n", &inputBuffer, &keyBuffer, &gammaSize, &ivBuffer, &regSize)) {
        return NULL;
    }

    if (keyBuffer.len != RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES || ivBuffer.len != regSize || regSize == 0 || regSize % RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES != 0 || gammaSize == 0 || gammaSize > RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES) {
        PyErr_SetString(PyExc_ValueError, "Some arguments are invalid! Please make sure that the key is 256 bits, register size is a multiple of block size (128 bits), initializing vector (IV) size is equal to register size and gamma size is greater than 0 and less than block size (128 bits)");
        return NULL;
    }

    rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_gost_34_13_base_cipher_init(&baseCipher, rucrypto_gost_34_12_encrypt_128, rucrypto_gost_34_12_decrypt_128, RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES, keyBuffer.buf, RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES);

    rucrypto_gost_34_13_ofb_context_t* ctx;
    rucrypto_gost_34_13_ofb_context_init(&ctx, baseCipher, gammaSize, regSize, ivBuffer.buf);

    size_t size = inputBuffer.len;
    uint8_t* buffer = malloc(size);
    if (buffer == NULL) {
        return PyErr_NoMemory();
    }
    RUCRYPTO_MEMCPY(buffer, inputBuffer.buf, inputBuffer.len);

    size_t i = 0;
    for (; i + gammaSize < size; i += gammaSize) {
        rucrypto_gost_34_13_ofb_encrypt_block(ctx, &buffer[i], gammaSize);
    }
    rucrypto_gost_34_13_ofb_encrypt_block(ctx, &buffer[i], (size % gammaSize == 0) ? gammaSize : size % gammaSize);

    rucrypto_gost_34_13_ofb_context_fini(&ctx);
    rucrypto_gost_34_13_base_cipher_fini(&baseCipher);
    PyObject* res = PyBytes_FromStringAndSize((char*)buffer, (Py_ssize_t)size);
    free(buffer);

    return res;
}

static PyObject* method_decrypt_128_ofb(PyObject *self, PyObject *args) {
    Py_buffer inputBuffer, keyBuffer, ivBuffer;
    Py_ssize_t gammaSize, regSize;

    /* Parse arguments */
    if (!PyArg_ParseTuple(args, "y*y*ny*n", &inputBuffer, &keyBuffer, &gammaSize, &ivBuffer, &regSize)) {
        return NULL;
    }

    if (keyBuffer.len != RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES || ivBuffer.len != regSize || regSize == 0 || regSize % RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES != 0 || gammaSize == 0 || gammaSize > RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES) {
        PyErr_SetString(PyExc_ValueError, "Some arguments are invalid! Please make sure that the key is 256 bits, register size is a multiple of block size (128 bits), initializing vector (IV) size is equal to register size and gamma size is greater than 0 and less than block size (128 bits)");
        return NULL;
    }

    rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_gost_34_13_base_cipher_init(&baseCipher, rucrypto_gost_34_12_encrypt_128, rucrypto_gost_34_12_decrypt_128, RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES, keyBuffer.buf, RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES);

    rucrypto_gost_34_13_ofb_context_t* ctx;
    rucrypto_gost_34_13_ofb_context_init(&ctx, baseCipher, gammaSize, regSize, ivBuffer.buf);

    size_t size = inputBuffer.len;
    uint8_t* buffer = malloc(size);
    if (buffer == NULL) {
        return PyErr_NoMemory();
    }
    RUCRYPTO_MEMCPY(buffer, inputBuffer.buf, inputBuffer.len);

    size_t i = 0;
    for (; i + gammaSize < size; i += gammaSize) {
        rucrypto_gost_34_13_ofb_decrypt_block(ctx, &buffer[i], gammaSize);
    }
    rucrypto_gost_34_13_ofb_decrypt_block(ctx, &buffer[i], (size % gammaSize == 0) ? gammaSize : size % gammaSize);

    rucrypto_gost_34_13_ofb_context_fini(&ctx);
    rucrypto_gost_34_13_base_cipher_fini(&baseCipher);
    PyObject* res = PyBytes_FromStringAndSize((char*)buffer, (Py_ssize_t)size);
    free(buffer);

    return res;
}

static PyObject* method_encrypt_128_cbc(PyObject *self, PyObject *args) {
    Py_buffer inputBuffer, keyBuffer, ivBuffer;
    Py_ssize_t regSize;

    /* Parse arguments */
    if (!PyArg_ParseTuple(args, "y*y*y*n", &inputBuffer, &keyBuffer, &ivBuffer, &regSize)) {
        return NULL;
    }

    if (keyBuffer.len != RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES || ivBuffer.len != regSize || regSize == 0 || regSize % RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES != 0) {
        PyErr_SetString(PyExc_ValueError, "Some arguments are invalid! Please make sure that the key is 256 bits, register size is a multiple of block size (128 bits) and initializing vector (IV) size is equal to register size");
        return NULL;
    }

    rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_gost_34_13_base_cipher_init(&baseCipher, rucrypto_gost_34_12_encrypt_128, rucrypto_gost_34_12_decrypt_128, RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES, keyBuffer.buf, RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES);

    rucrypto_gost_34_13_cbc_context_t* ctx;
    rucrypto_gost_34_13_cbc_context_init(&ctx, baseCipher, regSize, ivBuffer.buf);

    size_t size = (inputBuffer.len / RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES) * RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES + ((inputBuffer.len % RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES == 0) ? 0 : RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES);
    uint8_t* buffer = malloc(size);
    if (buffer == NULL) {
        return PyErr_NoMemory();
    }
    RUCRYPTO_MEMCPY(buffer, inputBuffer.buf, inputBuffer.len);
    if (inputBuffer.len % RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES != 0) {
        rucrypto_gost_34_13_pad_2(&buffer[size - RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES], inputBuffer.len % RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES, RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES);
    }
    for (size_t i = 0; i < size; i += RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES) {
        rucrypto_gost_34_13_cbc_encrypt_block(ctx, &buffer[i]);
    }

    rucrypto_gost_34_13_cbc_context_fini(&ctx);
    rucrypto_gost_34_13_base_cipher_fini(&baseCipher);
    PyObject* res = PyBytes_FromStringAndSize((char*)buffer, (Py_ssize_t)size);
    free(buffer);

    return res;
}

static PyObject* method_decrypt_128_cbc(PyObject *self, PyObject *args) {
    Py_buffer inputBuffer, keyBuffer, ivBuffer;
    Py_ssize_t regSize;

    /* Parse arguments */
    if (!PyArg_ParseTuple(args, "y*y*y*n", &inputBuffer, &keyBuffer, &ivBuffer, &regSize)) {
        return NULL;
    }

    if (keyBuffer.len != RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES || ivBuffer.len != regSize || regSize == 0 || regSize % RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES != 0 || inputBuffer.len % RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES != 0) {
        PyErr_SetString(PyExc_ValueError, "Some arguments are invalid! Please make sure that the key is 256 bits, register size is a multiple of block size (128 bits), initializing vector (IV) size is equal to register size and input buffer size is a multiple of block size (128 bits)");
        return NULL;
    }

    rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_gost_34_13_base_cipher_init(&baseCipher, rucrypto_gost_34_12_encrypt_128, rucrypto_gost_34_12_decrypt_128, RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES, keyBuffer.buf, RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES);

    rucrypto_gost_34_13_cbc_context_t* ctx;
    rucrypto_gost_34_13_cbc_context_init(&ctx, baseCipher, regSize, ivBuffer.buf);

    size_t size = inputBuffer.len;
    uint8_t* buffer = malloc(size);
    if (buffer == NULL) {
        return PyErr_NoMemory();
    }
    RUCRYPTO_MEMCPY(buffer, inputBuffer.buf, inputBuffer.len);
    for (size_t i = 0; i < size; i += RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES) {
        rucrypto_gost_34_13_cbc_decrypt_block(ctx, &buffer[i]);
    }

    size_t paddedBytes = 1;
    uint8_t cur = buffer[size - 1];
    while (cur == 0x00 && paddedBytes < size) {
        cur = buffer[size - paddedBytes];
        paddedBytes++;
    }
    if (paddedBytes == size || cur != 0x80) {
        paddedBytes = 0;
    }

    rucrypto_gost_34_13_cbc_context_fini(&ctx);
    rucrypto_gost_34_13_base_cipher_fini(&baseCipher);
    PyObject* res = PyBytes_FromStringAndSize((char*)buffer, (Py_ssize_t)(size - paddedBytes));
    free(buffer);

    return res;
}

static PyObject* method_encrypt_128_cfb(PyObject *self, PyObject *args) {
    Py_buffer inputBuffer, keyBuffer, ivBuffer;
    Py_ssize_t gammaSize, regSize;

    /* Parse arguments */
    if (!PyArg_ParseTuple(args, "y*y*ny*n", &inputBuffer, &keyBuffer, &gammaSize, &ivBuffer, &regSize)) {
        return NULL;
    }

    if (keyBuffer.len != RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES || ivBuffer.len != regSize || regSize == 0 || regSize < RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES || gammaSize > RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES) {
        PyErr_SetString(PyExc_ValueError, "Some arguments are invalid! Please make sure that the key is 256 bits, register size is a multiple of block size (128 bits), initializing vector (IV) size is equal to register size and gamma size is greater than 0 and less than block size (128 bits)");
        return NULL;
    }

    rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_gost_34_13_base_cipher_init(&baseCipher, rucrypto_gost_34_12_encrypt_128, rucrypto_gost_34_12_decrypt_128, RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES, keyBuffer.buf, RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES);

    rucrypto_gost_34_13_cfb_context_t* ctx;
    rucrypto_gost_34_13_cfb_context_init(&ctx, baseCipher, gammaSize, regSize, ivBuffer.buf);

    size_t size = inputBuffer.len;
    uint8_t* buffer = malloc(size);
    if (buffer == NULL) {
        return PyErr_NoMemory();
    }
    RUCRYPTO_MEMCPY(buffer, inputBuffer.buf, inputBuffer.len);

    size_t i = 0;
    for (; i + gammaSize < size; i += gammaSize) {
        rucrypto_gost_34_13_cfb_encrypt_block(ctx, &buffer[i], gammaSize);
    }
    rucrypto_gost_34_13_cfb_encrypt_block(ctx, &buffer[i], (size % gammaSize == 0) ? gammaSize : size % gammaSize);

    rucrypto_gost_34_13_cfb_context_fini(&ctx);
    rucrypto_gost_34_13_base_cipher_fini(&baseCipher);
    PyObject* res = PyBytes_FromStringAndSize((char*)buffer, (Py_ssize_t)size);
    free(buffer);

    return res;
}

static PyObject* method_decrypt_128_cfb(PyObject *self, PyObject *args) {
    Py_buffer inputBuffer, keyBuffer, ivBuffer;
    Py_ssize_t gammaSize, regSize;

    /* Parse arguments */
    if (!PyArg_ParseTuple(args, "y*y*ny*n", &inputBuffer, &keyBuffer, &gammaSize, &ivBuffer, &regSize)) {
        return NULL;
    }

    if (keyBuffer.len != RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES || ivBuffer.len != regSize || regSize == 0 || regSize < RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES || gammaSize > RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES) {
        PyErr_SetString(PyExc_ValueError, "Some arguments are invalid! Please make sure that the key is 256 bits, register size is a multiple of block size (128 bits), initializing vector (IV) size is equal to register size and gamma size is greater than 0 and less than block size (128 bits)");
        return NULL;
    }

    rucrypto_gost_34_13_base_cipher_t* baseCipher;
    rucrypto_gost_34_13_base_cipher_init(&baseCipher, rucrypto_gost_34_12_encrypt_128, rucrypto_gost_34_12_decrypt_128, RUCRYPTO_GOST_34_12_BLOCK_SIZE_BYTES, keyBuffer.buf, RUCRYPTO_GOST_34_12_KEY_SIZE_BYTES);

    rucrypto_gost_34_13_cfb_context_t* ctx;
    rucrypto_gost_34_13_cfb_context_init(&ctx, baseCipher, gammaSize, regSize, ivBuffer.buf);

    size_t size = inputBuffer.len;
    uint8_t* buffer = malloc(size);
    if (buffer == NULL) {
        return PyErr_NoMemory();
    }
    RUCRYPTO_MEMCPY(buffer, inputBuffer.buf, inputBuffer.len);

    size_t i = 0;
    for (; i + gammaSize < size; i += gammaSize) {
        rucrypto_gost_34_13_cfb_decrypt_block(ctx, &buffer[i], gammaSize);
    }
    rucrypto_gost_34_13_cfb_decrypt_block(ctx, &buffer[i], (size % gammaSize == 0) ? gammaSize : size % gammaSize);

    rucrypto_gost_34_13_cfb_context_fini(&ctx);
    rucrypto_gost_34_13_base_cipher_fini(&baseCipher);
    PyObject* res = PyBytes_FromStringAndSize((char*)buffer, (Py_ssize_t)size);
    free(buffer);

    return res;
}

static PyMethodDef RuCryptoPyMethods[] = {
        {"encrypt_128_ecb", method_encrypt_128_ecb, METH_VARARGS, "Python interface for RuCrypto C library ECB encrypt function using 128-bit cipher from GOST 34.12-2018"},
        {"decrypt_128_ecb", method_decrypt_128_ecb, METH_VARARGS, "Python interface for RuCrypto C library ECB decrypt function using 128-bit cipher from GOST 34.12-2018"},
        {"encrypt_128_ctr", method_encrypt_128_ctr, METH_VARARGS, "Python interface for RuCrypto C library CTR encrypt function using 128-bit cipher from GOST 34.12-2018"},
        {"decrypt_128_ctr", method_decrypt_128_ctr, METH_VARARGS, "Python interface for RuCrypto C library CTR decrypt function using 128-bit cipher from GOST 34.12-2018"},
        {"encrypt_128_ofb", method_encrypt_128_ofb, METH_VARARGS, "Python interface for RuCrypto C library OFB encrypt function using 128-bit cipher from GOST 34.12-2018"},
        {"decrypt_128_ofb", method_decrypt_128_ofb, METH_VARARGS, "Python interface for RuCrypto C library OFB decrypt function using 128-bit cipher from GOST 34.12-2018"},
        {"encrypt_128_cbc", method_encrypt_128_cbc, METH_VARARGS, "Python interface for RuCrypto C library CBC encrypt function using 128-bit cipher from GOST 34.12-2018"},
        {"decrypt_128_cbc", method_decrypt_128_cbc, METH_VARARGS, "Python interface for RuCrypto C library CBC decrypt function using 128-bit cipher from GOST 34.12-2018"},
        {"encrypt_128_cfb", method_encrypt_128_cfb, METH_VARARGS, "Python interface for RuCrypto C library CFB encrypt function using 128-bit cipher from GOST 34.12-2018"},
        {"decrypt_128_cfb", method_decrypt_128_cfb, METH_VARARGS, "Python interface for RuCrypto C library CFB decrypt function using 128-bit cipher from GOST 34.12-2018"},
        {NULL, NULL, 0, NULL}
};

static struct PyModuleDef RuCryptoPyModule = {
        PyModuleDef_HEAD_INIT,
        "rucryptopy",
        "Python interface for the RuCrypto C library",
        -1,
        RuCryptoPyMethods
};

PyMODINIT_FUNC PyInit_rucryptopy(void) {
    return PyModule_Create(&RuCryptoPyModule);
}
