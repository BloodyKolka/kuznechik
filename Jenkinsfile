pipeline {
    agent {
        dockerfile true
    }

    stages {
        stage('Build_Linux_Tests') {
            steps {
                sh './pipeline/build_test_linux_job.sh 2>&1 | tee Build_Linux_Tests.log'
                archiveArtifacts artifacts: 'Build_Linux_Tests.log', fingerprint: true
                stash includes: '.test_build/**/*tests, .test_build/**/*.gcno', name: 'tests'
            }
        }
        stage('Test_Linux_Coverage') {
            steps {
                unstash 'tests'
                sh './pipeline/tests_with_coverage_linux_job.sh 2>&1 | tee Test_Linux_Coverage.log'
                archiveArtifacts artifacts: 'Test_Linux_Coverage.log, coverage_report.tar.gz', fingerprint: true
            }
            post {
                always {
                    junit 'report*.xml'
                }
            }
        }
        stage('Test_Linux_Memcheck') {
            steps {
                unstash 'tests'
                sh './pipeline/tests_with_memcheck_linux_job.sh 2>&1 | tee Test_Linux_Memcheck.log'
                archiveArtifacts artifacts: 'Test_Linux_Memcheck.log', fingerprint: true
            }
        }
        stage('Build_Linux_Release') {
            steps {
                sh './pipeline/build_release_job.sh 2>&1 | tee Build_Linux_Release.log'
                archiveArtifacts artifacts: 'Build_Linux_Release.log', fingerprint: true
                archiveArtifacts artifacts: '.release/x86_64-linux-gnu/rucrypto_*_amd64.deb, .release/x86_64-w64-mingw/rucrypto-*-win64.exe', fingerprint: true, onlyIfSuccessful: true
            }
        }
    }
    post {
        // Clean after build
        always {
            cleanWs()
        }
    }
}
