# RuCrypto library
## Disclaimer
This library has not been tested against side channel attacks and other attacks on implementation. Should you want to use it - you do so at your own risk.
## Features
- Implements GOST 34.10-2018 512-bit and 1024-bit ECDSA 
- Implements GOST 34.11-2018 256-bit and 512-bit hash functions
- Implements GOST 34.12-2018 256-bit block cipher
- Implements GOST 34.13-2018 block cipher encryption algorithms
- Implements R 1323565.1.022 HMAC256, HMAC512, NMAC, kdf and format functions
- Implements R 1323565.1.042 key system and sector encryption/unencryption functions

## Change notes
* 1.3.1 - Add subproject mode which prefixes targets with 'rucrypto_'
* 1.3.0 - Implement R 1323565.1.0.42 key system and sector access functions
* 1.2.1 - Fix installation of R 1323565.1.022 (it was missing headers)
* 1.2.0 - Implement R 1323565.1.022 HMAC256, HMAC512, NMAC, format and kdf functions. Use internal types everywhere. Allow building for kernel
* 1.1.0 - Restructure project, add python bindings for GOST 34.13-2018 (with block cipher from GOST 34.12-2018)
* 1.0.1 - Extract secure_zero function to common library, add clearing of buffers to ECDSA GOST 34.10-2018
* 1.0.0 - Initial release
