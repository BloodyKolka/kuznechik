FROM ubuntu:latest

# Set up env for apt
ENV DEBIAN_FRONTEND=noninteractive

# Update apt cache
RUN apt update

# Install build tools
RUN apt -y install  \
    cmake make

# Install native compilers \
RUN apt -y install  \
    gcc g++

# Install x86_64 compilers (Linux)
RUN apt -y install  \
    gcc-x86-64-linux-gnu g++-x86-64-linux-gnu

# Install x86_64 compilers (Windows, mingw32-w64)
RUN apt -y install  \
    gcc-mingw-w64-x86-64-win32 g++-mingw-w64-x86-64-win32

# Install test dependencies
RUN apt -y install libgtest-dev

# Install utilities
RUN apt -y install clang-tidy valgrind lcov nsis
